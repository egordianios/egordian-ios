//
//  egJobListForPhotoTableViewCell.h
//  eGordian-iOS
//
//  Created by Laila on 28/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface egJobListForPhotoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *egJobLabel;
@property (weak, nonatomic) IBOutlet UIImageView *egSelectedJobImgView;
@property (weak, nonatomic) IBOutlet UILabel *egJobOwnerLbl;

@property (weak, nonatomic) IBOutlet UILabel *egJobIDLbl;
@end
