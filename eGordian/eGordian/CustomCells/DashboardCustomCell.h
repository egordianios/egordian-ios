//
//  DashboardCustomCell.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/22/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButtonView.h"

@interface DashboardCustomCell : UITableViewCell
@property (nonatomic,strong)IBOutlet UIImageView *logoImgView;
@property (nonatomic,strong)IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet CustomButtonView *manualSyncView;
@property (nonatomic,strong)IBOutlet UILabel *countLabel;

-(void)syncStarted;
-(void)syncEnded;
@end
