//
//  JobOrderListCell.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/26/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobOrderListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *jobListCellBackGroundImage;
@property (weak, nonatomic) IBOutlet UIButton *locationIconBtn;
@property (weak, nonatomic) IBOutlet UILabel *jobTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobClientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *directionsLabel;


@end
