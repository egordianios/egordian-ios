//
//  RightSlideMenuCell.m
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "RightSlideMenuCell.h"

@implementation RightSlideMenuCell

@synthesize activeIconName = _activeIconName;
@synthesize inactiveIconName = _inactiveIconName;
@synthesize titleText = _titleText;

- (void)awakeFromNib {
    // Initialization code
    
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView = nil;
    self.selectedBackgroundView = nil;
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setTitleText:(NSString *)titleText
{
    _titleText = titleText;
    self.titleLabel.text = _titleText;
}

- (void)setInactiveIconName:(NSString *)inactiveIconName
{
    _inactiveIconName = inactiveIconName;
    self.thumbImage.image = [UIImage imageNamed:_inactiveIconName];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
    if (selected) {
        self.thumbImage.image = [UIImage imageNamed:_activeIconName];
        self.titleLabel.textColor = [UIColor colorWithRed:251.0/255.0 green:149.0/255.0 blue:35.0/255.0 alpha:1.0];
        
    }
    else{
        self.thumbImage.image = [UIImage imageNamed:_inactiveIconName];
        self.titleLabel.textColor = [UIColor whiteColor];
    }
}



@end
