//
//  JobOrderListCell.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/26/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "JobOrderListCell.h"

@implementation JobOrderListCell

- (void)awakeFromNib {
    // Initialization code
    [self setAccessoryType:UITableViewCellAccessoryNone];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
