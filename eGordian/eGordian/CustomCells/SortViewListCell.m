//
//  SortViewListCell.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/26/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "SortViewListCell.h"

@implementation SortViewListCell
@synthesize titleText = _titleText;

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setTitleText:(NSString *)titleText
{
    _titleText = titleText;
    self.titleLabel.text = _titleText;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
