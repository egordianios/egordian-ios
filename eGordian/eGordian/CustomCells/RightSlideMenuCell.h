//
//  RightSlideMenuCell.h
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightSlideMenuCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *thumbImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSString *activeIconName;
@property (nonatomic, strong) NSString *inactiveIconName;
@property (nonatomic, strong) NSString *titleText;

@end
