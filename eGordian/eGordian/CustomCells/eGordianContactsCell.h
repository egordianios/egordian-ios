//
//  eGordianContactsCell.h
//  eGordian-iOS
//
//  Created by Laila on 25/08/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGordianContactsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel* addressLbl;
@property (weak, nonatomic) IBOutlet UILabel* companyLbl;
//@property (nonatomic, strong) UIButton* eGPhoneBtn;

//-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
