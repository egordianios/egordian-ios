//
//  eGordianContactsCell.m
//  eGordian-iOS
//
//  Created by Laila on 25/08/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianContactsCell.h"

@implementation eGordianContactsCell

@synthesize addressLbl, companyLbl;

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        int space = 15;
        
//        UIImage *phoneImg = [UIImage imageNamed:@"eG_Contacts_Phone"];
        
        // Initialization code
        self.addressLbl = [[UILabel alloc]init];
        self.addressLbl.textAlignment = NSTextAlignmentLeft;
        self.addressLbl.font = [UIFont fontWithName:@"OpenSans-Regular" size:13];
        self.addressLbl.numberOfLines = 0;
        self.addressLbl.tag = 1001;
        self.addressLbl.textColor = [UIColor colorWithRed:70.0/255.0 green:81.0/255.0 blue:88.0/255.0 alpha:1.0];
        self.addressLbl.frame = CGRectMake(space, 10, self.frame.size.width-(space*2), self.frame.size.height/2);
    
        self.companyLbl = [[UILabel alloc]init];
        self.companyLbl.textAlignment = NSTextAlignmentLeft;
        self.companyLbl.tag   = 1002;
        self.companyLbl.font = [UIFont fontWithName:@"OpenSans-Regular" size:10];
        self.companyLbl.frame = CGRectMake( space, self.addressLbl.frame.origin.y+ self.addressLbl.frame.size.height, self.frame.size.width -(space*2), self.frame.size.height/2.3);
        self.companyLbl.textColor = [UIColor colorWithRed:50.0/255.0 green:53.0/255.0 blue:55.0/255.0 alpha:1.0];
        
        self.eGPhoneBtn = [[UIButton alloc] initWithFrame: CGRectMake(self.companyLbl.frame.size.width+self.companyLbl.frame.origin.x, (self.frame.size.height-phoneImg.size.height), 2*phoneImg.size.width, self.frame.size.height)];
        [self.eGPhoneBtn setImage:phoneImg forState:UIControlStateNormal];
 
        [self.contentView addSubview:self.addressLbl];
        [self.contentView addSubview:self.companyLbl];
//        [self.contentView addSubview:self.eGPhoneBtn];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
}
*/
@end
