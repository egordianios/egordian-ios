//
//  TransactionDateCell.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionDateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannedLabel;


@end
