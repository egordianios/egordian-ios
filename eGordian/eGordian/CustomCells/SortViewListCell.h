//
//  SortViewListCell.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/26/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortViewListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImage;
@property (nonatomic, strong) NSString *titleText;
@end
