//
//  TransactionDeatilCell.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/18/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionDeatilCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *transactionDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *plannedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *adjustedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *actualDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *egCalendarButton;

@end
