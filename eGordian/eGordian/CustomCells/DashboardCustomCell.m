//
//  DashboardCustomCell.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/22/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "DashboardCustomCell.h"



@implementation DashboardCustomCell

- (void)awakeFromNib {
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncStarted) name:CTSYNCICONSTART object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncEnded) name:CTSYNCICONEND object:nil];
}

-(void)syncStarted{
    
    UIImageView *customButtonImageView = self.manualSyncView.imageView;

    NSMutableArray *animationImages=[NSMutableArray array];
    for (int i=21; i>=1; i--){
        NSString *imgName =[NSString stringWithFormat:@"SyncAnimationFrame%d",i];
        UIImage *currentImage =[[UIImage imageNamed:imgName] imageWithColor:[UIColor darkGrayColor]];
        [animationImages addObject:currentImage];
    }
    customButtonImageView.animationImages =animationImages;
    customButtonImageView.animationDuration = 1.0f;
    customButtonImageView.animationRepeatCount = 0;
    [customButtonImageView startAnimating];
    
}
-(void)syncEnded{
    UIImageView *customButtonImageView = self.manualSyncView.imageView;

    [customButtonImageView stopAnimating];
    
    UIImage *image = [[UIImage imageNamed:@"SyncAnimationFrame"]imageWithColor:[UIColor darkGrayColor]];
    
    customButtonImageView.animationImages = @[image];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
