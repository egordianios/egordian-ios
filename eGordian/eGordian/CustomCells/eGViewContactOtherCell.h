//
//  eGViewContactOtherCell.h
//  eGordian-iOS
//
//  Created by Laila on 07/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGViewContactOtherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *GViewContactOtherHeaderLbl;

@property (weak, nonatomic) IBOutlet UITextField *eG_ViewContact_OtherTxtfld;
@property (weak, nonatomic) IBOutlet UIButton *eG_ViewContact_OtherBtn;


@end
