//
//  eGViewContactAddressCell.h
//  eGordian-iOS
//
//  Created by Laila on 07/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGViewContactAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *contactsViewNameTxtfld;
@property (weak, nonatomic) IBOutlet UITextField *contactsViewCompanyTxtfld;
@property (weak, nonatomic) IBOutlet UITextField *contactsViewAddressTxtfld;
@property (weak, nonatomic) IBOutlet UITextField *contactsViewCityTxtfld;
@property (weak, nonatomic) IBOutlet UITextField *contactsViewStateTxtfld;
@property (weak, nonatomic) IBOutlet UITextField *contactsViewZipTxtfld;

@end
