//
//  egJobListForPhotoTableViewCell.m
//  eGordian-iOS
//
//  Created by Laila on 28/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "egJobListForPhotoTableViewCell.h"

@implementation egJobListForPhotoTableViewCell

- (void)awakeFromNib
{
    //Changes done directly here, we have an object
    
    [self.egJobLabel setTextColor:[UIColor colorWithRed:11.0/255 green:16.0/255 blue:18.0/255 alpha:1.0]];
    [self.egJobOwnerLbl setTextColor:[UIColor colorWithRed:11.0/255 green:124.0/255 blue:130.0/255 alpha:1.0]];
    [self.egJobIDLbl setTextColor:[UIColor colorWithRed:70.0/255 green:81.0/255 blue:88.0/255 alpha:1.0]];
}
@end
