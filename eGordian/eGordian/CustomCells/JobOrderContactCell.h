//
//  JobOrderContactCell.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/21/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobOrderContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contactRoleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;


@end
