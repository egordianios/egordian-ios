//
//  eGordianNotesCell.h
//  eGordian-iOS
//
//  Created by Laila on 24/11/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGordianNotesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *eGDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *eGSubjectLbl;
@property (weak, nonatomic) IBOutlet UIImageView *egFileImgView;
@property (weak, nonatomic) IBOutlet UILabel *eGSnippetLbl;
@end
