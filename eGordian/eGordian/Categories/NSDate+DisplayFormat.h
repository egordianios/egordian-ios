//  NSDate+APIDateFormat.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface NSDate (DisplayFormat)
- (NSString *)userDisplayDate;
- (NSString *)userDisplayTime;
- (NSString *)userDisplayDateAndTime;
@end
