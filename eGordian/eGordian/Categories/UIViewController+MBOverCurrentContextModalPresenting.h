//
//  UIViewController+MBOverCurrentContextModalPresenting.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/12/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MBOverCurrentContextModalPresenting)
- (void)MBOverCurrentContextPresentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion;

@end
