//
//  NSDictionary+JSON.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/11/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "NSDictionary+JSON.h"
#import "APPConfig.h"

@implementation NSDictionary (JSON)

- (BOOL)hasValidJSONKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return  true;
    return false;
}

- (id)getObjectForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [self objectForKey:key];
    return nil;
}

- (int)getIntForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] intValue];
    return 0;
}

- (NSInteger)getIntegerForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] integerValue];
    return 0;
}

- (float)getFloatForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] floatValue];
    return 0.0;
}


- (NSNumber*)getNSNumberForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        if([[self objectForKey:key] isKindOfClass:[NSNumber class]])
            return (NSNumber*)[self objectForKey:key];
    }
    return [NSNumber numberWithInt:0];
}

- (BOOL)getBoolForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] boolValue];
    return false;
}

- (NSString*)getStringForKey:(NSString*)key
{
    id obj = nil;
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        obj = [self objectForKey:key];
    if ([obj isKindOfClass:[NSNumber class]]) {
        obj = [obj stringValue];
    }
    return obj;
}

- (NSString*)getURLStringForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        NSString *urlStr = [self objectForKey:key];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        return urlStr;
    }
    return nil;
}

- (NSData*)getNSDataForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
        return [[self objectForKey:key] dataUsingEncoding:NSUTF8StringEncoding];
    return false;
}

- (NSDate *)getDateForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
        [dateFmt setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [dateFmt setDateFormat:API_DATE_FORMAT];
        NSDate *date;
        date= [dateFmt dateFromString:[self valueForKey:key]];
        if(!date)
        {
            [dateFmt setDateFormat:API_DATETIME_FORMAT];
            date=[dateFmt dateFromString:[self valueForKey:key]];
            if (!date)
            {
                [dateFmt setDateFormat:API_DATETIME_FORMAT_UTC];
                date=[dateFmt dateFromString:[self valueForKey:key]];
            }
            
        }
        return date;
    }
    return nil;
}

- (NSDate*)getTimeForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
        [dateFmt setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [dateFmt setDateFormat:API_TIME_FORMAT];
        NSDate *date;
        date= [dateFmt dateFromString:[self valueForKey:key]];
        if(!date)
        {
            [dateFmt setDateFormat:API_DATETIME_FORMAT];
            date=[dateFmt dateFromString:[self valueForKey:key]];
        }
        return date;
    }
    return nil;
}

- (NSTimeInterval)getNSTimeIntervalForKey:(NSString*)key
{
    if([self objectForKey:key] && [self objectForKey:key] != [NSNull null])
    {
        NSDate *date=[self getDateForKey:key];
        if (date){
            NSCalendar *gregorian = [[NSCalendar alloc]
                                     initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *comps = [gregorian components:NSCalendarUnitYear fromDate:date];
            if (comps.year>1970){
                return [date timeIntervalSince1970];
            }
        }
        
    }
    return 0;
}


@end
