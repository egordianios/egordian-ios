//
//  NSDate+Utilities.h
//  ClearTouch
//
//  Created by Shineeth Hamza on 20/10/14.
//  Copyright (c) 2014 ClearPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

- (NSDate*)startDateOfWeek;
- (NSDate*)endDateOfWeek;
+ (NSDate*)today;

@end
