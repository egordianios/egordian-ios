//
//  UIViewController+MBOverCurrentContextModalPresenting.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/12/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "UIViewController+MBOverCurrentContextModalPresenting.h"

@implementation UIViewController (MBOverCurrentContextModalPresenting)
- (void)MBOverCurrentContextPresentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    
    
    if(SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.parentViewController.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    }else{
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationOverFullScreen;
    }
    
    [self presentViewController:viewControllerToPresent animated:YES completion:completion];
    
    return;
    
    UIViewController *presentingVC = self;
    
    // iOS 8 before
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        UIViewController *root = presentingVC;
        while (root.parentViewController) {
            root = root.parentViewController;
        }
        
        [presentingVC presentViewController:viewControllerToPresent animated:YES completion:^{
            [viewControllerToPresent dismissViewControllerAnimated:NO completion:^{
                UIModalPresentationStyle orginalStyle = root.modalPresentationStyle;
                if (orginalStyle != UIModalPresentationCurrentContext) {
                    root.modalPresentationStyle = UIModalPresentationOverCurrentContext;//UIModalPresentationCurrentContext;
                }
                [presentingVC presentViewController:viewControllerToPresent animated:NO completion:completion];
                if (orginalStyle != UIModalPresentationCurrentContext) {
                    root.modalPresentationStyle = orginalStyle;
                }
            }];
        }];
        return;
    }
    
    UIModalPresentationStyle orginalStyle = viewControllerToPresent.modalPresentationStyle;
    if (orginalStyle != UIModalPresentationOverCurrentContext) {
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    [presentingVC presentViewController:viewControllerToPresent animated:YES completion:completion];
    if (orginalStyle != UIModalPresentationOverCurrentContext) {
        viewControllerToPresent.modalPresentationStyle = orginalStyle;
    }
}
@end
