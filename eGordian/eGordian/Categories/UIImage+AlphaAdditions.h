// UIImage+AlphaAdditions.h
//  MChat
//
//  Created by Shineeth Hamza on 20/01/14.
//  Copyright (c) 2014 Marlabs. All rights reserved.
//

// Helper methods for adding an alpha layer to an image

// http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/

@interface UIImage (Alpha)
- (BOOL)hasAlpha;
- (UIImage *)imageWithAlpha;
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;
@end