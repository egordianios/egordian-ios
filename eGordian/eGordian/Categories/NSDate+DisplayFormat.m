//  NSDate+APIDateFormat.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "NSDate+DisplayFormat.h"
#import "APPConfig.h"

@implementation NSDate (DisplayFormat)
- (NSString *)userDisplayDate
{
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
    dateFmt.timeZone = [NSTimeZone systemTimeZone];
    [dateFmt setDateFormat:APP_DATE_FORMAT];
    return [dateFmt stringFromDate:self];
}

- (NSString *)userDisplayTime
{
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
    [dateFmt setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFmt setDateFormat:APP_TIME_FORMAT];
    
    return [dateFmt stringFromDate:self];
}

- (NSString *)userDisplayDateAndTime
{
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
    [dateFmt setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFmt setDateFormat:APP_DATETIME_FORMAT];
    
    return [dateFmt stringFromDate:self];
}

@end
