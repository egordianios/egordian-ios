// UIImage+RoundedCornerAdditions.h
//  MChat
//
//  Created by Shineeth Hamza on 20/01/14.
//  Copyright (c) 2014 Marlabs. All rights reserved.
//

// Extends the UIImage class to support making rounded corners

// http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/

@interface UIImage (RoundedCorner)
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;
@end