//
//  NSDate+APIDateFormat.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "NSDate+APIDateFormat.h"
#import "APPConfig.h"

@implementation NSDate (APIDateFormat)
- (NSString*)dateStringForAPI
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateFormat:API_DATE_FORMAT];
    return [formatter stringFromDate:self];
}

- (NSString*)timeStringForAPI
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateFormat:API_TIME_FORMAT];
    return [formatter stringFromDate:self];
}

- (NSString*)dateTimeStringForAPI
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateFormat:API_DATETIME_FORMAT];
    return [formatter stringFromDate:self];
}
@end
