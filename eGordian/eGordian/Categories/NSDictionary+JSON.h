//
//  NSDictionary+JSON.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/11/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

- (BOOL)hasValidJSONKey:(NSString*) key;
- (id)getObjectForKey:(NSString*) key;
- (int)getIntForKey:(NSString*) key;
- (NSInteger)getIntegerForKey:(NSString*) key;
- (NSNumber*)getNSNumberForKey:(NSString*) key;
- (BOOL)getBoolForKey:(NSString*) key;
- (NSString*)getStringForKey:(NSString*) key;
- (NSString*)getURLStringForKey:(NSString*) key;
- (NSData*)getNSDataForKey:(NSString*) key;
- (NSDate *)getDateForKey:(NSString*) key;
- (NSDate*)getTimeForKey:(NSString*)key;
- (NSTimeInterval) getNSTimeIntervalForKey:(NSString*) key;
- (float)getFloatForKey:(NSString*) key;
@end
