//
//  UIImage+Utilities.h
//  ClearTouch
//
//  Created by Shineeth Hamza on 17/09/14.
//  Copyright (c) 2014 ClearPoint. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utilities)
- (UIImage*)cropToRect:(CGRect)rect;
- (UIImage*)scaledToSize:(CGSize)newSize;
- (UIImage*)scaleToWidth:(CGFloat)width;
- (UIImage*)scaleToHeight:(CGFloat)height;
- (UIImage*)convertToGreyscale;
- (UIImage*)imageWithColor:(UIColor *)color;
+ (UIImage *)imageUsingColor:(UIColor *)color rect:(CGRect)rect;
+ (UIImage *)imageUsingColor:(UIColor *)color diameter:(CGFloat)diameter;
@end

@interface UIImage (Resize)
- (UIImage *)croppedImage:(CGRect)bounds;
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;
@end