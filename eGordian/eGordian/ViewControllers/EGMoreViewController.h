//
//  EGMoreViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/10/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopoverView.h"

@interface EGMoreViewController : UIViewController <PopoverViewDelegate>

@end
