//
//  EGScopeOfWorkEditViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/31/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGScopeOfWorkEditViewController.h"

@interface EGScopeOfWorkEditViewController ()

@end

@implementation EGScopeOfWorkEditViewController
@synthesize currentJobOrder,scopeOfWorkString,isCalledFromOffline;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setIntialUI];
    
}

-(void)setIntialUI{
    
    [self.scrollView contentSizeToFit];
    
    self.eGScopeEditTxtVw.attributedText     = self.scopeOfWorkString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSave:(id)sender{
   
    NSString *jsonString = [NSString stringWithFormat:@"{{\"Text\":\"%@\"}",self.eGScopeEditTxtVw.attributedText];
   
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    id jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:self.currentJobOrder.detailedScopeLink andserviceType:EGORDIAN_API_SCOPEOFWORK_UPDATE];
}


#pragma mark - Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_SCOPEOFWORK_UPDATE){
        
    }
}
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{

    NSString *alertMessage = @"";
    
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message--%@",alertMessage);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
