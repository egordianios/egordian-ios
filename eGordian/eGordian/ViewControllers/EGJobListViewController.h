//
//  EGJobListViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/25/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EGJobListViewController : UIViewController <DataDownloaderDelegate,UIScrollViewDelegate>
{
    NSInteger sortKey;
    NSInteger searchKey;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UITextField *jobListSearchField;
- (IBAction)jobsBackButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIView *jobListSortView;
@property (weak, nonatomic) IBOutlet UITableView *jobListTableView;
//@property (weak, nonatomic) IBOutlet UITableView *sortListTableView;
- (IBAction)rightMenuTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *eGSearchBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *eGSortByBtnCollection;
@property (weak, nonatomic) IBOutlet UITableView *eGSearchByTblVw;
@property (weak, nonatomic) IBOutlet UIView *eGSearchByView;
@property (weak, nonatomic) IBOutlet UIButton *eGSearchByBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchImage;

- (IBAction)onTextFieldEndEditing:(id)sender;
- (IBAction)onSearchTextChanged:(id)sender;
- (IBAction)onSearch:(id)sender;
- (IBAction)searchResultCancelTapped:(id)sender;
- (IBAction)onSortBtn:(id)sender;
- (IBAction)onSearchByBtn:(id)sender;

//for sorting
@property (nonatomic,strong) NSMutableDictionary *sections;
@property (nonatomic, strong) NSMutableArray* sortedJobListArray;
@end
