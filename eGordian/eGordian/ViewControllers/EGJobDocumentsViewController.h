//
//  EGJobDocumentsViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrderDetail.h"

@interface EGJobDocumentsViewController : UIViewController <DataDownloaderDelegate>

@property (strong, nonatomic) UILabel *jobClientNameLabel;
@property (strong, nonatomic) UILabel *jobTitleLabel;
@property (strong, nonatomic) UILabel *jobIdLabel;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UILabel *detailedScopeDataLabel;
@property (weak, nonatomic) IBOutlet UITableView *documentsTable;
@property (weak, nonatomic) IBOutlet UILabel *infoTextLabel;

@property (nonatomic, retain) EG_JobOrderDetail *currentJobOrder;
@property (weak, nonatomic) IBOutlet UIButton *scopeOfWorkButton;
@property (weak, nonatomic) IBOutlet UIButton *scopeOfworkAddButton;
@property (nonatomic, strong) NSString *strJobOrderLink;
@property (weak, nonatomic) IBOutlet UIView *documentsJobDetailView;
@property (weak, nonatomic) IBOutlet UIView *jobOrderTitleInfoView;
@property (weak, nonatomic) IBOutlet UIView *scopeOfWorkView;

- (IBAction)documentsBackButtonTapped:(id)sender;
- (IBAction)scopeOfworkTapped:(id)sender;
- (IBAction)addScopeOfWorkTapped:(id)sender;

@end
