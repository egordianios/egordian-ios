//
//  EGTrackingDatesViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/14/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrderDetail.h"

@interface EGTrackingDatesViewController : UIViewController<DataDownloaderDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) UILabel *jobClientNameLabel;
@property (strong, nonatomic) UILabel *jobTitleLabel;
@property (strong, nonatomic) UILabel *jobIdLabel;
@property (weak, nonatomic) IBOutlet UITableView *pickerTable;
@property (weak, nonatomic) IBOutlet UILabel *milestoneLabel;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIView *actualPopOverView;
@property (weak, nonatomic) IBOutlet UITableView *trackingDatesTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *trackingDatesScrollView;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (nonatomic, retain) EG_JobOrderDetail *currentJobOrder;
@property (weak, nonatomic) IBOutlet UIButton *egCalendarButton;
@property (weak, nonatomic) IBOutlet UIView *jobOrderTitleInfoView;
@property (weak, nonatomic) IBOutlet UIView *trackingDatesListView;

- (IBAction)trackingDatesBackButtonTapped:(id)sender;
- (IBAction)milestoneButtonTapped:(id)sender;
- (IBAction)onTrackingDates:(id)sender;
@end
