//
//  EGJobContactsAssignViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EGJobContactsAssignViewController.h"
#import "eGordianContactsCell.h"
#import "eGordianContact.h"
#import "EG_OwnerContact.h"
#import "EGJobConatctAddARoleViewController.h"

#define searchKeyFirstName @"FirstName"
#define searchKeyLastName @"LastName"
#define searchKeyCompany @"CompanyName"
#define searchKeyDisplayName @"DisplayName"
#define PAGESIZE 100

@interface EGJobContactsAssignViewController (){
    
    NSMutableArray *contactAssignLoadArray;
    int currentPageIndex;
    int pgSize;
    BOOL isAppendData;
    UILabel *infoTextLabel;
    BOOL isSearch;
    NSMutableArray *sectionIndexArray;
}
@property (strong, nonatomic) NSMutableDictionary* sectionsDictionary;
@property (nonatomic) BOOL loading;
@property (strong, nonatomic) NSArray *filteredContacts;

// contains the contacts as got from the Search Contacts service request
@property (nonatomic, strong) NSMutableArray* searchContactsArr;

@end

@implementation EGJobContactsAssignViewController

@synthesize jobOrderID,rolesURl,searchContactsArr,jobOrderOwnerId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setIntialUI];
}

-(void)setIntialUI{
    
    [self.searchImage.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    
    self.searchContactsArr = [[NSMutableArray alloc] init];
    
    self.eGSearchBtn.hidden = NO;
    
    isSearch = FALSE;
    
    self.cancelButton.hidden = YES;
    
    pgSize = PAGESIZE;
    
    currentPageIndex = 1;
    
    isAppendData = FALSE;
    
    self.loading = FALSE;
    
    contactAssignLoadArray = [[NSMutableArray alloc]init];
    
    sectionIndexArray = [[NSMutableArray alloc]init];
    
    self.assignContactsTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [self.loaderView setHidden:YES];
    
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    
    [self fetchGlobalContacts];
}


-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

-(void)fetchOwnerContacts:(NSString *)contactsUrl{
    
    [self actvityStart];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader]getResponseFormUrl:contactsUrl serviceType:EGORDIAN_API_OWNER_CONTACTS];
}

-(void) fetchGlobalContacts{

    
    NSString *contactsString = [NSString stringWithFormat:@"%@Owners/%@/Contacts?sortBy=LastName&pageSize=%d&pageNumber=%d", APP_HEADER_URL, self.jobOrderOwnerId, pgSize, currentPageIndex];
    
    [self fetchOwnerContacts:contactsString];
    
//    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
//    
//    [[DataDownloader sharedDownloader]getResponseFormUrl:contactsString serviceType:EGORDIAN_API_GLOBAL_CONTATCS];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.sectionsDictionary allKeys] count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if([self.sectionsDictionary allKeys].count){
        [sectionIndexArray removeAllObjects];
        for(int i=0; i < [self.sectionsDictionary allKeys].count; i++){
            NSString *indexTitle = [[self.sectionsDictionary allKeys]objectAtIndex:i];
            indexTitle = [NSString stringWithFormat:@" %@    ",indexTitle];
            [sectionIndexArray addObject:indexTitle];
        }
    }

    return [sectionIndexArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if ([title containsString:@"    "])
    {
        title = [title stringByReplacingOccurrencesOfString:@"    " withString:@""];
    }
    //Your code here
    
    return [self.sectionsDictionary allKeys].count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return [[self.sectionsDictionary valueForKey:[[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
}
    
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [headerView setBackgroundColor:[UIColor colorWithRed:156.0/255.0 green:182.0/255.0 blue:197.0/255.0 alpha:1.0]];
    //[headerView setBackgroundColor:[UIColor lightGrayColor]];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,5,tableView.bounds.size.width-30,20)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = [UIColor blackColor];
    tempLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:15];
    NSString *headerTitle = @"";
    
    headerTitle = [[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    
    tempLabel.text=headerTitle;//@"Most Recent";
    [headerView addSubview:tempLabel];
    
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"eGordianContactsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    EG_OwnerContact *contact = [[self.sectionsDictionary valueForKey:[[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",contact.FirstName,contact.LastName];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EG_OwnerContact* theContact = [[self.sectionsDictionary valueForKey:[[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    //    if([theContact.OwnerID isEqualToString:self.jobOrderOwnerId]){
    
    EGJobConatctAddARoleViewController* addRoleVC = (EGJobConatctAddARoleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGJobConatctAddARoleViewController"];
    
    addRoleVC.strRolesURL = self.rolesURl;
    
    addRoleVC.strJobOrderId = self.jobOrderID;
    
    addRoleVC.eGOwnerContact = theContact;
    
    [self.navigationController pushViewController:addRoleVC animated:YES];
}

#pragma API Manager Delegates
#pragma mark Data Downloader Delegate Methods
    
-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    if(type == EGORDIAN_API_OWNER_CONTACTS){
        
//        NSArray *formattedArray = [eGordianContact getContactArray:responseArray];
        
        if(currentPageIndex > 1)
        {
            [contactAssignLoadArray addObjectsFromArray:responseArray];
        }
        else{
            contactAssignLoadArray = [responseArray mutableCopy];
            [self.assignContactsTableView setContentOffset:CGPointMake(0, 0)];
        }
        
        if (responseArray.count > 0) {
            self.loading = FALSE;
        }
        else{
            self.loading = TRUE;
        }
        
        if(![contactAssignLoadArray count])
        {
            self.sectionsDictionary = nil;
            [self.assignContactsTableView reloadData];
            [self showNullRecords];
        }
        else
        {
            
            [infoTextLabel removeFromSuperview];
            [self sortArrayAlaphabetically:[contactAssignLoadArray copy]];
        }
        
        isAppendData = false;
    }
//    else if(type == EGORDIAN_API_CONTACTS_SEARCH){
//        
//        [self.searchContactsArr removeAllObjects];
//        
//        self.searchContactsArr = [eGordianContact getContactArray:responseArray];
//        
//        if (responseArray.count > 0) {
//            self.loading = FALSE;
//        }
//        else{
//            self.loading = TRUE;
//        }
//        
//        if(![self.searchContactsArr count])
//        {
//            self.sectionsDictionary = nil;
//            
//            [self.assignContactsTableView reloadData];
//            
//            [self showNullRecords];
//        }
//        else
//        {
//            [infoTextLabel removeFromSuperview];
//            
//            [self sortArrayAlaphabetically:self.searchContactsArr];
//        }
//    }
    
    [self actvityStop];
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    NSString *alertMessage = @"";
    
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    NSLog(@"Error Message--%@",alertMessage);
    
    [self actvityStop];
}

/*
 * Method Name	: showNullRecords
 * Description	: show null records label when there are no records to be displayed
 * Parameters	: nil
 * Return value	: nil
 */
-(void) showNullRecords
{
    if (!infoTextLabel)
        infoTextLabel = [[UILabel alloc] init];
    infoTextLabel.frame = CGRectMake(self.assignContactsTableView.frame.origin.x, self.view.frame.size.height/2, self.assignContactsTableView.frame.size.width, 60);
    infoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    infoTextLabel.backgroundColor = [UIColor clearColor];
    infoTextLabel.textAlignment = NSTextAlignmentCenter;
    infoTextLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    infoTextLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:20];
    infoTextLabel.text = @"No Items Found";
    [self.view addSubview:infoTextLabel];
    [self.view bringSubviewToFront:infoTextLabel];
}

-(void)sortArrayAlaphabetically:(NSArray *)rawArray
{
    if(![rawArray count]){
        return;
    }
    
    self.sectionsDictionary = nil;
    
    self.sectionsDictionary = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    for (EG_OwnerContact *contact in rawArray)
    {
        NSString *c = nil;
        
        if(![contact.LastName length]){
//            continue;
            contact.LastName = @" ";
        }
        
        c = [[contact.LastName uppercaseString] substringToIndex:1];
        
//        c = [NSString stringWithFormat:@"%@",c];
        
        found = NO;
        
        for (NSString *str in [self.sectionsDictionary allKeys])
        {
            if ([str caseInsensitiveCompare:c] == NSOrderedSame)
            {
                found = YES;
            }
        }
        if (!found)
        {
            [self.sectionsDictionary setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    for (EG_OwnerContact *contact in rawArray)
    {
        if(![contact.LastName length]){
//            continue;
            contact.LastName = @" ";
        }
        
//        NSString *h1 = [[contact.LastName uppercaseString] substringToIndex:1];
//        
//        h1 = [NSString stringWithFormat:@"%@   ",h1];
        
        [[self.sectionsDictionary objectForKey:[[contact.LastName uppercaseString] substringToIndex:1]] addObject:contact];
    }
    
    // Sort each section array
    for (NSString *key in [self.sectionsDictionary allKeys])
    {
        [[self.sectionsDictionary objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:searchKeyLastName ascending:YES]]];
    }
    
    [self.assignContactsTableView reloadData];
    
    [self actvityStop];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.assignContactSearchField.text.length > 0)
        return;
    
    if (!self.loading) {
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        if (endScrolling >= scrollView.contentSize.height)
        {
            [self performSelector:@selector(loadDataDelayed) withObject:nil afterDelay:0.2];
        }
    }
}

-(void)loadDataDelayed{
    
    currentPageIndex++;
    
    isAppendData = true;
    
    [self fetchGlobalContacts];
}

#pragma mark - TextField Delegate

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField
{
    [self.assignContactSearchField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.assignContactSearchField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(self.assignContactSearchField.text.length > 0){
        
        [self onSearch:nil];
    }
    else{
        
        currentPageIndex = 1;
        
        [self fetchGlobalContacts];
    }
}

/*
 * Method Name	: onTextFieldEndEditing
 * Description	: on editing did end
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onTextFieldEndEditing:(id)sender {
    
    UITextField* textField = sender;
    
    if(textField.text.length >= 2){
        [self.eGSearchBtn setHidden:YES];
    }
    else if (textField.text.length == 0){
        [self.cancelButton setHidden:YES];
        [self.eGSearchBtn setHidden:YES];
    }
    else{
        [self.eGSearchBtn setHidden:NO];
        [self.cancelButton setHidden:YES];
    }
}

-(IBAction)onSearch:(id)sender{
    
    if([self.assignContactSearchField.text length])
    {
        [self actvityStart];
        
        isSearch = TRUE;
        
        currentPageIndex = 1;
        
        if(self.searchContactsArr)
            self.searchContactsArr = NULL;
        
        NSString *strParamValue = [NSString stringWithFormat:@"DisplayName eq %@", self.assignContactSearchField.text];
        
        strParamValue = [AppConfig urlencode:strParamValue];
        NSString *contactSearchURl = [NSString stringWithFormat:@"%@Owners/%@/Contacts?filter=%@",APP_HEADER_URL,self.jobOrderOwnerId,strParamValue];
        
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        
        [[DataDownloader sharedDownloader]getResponseFormUrl:contactSearchURl serviceType:EGORDIAN_API_OWNER_CONTACTS];
        
    }
}

- (IBAction)onSearchTextChanged:(id)sender {
    if(self.assignContactSearchField.text.length >= 2)
    {
        self.cancelButton.hidden = FALSE;
        self.eGSearchBtn.hidden = TRUE;
    }
    else
    {
        self.cancelButton.hidden = TRUE;
        self.eGSearchBtn.hidden = FALSE;
    }
    
}

- (IBAction)searchResultCancelTapped:(id)sender{
    
    self.eGSearchBtn.hidden = NO;
    
    self.cancelButton.hidden = YES;
    
    isSearch = FALSE;
    
    self.assignContactSearchField.text = @"";
    
    currentPageIndex = 1;
    
    [self fetchGlobalContacts];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)assignBackTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
