//
//  EGOfflineJobDetailViewController.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobOrder.h"
@interface EGOfflineJobDetailViewController : UIViewController{
    
    
}
@property (weak, nonatomic) IBOutlet UILabel *jobClientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobIdLabel;
@property (nonatomic,strong) JobOrder *selJobOrder;

@property (weak, nonatomic) IBOutlet UILabel *documentsLabelCount;
@property (weak, nonatomic) IBOutlet UILabel *photosLabelcount;
@property (weak, nonatomic) IBOutlet UILabel *contactsLabelCount;


@end
