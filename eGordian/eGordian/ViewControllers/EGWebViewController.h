//
//  EGWebViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/5/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EGWebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webViewToLoadContent;
@property (assign, nonatomic) NSInteger resourceViaIndex;
@property (nonatomic, strong) NSString *redirectVia;
@property (nonatomic, strong) NSString *webViewLoadURl;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
- (IBAction)backButtonTapped:(id)sender;

@end
