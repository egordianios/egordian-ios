//
//  EGTrackingDatesViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/14/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EGTrackingDatesViewController.h"
#import "RightSlideMenuCell.h"
#import "TransactionDeatilCell.h"
#import "EG_TransactionDate.h"
#import "eGordianCalendarViewController.h"

@interface EGTrackingDatesViewController (){
    NSMutableArray *mileStoneArray;
    NSMutableArray *transactionDatesArray;
    NSMutableArray *titleArray;
}
@property (assign, nonatomic) CGRect mySensitiveRect;
@end

@implementation EGTrackingDatesViewController
@synthesize currentJobOrder,mySensitiveRect,jobTitleLabel, jobClientNameLabel, jobIdLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    [self setIntialUI];
}

-(void)setIntialUI
{
    mileStoneArray = [[NSMutableArray alloc]initWithObjects:@"Joint Scope Meeting",@"Proposal Due",@"Construction Start",@"Construction Complete", nil];
    
    transactionDatesArray = [[NSMutableArray alloc]init];
    
    [self.loaderView setHidden:YES];
    
    titleArray = [[NSMutableArray alloc]init];
    
    self.jobTitleLabel.text             = currentJobOrder.jobTitle;
    
//    [self.jobTitleLabel sizeToFit];
    
    self.jobIdLabel.text                = [NSString stringWithFormat:@"ID:%@",currentJobOrder.jobOrderId];
    
    self.jobClientNameLabel.text        = currentJobOrder.clientName;
    
    self.popOverView.hidden = YES;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    [gestureRecognizer setNumberOfTapsRequired:1.0];
    
    [gestureRecognizer setDelegate:self];
    
    [self.popOverView addGestureRecognizer:gestureRecognizer];
    
    [self setJobOrderInfoUI];
}

-(void)setJobOrderInfoUI{
    
    CGFloat padding = 15.0f;
    
    CGFloat labelWidth = self.view.frame.size.width - (2*padding);
    
    CGFloat dy = padding;
    
    CGFloat defaultLabelHeight = 20.0f;
    
    NSString *jobTitleText = [NSString stringWithFormat:@"%@",currentJobOrder.jobTitle];
    
    NSString *jobClientName = [NSString stringWithFormat:@"%@",currentJobOrder.clientName];
    
    NSString *jobOrderId = [NSString stringWithFormat:@"ID:%@",currentJobOrder.jobOrderNumber];
    
    [self.jobOrderTitleInfoView  setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if(jobTitleText.length > 0){
        
        jobTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobTitleLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:15.0]];
        
        [jobTitleLabel setNumberOfLines:0];
        
        [jobTitleLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobTitleLabel setText:jobTitleText];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobTitleLabel WithText:jobTitleLabel.text];
        
        [jobTitleLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobTitleLabel];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(jobClientName.length > 0){
        
        jobClientNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobClientNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobClientNameLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobClientNameLabel setNumberOfLines:0];
        
        [jobClientNameLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobClientNameLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobClientNameLabel setText:jobClientName];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobClientNameLabel WithText:jobClientNameLabel.text];
        
        [jobClientNameLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobClientNameLabel];
        
        dy = dy+dynamicTextHeight+(jobOrderId.length?3.0:padding);
    }
    
    if(jobOrderId.length > 0){
        
        jobIdLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobIdLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobIdLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobIdLabel setNumberOfLines:0];
        
        [jobIdLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobIdLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobIdLabel setText:jobOrderId];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobIdLabel WithText:jobIdLabel.text];
        
        [jobIdLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobIdLabel];
        
        dy = dy+dynamicTextHeight+padding;
    }
    
    
    [self.jobOrderTitleInfoView setFrame:CGRectMake(0, 0, self.view.frame.size.width, dy)];
    [self.trackingDatesListView setFrame:CGRectMake(0, self.jobOrderTitleInfoView.frame.origin.y+self.jobOrderTitleInfoView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-62.0f-self.jobOrderTitleInfoView.frame.size.height - 62.0f)];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    [self fetchTrackingDates];
}

-(void)fetchTrackingDates{

    [self.loaderView setHidden:NO];
    [self.trackingDatesScrollView bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader] getResponseFormUrl:currentJobOrder.trackingDateLink serviceType:EGORDIAN_API_TRACKING_DATES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)trackingDatesBackButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 0.0;
    
    if(tableView == self.pickerTable){
        height = 44.0;
    }
    else if(tableView == self.trackingDatesTableView){
        height = 90;
    }
    
    return height;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    NSInteger rowCount = 0;
    
    if(tableView == self.pickerTable){
        rowCount = mileStoneArray.count;
    }
    else if(tableView == self.trackingDatesTableView){
        rowCount = transactionDatesArray.count;
    }
    return rowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id cellToReturn=nil;
    
    if(tableView == self.pickerTable){
        
        RightSlideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RightSlideMenuCell"];
        
        if (!cell)
        {
            cell = [[RightSlideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RightSlideMenuCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        cell.titleText = [mileStoneArray objectAtIndex:indexPath.row];
        
        UIView *bgColorView = [[UIView alloc] init];
        
        bgColorView.backgroundColor = [UIColor blackColor];
        
        [cell setSelectedBackgroundView:bgColorView];
        
        cellToReturn = cell;
        
    }
    else if(tableView == self.trackingDatesTableView){
        
        TransactionDeatilCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionDeatilCell"];
        
        if (!cell)
        {
            cell = [[TransactionDeatilCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TransactionDeatilCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        EG_TransactionDate *currentTransaction = [transactionDatesArray objectAtIndex:indexPath.row];
        
        NSLog(@"%@\n%@\n%@",currentTransaction.jobPlannedDate,currentTransaction.jobAdjustedDate,currentTransaction.jobActualDate);

        

        NSString *plannedDate = [AppConfig dateStringFromString:currentTransaction.jobPlannedDate.length?[currentTransaction.jobPlannedDate substringToIndex: MIN(10, [currentTransaction.jobPlannedDate length])]:@""
                                                   sourceFormat:@"yyyy-MM-dd"
                                              destinationFormat:@"MM/dd"];
        
        NSString *actualDate = [AppConfig dateStringFromString:currentTransaction.jobActualDate.length?[currentTransaction.jobActualDate substringToIndex: MIN(10, [currentTransaction.jobActualDate length])]:@""
                                                  sourceFormat:@"yyyy-MM-dd"
                                             destinationFormat:@"MM/dd"];
        
        NSString *adjustedDate = [AppConfig dateStringFromString:currentTransaction.jobAdjustedDate.length?[currentTransaction.jobAdjustedDate substringToIndex: MIN(10, [currentTransaction.jobAdjustedDate length])]:@""
                                                  sourceFormat:@"yyyy-MM-dd"
                                             destinationFormat:@"MM/dd"];
         
        cell.egCalendarButton.tag = indexPath.row;
        cell.transactionDescriptionLabel.text = currentTransaction.jobDescription;
        
        cell.plannedDateLabel.text = plannedDate;
        
        cell.adjustedDateLabel.text = adjustedDate;
        
        cell.actualDateLabel.text = actualDate;
        
        cellToReturn = cell;
    }
    return cellToReturn;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView == self.pickerTable){
        [self.popOverView setHidden:YES];
        self.milestoneLabel.text = [mileStoneArray objectAtIndex:indexPath.row];
        
        if([self checkStingInArray:self.milestoneLabel.text arrayWithStrings:titleArray]){
            NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:[titleArray indexOfObject:self.milestoneLabel.text] inSection:0];
            [[self trackingDatesTableView] scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
    else if(tableView ==  self.trackingDatesTableView){
        
        if(![AppDelegate isConnectedToInternet])
        {
            [EGordianAlertView showFeatureNotAvailableMessage];
            return;
        }
        
        eGordianCalendarViewController *egCalendar = [[eGordianCalendarViewController alloc] init];
        egCalendar.transactionDate = [transactionDatesArray objectAtIndex:indexPath.row];
        egCalendar.jobID = currentJobOrder.jobOrderId;
        [self.navigationController pushViewController:egCalendar animated:YES];
    }
    
}

- (IBAction)milestoneButtonTapped:(id)sender{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [self.trackingDatesTableView bringSubviewToFront:self.popOverView];
    [self.pickerTable reloadData];
    self.popOverView.hidden = NO;
}

- (IBAction)onTrackingDates:(id)sender {
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    UIButton *clickedButton = (UIButton*)sender;
    
    eGordianCalendarViewController *egCalendar = [[eGordianCalendarViewController alloc] init];
    egCalendar.transactionDate = [transactionDatesArray objectAtIndex:clickedButton.tag];
    egCalendar.jobID = currentJobOrder.jobOrderId;
    [self.navigationController pushViewController:egCalendar animated:YES];
}

-(BOOL)checkStingInArray: (NSString *)aString arrayWithStrings:(NSMutableArray *)array

{
    if ([array containsObject: aString] ) {
        NSLog(@" %@ found in Array",aString );
        return YES;
        
    } else {
        NSLog(@" %@ not found in Array",aString );
        return NO;
    }
}

#pragma API Manager Delegates

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_TRACKING_DATES) {
        
        if([transactionDatesArray count])
            [transactionDatesArray removeAllObjects];
        
        if([titleArray count])
            [titleArray removeAllObjects];
        
        for (EG_TransactionDate *currentTransactionDate in responseArray) {
            NSString *titleDescription = currentTransactionDate.jobDescription;
            [titleArray addObject:titleDescription];
        }
        
        transactionDatesArray = [responseArray mutableCopy];
        
        [self.trackingDatesTableView reloadData];
        
        
        [self.activityLoader stopAnimating];
        [self.loaderView setHidden:YES];
    }
}
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message--%@",alertMessage);
}

#pragma UIGestureRecogniser Delegates

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.pickerTable]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    mySensitiveRect = self.actualPopOverView.frame;
    CGPoint p = [gestureRecognizer locationInView:self.popOverView];
    if (!CGRectContainsPoint(mySensitiveRect, p)) {
        self.popOverView.hidden = YES;
        NSLog(@"got a tap in the region i care about");
    } else {
        NSLog(@"got a tap, but not where i need it");
    }
}

@end
