//
//  CustomTabBarViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/10/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "CustomTabBarViewController.h"
#import "EGHomeNavigationController.h"
#import "eGordianCameraObject.h"
#import "EGMoreViewController.h"
#import "APIClient.h"
#import "AppDelegate.h"
#import "DashBoardViewController.h"

#define kStringArray [NSArray arrayWithObjects:@"Support",@"Help", @"Logout", nil]

@interface CustomTabBarViewController (){
    PopoverView *pv;
}

@end

@implementation CustomTabBarViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self.tabBarController.tabBar setDelegate:self];
        [self.tabBarController.tabBar setTintColor:[UIColor colorWithRed:53/255.0 green:62/255.0 blue:68/255.0 alpha:1.0]];
//        [self.tabBarController.tabBar setSelectedImageTintColor:[UIColor colorWithRed:64/255.0 green:77/255.0 blue:85/255.0 alpha:1.0]];
        [self setDelegate:self];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    
    NSUInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
    
    switch (indexOfTab) {
        case 0:{
            break;
        }
        case 1:{
            return NO;
            break;
        }
        case 2:{
            return NO;
            break;
        }
        default:
            break;
    }
    
    return YES;
}

- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    
    
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    
    switch (indexOfTab) {
        case 0:{
            for (UIViewController *v in self.viewControllers)
            {
                if ([v isKindOfClass:[EGHomeNavigationController class]]){
                    
                    EGHomeNavigationController *navcon = (EGHomeNavigationController*)v;
                    [navcon  popToRootViewControllerAnimated:YES];
                    
                }
            }
            break;
        }
        case 1:{
            eGordianCameraObject *cameraVC =  [[eGordianCameraObject alloc] init];
            [self.navigationController presentViewController:cameraVC animated:YES completion:nil];
            break;
        }
                    
        default:
            break;
    }
}

-(void)selectedTab:(NSUInteger)index{
    NSLog(@"Overlay-Tab-Fired");
    switch (index) {
        case 0:
        {
            [self.tabBarController.delegate tabBarController:self.tabBarController shouldSelectViewController:[[self.tabBarController viewControllers] objectAtIndex:index]];
            [self.tabBarController setSelectedIndex:index];
        }
            break;
            
        default:
            break;
    }
    
}

-(void) selectItemWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:{
            for (UIViewController *v in self.viewControllers)
            {
                if ([v isKindOfClass:[EGHomeNavigationController class]]){
                    
                    EGHomeNavigationController *navcon = (EGHomeNavigationController*)v;
                    [navcon  popToRootViewControllerAnimated:YES];
                    
                }
            }
            break;
        }
        case 1:{
            eGordianCameraObject *cameraVC =  [[eGordianCameraObject alloc] init];
            [self.navigationController presentViewController:cameraVC animated:YES completion:nil];
            break;
        }
            
        default:
            break;
    }
}


- (void)tabBar:(UITabBar *)theTabBar didSelectItem:(UITabBarItem *)item {
    NSUInteger indexOfTab = [[theTabBar items] indexOfObject:item];
    NSLog(@"Tab index = %u", (int)indexOfTab);
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    switch (indexOfTab) {
        case 0:{
            for (UIViewController *v in self.viewControllers)
            {
                if ([v isKindOfClass:[EGHomeNavigationController class]]){
                    
                    EGHomeNavigationController *navcon = (EGHomeNavigationController*)v;
                    [navcon  popToRootViewControllerAnimated:YES];
                    
                }
            }
            break;
        }
        case 1:{
            eGordianCameraObject *cameraVC =  [[eGordianCameraObject alloc] init];
            [self.navigationController presentViewController:cameraVC animated:YES completion:nil];
            break;
        }
        case 2:
        {
            pv = [PopoverView showPopoverAtPoint:CGPointMake(270, [[UIScreen mainScreen] applicationFrame].size.height-theTabBar.frame.size.height/2)
                                          inView:self.view
                                 withStringArray:kStringArray
                                        delegate:self];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.5f];
    
    switch (index) {
        case 0:{
            
            break;
        }
        case 1:
        {
            
            break;
        }
        case 2:{
            
            [APIClient cancelAllRequest];
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_USERID];
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_OWNERID];
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_AUTHORIZATION_PREFERENCE];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            [appDelegate resetRootViewController];
            
            break;
        }
        default:
            break;
    }
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

-(void) toggleButton
{
    
    self.navigationController.tabBarItem.title = @"desired title text";
    UINavigationController* nav =[self.viewControllers objectAtIndex:0];
    UINavigationController* nav1 =[self.viewControllers objectAtIndex:1];
    UINavigationController* nav2 =[self.viewControllers objectAtIndex:2];
    nav.title = @"ADD TO JOB";
    nav1.tabBarItem.title = @"";
    nav2.tabBarItem.title = @"DELETE";
    nav.tabBarItem.image = nil;
    nav1.tabBarItem.image = nil;
    nav2.tabBarItem.image = nil;
    
    for(UINavigationController* nav in self.viewControllers)
    {
        
        
        //        if (nav.viewControllers && nav.viewControllers.count >0) {
        //            DashBoardViewController *dashBoardVC =(DashBoardViewController *)[nav.viewControllers objectAtIndex:0];
        //            if ([dashBoardVC isKindOfClass:[DashBoardViewController class]])
        //            {
        //                UITabBarItem *xyz=dashBoardVC.tabBarController.tabBarItem;
        //                dashBoardVC.tabBarItem.title = @"Ajey";
        //                dashBoardVC.tabBarItem.image =nil;
        //            }
        //        }
        //        NSLog(@"%@", nav.viewControllers);
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (void)onThumbnailImage
//{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
//    eGordianPhotoViewController *photosCollectionVC = (eGordianPhotoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianPhotosViewController"];
//    [self.navigationController pushViewController:photosCollectionVC animated:YES];
////    [self performSegueWithIdentifier:@"photosCollectionVC" sender:self];
//}

@end
