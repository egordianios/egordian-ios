//
//  EGScopeOfWorkDetailViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/31/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGScopeOfWorkDetailViewController.h"
#import "EGScopeOfWorkEditViewController.h"


@interface EGScopeOfWorkDetailViewController ()

@end

@implementation EGScopeOfWorkDetailViewController
@synthesize currentJobOrder,scopeOfWorkString,strScopeOfWorkUrl, jobTitleLabel, jobClientNameLabel, jobIdLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setIntialUI];
}

-(void) viewWillAppear:(BOOL)animated
{
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    [super viewWillAppear:YES];
}


-(void)setIntialUI{
    
    self.jobTitleLabel.text             = self.currentJobOrder.jobTitle;
    
//    [self.jobTitleLabel sizeToFit];
    
    self.jobIdLabel.text                = [NSString stringWithFormat:@"ID:%@",self.currentJobOrder.jobOrderId];
    
    self.jobClientNameLabel.text        = self.currentJobOrder.clientName;
    
    NSError *error ;
    
//    NSData *data = [self.scopeOfWorkString dataUsingEncoding:NSUTF8StringEncoding];
//    
//    NSAttributedString *str = [[NSAttributedString alloc] initWithData:data
//                               
//                                                               options:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType,
//                                                                         NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}
//                                                    documentAttributes:nil error:&error];
//    
//    self.detailedScopeTextView.attributedText = str;
    
    self.detailedScopeTextView.attributedText   = self.scopeOfWorkString;
    
    [self setJobOrderInfoUI];
}

-(void)setJobOrderInfoUI{
    
    CGFloat padding = 15.0f;
    
    CGFloat labelWidth = self.view.frame.size.width - (2*padding);
    
    CGFloat dy = padding;
    
    CGFloat defaultLabelHeight = 20.0f;
    
    NSString *jobTitleText = [NSString stringWithFormat:@"%@",currentJobOrder.jobTitle];
    
    NSString *jobClientName = [NSString stringWithFormat:@"%@",currentJobOrder.clientName];
    
    NSString *jobOrderId = [NSString stringWithFormat:@"ID:%@",currentJobOrder.jobOrderNumber];
    
    [self.jobOrderTitleInfoView  setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if(jobTitleText.length > 0){
        
        jobTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobTitleLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:15.0]];
        
        [jobTitleLabel setNumberOfLines:0];
        
        [jobTitleLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobTitleLabel setText:jobTitleText];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobTitleLabel WithText:jobTitleLabel.text];
        
        [jobTitleLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobTitleLabel];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(jobClientName.length > 0){
        
        jobClientNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobClientNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobClientNameLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobClientNameLabel setNumberOfLines:0];
        
        [jobClientNameLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobClientNameLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobClientNameLabel setText:jobClientName];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobClientNameLabel WithText:jobClientNameLabel.text];
        
        [jobClientNameLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobClientNameLabel];
        
        dy = dy+dynamicTextHeight+(jobOrderId.length?3.0:padding);
    }
    
    if(jobOrderId.length > 0){
        
        jobIdLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobIdLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobIdLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobIdLabel setNumberOfLines:0];
        
        [jobIdLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobIdLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobIdLabel setText:jobOrderId];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobIdLabel WithText:jobIdLabel.text];
        
        [jobIdLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobIdLabel];
        
        dy = dy+dynamicTextHeight+padding;
    }
    
    
    [self.jobOrderTitleInfoView setFrame:CGRectMake(0, 62.0f, self.view.frame.size.width, dy)];
    [self.scopeOfWorkView setFrame:CGRectMake(0, self.jobOrderTitleInfoView.frame.origin.y+self.jobOrderTitleInfoView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-62.0f-self.jobOrderTitleInfoView.frame.size.height - 62.0f)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)scopeOfWorkBackButtonTapped:(id)sender{
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDOCUMENTS_REFRESH_FLAG];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)scopeOfWorkEditButtonTapped:(id)sender{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EGScopeOfWorkEditViewController *addScopeVC = (EGScopeOfWorkEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGScopeOfWorkEditViewController"];
    
    addScopeVC.isCalledFromOffline = FALSE;
        
    addScopeVC.strDetailedScopeLink = self.strScopeOfWorkUrl;
    
    addScopeVC.scopeOfWorkString = self.detailedScopeTextView.attributedText;
    
    [self.navigationController pushViewController:addScopeVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
