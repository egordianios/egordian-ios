//
//  EGRightMenuViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/25/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EGRightMenuViewController.h"
#import "RightSlideMenuCell.h"
#import "EGJobListViewController.h"
#import "eGordianContactsViewController.h"
#import "eGordianPhotoViewController.h"
#import "APIClient.h"
#import "DashBoardViewController.h"
#import "AppDelegate.h"
#import "TAGContainer.h"
#import "TAGDataLayer.h"
#import "TAGManager.h"

@interface EGRightMenuViewController ()
@property (nonatomic,retain) NSMutableArray *rightMenuArray;
@property(nonatomic, assign) AppDelegate *delegate;

@end

@implementation EGRightMenuViewController

@synthesize rightMenuArray;
@synthesize rightMenuDelegate = _rightMenuDelegate;
@synthesize parentView=_parentView;
@synthesize delegate = _delegate;

+ (id)instantiateRightMenuController {
    static UIViewController *rightMenuController = nil;  // Shared view controller.
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        rightMenuController = [storyboard instantiateViewControllerWithIdentifier:@"EGRightMenuViewController"];
    });
    return rightMenuController;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.rightMenuArray = [NSMutableArray arrayWithObjects:@"Home",@"Job Orders",@"All Contacts",@"Unattached Photos",@"About",@"Support",@"Logout",APP_VERSION_NUMBER, nil];
    self.delegate = [UIApplication sharedApplication].delegate;

}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.logoutView setHidden:YES];
    [self.rightMenuTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSString *)getHomeViewControllerName:(id)myparentViewController{
    NSString *parentView;
    if ([myparentViewController isKindOfClass:[EGJobListViewController class]]) {
        parentView =@"Job List";
    }else if([myparentViewController isKindOfClass:[DashBoardViewController class]]){
        parentView =@"Home";
        
    }else{
        parentView =@"";
    }
    return parentView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rightMenuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RightSlideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RightSlideMenuCell"];
    
    if (!cell)
    {
        cell = [[RightSlideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RightSlideMenuCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    
    switch (indexPath.row) {
        case 0:
            cell.titleText = self.rightMenuArray[0];
            cell.activeIconName = @"eG_Icon-Home_Over";
            cell.inactiveIconName = @"eG_Icon-Home_Norm";
            break;
        case 1:
            cell.titleText = self.rightMenuArray[1];
            cell.activeIconName = @"eG_Icon-JobOrder_Over";
            cell.inactiveIconName = @"eG_Icon-JobOrder_Norm";
            break;
        case 2:
            cell.titleText = self.rightMenuArray[2];
            cell.activeIconName = @"eG_Icon-Contacts_Over";
            cell.inactiveIconName = @"eG_Icon-Contacts_Norm";
            break;
        case 3:
            cell.titleText = self.rightMenuArray[3];
            cell.activeIconName = @"eG_Icon-Camera_Over";
            cell.inactiveIconName = @"eG_Icon-Camera_Norm";
            break;
        case 4:
            cell.titleText = self.rightMenuArray[4];
            cell.activeIconName = @"eG_Icon-eGordian_Over";
            cell.inactiveIconName = @"eG_Icon-eGordian_Norm";
            break;
        case 5:
            cell.titleText = self.rightMenuArray[5];
            cell.activeIconName = @"eG_Icon-Support_Over";
            cell.inactiveIconName = @"eG_Icon-Support_Norm";
            break;
        case 6:
            cell.titleText = self.rightMenuArray[6];
            cell.activeIconName = @"eG_Icon-LogOut_Over";
            cell.inactiveIconName = @"eG_Icon-LogOut_Norm";
            break;
        case 7:
            cell.titleText = self.rightMenuArray[7];
            break;
        default:
            break;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor blackColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    if(_selectedModuleIndex == indexPath.row && !(_selectedModuleIndex == EGORDIAN_LOGOUT_MODULE)){
        [self rightMenuCloseButtonTapped:nil];
        return;
    }
    else{
        
        _selectedModuleIndex = indexPath.row;
        //GTM Implementation
        NSString *menuTitle =rightMenuArray[indexPath.row];
        NSString *homeScreenName =[self getHomeViewControllerName:_parentView];
        [self.delegate.tagManager.dataLayer pushValue:menuTitle.length?menuTitle:@"" forKey:@"navLink"];
        [self.delegate.tagManager.dataLayer pushValue:menuTitle.length?menuTitle:@"" forKey:@"eventLabel"];
        [self.delegate.tagManager.dataLayer pushValue:@"234" forKey:@"jobId"];
        

        NSDictionary *pushDict  = @{@"event": @"navbarLink",// openScreen// Event, name of,
//                                    @"eventCategory":@"",
                                    @"screenName": homeScreenName.length?homeScreenName:@""};
        
                                   // @"navLink":menuTitle.length?menuTitle:@""};
        [self.delegate.tagManager.dataLayer push:pushDict];
        NSLog(@"Nav Link Custom Value sent:%@",menuTitle);
        
        
     // [GTMLogger logEventToGTM:@"loginEvent" andinputParams:pushDict];
        if(indexPath.row == 0){
            
            NSLog(@"---ViewControllers Stack:\n%@",self.navigationController.viewControllers);
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if(indexPath.row == 1){
            [self openJobOrdersList];
        }
        else if(indexPath.row == 2)
        {
            [self openContactsList];
        }
        else if(indexPath.row == 3)
        {
            [self openUnattachedPhotos];
        }
        else if(indexPath.row == 6){
            [UIView animateWithDuration:0.20f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 [self.view bringSubviewToFront:self.logoutView];
                                 [self.logoutView setHidden:NO];
                             }
                             completion:nil];
        }
        else if(indexPath.row == 7){
            _selectedModuleIndex = 8;
            return;
        }
        else{
            _selectedModuleIndex = 8;
            
            UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"Coming Soon" message:@"This page is under development and will be updated soon." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [displayAlert show];
        }
    }
}

- (IBAction)logoutCancelButtonTapped:(id)sender {
    
    [UIView animateWithDuration:0.20f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.logoutView setHidden:YES];
                     }
                     completion:nil];
}

- (IBAction)logoutConfirmButtonTapped:(id)sender {
    
    [UIView animateWithDuration:0.20f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [APIClient cancelAllRequest];
                         [self.logoutView setHidden:YES];
                     }
                     completion:^(BOOL finished) {
                         
                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_USERID];
                         
                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_OWNERID];
                         
                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_AUTHORIZATION_PREFERENCE];
                         
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                         AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                         
                         [appDelegate resetRootViewController];
                         
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }];
}

#pragma mark misc
/*
 * Method Name	: openJobOrdersList
 * Description	: open the contacts screen
 * Parameters	: nil
 * Return value	: nil
 */
-(void)openJobOrdersList{
    
    [self rightMenuCloseButtonTapped:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EGJobListViewController *contactListVC = (EGJobListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGJobListViewController"];
    [self.navigationController pushViewController:contactListVC animated:YES];
}

/*
 * Method Name	: openContactsList
 * Description	: open the contacts screen
 * Parameters	: nil
 * Return value	: nil
 */
-(void) openContactsList
{
    [self rightMenuCloseButtonTapped:nil];
    
    UIStoryboard *contactsStoryboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    eGordianContactsViewController *contactListVC = (eGordianContactsViewController *)[contactsStoryboard instantiateViewControllerWithIdentifier:@"eGordianContactsViewController"];
    [self.navigationController pushViewController:contactListVC animated:YES];
}

/*
 * Method Name	: openUnattachedPhotos
 * Description	: open the unattached photos
 * Parameters	: nil
 * Return value	: nil
 */
-(void) openUnattachedPhotos
{
    [self rightMenuCloseButtonTapped:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    eGordianPhotoViewController *photosCollectionVC = (eGordianPhotoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianPhotosViewController"];
    [self.navigationController pushViewController:photosCollectionVC animated:YES];
}


- (IBAction)rightMenuCloseButtonTapped:(id)sender {

    if(_rightMenuDelegate && [_rightMenuDelegate respondsToSelector:@selector(slideMenuCloseButtonTapped:)]){
        [_rightMenuDelegate slideMenuCloseButtonTapped:self];
    }
}
#pragma UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_USERID];
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_OWNERID];
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_DATABASE];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [appDelegate resetRootViewController];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}



@end
