//
//  EGJobDetailViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/3/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EGJobDetailViewController.h"
#import "EG_TransactionDate.h"
#import "EG_JobLocation.h"
#import "SortViewListCell.h"
#import "JobOrderListCell.h"
#import "TransactionDateCell.h"
#import "EGTrackingDatesViewController.h"
#import "EGJobContactsViewController.h"
#import "eGMapViewController.h"
#import "AppDelegate.h"
#import "TAGContainer.h"
#import "TAGDataLayer.h"
#import "TAGManager.h"
#import "eGordianNotesCell.h"  
#import "eG_Notes.h"
#import "eGordianNotesViewController.h"
#import "EGJobDocumentsViewController.h"
#import "eGordianJobPhotoViewController.h"
#import "eGordianNotesEditViewController.h"
#import "APIClient.h"
#import "eGordianCalendarViewController.h"

@interface EGJobDetailViewController (){
    
    NSMutableArray *dataArray;
    UILabel *infoTextLabel;
    NSIndexPath *selectedIndexPath;
}

#define maxCellsVisibleInNotes 6
#define NotesPAGESIZE 100

@property (nonatomic,assign) AppDelegate *delegate;
@property(nonatomic,retain) EG_JobOrderDetail *jobOrderDeatilDict;
@property(nonatomic,retain) NSMutableArray *trackingDataArray;
@property(nonatomic,retain) NSMutableArray *locationDataArray;
@property (nonatomic) BOOL alreadyAppeared;
@end

@implementation EGJobDetailViewController

@synthesize currentJobOrder,mySensitiveRect, egNotesArray, egNotesDesc;

@synthesize delegate=_delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    self.delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    [self setIntialUI];
    //GTM Implementation
    [GTMLogger pushOpenScreenEventWithScreenName:@"job detail"];
    self.egNotesArray = nil;
    notesFetched = FALSE;
    photosCount = 0;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    notesPageNumber = 1;
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:APP_JOBDETAIL_REFRESH_FLAG]boolValue]){
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:APP_JOBDETAIL_REFRESH_FLAG];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self fetchJobDetails:currentJobOrder.jobOrderLink];
    }
    
//    if (!self.alreadyAppeared) {
//        self.alreadyAppeared = YES;
//        
//    }
    [self.jobDetailScrollView setContentOffset:CGPointMake(0, 0)];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    notesFetched = FALSE;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

-(void)fetchJobDetails:(NSString *)jobOrderLink{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self actvityStart];
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:jobOrderLink serviceType:EGORDIAN_API_JOBORDER_DETAIL];
}

-(void)fetchTrackingDates:(NSString *)trackingDateLink{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:trackingDateLink serviceType:EGORDIAN_API_TRACKING_DATES];
}

-(void)fetchJobOrderStatus:(NSString *)statusLink{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:statusLink serviceType:EGORDIAN_API_JOBORDER_STATUS];
}

-(void)fetchNotes{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self.activityLoader startAnimating];
    NSString *notesQueryString = [NSString stringWithFormat:@"%@/Users/%@/JobOrders/%@/Notes?pageSize=%d&pageNumber=%d&sortBy=-%@", APP_HEADER_URL,[AppConfig getUserId],currentJobOrder.jobOrderId,  NotesPAGESIZE, notesPageNumber, createdDate];

    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:notesQueryString serviceType:EGORDIAN_API_NOTES];
}

//dynamically udjust the frame of NotesTable depending on the number of Notes given by the service response
-(void)setNotesTableFrame
{
//    CGFloat height =  self.buttonView.frame.origin.y-self.jobTitleView.frame.size.height;
    int notesViewYPos = self.trackDatesView.frame.size.height + self.trackDatesView.frame.origin.y;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    if ([self.egNotesArray count]<maxCellsVisibleInNotes)
    {
        int count = [self.egNotesArray count]?(int)[self.egNotesArray count]:1;
        [self.eGJobNotesTableView setFrame:CGRectMake(0, self.eGJobNotesTableView.frame.origin.y, self.eGJobNotesTableView.frame.size.width, (count*[self.eGJobNotesTableView cellForRowAtIndexPath:indexPath].frame.size.height))];
        [self.egNotesView setFrame:CGRectMake(self.egNotesView.frame.origin.x, notesViewYPos, self.egNotesView.frame.size.width, self.eGJobNotesTableView.frame.size.height+self.egNotesHeaderView.frame.size.height )];
        [self.jobDetailScrollView setContentSize:CGSizeMake(0, self.egNotesView.frame.origin.y + self.egNotesView.frame.size.height)];
        return;
    }
    else
    {
        [self.eGJobNotesTableView setFrame:CGRectMake(0, self.eGJobNotesTableView.frame.origin.y, self.eGJobNotesTableView.frame.size.width, self.eGJobNotesTableView.frame.size.height)];
        [self.egNotesView setFrame:CGRectMake(self.egNotesView.frame.origin.x, notesViewYPos, self.egNotesView.frame.size.width, self.eGJobNotesTableView.frame.size.height+self.egNotesHeaderView.frame.size.height)];
        [self.jobDetailScrollView setContentSize:CGSizeMake(0, self.egNotesView.frame.origin.y + self.egNotesView.frame.size.height)];
        return;

    }
}

-(void)setIntialUI{
    
    notesCount = 0;
    
    [self.loaderView setHidden:YES];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    [gestureRecognizer setNumberOfTapsRequired:1.0];
    
    [gestureRecognizer setDelegate:self];
    
    [self.popOverView addGestureRecognizer:gestureRecognizer];
    
    self.documentsLabelCount.text       = @"";
    
    self.photosLabelcount.text          = @"";
    
    self.contactsLabelCount.text        = @"";
    
    self.popOverView.hidden = YES;
    
    dataArray = [[NSMutableArray alloc]init];//[NSMutableArray arrayWithObjects:@"Project Start",@"Joint Scope",@"Project Completed",@"Joint Scope", nil];
    
    self.trackingDataArray = [[NSMutableArray alloc]init];
    
    self.locationDataArray = [[NSMutableArray alloc]init];
    
    self.jobOrderDeatilDict = [[EG_JobOrderDetail alloc]init];
    
//    self.jobStatusLabel.text = [dataArray objectAtIndex:0];
    
//    [self.pickerTable reloadData];
    
//    CGFloat height =  self.buttonView.frame.origin.y-self.jobTitleView.frame.size.height;
    
    [self.locationView setFrame:CGRectMake(self.locationView.frame.origin.x, self.buttonView.frame.origin.y+self.buttonView.frame.size.height, self.locationView.frame.size.width, self.locationView.frame.size.height)];
    
    [self.trackDatesView setFrame:CGRectMake(self.trackDatesView.frame.origin.x, self.locationView.frame.origin.y+self.locationView.frame.size.height, self.trackDatesView.frame.size.width, self.trackDatesView.frame.size.height)];
    
    [self.egNotesView setFrame:CGRectMake(self.egNotesView.frame.origin.x, self.trackDatesView.frame.origin.y+self.trackDatesView.frame.size.height, self.egNotesView.frame.size.width, self.egNotesView.frame.size.height)];
    
    [self.jobDetailScrollView setContentSize:CGSizeMake(0, self.egNotesView.frame.origin.y+self.egNotesView.frame.size.height)];
}

#pragma UIGestureRecogniser Delegates

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.pickerTable]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    mySensitiveRect = actualPopOverView.frame;
    CGPoint p = [gestureRecognizer locationInView:self.popOverView];
    if (!CGRectContainsPoint(mySensitiveRect, p)) {
        self.popOverView.hidden = YES;
        NSLog(@"got a tap in the region i care about");
    } else {
        NSLog(@"got a tap, but not where i need it");
    }
}

- (IBAction)jobDetailBackButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)statusButtonTapped:(UIButton *)sender{
  
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    if(!dataArray.count){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                        message:@"Status not available for this owner."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        CGPoint p = [self.statusButton.superview convertPoint:self.statusButton.center toView:self.view];
        
        [actualPopOverView setFrame:CGRectMake(actualPopOverView.frame.origin.x, (p.y+(self.statusButton.frame.size.height/2)-65), actualPopOverView.frame.size.width, actualPopOverView.frame.size.height)];
        
        [self.view bringSubviewToFront:self.popOverView];
        
        [self.pickerTable reloadData];
        
        self.popOverView.hidden = NO;
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    NSInteger rowCount = 0;
    
    if(tableView == self.pickerTable){
        rowCount = dataArray.count;
    }
    else if(tableView == self.locationTable){
        rowCount = self.locationDataArray.count?self.locationDataArray.count:1;
    }
    else if(tableView == self.trackingDatesTable){
        rowCount = self.trackingDataArray.count?self.trackingDataArray.count:1;
    }
    else if (tableView == self.eGJobNotesTableView)
    {
        rowCount = self.egNotesArray.count?self.egNotesArray.count:1;
    }
    
    return rowCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id cellToReturn=nil;
    
    if(tableView == self.pickerTable){
        
        SortViewListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SortViewListCell"];
        
        if (!cell)
        {
            cell = [[SortViewListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SortViewListCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        if(dataArray.count)
            cell.titleText = [[dataArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
        
        if([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Name"] isEqualToString:self.jobStatusLabel.text]){
            cell.selectionImage.image = [UIImage imageNamed:@"egSelectJobTick"];
        }
        else{
            cell.selectionImage.image = nil;
        }
        //cellToReturn = cell;
        return cell;
        
    }
    else if(tableView == self.locationTable){
        
        JobOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobOrderListCell"];
        
        if (!cell)
        {
            cell = [[JobOrderListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JobOrderListCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        NSString *locationValue = @"";
        
        if(self.locationDataArray.count > 0)
        {
            EG_JobLocation *currentLocation = (EG_JobLocation *)[self.locationDataArray objectAtIndex:indexPath.row];
            
            locationValue             = [NSString stringWithFormat:@"%@%@%@%@",currentLocation.address1.length?[currentLocation.address1 stringByAppendingString:@","]:@"",currentLocation.city.length?[currentLocation.city stringByAppendingString:@","]:@"",currentLocation.state.length?[currentLocation.state stringByAppendingString:@","]:@"",currentLocation.zipCode.length?currentLocation.zipCode:@""];
            
            cell.jobLocationLabel.text      = [NSString stringWithFormat:@"%@",locationValue.length?locationValue:@"No address is currently set for this job order"];
            
            cell.locationIconBtn.hidden = TRUE;
            cell.directionsLabel.hidden = TRUE;
            
            if(([currentLocation.address1 length]) && [currentLocation.city length] && ([currentLocation.state length] || [currentLocation.zipCode length])){
                
                [cell.locationIconBtn setImage:[UIImage imageNamed:@"eG_JobList_Direction_Active" ] forState:UIControlStateNormal] ;
                cell.locationIconBtn.hidden = FALSE;
                cell.directionsLabel.hidden = FALSE;
                [cell.locationIconBtn addTarget:self action:@selector(onLocationBtn:) forControlEvents:UIControlEventTouchUpInside];
                cell.locationIconBtn.tag = indexPath.row;
            }
            else{
                cell.locationIconBtn.hidden = TRUE;
                cell.directionsLabel.hidden = TRUE;
            }
        }
        else{
            cell.locationIconBtn.hidden = TRUE;
            cell.directionsLabel.hidden = TRUE;
            cell.jobLocationLabel.text      = @"No address is currently set for this job order";
        }
                //cellToReturn = cell;
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else if(tableView == self.trackingDatesTable){
        
        TransactionDateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionDateCell"];
        
        if (!cell)
        {
            cell = [[TransactionDateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TransactionDateCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if(self.trackingDataArray.count > 0){
            
            EG_TransactionDate *currentTrackingDate = (EG_TransactionDate *)[self.trackingDataArray objectAtIndex:indexPath.row];
            
            NSString *plannedDate = [AppConfig dateStringFromString:currentTrackingDate.jobPlannedDate.length?[currentTrackingDate.jobPlannedDate substringToIndex: MIN(10, [currentTrackingDate.jobPlannedDate length])]:@""
                                                       sourceFormat:@"yyyy-MM-dd"
                                                  destinationFormat:@"MM/dd"];
            
            cell.dateLabel.text = plannedDate;
            cell.descriptionLabel.text = currentTrackingDate.jobDescription;
            cell.plannedLabel.text = @"(planned)";
        }
        else{
            cell.descriptionLabel.numberOfLines = 2;
            cell.dateLabel.text = @"";
            cell.plannedLabel.text = @"";
            cell.descriptionLabel.text = @"No tracking dates were set for this job order";
        }
        //cellToReturn = cell;
        return cell;
    }
    //the notes table view
    else if (tableView == self.eGJobNotesTableView)
    {
        NSString* reuseString = @"egNotesCell";
        eGordianNotesCell * egNotesCell = [tableView dequeueReusableCellWithIdentifier:reuseString];
        
        if(!egNotesCell)
        {
            egNotesCell = [[eGordianNotesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseString];
            egNotesCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if([self.egNotesArray count])
        {
            eG_Notes* notesObj = (eG_Notes*)[self.egNotesArray objectAtIndex:indexPath.row];
            egNotesCell.eGDateLbl.text = notesObj.CreatedDate;
            egNotesCell.eGSubjectLbl.text = notesObj.Subject;
            egNotesCell.eGSnippetLbl.text = notesObj.Value;
            [egNotesCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            egNotesCell.eGSubjectLbl.textAlignment = NSTextAlignmentLeft;
            egNotesCell.eGSubjectLbl.numberOfLines = 0;
        }
        else
        {
            egNotesCell.eGSubjectLbl.numberOfLines = 2;
            egNotesCell.eGDateLbl.text = @"";
            egNotesCell.eGSnippetLbl.text = @"";
            egNotesCell.eGSubjectLbl.text = @"No Notes were set for this job";
            egNotesCell.eGSubjectLbl.textAlignment = NSTextAlignmentCenter;
            [egNotesCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [egNotesCell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        cellToReturn = egNotesCell;
    }
    return cellToReturn;
}

-(void)onLocationBtn:(id)sender
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    UIButton* locationBtn = (UIButton*)sender;
    
    NSIndexPath *indexPath = [self.locationTable indexPathForCell:((JobOrderListCell*)locationBtn.superview)];
    
    if(self.locationDataArray.count > 0){
    
        EG_JobLocation *location = [self.locationDataArray objectAtIndex:indexPath.row];
        
        if(location.zipCode.length || location.city.length || location.address1.length){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
            
            eGMapViewController *mapsVC = (eGMapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGMapViewController"];
            
            NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] init];
            
            [addressDict setObject:[NSString stringWithFormat:@"%@", [currentJobOrder.jobTitle length]?[NSString stringWithFormat:@"%@",currentJobOrder.jobTitle]:@""] forKey:@"Name"] ;
            [addressDict setObject:[NSString stringWithFormat:@"%@", [location.address1 length]?[NSString stringWithFormat:@"%@",location.address1]:@""] forKey:@"Address"] ;
            
            [addressDict setObject:[NSString stringWithFormat:@"%@%@%@",location.city.length?[location.city stringByAppendingString:@","]:@"",location.state.length?[location.state stringByAppendingString:@","]:@"",location.zipCode.length?location.zipCode:@""] forKey:@"A_Address1"] ;
            
            mapsVC.addressDict = addressDict;
            //        [mapsVC setIntialUI:addressDict];
            
            [self.navigationController pushViewController:mapsVC animated:NO];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                            message:@"Address not available for this contact"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    
    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    NSString *currentValue =@"";
    
    NSString *prevValue;
    
    prevValue =self.jobStatusLabel.text;
    
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    if(tableView == self.pickerTable){
        
        [self.popOverView setHidden:YES];
        
        self.jobStatusLabel.text = [[dataArray objectAtIndex:indexPath.row]objectForKey:@"Name"];
        
        currentValue =[[dataArray objectAtIndex:indexPath.row]objectForKey:@"Name"];
        
        [self updateJobOrderStatus:[[dataArray objectAtIndex:indexPath.row]objectForKey:@"Id"] withJobTitle:self.jobOrderDeatilDict.jobTitle];
        
        [self.delegate.tagManager.dataLayer pushValue:prevValue forKey:@"beforeStatus"];
        
        [self.delegate.tagManager.dataLayer pushValue:currentValue forKey:@"afterStatus"];
        
        [self.delegate.tagManager.dataLayer pushValue:@"statusChange" forKey:@"event"];
        
    }
    else if (tableView == self.trackingDatesTable){

        if(!self.trackingDataArray.count)
            return;
        
        eGordianCalendarViewController *egCalendar = [[eGordianCalendarViewController alloc] init];
        egCalendar.transactionDate = [self.trackingDataArray objectAtIndex:indexPath.row];
        egCalendar.jobID = self.currentJobOrder.jobOrderId;
        [self.navigationController pushViewController:egCalendar animated:YES];
    }
    else if (tableView == self.eGJobNotesTableView)
    {
        if(self.egNotesArray.count>0){
            
            eG_Notes* notes = [self.egNotesArray objectAtIndex:indexPath.row];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            eGordianNotesViewController *notesVC = (eGordianNotesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianNotesViewController"];
            notesVC.notes = notes;
            notesVC.jobTitle = self.currentJobOrder.jobTitle;
            [self.navigationController pushViewController:notesVC animated:YES];
        }
    }
}

-(void)updateJobOrderStatus:(NSString *)statusID withJobTitle:(NSString *)title{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self actvityStart];
    
    NSString *jsonString = [NSString stringWithFormat:@" {\"Fields\":[{\"Name\":\"JobTitle\",\"Value\":\"%@\",\"Security\":\"Write\"},{\"Name\":\"StatusId\",\"Value\":\"%@\",\"Security\":\"Write\"}]}",title,statusID];
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    id jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:currentJobOrder.jobOrderLink andserviceType:EGORDIAN_API_JOBSTATUS_UPDATE];
}

-(void)callGTMTrackerForStatusChange:(NSString *)previousState andCurrentState:(NSString *)currentStat
{
    //GTM Implementation
    NSDictionary *pushDict  = @{@"event": @"statusChange",// openScreen// Event, name of
                                @"beforeStatus": previousState.length?previousState:@"",
                                @"afterStatus": currentStat.length?currentStat:@""
                                };
    [GTMLogger logEventToGTM:@"openScreenfromJobListingDetail" andinputParams:pushDict];
    
}

#pragma UIScrollView Delegates

/*
 * Method Name	: scrollViewDidEndDecelerating
 * Description	: called on scrolling end.
 * Parameters	: UIScrollView
 * Return value	: nil
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat bottomInset = scrollView.contentInset.bottom;
    CGFloat bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height - bottomInset;
    
    if(scrollView == self.eGJobNotesTableView)
    {
        if (bottomEdge == scrollView.contentSize.height) {
            // Scroll view is scrolled to bottom
            if(notesCount>=(notesPageNumber*NotesPAGESIZE))
            {
                notePrevPageNumber = notesPageNumber;
                notesPageNumber++;
                [self performSelector:@selector(fetchNotes) withObject:nil afterDelay:1];
            }
        }
    }
}

-(void)setJobOrderViewFrame:(EG_JobOrderDetail *)detailDict{
    
    CGFloat padding = 15.0f;
    
    CGFloat labelWidth = self.view.frame.size.width - (2*padding);
    
    CGFloat dy = 10.0;
    
    NSString *jobTitleText = [NSString stringWithFormat:@"%@",detailDict.jobTitle];
    
    NSString *jobClientName = [NSString stringWithFormat:@"%@",detailDict.clientName];
    
    NSString *jobOrderId = [NSString stringWithFormat:@"ID:%@",detailDict.jobOrderNumber];
    
    self.documentsLabelCount.text       = [NSString stringWithFormat:@"%ld",(long)detailDict.totalDocumentsCount];
    
    self.photosLabelcount.text          = [NSString stringWithFormat:@"%ld",(long)detailDict.totalPhotosCount];
    
    photosCount = (int)detailDict.totalPhotosCount;
    
    self.contactsLabelCount.text        = [NSString stringWithFormat:@"%ld",(long)detailDict.totalContactsCount];
    
    self.jobOrderDeatilDict             = detailDict;
    
    if(jobTitleText.length > 0){
        
        [self.jobTitleLabel setNumberOfLines:0];
        
        [self.jobTitleLabel setText:jobTitleText];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:self.jobTitleLabel WithText:self.jobTitleLabel.text];
        
        [self.jobTitleLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(jobClientName.length > 0){
        
        [self.jobClientNameLabel setNumberOfLines:0];
        
        [self.jobClientNameLabel setText:jobClientName];//jobClientName
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:self.jobClientNameLabel WithText:self.jobClientNameLabel.text];
        
        [self.jobClientNameLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        dy = dy+dynamicTextHeight+(jobOrderId.length?3.0:padding);
    }
    
    if(jobOrderId.length > 0){
        
        [self.jobIdLabel setNumberOfLines:0];
        
        [self.jobIdLabel setText:jobOrderId];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:self.jobIdLabel WithText:self.jobIdLabel.text];
        
        [self.jobIdLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        dy = dy+dynamicTextHeight+padding;
    }
    
    [self.titleViewSeperator setFrame:CGRectMake(0, dy, self.titleViewSeperator.frame.size.width, self.titleViewSeperator.frame.size.height)];
    
    dy = dy + self.titleViewSeperator.frame.size.height;
    
    [self.jobTitleView setFrame:CGRectMake(0, 0, self.view.frame.size.width, dy)];
    
    [self.statusView setFrame:CGRectMake(0, self.jobTitleView.frame.origin.y+self.jobTitleView.frame.size.height, self.statusView.frame.size.width, self.statusView.frame.size.height)];
    
    [self.buttonView setFrame:CGRectMake(0, self.statusView.frame.origin.y+self.statusView.frame.size.height, self.buttonView.frame.size.width, self.buttonView.frame.size.height)];
    
    [EGordianUtilities writeJobDetails:self.jobOrderDeatilDict.jobOrderId ownerID:self.jobOrderDeatilDict.ownerID];
    
    NSUInteger locationArrayCount = detailDict.locationArray.count?detailDict.locationArray.count:1;
    
    [self.locationView setFrame:CGRectMake(self.locationView.frame.origin.x, self.buttonView.frame.origin.y+self.buttonView.frame.size.height, self.locationView.frame.size.width, (locationArrayCount*90.0)+44.0)];
    
    [self.locationTable setFrame:CGRectMake(self.locationTable.frame.origin.x, self.locationTable.frame.origin.y, self.locationTable.frame.size.width, self.locationView.frame.size.height-44.0)];
    
    self.locationDataArray = [detailDict.locationArray copy];
    
    [self.locationTable reloadData];
    
    if([currentJobOrder.jobOrderLink rangeOfString:@"Owners"].location != NSNotFound){
        
        NSString *originalString = currentJobOrder.jobOrderLink;
        
        NSRange end = [originalString rangeOfString:@"JobOrders/"];
        
        NSString *statusURL = [NSString stringWithFormat:@"%@Status",[originalString substringToIndex:end.location]];
        
        [self fetchJobOrderStatus:statusURL];
    }
    
}

#pragma mark - Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    switch (type) {
        case EGORDIAN_API_TRACKING_DATES:{
            
            NSUInteger trackingDatesArrayCount = responseArray.count?responseArray.count:1;
            
            [self.trackDatesView setFrame:CGRectMake(self.trackDatesView.frame.origin.x, self.locationView.frame.origin.y+self.locationView.frame.size.height, self.trackDatesView.frame.size.width, (trackingDatesArrayCount*50.0)+44.0)];
            
            [self.jobDetailScrollView setContentSize:CGSizeMake(0, self.trackDatesView.frame.origin.y + self.trackDatesView.frame.size.height)];
            
            [self.trackingDatesTable setFrame:CGRectMake(self.trackingDatesTable.frame.origin.x, self.trackingDatesTable.frame.origin.y, self.trackingDatesTable.frame.size.width, self.trackDatesView.frame.size.height-44.0)];
            
            self.trackingDataArray = [responseArray copy];
            
            [self.trackingDatesTable reloadData];
            
            if(!notesFetched)
            {
                notesFetched = TRUE;
                [self performSelector:@selector(fetchNotes) withObject:nil afterDelay:1];
            }
        }
            break;
        case EGORDIAN_API_NOTES:{
            
            self.egNotesArray = [responseArray copy];
            
            [self.eGJobNotesTableView reloadData];
            
            [self setNotesTableFrame];
            
            [self actvityStop];
        }
            break;
        case EGORDIAN_API_JOBORDER_STATUS:{
            
            dataArray = [responseArray copy];
            
            BOOL stausFlag = FALSE;
            
            for (NSDictionary *dict in dataArray) {
                if([[dict objectForKey:@"Id"] isEqualToString:self.jobOrderDeatilDict.jobStatus]){
                    self.jobStatusLabel.text = [dict objectForKey:@"Name"];
                    stausFlag = TRUE;
                    break;
                }
            }
            
            if(!stausFlag){
                self.jobStatusLabel.text = @"Status not available";
            }
            
            [self fetchTrackingDates:[NSString stringWithFormat:@"%@/?&pageSize=4&pageNumber=1&sortBy=-plannedDate",self.jobOrderDeatilDict.trackingDateLink]];
        }
            break;
        case EGORDIAN_API_JOBSTATUS_UPDATE:{
            [self actvityStop];
        }
            default:
            break;
    }
}

-(void)serviceRequestFinishedWithDict:(NSMutableDictionary *)responseDict serviceType:(APIKey)type{
    
       switch (type) {
        case EGORDIAN_API_JOBORDER_DETAIL:
        {
            EG_JobOrderDetail* jobDetailDictionary = (EG_JobOrderDetail*)responseDict;
            
            jobDetailDictionary.jobOrderNumber = currentJobOrder.jobOrderNumber;
            
            self.jobTitleLabel.numberOfLines = 2;
            
            self.jobTitleLabel.minimumScaleFactor = 0.6;
            
            [self.jobTitleLabel adjustsFontSizeToFitWidth];
            
//            self.jobTitleLabel.text             = jobDetailDictionary.jobTitle;
//            
//            self.jobIdLabel.text                = [NSString stringWithFormat:@"ID:%@",jobDetailDictionary.jobOrderId];
//            
//            self.jobClientNameLabel.text        = jobDetailDictionary.clientName;
            
            
            [self setJobOrderViewFrame:jobDetailDictionary];
            
            
            
//            ((AppDelegate*)[[UIApplication sharedApplication] delegate]).jobOrder = self.jobOrderDeatilDict;
            
                        
        }
               break;
            
        default:
            break;
    }
}



-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
 
    [self actvityStop];
    
    if(type == EGORDIAN_API_TRACKING_DATES)//needs to be called because even if the tracking api request fails, the notes fetch API must be called.
       [self fetchNotes];
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    [self.activityLoader stopAnimating];
    NSLog(@"Error Message--%@",alertMessage);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)trackingDatesButtonTapped:(id)sender {
    
    if(self.trackingDataArray.count > 0){
        
        if(![AppDelegate isConnectedToInternet])
        {
            [EGordianAlertView showFeatureNotAvailableMessage];
            return;
        }
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EGTrackingDatesViewController *trackDatesVC = (EGTrackingDatesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGTrackingDatesViewController"];
        trackDatesVC.currentJobOrder = self.jobOrderDeatilDict;
        [self.navigationController pushViewController:trackDatesVC animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                        message:@"No tracking dates were set for this job order"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)documentsButtonTapped:(id)sender{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EGJobDocumentsViewController *documentsVC = (EGJobDocumentsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGJobDocumentsViewController"];
    documentsVC.currentJobOrder = self.jobOrderDeatilDict;
    documentsVC.strJobOrderLink = currentJobOrder.jobOrderLink;
    [self.navigationController pushViewController:documentsVC animated:YES];
}

- (IBAction)jobContactsButtonTapped:(id)sender {
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EGJobContactsViewController *jobContactVC = (EGJobContactsViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"EGJobContactsViewController"];
    jobContactVC.currentJobOrder = self.jobOrderDeatilDict;
    jobContactVC.strJobOwnerId = currentJobOrder.ownerID;
    [self.navigationController pushViewController:jobContactVC animated:YES];
}

- (IBAction)onJobPhotos:(id)sender {
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    if(photosCount)
    {
        UIStoryboard *contactsStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        eGordianJobPhotoViewController *jobPhotosVC = (eGordianJobPhotoViewController *)[contactsStoryboard instantiateViewControllerWithIdentifier:@"eGordianJobPhotoViewController"];
        jobPhotosVC.currentJob = self.currentJobOrder;
        [self.navigationController pushViewController:jobPhotosVC animated:YES];
    }
}

- (IBAction)addNoteTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    eGordianNotesEditViewController *addNotesVC = (eGordianNotesEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianNotesEditViewController"];
    addNotesVC.note = nil;
    addNotesVC.strJobOrderId = currentJobOrder.jobOrderId;
    [self.navigationController pushViewController:addNotesVC animated:YES];
}
@end
