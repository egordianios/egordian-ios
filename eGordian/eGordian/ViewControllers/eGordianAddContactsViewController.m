//
//  eGordianAddContactsViewController.m
//  eGordian-iOS
//
//  Created by Laila Varghese on 9/3/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianAddContactsViewController.h"
#import "AFHTTPRequestOperation.h"
#import "EGJobContactsViewController.h"

@interface eGordianAddContactsViewController (){
    BOOL shouldShowValidation;
}

@end

@implementation eGordianAddContactsViewController

@synthesize isEditing, egContactDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //if in edit mode populate the data to edit
    
    shouldShowValidation = YES;
    
    [self.loaderView setHidden:YES];
    
    [self.eGordianAddContactsName.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactsStreet.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactsCity.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactsState.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactZipCode.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactEMail.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactPhone.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    [self.eGordianAddContactMobile.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    
    
    if(isEditing)
    {
       [self populateContact];
        self.eGHeaderLabel.text = @"CONTACT EDIT";
        self.eGordianEditContactLbl.text = @"Edit Existing Contact";
    }
    else{
        self.eGHeaderLabel.text = @"CONTACT ADD";
        self.eGordianEditContactLbl.text = @"Add New Contact";
    }
    
    [self.eGordianAddContactScrollView setContentSize:CGSizeMake(320, 600)];
    
    [self.eGordianAddContactScrollView contentSizeToFit];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 * Method Name	: viewDidLayoutSubviews
 * Description	: initialize those which are needed for alphabetical sorting
 * Parameters	: nil
 * Return value	: nil
 */
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}

#pragma mark - UITextView delegates
/*
 * Method Name	: textViewDidBeginEditing
 * Description	: called when a textview editing begins
 * Parameters	: UITextView - textview
 * Return value	: nil
 */
//- (void)textViewDidBeginEditing:(UITextView *)textView
//{
//     self.activeTextView = textView;
//}

/*
 * Method Name	: textViewDidEndEditing
 * Description	: called when a textview editing ends
 * Parameters	: UITextView - textview
 * Return value	: nil
 */
//- (void)textViewDidEndEditing:(UITextView *)textView
//{
//    if(textView == self.eGordianAddContactsName)
//        self.egContact.FirstName = textView.text;
//    
//    else if (textView == self.eGordianAddContactsStreet)
//        self.egContact.CompanyName= textView.text;
//    
//    else if (textView == self.eGordianAddContactsCity)
//        self.egContact.City = textView.text;
//    
//    else if (textView == self.eGordianAddContactsState)
//        self.egContact.State = textView.text;
//    
//    else if (textView == self.eGordianAddContactZipCode)
//        self.egContact.ZipCode = textView.text;
//    
//    else if (textView == self.eGordianAddContactEMail)
//        self.egContact.eMail = textView.text;
//    
//    else if (textView == self.eGordianAddContactPhone)
//        self.egContact.BusinessPhone1 = textView.text;
//    
//    else if (textView == self.eGordianAddContactMobile)
//        self.egContact.MobilePhone = textView.text;
//    
//    self.activeTextView = nil;
//}
/*
 * Method Name	: textFieldShouldReturn
 * Description	: called when a textview editing ends
 * Parameters	: UITextField - textField
 * Return value	: nil
 */

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.eGordianAddContactsName) {
        [textField resignFirstResponder];
        [self.eGordianAddContactsStreet becomeFirstResponder];
    } else if (textField == self.eGordianAddContactsStreet) {
        [textField resignFirstResponder];
        [self.eGordianAddContactsCity becomeFirstResponder];
    }else if (textField == self.eGordianAddContactsCity) {
        [textField resignFirstResponder];
        [self.eGordianAddContactsState becomeFirstResponder];
    }else if (textField == self.eGordianAddContactsState) {
        [textField resignFirstResponder];
        [self.eGordianAddContactZipCode becomeFirstResponder];
    }else if (textField == self.eGordianAddContactZipCode) {
        [textField resignFirstResponder];
        [self.eGordianAddContactEMail becomeFirstResponder];
    }else if (textField == self.eGordianAddContactEMail) {
        [textField resignFirstResponder];
        [self.eGordianAddContactPhone becomeFirstResponder];
    }else if (textField == self.eGordianAddContactPhone) {
        [textField resignFirstResponder];
    }
    else if (textField == self.eGordianAddContactMobile)
    {
        [self.eGordianAddContactMobile becomeFirstResponder];
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    NSString* message = @"";
    NSString* title = @"";
    BOOL isValid = TRUE;
    
    if (textField == self.eGordianAddContactZipCode) {
        isValid = [self isValidPinCode:self.eGordianAddContactZipCode.text];
        
        title = @"Invalid Zip Code";
        message = @"Please check the Zip Code";
        
    }else if (textField == self.eGordianAddContactEMail) {
        isValid = [self isValidEmail: self.eGordianAddContactEMail.text];
        title = @"Invalid Email Address";
        message = @"Please check the Email Address";
        
    }else if (textField == self.eGordianAddContactPhone) {
        isValid = [self validatePhone:self.eGordianAddContactPhone.text];
        
        title = @"Invalid Phone Number";
        message = @"Please check the phone number";
    }
    else if (textField == self.eGordianAddContactMobile)
    {
        isValid = [self validatePhone:self.eGordianAddContactMobile.text];
        title = @"Invalid Phone Number";
        message = @"Please check the phone number";
    }
    
    if(!isValid)
    {
        if(shouldShowValidation){
            if ([UIAlertController class])
            {
                // use UIAlertController
                UIAlertController *alert= [UIAlertController
                                           alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action){
                                                               //Do Some action here
//                                                               textField.text = @"";
                                                               
                                                           }];
                
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            else
            {
                // use UIAlertView
                UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:title
                                                                 message:message
                                                                delegate:self
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil, nil];
                dialog.tag = 450;
//                textField.text = @"";
                [dialog show];
                
            }
        }
    }
}

//- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
//    if (textField == self.eGordianAddContactZipCode) {
//        [self isValidPinCode:textField];
//    }else if (textField == self.eGordianAddContactEMail) {
//        [self isValidEmail:textField];
//    }else if (textField == self.eGordianAddContactPhone) {
//        [self validatePhone:textField];
//    }else if (textField == self.eGordianAddContactMobile) {
//        [self validatePhone:textField];
//    }
//    return YES;
//}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    
    if(!phoneNumber.length)
        return TRUE;
    
    NSString *phoneRegex = @"^\\(?\\d{3}\\)?[- ]?\\d{3}[- ]?\\d{4}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL isValid = [phoneTest evaluateWithObject:phoneNumber];
    return isValid;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 450)
    {
        
    }
}


-(BOOL)isValidPinCode:(NSString*)pincode   {
    
    if(!pincode.length)
        return TRUE;
    
    NSString *pinRegex = @"^\\d{5}(-\\d{4})?$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    
    BOOL pinValidates = [pinTest evaluateWithObject:pincode];
    
    return pinValidates;
}

-(BOOL)isValidEmail:(NSString*)email
{
    
    if(!email.length)
        return FALSE;
    
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValidEmail = [emailTest evaluateWithObject:email];
    
    return isValidEmail;
}

-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

#pragma mark - Button Handlers
/*
 * Method Name	: onBackBtn
 * Description	: on back button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onBackBtn:(id)sender
{
    shouldShowValidation = NO;
    
    [self.view endEditing:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Method Name	: onSaveBtn
 * Description	: on save button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSaveBtn:(id)sender
{
    for(id currentField in [self.eGordianAddContactScrollView subviews]){
        if([currentField isKindOfClass:[UITextField class]]){
            [currentField resignFirstResponder];
        }
    }
    
    BOOL isValid = TRUE;
    BOOL isPhonePresent = FALSE;
    BOOL isMailPresent = FALSE;
    
    NSString* title=@"eGordian";
    NSString* message=@"";
    
    if(![self isValidEmail:self.eGordianAddContactEMail.text])
    {
        isValid = FALSE;
//        self.eGordianAddContactEMail.text = @"";
        title = @"Invalid Email Address";
        message = @"Please check the Email Address";
    }
    
    if(![self isValidPinCode:self.eGordianAddContactZipCode.text])
    {
        isValid = FALSE;
//        self.eGordianAddContactZipCode.text = @"";
        title = @"Invalid Zip Code";
        message = @"Please check the Zip Code";
    }
    
    if(![self validatePhone:self.eGordianAddContactMobile.text])
    {
        isValid = FALSE;
//        self.eGordianAddContactMobile.text = @"";
        title = @"Invalid Phone Number";
        message = @"Please check the phone number";
    }
    
    if(![self validatePhone:self.eGordianAddContactPhone.text])
    {
        isValid = FALSE;
//        self.eGordianAddContactPhone.text = @"";
        title = @"Invalid Phone Number";
        message = @"Please check the phone number";
    }
    
    if(self.eGordianAddContactMobile.text.length || self.eGordianAddContactPhone.text.length)
        isPhonePresent = TRUE;
    
    if(!isPhonePresent)
    {
        message = @"Please give atleast one phone number.";
    }
    
    if(self.eGordianAddContactEMail.text.length)
        isMailPresent = TRUE;
    
    if(!isMailPresent)
    {
        message = @"Email Address is mandatory.";
    }
    
    NSString *trimmedString = [self.eGordianAddContactsName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(!trimmedString.length)
         message = @"Name cannot be blank. Please give a name";
    
    if(!trimmedString.length || !isPhonePresent || !isMailPresent || !isValid)
    {
       
        if ([UIAlertController class])
        {
            // use UIAlertController
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:title
                                       message:message
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           
                                                       }];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            // use UIAlertView
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:title
                                                             message:message
                                                            delegate:self
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil, nil];
            dialog.tag = 450;
            [dialog show];
            
        }
    }
    else
    {
        self.eGordianAddContactsName.text = trimmedString;
        [self putContact];
    }
}

#pragma mark - misc
/*
 * Method Name	: populateContact
 * Description	: populate the value of the textView from the current Contact Object
 * Parameters	: id - sender
 * Return value	: IBAction
 */
-(void) populateContact
{
    self.eGordianAddContactsName.text = self.egContactDetail.contactDisplayName;
    self.eGordianAddContactsStreet.text = self.egContactDetail.contactAddress1;
    self.eGordianAddContactsCity.text = self.egContactDetail.contactCity;
    self.eGordianAddContactsState.text = self.egContactDetail.contactState;
    self.eGordianAddContactZipCode.text = self.egContactDetail.contactZipCode;
    self.eGordianAddContactEMail.text = self.egContactDetail.contactEMail;
    self.eGordianAddContactPhone.text = self.egContactDetail.contactBusinessPhone1;
    self.eGordianAddContactMobile.text = self.egContactDetail.contactMobilePhone;
}

#pragma mark - misc
/*
 * Method Name	: putContact
 * Description	: save the new contact values to the server
 * Parameters	: nil
 * Return value	: nil
 */
-(void) putContact
{
    
    [self actvityStart];
    
    NSString *updateContactUrl = [NSString stringWithFormat:@"%@/owners/%@/JobOrders/%@/Contacts/%@",APP_HEADER_URL,self.egContactDetail.contactOwnerID,self.strJobOrderID,self.egContactDetail.contactId];
    
    NSDictionary *dictToPass = @{@"Id":self.egContactDetail.contactId,
                                 @"DisplayName":self.eGordianAddContactsName.text.length?self.eGordianAddContactsName.text:@"",
                                 @"Email":self.eGordianAddContactEMail.text.length?self.eGordianAddContactEMail.text:@"",
                                 @"Address1":self.eGordianAddContactsStreet.text.length?self.eGordianAddContactsStreet.text:@"",
                                 @"City":self.eGordianAddContactsCity.text.length?self.eGordianAddContactsCity.text:@"",
                                 @"State":self.eGordianAddContactsState.text.length?self.eGordianAddContactsState.text:@"",
                                 @"ZipCode":self.eGordianAddContactZipCode.text.length?self.eGordianAddContactZipCode.text:@"",
                                 @"BusinessPhone1":self.eGordianAddContactPhone.text.length?self.eGordianAddContactPhone.text:@"",
                                 @"MobilePhone":self.eGordianAddContactMobile.text.length?self.eGordianAddContactMobile.text:@"",
                                 };
  
  
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictToPass
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        
    
    [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:updateContactUrl andserviceType:EGORDIAN_API_JOBORDER_CONTACT_UPDATE];
   
    
}

#pragma mark - Data Downloader Delegates
/*
 * Method Name	: serviceRequestFinished
 * Description	: on successful completion of the service request call
 * Parameters	: NSArray - responseArray APIKey - service type
 * Return value	: nil
 */
-(void) serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_JOBORDER_CONTACT_UPDATE){
//        NSArray *arrViewControllers = [self.navigationController viewControllers];
//        for (UIViewController *currVC in arrViewControllers) {
//            if([currVC isKindOfClass:[EGJobContactsViewController class]]){
//                [self.navigationController popToViewController:currVC animated:YES];
//                break;
//            }
//        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    [self actvityStop];
}

/*
 * Method Name	: serviceRequestFailed
 * Description	: on failure of the service request call
 * Parameters	: NSError - error APIKey - service type
 * Return value	: nil
 */
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    [self actvityStop];
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    NSLog(@"Error Message--%@",alertMessage);
}



@end
