//
//  eGordianJobPhotoViewController.m
//  eGordian-iOS
//
//  Created by Laila on 04/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "eGordianJobPhotoViewController.h"
#import "eGordianPhotoCollectionViewCell.h"
#import "EG_JobPhoto.h"
#import "EGContainerViewController.h"
#import "eGordianJobPhotoDetailViewController.h"
#import "UIImageView+WebCache.h"
#define PAGESIZE 25;

@interface eGordianJobPhotoViewController ()

@end

@implementation eGordianJobPhotoViewController

@synthesize eGJobPhotoCollectionView, eGSynchedPhotoArray, currentJob, egSelectBtn, egSelectedIndices, totalPhotoCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.eGJobPhotoCollectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    isSelectionMode = FALSE;
    [self.eGJobPhotoCollectionView setAllowsMultipleSelection:YES];
    self.egSelectedIndices = [[NSMutableArray alloc] init];
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.delegate = self;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    
    pgNo = 1;
    pgSize = PAGESIZE;
    previousPage = 0;
    
    [self fetchPicture];
}

-(void) viewWillAppear:(BOOL)animated
{
    
    
    if([EGordianUtilities getRefreshStatusForPage:APP_JOBPHOTO_REFRESH_FLAG])
    {
        pgNo = 1;
        pgSize = PAGESIZE;
        previousPage = 0;
        
        [self fetchPicture];
    }
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
//    for(int i=0; i<10; i++)
//    {
//        if(self.eGSynchedPhotoArray.count)
//            [self.eGSynchedPhotoArray removeLastObject];
//    }
//    
//    if(self.eGSynchedPhotoArray.count)
//        [self.eGJobPhotoCollectionView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDataSource
/*
 * Method Name	: numberOfSectionsInCollectionView
 * Description	: number of sections in the collectionview
 * Parameters	: UICollectionView
 * Return value	: NSInteger (number of sections)
 */
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
    
}

/*
 * Method Name	: numberOfItemsInSection
 * Description	: number of items in the section
 * Parameters	: UICollectionView, section
 * Return value	: NSInteger (number of items in section)
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   
    int synchedPhotoCount = 0;
    if(self.eGSynchedPhotoArray)
        synchedPhotoCount = (int)[self.eGSynchedPhotoArray count];
    return synchedPhotoCount;
}

/*
 * Method Name	: cellForItemAtIndexPath
 * Description	: create the cell at Indexpath
 * Parameters	: UICollectionView, IndexPath
 * Return value	: UICollectionViewCell
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const reuseIdentifier = @"eGordianPhotoCollectionViewCell";
    
    eGordianPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    
    cell.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    // Configure the cell
    
    EG_JobPhoto* photoObj = [self.eGSynchedPhotoArray objectAtIndex:indexPath.row ];
    cell.eGPhotoCollectionImageView.image = [UIImage imageWithData:photoObj.Thumbnail];
//    [cell.eGPhotoCollectionImageView sd_setImageWithURL:[NSURL URLWithString:photoObj.fullSizeLink]
//                                       placeholderImage:[UIImage imageWithData:photoObj.Thumbnail]];

    
    if(isSelectionMode)
    {
        cell.eGSelectImgVw.hidden = cell.selected ? NO : YES;
        
    }
    else
    {
        cell.eGSelectImgVw.hidden = YES;
    }
    
    return cell;
}


/*
 * Method Name	: didDeselectItemAtIndexPath
 * Description	: called when an item in the list is deselected
 * Parameters	: UICollectionView, IndexPath
 * Return value	: void
 */
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    eGordianPhotoCollectionViewCell *cell = (eGordianPhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    //select button is pressed
    if(isSelectionMode)
    {
        cell.eGSelectImgVw.hidden = YES;
        [cell setSelected:NO];
        if(self.egSelectedIndices)
        {
            if(self.egSelectedIndices.count)
                [self.egSelectedIndices removeObject:[NSNumber numberWithInt:(int)indexPath.row]];
        }
        
        if(![self.egSelectedIndices count])
        {
            EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
            [containerVC.deleteButton setEnabled:FALSE];
        }
    }
    
}

/*
 * Method Name	: didSelectItemAtIndexPath
 * Description	: called when an item in the list is selected
 * Parameters	: UICollectionView, IndexPath
 * Return value	: void
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    eGordianPhotoCollectionViewCell *cell = (eGordianPhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    if(isSelectionMode)
    {
        cell.eGSelectImgVw.hidden = NO;
        [cell setSelected:YES];
        if(!self.egSelectedIndices)
            self.egSelectedIndices = [[NSMutableArray alloc] init];
        [self.egSelectedIndices addObject:[NSNumber numberWithInt: (int)indexPath.row]];
        EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
        if(self.egSelectedIndices.count)
            [containerVC.deleteButton setEnabled:TRUE];
    }
    else
    {
        [[((UINavigationController *)self.parentViewController) viewControllers] objectAtIndex:0].hidesBottomBarWhenPushed = YES;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        eGordianJobPhotoDetailViewController *eGPhotoDetailVC = (eGordianJobPhotoDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGordianJobPhotoDetailViewController"];
        
        EG_JobPhoto* photo = [self.eGSynchedPhotoArray objectAtIndex:indexPath.row];
        eGPhotoDetailVC.currentJobOrder = self.currentJob;
        eGPhotoDetailVC.currentPhoto = photo;
        
        [self.navigationController pushViewController:eGPhotoDetailVC animated:YES];
    }
}


-(void) fetchPicture
{
    
    [self startAnimating];
    
    if(pgNo==1)
        self.eGSynchedPhotoArray = nil;
    
    NSString* photoURL = [NSString stringWithFormat:@"%@owners/%@/JobOrders/%@/pictures?pageSize=%d&pageNumber=%d", APP_HEADER_URL, self.currentJob.ownerID, self.currentJob.jobOrderId, pgSize, pgNo];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:photoURL serviceType:EGORDIAN_API_JOB_PHOTO_THUMBNAIL];
    
}

/*
 * Method Name	: selectStatusChange
 * Description	: on select status change Select/DeSelect
 * Parameters	: nil
 * Return value	: nil
 */
-(void) selectStatusChange
{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    
    if(isSelectionMode)
    {
        isSelectionMode = TRUE;
        [self.egSelectBtn setTitle:@"Deselect" forState:UIControlStateNormal];
        [containerVC updateFooterView:@"PhotoList"];
        
        if(self.egSelectedIndices.count)
            [containerVC.deleteButton setEnabled:TRUE];
        else
            [containerVC.deleteButton setEnabled:FALSE];
        
    }
    else
    {
        isSelectionMode = FALSE;
        [self.egSelectBtn setTitle:@"Select" forState:UIControlStateNormal];
        [containerVC resetFooterView];
    }
    
    if(self.egSelectedIndices.count)
        [containerVC.deleteButton setEnabled:TRUE];
    else
        [containerVC.deleteButton setEnabled:FALSE];
    
    [containerVC.addtoJobButton setHidden:TRUE];
    self.egSelectedIndices = nil;
    [self.eGJobPhotoCollectionView reloadData];
}

- (IBAction)onSelectBtn:(id)sender {
    isSelectionMode =! isSelectionMode;
    
    [self selectStatusChange];
    
    [self.eGJobPhotoCollectionView reloadData];
}

- (IBAction)onBack:(id)sender {
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    [containerVC resetFooterView];
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) deletePhoto
{
    if(!self.egSelectedIndices) return;
    if(!self.egSelectedIndices.count)
        return;
    
    [self startAnimating];
    //    NSDictionary* jsonDict = nil;
   
    //EARLIER IMPLIMENTATION WHEN JOBID WAS PASSED IN BODY
    /*
    NSMutableArray* jsonDictArr = [[NSMutableArray alloc] init];
    
    NSString *deletePhotoURL = [NSString stringWithFormat:@"%@owners/%@/JobOrders/%@/pictures", APP_HEADER_URL, currentJob.ownerID, currentJob.jobOrderId];
    
    if([self.egSelectedIndices count])//multiple synched photos delete
    {
        
        for (NSNumber *number in self.egSelectedIndices) {
            
            EG_JobPhoto* jobPhoto = ((EG_JobPhoto*) [self.eGSynchedPhotoArray objectAtIndex:[number intValue] ]);
            
            NSString *photoID= @"";
            
            if([jobPhoto.DownloadLink rangeOfString:@"Pictures/"].location != NSNotFound){
                NSString *originalString = jobPhoto.DownloadLink;
                NSRange end = [originalString rangeOfString:@"Pictures/"];
                photoID = [originalString substringFromIndex:(end.location+@"Pictures/".length)];
                end = [photoID rangeOfString:@"/"];
                photoID = [photoID substringToIndex:end.location];
            }
            NSDictionary *dictToPass =@{@"id":[NSNumber numberWithInt:[photoID intValue]]};
            //jsonDict = @{@"id" : photoID};
            
            [jsonDictArr addObject:dictToPass];
        }
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictArr
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        
        [[DataDownloader sharedDownloader] deleteData:jsonDict andServiceURl:deletePhotoURL andserviceType:EGORDIAN_API_JOB_PHOTO_DELETE];
        
    }
    */
    
    deleteCount = (int)self.egSelectedIndices.count;
    
    [self startAnimating];
    
    for (NSNumber *number in self.egSelectedIndices) {
        EG_JobPhoto* jobPhoto = ((EG_JobPhoto*) [self.eGSynchedPhotoArray objectAtIndex:[number intValue] ]);
        
        NSString *photoID= @"";
        
        if([jobPhoto.DownloadLink rangeOfString:@"Pictures/"].location != NSNotFound){
            NSString *originalString = jobPhoto.DownloadLink;
            NSRange end = [originalString rangeOfString:@"Pictures/"];
            photoID = [originalString substringFromIndex:(end.location+@"Pictures/".length)];
            end = [photoID rangeOfString:@"/"];
            photoID = [photoID substringToIndex:end.location];
        }
        
        NSString *deleteJobPhotoURL = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/Pictures/%@", APP_HEADER_URL, currentJob.ownerID, currentJob.jobOrderId, photoID];
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        [[DataDownloader sharedDownloader]getResponseFormUrl:deleteJobPhotoURL serviceType:EGORDIAN_API_JOB_PHOTO_DELETE];
    }
    
}

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    switch (type) {
        case EGORDIAN_API_JOB_PHOTO_THUMBNAIL:
        {
            if(pgNo==1)
                self.eGSynchedPhotoArray = nil;
            
            if(!self.eGSynchedPhotoArray)
                self.eGSynchedPhotoArray = [[NSMutableArray alloc] initWithArray:responseArray];
            else
                [self.eGSynchedPhotoArray addObjectsFromArray:responseArray];
            [self.eGJobPhotoCollectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            self.egSelectedIndices = nil;
            [self stopAnimating];
            [EGordianUtilities updateRefreshStatus:FALSE forPage:APP_JOBPHOTO_REFRESH_FLAG];
        }
            break;
            
        case EGORDIAN_API_JOB_PHOTO_DELETE:
        {
            //this needs to be done as photos are deleted one at a time in a loop
            deleteCount--;
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_JOBPHOTO_REFRESH_FLAG];
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_JOBDETAIL_REFRESH_FLAG];
            if(deleteCount == 0)
            {
                isSelectionMode = FALSE;
                [self selectStatusChange];
                [self fetchPicture];
                [self stopAnimating];
                self.egSelectedIndices = nil;
            }
        }
            break;
            
        default:
            break;
    }
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type
{
    isSelectionMode = FALSE;
    [self selectStatusChange];
    [self stopAnimating];
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message---%@",alertMessage);
    
    //this needs to be done as photos are deleted one at a time in a loop
    switch (type) {
        case EGORDIAN_API_JOB_PHOTO_DELETE:
        {
            
            deleteCount--;
            
            if(deleteCount == 0)
            {
                isSelectionMode = FALSE;
                [self selectStatusChange];
                [self fetchPicture];
                [self stopAnimating];
                self.egSelectedIndices = nil;
            }
        }
            break;
            
        default:
            break;
    }
    
}

- (void)serviceRequestFinishedWithError:(NSString*)error serviceType:(APIKey)type
{
    isSelectionMode = FALSE;
    [self selectStatusChange];
    [self stopAnimating];
    
    NSString *alertMessage = @"";
    if(error.length){
        alertMessage = error;
    }
    
    NSLog(@"Error Message---%@",alertMessage);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.eGSynchedPhotoArray.count>=totalPhotoCount)
    {
        return;
    }
    
    CGFloat bottomInset = scrollView.contentInset.bottom;
    CGFloat bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height - bottomInset;
    if (bottomEdge == scrollView.contentSize.height) {
        // Scroll view is scrolled to bottom
        previousPage = pgNo;
        pgNo++;
        [self performSelector:@selector(fetchPicture) withObject:nil afterDelay:1];
    }
    
}

-(void) startAnimating
{
    [self.egLoaderView setHidden:NO];
    [self.view bringSubviewToFront:self.egLoaderView];

}

-(void) stopAnimating
{
    [self.egLoaderView setHidden: TRUE];

}
@end
