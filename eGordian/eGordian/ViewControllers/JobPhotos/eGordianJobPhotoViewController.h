//
//  eGordianJobPhotoViewController.h
//  eGordian-iOS
//
//  Created by Laila on 04/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrder.h"
#import "EGcontainerViewController.h"

@interface eGordianJobPhotoViewController : UIViewController<egordianContainerDelegate>
{
    BOOL isSelectionMode;
    int pgNo;
    int pgSize;
    int previousPage;
    int deleteCount;
    
}

@property (weak, nonatomic) IBOutlet UIButton *egSelectBtn;
@property (weak, nonatomic) IBOutlet UICollectionView *eGJobPhotoCollectionView;
@property (nonatomic, strong) NSArray *egJobPhotoAssets;
@property (nonatomic, strong) NSMutableArray* eGSynchedPhotoArray;
//@property (nonatomic, strong) NSMutableArray* assets;
@property (nonatomic, strong) EG_JobOrder* currentJob;
@property (strong, nonatomic) NSMutableArray* egSelectedIndices;//contains the index of the photos which are selected
@property (weak, nonatomic) IBOutlet UIView *egLoaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *jobPhotoActivityIndicator;
@property (nonatomic, assign) NSInteger totalPhotoCount;

- (IBAction)onSelectBtn:(id)sender;
- (IBAction)onBack:(id)sender;

@end
