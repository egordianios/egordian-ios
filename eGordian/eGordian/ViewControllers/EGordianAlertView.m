//
//  EGordianAlertView.m
//  eGordian-iOS
//
//  Created by Laila on 12/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EGordianAlertView.h"

static EGordianAlertView *singletonObj = nil;

@implementation EGordianAlertView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(EGordianAlertView *)getSingletonInstance
{
    @synchronized(self){
        if (singletonObj == nil)
        {
            singletonObj = [[self alloc] init];
            
            singletonObj.cancelButtonIndex=0;
            singletonObj.title = @"eGordian";
//            [[NSNotificationCenter defaultCenter] addObserver:singletonObj
//                                                     selector:@selector(updateLanguage)
//                                                         name:LanguageChanged
//                                                       object:nil];
        }
        return singletonObj;
    }
}

+ (void)alertWithMessage : (NSString *)message withDelegate:(id)delegate
{
    if(message && [message length]>0)
    {
        if(!singletonObj)
        {
            [self getSingletonInstance];
        }
        singletonObj.message=message;
        singletonObj.delegate=delegate;
        
        [singletonObj show];
    }
}

//+ (void)photoDeleteMessage:(id)delegate
//{
//    if(!singletonObj)
//    {
//        [self getSingletonInstance];
//    }
//    singletonObj.message=@"Please confirm that you wish to Delete these items from the eGordian.";
//    singletonObj.delegate=delegate;
//    singletonObj.title = @"Are You Sure?";
//    
//    [singletonObj setCancelButtonIndex:[singletonObj addButtonWithTitle:@"Cancel Deletion"]];
//    [singletonObj addButtonWithTitle:@"Delete Photo"];
//    [singletonObj show];
//    
//}

+ (void)alertWithMessage : (NSString *)message
{
    if(message && [message length]>0)
    {
        if(!singletonObj)
        {
            [self getSingletonInstance];
        }
        singletonObj.message=message;
        singletonObj.delegate=nil;
        
        [singletonObj buttonTitleAtIndex:0];
        [singletonObj show];
    }
    
}

+ (void)showFeatureNotAvailableMessage
{
        if(!singletonObj)
        {
            [self getSingletonInstance];
        }
        [singletonObj addButtonWithTitle:@"OK"];
        singletonObj.message=@"This feature is not supported offline.";
        singletonObj.delegate=nil;
        
        [singletonObj buttonTitleAtIndex:0];
        [singletonObj show];
}

+ (void)showNetworkFailureMessage
{
    if(!singletonObj)
    {
        [self getSingletonInstance];
    }
    [singletonObj addButtonWithTitle:@"OK"];
    singletonObj.message=@"Unable to reach the server. Please try again later.";
    singletonObj.delegate=nil;
    
    [singletonObj buttonTitleAtIndex:0];
    [singletonObj show];
}

@end
