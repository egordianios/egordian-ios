//
//  EGScopeOfWorkDetailViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/31/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrderDetail.h"

@interface EGScopeOfWorkDetailViewController : UIViewController

@property (strong, nonatomic)  UILabel *jobClientNameLabel;
@property (strong, nonatomic)  UILabel *jobTitleLabel;
@property (strong, nonatomic)  UILabel *jobIdLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailedScopeTextView;
@property (nonatomic, retain) EG_JobOrderDetail *currentJobOrder;
@property (nonatomic, retain) NSAttributedString *scopeOfWorkString;
@property (nonatomic, retain) NSString *strScopeOfWorkUrl;
- (IBAction)scopeOfWorkBackButtonTapped:(id)sender;
- (IBAction)scopeOfWorkEditButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *jobOrderTitleInfoView;
@property (weak, nonatomic) IBOutlet UIView *scopeOfWorkView;

@end
