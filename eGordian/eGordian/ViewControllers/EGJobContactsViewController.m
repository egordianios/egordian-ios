//
//  EGJobContactsViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/21/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EGJobContactsViewController.h"
#import "JobOrderContactCell.h"
#import "EG_JobConatcts.h"
#import "eGordianContactsCell.h"
#import "eGordianContact.h"
#import "EGJobConatctAddARoleViewController.h"
#import "eGordianViewContactsViewController.h"
#import "EGJobContactsAssignViewController.h"

#define searchKeyFirstName @"FirstName"
#define searchKeyLastName @"LastName"
#define searchKeyCompany @"CompanyName"
#define searchKeyDisplayName @"DisplayName"
#define PAGESIZE 200

@interface EGJobContactsViewController ()
{
    NSMutableArray *contactLoadArray;
    NSMutableArray *contactAssignLoadArray;
    int currentPageIndex;
    BOOL isAppendData;
    int pgSize;
    NSString *jobOrderID;
    
    UILabel *jobClientNameLabel;
    UILabel *jobTitleLabel;
    UILabel *jobIdLabel;

}
@property (strong, nonatomic) NSMutableDictionary* sectionsDictionary;
@property (nonatomic) BOOL loading;
@property (strong, nonatomic) NSArray *filteredContacts;
@end

@implementation EGJobContactsViewController

@synthesize currentJobOrder,strRolesURL,strJobOwnerId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setIntialUI];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    
    [self fetchJobConatcts:[NSString stringWithFormat:@"%@",currentJobOrder.contactsLink]];
}

-(void)fetchJobConatcts:(NSString *)contatsURL
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
    
    if([contatsURL rangeOfString:@"Owners"].location != NSNotFound){
        
        NSString *originalString = contatsURL;
        
        NSRange start = [originalString rangeOfString:@"JobOrders/"];
        
        NSString *jobId = [originalString substringFromIndex:start.location+start.length];
        
        NSRange end = [jobId rangeOfString:@"/"];
        
        jobId =[jobId substringToIndex:end.location];
        
        jobOrderID = jobId;
        
        NSString *statusURL = [NSString stringWithFormat:@"%@Roles",[originalString substringToIndex:start.location]];
        
        self.strRolesURL = statusURL;
    }
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:contatsURL serviceType:EGORDIAN_API_JOBORDER_CONTACTS];
    
    
}

-(void)fetchGlobalConacts{
    
    [self.loaderView setHidden:NO];
    
    [self.view bringSubviewToFront:self.loaderView];
    
    [self.activityLoader startAnimating];
    
    NSString* userID = [AppConfig getUserId];
    
    NSString *contactsString = [NSString stringWithFormat:@"%@users/%@/Contacts??sortBy=LastName&pageSize=%d&pageNumber=%d", APP_HEADER_URL, userID, PAGESIZE, currentPageIndex];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:contactsString serviceType:EGORDIAN_API_GLOBAL_CONTATCS];
    
}

-(void)setJobOrderInfoUI{
    
    CGFloat padding = 15.0f;
        
    CGFloat labelWidth = self.view.frame.size.width - (2*padding);
    
    CGFloat dy = padding;
    
    CGFloat defaultLabelHeight = 20.0f;
    
    NSString *jobTitleText = [NSString stringWithFormat:@"%@",currentJobOrder.jobTitle];
    
    NSString *jobClientName = [NSString stringWithFormat:@"%@",currentJobOrder.clientName];
    
    NSString *jobOrderId = [NSString stringWithFormat:@"ID:%@",currentJobOrder.jobOrderNumber];
    
    [self.jobOrderTitleInfoView  setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if(jobTitleText.length > 0){
        
        jobTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobTitleLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:15.0]];
        
        [jobTitleLabel setNumberOfLines:0];
        
        [jobTitleLabel setTextColor:[EGordianUtilities labelTitleColor]];

        [jobTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobTitleLabel setText:jobTitleText];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobTitleLabel WithText:jobTitleLabel.text];
        
        [jobTitleLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobTitleLabel];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(jobClientName.length > 0){
     
        jobClientNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobClientNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobClientNameLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobClientNameLabel setNumberOfLines:0];
        
        [jobClientNameLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobClientNameLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobClientNameLabel setText:jobClientName];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobClientNameLabel WithText:jobClientNameLabel.text];
        
        [jobClientNameLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobClientNameLabel];
        
        dy = dy+dynamicTextHeight+(jobOrderId.length?3.0:padding);
    }
    
    if(jobOrderId.length > 0){
        
        jobIdLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobIdLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobIdLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobIdLabel setNumberOfLines:0];
        
        [jobIdLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobIdLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobIdLabel setText:jobOrderId];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobIdLabel WithText:jobIdLabel.text];
        
        [jobIdLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobIdLabel];
        
        dy = dy+dynamicTextHeight+padding;
    }
    
     
    [self.jobOrderTitleInfoView setFrame:CGRectMake(0, 0, self.view.frame.size.width, dy)];
    [self.JobContactsListView setFrame:CGRectMake(0, self.jobOrderTitleInfoView.frame.origin.y+self.jobOrderTitleInfoView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-62.0f-self.jobOrderTitleInfoView.frame.size.height - 62.0f)];
    
}

-(void)setIntialUI{
    
    self.jobContactsTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [self.assignPersonView setFrame:CGRectMake(self.view.frame.size.width, 0.0f, self.assignPersonView.frame.size.width, self.assignPersonView.frame.size.height)];
    
    pgSize = PAGESIZE;
    
    isAppendData = FALSE;
    
    self.loading = FALSE;
    
    self.callView.hidden = YES;
    
    contactLoadArray = [[NSMutableArray alloc]init];
    
    contactAssignLoadArray = [[NSMutableArray alloc]init];
    
    self.assignContactsTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [self.loaderView setHidden:YES];

    self.countLabel.text                = @"";
    
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    
    [self setJobOrderInfoUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view delegate

/*
 * Method Name	: sectionIndexTitlesForTableView
 * Description	: return list of section titles to display in section index view (e.g. "ABCD...Z#")
 * Parameters	: UITableView
 * Return value	: NSArray
 */
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{

    return [[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 0.0;
    
    if (IS_IPHONE_6_PLUS) {
        height =80.0;
    }
    else{
        height = 58.0;
    }
    return height;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(tableView == self.assignContactsTableView)
        return 30;
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    
    if(tableView == self.jobContactsTableView){
        return 1;
    }
    else{
        return [[self.sectionsDictionary allKeys] count];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    NSInteger rowCount = 0;
    
    if(tableView == self.assignContactsTableView){
        rowCount = [[self.sectionsDictionary valueForKey:[[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
    }
    else if(tableView == self.jobContactsTableView){
        rowCount = contactLoadArray.count;
    }
    return rowCount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView == self.assignContactsTableView)
    {
        return [[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    }
    
    return nil;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView == self.assignContactsTableView){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor colorWithRed:156.0/255.0 green:182.0/255.0 blue:197.0/255.0 alpha:1.0]];
        //[headerView setBackgroundColor:[UIColor lightGrayColor]];
        
        UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,5,tableView.bounds.size.width-30,20)];
        tempLabel.backgroundColor=[UIColor clearColor];
        tempLabel.textColor = [UIColor blackColor];
        tempLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:15];
        NSString *headerTitle = @"";
        
        headerTitle = [[[self.sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        
        tempLabel.text=headerTitle;//@"Most Recent";
        [headerView addSubview:tempLabel];
        
        return headerView;
    }
    else{
        return nil;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id cellToReturn=nil;
    
    if(tableView == self.jobContactsTableView){
        
        JobOrderContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobOrderContactCell"];
        
        if (!cell)
        {
            cell = [[JobOrderContactCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JobOrderContactCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        EG_JobConatcts *contatcDict = (EG_JobConatcts*)[contactLoadArray objectAtIndex:indexPath.row];
        
        cell.contactNameLabel.text = contatcDict.contactDisplayName;
        
        cell.contactRoleLabel.text = contatcDict.contactRole;
        
        cell.callButton.tag = indexPath.row;
        
        [cell.callButton addTarget:self action:@selector(callButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        cellToReturn = cell;
    }
    
    return cellToReturn;
}

-(void)callButtonTapped:(UIButton *)sender{
    
    self.displayNameLabel.text = @"";
    self.phoneNumberLabel.text = @"";
    
    UIButton *call = (UIButton *)sender;
    
    NSLog(@"Call Button Index--%ld",(long)call.tag);
    
    EG_JobConatcts *contactDict = (EG_JobConatcts*)[contactLoadArray objectAtIndex:call.tag];
    
    self.displayNameLabel.text = contactDict.contactDisplayName;
    
    if(contactDict.contactBusinessPhone1.length)
        self.phoneNumberLabel.text = contactDict.contactBusinessPhone1;
    else if (contactDict.contactBusinessPhone2.length)
        self.phoneNumberLabel.text = contactDict.contactBusinessPhone2;
    else if(contactDict.contactBusinessMobile.length)
        self.phoneNumberLabel.text = contactDict.contactBusinessMobile;
    
    if(self.phoneNumberLabel.text.length > 0){
        [self.callView setHidden:NO];
        [self.view bringSubviewToFront:self.callView];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                        message:@"No phone numbers were set for this contact"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(tableView == self.jobContactsTableView){
        
        EG_JobConatcts *contatcDict = (EG_JobConatcts*)[contactLoadArray objectAtIndex:indexPath.row];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
        
        eGordianViewContactsViewController* contactViewController = (eGordianViewContactsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianViewContactsViewController"];
        
        contactViewController.jobcontactURl = contatcDict.contactLink;
        
        contactViewController.redirectVia = @"JobContacts";
        
        contactViewController.jobcontactCompanyName = contatcDict.companyName;
        
        contactViewController.jobOrderId    = jobOrderID;
        
        [self.navigationController pushViewController:contactViewController animated:YES];
        
    }
}

- (IBAction)jobConatctsBackButtonTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)assignPersonTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EGJobContactsAssignViewController* assignPersonVC = (EGJobContactsAssignViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGJobContactsAssignViewController"];
    
    assignPersonVC.rolesURl = self.strRolesURL;
    
    assignPersonVC.jobOrderID = jobOrderID;
    
    assignPersonVC.jobOrderOwnerId = self.strJobOwnerId;
    
    [self.navigationController pushViewController:assignPersonVC animated:YES];
    
}


#pragma API Manager Delegates

#pragma mark Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_JOBORDER_CONTACTS) {
        
        contactLoadArray = [responseArray copy];
        
        self.countLabel.text       = [NSString stringWithFormat:@"%ld Contacts",(long)responseArray.count];
        
        [self.jobContactsTableView reloadData];
    
    }
    else if(type == EGORDIAN_API_GLOBAL_CONTATCS){
        
        NSArray *formattedArray = [eGordianContact getContactArray:responseArray];
        
        if(isAppendData)
        {
            [contactAssignLoadArray addObjectsFromArray:formattedArray];
        }
        else{
            [contactAssignLoadArray removeAllObjects];
            contactAssignLoadArray = [formattedArray mutableCopy];
            [self.assignContactsTableView setContentOffset:CGPointMake(0, 0)];
        }
        
        if (responseArray.count > 0) {
            self.loading = FALSE;
        }
        else{
            self.loading = TRUE;
        }
        
        [self sortArrayAlaphabetically:[contactAssignLoadArray copy]];
        
        isAppendData = false;
    }
    
    [self.activityLoader stopAnimating];
    
    [self.loaderView setHidden:YES];
    
    self.loading = TRUE;

}
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
    
    NSLog(@"Error Message--%@",alertMessage);
}

-(void)sortArrayAlaphabetically:(NSArray *)rawArray{
    
    if(![rawArray count]){
        return;
    }
    
    self.sectionsDictionary = nil;
    
    self.sectionsDictionary = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    for (eGordianContact *contact in rawArray)
    {
        NSString *c = nil;
        
        if(![contact.LastName length])
            continue;
        
        c = [[contact.LastName uppercaseString] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [self.sectionsDictionary allKeys])
        {
            if ([str caseInsensitiveCompare:c] == NSOrderedSame)
            {
                found = YES;
            }
        }
        if (!found)
        {
            [self.sectionsDictionary setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    for (eGordianContact *contact in rawArray)
    {
        if(![contact.LastName length])
            continue;
        [[self.sectionsDictionary objectForKey:[contact.LastName substringToIndex:1] ] addObject:contact];
    }
    
    // Sort each section array
    for (NSString *key in [self.sectionsDictionary allKeys])
    {
        [[self.sectionsDictionary objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:searchKeyLastName ascending:YES]]];
    }
    
    [self.assignContactsTableView reloadData];
    
    [self.activityLoader stopAnimating];
    
    [self.loaderView setHidden:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (!self.loading) {
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        if (endScrolling >= scrollView.contentSize.height)
        {
            [self performSelector:@selector(loadDataDelayed) withObject:nil afterDelay:0.2];
        }
    }
}
-(void)loadDataDelayed{
    
    currentPageIndex++;
    
    isAppendData = true;
    
    [self fetchGlobalConacts];
}

#pragma mark - TextField Delegate
- (BOOL) textFieldShouldEndEditing:(UITextField *)textField
{
    [self.contactSearchField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.contactSearchField resignFirstResponder];
    
    return YES;
}



- (IBAction)onSearchTextChanged:(id)sender {
    
    if(![contactAssignLoadArray count])
        return;
    
    if([self.contactSearchField.text length])
    {
        
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"Name CONTAINS[c] %@", self.contactSearchField.text];
        
        if(self.filteredContacts)
            self.filteredContacts = Nil;
        
        self.filteredContacts = [[NSArray alloc] initWithArray:[contactAssignLoadArray filteredArrayUsingPredicate:filter]];
        
        [self sortArrayAlaphabetically:self.filteredContacts];
    }
    else
    {
        [self sortArrayAlaphabetically:contactAssignLoadArray];
    }
}

- (IBAction)callButtonTappedToRedirectDailer:(id)sender {
    
    NSString *cleanedString = [[self.phoneNumberLabel.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }

    
    [self.callView setHidden:YES];
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self.callView setHidden:YES];
}

@end
