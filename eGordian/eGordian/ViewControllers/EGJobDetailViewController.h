//
//  EGJobDetailViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/3/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrder.h"

@interface EGJobDetailViewController : UIViewController<DataDownloaderDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>{
    IBOutlet UIButton *statusButton;
    IBOutlet UIView *actualPopOverView;
    int notesCount;
    int notesPageNumber;
    int notePrevPageNumber;
    BOOL notesFetched;//flag to set notesfetch to done if already fetched ... notesfetch was  happening twice
    int photosCount;
}
@property (weak, nonatomic) IBOutlet UILabel *titleViewSeperator;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@property (weak, nonatomic) IBOutlet UITableView *pickerTable;
@property (weak, nonatomic) IBOutlet UITableView *trackingDatesTable;
@property (weak, nonatomic) IBOutlet UITableView *locationTable;
@property (weak, nonatomic) IBOutlet UILabel *jobOrderAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *jobClientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobStatusLabel;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIScrollView *jobDetailScrollView;
@property (weak, nonatomic) IBOutlet UIView *jobTitleView;
- (IBAction)statusButtonTapped:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *btnDocuments;
@property (weak, nonatomic) IBOutlet UIButton *btnPhotos;
@property (weak, nonatomic) IBOutlet UIButton *btnContacts;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UILabel *documentsLabelCount;
@property (weak, nonatomic) IBOutlet UILabel *photosLabelcount;
@property (weak, nonatomic) IBOutlet UILabel *contactsLabelCount;


@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UIView *trackDatesView;
@property (assign, nonatomic) CGRect mySensitiveRect;
@property (nonatomic, retain) EG_JobOrder *currentJobOrder;

- (IBAction)jobDetailBackButtonTapped:(id)sender;
- (IBAction)trackingDatesButtonTapped:(id)sender;
- (IBAction)documentsButtonTapped:(id)sender;
- (IBAction)jobContactsButtonTapped:(id)sender;
- (IBAction)onJobPhotos:(id)sender;
- (IBAction)addNoteTapped:(id)sender;


//Notes
@property (weak, nonatomic) IBOutlet UITableView *eGJobNotesTableView;
@property (strong, nonatomic) NSMutableArray* egNotesArray;
@property (weak, nonatomic) IBOutlet UIView *egNotesView;
@property (weak, nonatomic) IBOutlet UIView *egNotesHeaderView;
@property (strong, nonatomic) NSString* egNotesDesc;

@end
