//
//  eGordianViewContactsViewController.m
//  eGordian-iOS
//
//  Created by Laila on 07/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianViewContactsViewController.h"
#import "eGViewContactAddressCell.h"
#import "egViewContactOtherCell.h"
#import "AFNetworking.h"
#import "eGordianAddContactsViewController.h"
#import "eGMapViewController.h"
#import "eGordianTelephoneViewController.h"


@interface eGordianViewContactsViewController ()

@end

#define addressMacro @"A_Address1"
#define eMailMacro @"B_Email:"
#define bPhoneMacro1 @"C_Business Phone:"
//#define bPhoneMacro2 @"D_Business Phone2:"
#define mPhoneMacro @"E_Mobile:"


@implementation eGordianViewContactsViewController

@synthesize eGContactDetail, spinner, dataDict,jobcontactURl,jobcontactCompanyName,jobOrderId,redirectVia,contactDetailURL;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.eGordianViewContactsTableView setEditing:NO];
    
    self.eGordianViewContactsTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    self.dataDict = nil;
    
    self.callView.hidden = YES;
    
    if([self.redirectVia isEqualToString:@"GlobalContacts"]){
        self.btnEdit.hidden = YES;
    }
    else{
        self.btnEdit.hidden = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(self.jobcontactURl.length > 0){
        [self fetchJobContactDetialInformation];
    }
    else{
        [self getContact];
    }
}

-(void)fetchJobContactDetialInformation{
    
    if(self.jobcontactURl.length > 0){
        
        [self startAnimating];
        
        [[DataDownloader sharedDownloader] setDataDownloaderDelegate:(id)self];
        
        [[DataDownloader sharedDownloader] getResponseFormUrl:self.jobcontactURl serviceType:EGORDIAN_API_CONTACTS_DETAIL];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Table view data source
/*
 * Method Name	: numberOfSectionsInTableView
 * Description	: returns the number of sections in tableview
 * Parameters	: UITableView
 * Return value	: NSInteger (number of sections in tableview)
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/*
 * Method Name	: numberOfRowsInSection
 * Description	: returns the number of rows in a section
 * Parameters	: UITableView, NSInteger
 * Return value	: NSInteger (number of cells in the section)
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //dataDict contains email, bPHone, mPhone details. If any one of them is not present, no need to show that row.
   
    return [[self.dataDict allKeys] count];
}

/*
 * Method Name	: heightForRowAtIndexPath
 * Description	: variable height support
 * Parameters	: UITableView, NSIndexPath
 * Return value	: CGFloat
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0 && [[self.dataDict allKeys]containsObject:addressMacro])
        return 80;
    else
        return 65;
}

/*
 * Method Name	: cellForRowAtIndexPath
 * Description	: Customize the appearance of table view cells.
 * Parameters	: UITableView, NSIndexPath
 * Return value	: UITableViewCell
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
  
    if (indexPath.row == 0 && [[self.dataDict allKeys]containsObject:addressMacro])
    {
        static NSString *CellIdentifier = @"eGViewContactAddressCell";
        
        eGViewContactAddressCell *egContactAddressCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!egContactAddressCell)
        {
            egContactAddressCell = [[eGViewContactAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            egContactAddressCell.shouldIndentWhileEditing = NO;
        }

        egContactAddressCell.contactsViewAddressTxtfld.text = self.eGContactDetail.contactAddress1?self.eGContactDetail.contactAddress1:@"";
       
        egContactAddressCell.contactsViewCityTxtfld.text = [NSString stringWithFormat:@"%@ %@ %@", [self.eGContactDetail.contactCity length]?[NSString stringWithFormat:@"%@,",self.eGContactDetail.contactCity]:@"", [self.eGContactDetail.contactState length]?[NSString stringWithFormat:@"%@,",self.eGContactDetail.contactState]:@"", [self.eGContactDetail.contactZipCode length]?self.eGContactDetail.contactZipCode:@""];

        return egContactAddressCell;
    }
    else
    {
        static NSString *CellIdentifier = @"eGViewContactOtherCell";
        eGViewContactOtherCell *egViewOtherCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!egViewOtherCell)
        {
            egViewOtherCell = [[eGViewContactOtherCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            egViewOtherCell.shouldIndentWhileEditing = NO;
        }

        
        NSMutableArray *sortedKeys = [[[self.dataDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
        
        NSInteger currentIndex = 0;
        
        if([[self.dataDict allKeys]containsObject:addressMacro]){
            [sortedKeys removeObjectAtIndex:[sortedKeys indexOfObject:addressMacro]];
            currentIndex = indexPath.row-1;
        }
        else{
            currentIndex = indexPath.row;
        }
        
        NSString *headerLabelText = [sortedKeys objectAtIndex:currentIndex];
        
        NSString *headerLabelText1 = [headerLabelText substringWithRange:NSMakeRange(2, [headerLabelText length]-2)];
        
        egViewOtherCell.GViewContactOtherHeaderLbl.text = headerLabelText1;
        
        egViewOtherCell.eG_ViewContact_OtherTxtfld.text = [self.dataDict valueForKey:headerLabelText];//[[self.dataDict allValues] objectAtIndex:indexPath.row];
        
        
//        egViewOtherCell.GViewContactOtherHeaderLbl.text = [[self.dataDict allKeys] objectAtIndex:indexPath.row];
        if([[sortedKeys objectAtIndex:currentIndex] isEqualToString:eMailMacro])
        {
            [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:nil forState:UIControlStateNormal];
            [egViewOtherCell.eG_ViewContact_OtherBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:[UIImage imageNamed:@"eG_ContactView_EMail"] forState:UIControlStateNormal];
            [egViewOtherCell.eG_ViewContact_OtherBtn addTarget:self action:@selector(onEmail:) forControlEvents: UIControlEventTouchUpInside];
        }
        else
        {
            [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:nil forState:UIControlStateNormal];
            [egViewOtherCell.eG_ViewContact_OtherBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:[UIImage imageNamed:@"eG_Contacts_Phone"] forState:UIControlStateNormal];
            [egViewOtherCell.eG_ViewContact_OtherBtn addTarget:self action:@selector(onPhone:) forControlEvents: UIControlEventTouchUpInside];
            egViewOtherCell.eG_ViewContact_OtherBtn.tag = indexPath.row;
        }
        
        return egViewOtherCell;
    }
}

/*
 * Method Name	: canEditRowAtIndexPath
 * Description	: if the rows are editable
 * Parameters	: tableView, indexPath
 * Return value	: BOOL - is editable or not
 */
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

/*
 * Method Name	: shouldIndentWhileEditingRowAtIndexPath
 * Description	: must move the contects to the right or not
 * Parameters	: tableView, indexPath
 * Return value	: BOOL - 
 */
- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

/*
 * Method Name	: canEditRowAtIndexPath
 * Description	: Allows customization of the editingStyle for a particular cell located at 'indexPath'. If not implemented, all editable cells will have UITableViewCellEditingStyleDelete set for them when the table has editing property set to YES.
 * Parameters	: tableView, indexPath
 * Return value	: BOOL - is editable or not
 */
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
    for ( UIView* subView in cell.contentView.subviews)
    {
        if([subView isKindOfClass:[UITextField class]])
        {
            UITextField* textField = (UITextField*)subView;
            textField.userInteractionEnabled = YES;
            textField.enabled = YES;
            textField.backgroundColor = [UIColor colorWithRed:.95 green:.95 blue:.95 alpha:1.0];
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }
    }
    return UITableViewCellEditingStyleNone;
}

#pragma mark -
#pragma mark Button Handlers
/*
 * Method Name	: onViewContactsBackBtn
 * Description	: on clicking the back button in ContactsView
 * Parameters	: ID
 * Return value	: IBAction
 */
- (IBAction)onViewContactsBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Method Name	: eGordianViewContactsAddressBtn
 * Description	: on clicking the Address Button
 * Parameters	: ID
 * Return value	: IBAction
 */
- (IBAction)eGordianViewContactsAddressBtn:(id)sender {
    
    if(self.eGContactDetail.contactZipCode.length || self.eGContactDetail.contactCity.length || self.eGContactDetail.contactAddress1.length){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
       
        eGMapViewController *mapsVC = (eGMapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGMapViewController"];
        
        NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] init];
        
        [addressDict setObject:[NSString stringWithFormat:@"%@", [self.eGContactDetail.contactDisplayName length]?[NSString stringWithFormat:@"%@",self.eGContactDetail.contactDisplayName]:@""] forKey:@"Name"] ;

        [addressDict setObject:[NSString stringWithFormat:@"%@", [self.eGContactDetail.contactAddress1 length]?[NSString stringWithFormat:@"%@",self.eGContactDetail.contactAddress1]:@""] forKey:@"Address"] ;
        
        [addressDict setObject:[NSString stringWithFormat:@"%@%@%@",self.eGContactDetail.contactCity.length?[self.eGContactDetail.contactCity stringByAppendingString:@","]:@"",self.eGContactDetail.contactState.length?[self.eGContactDetail.contactState stringByAppendingString:@","]:@"",self.eGContactDetail.contactZipCode.length?self.eGContactDetail.contactZipCode:@""] forKey:@"A_Address1"] ;
        
        mapsVC.addressDict = addressDict;
        
        [self.navigationController pushViewController:mapsVC animated:NO];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                        message:@"Address not available for this contact"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

/*
 * Method Name	: onEditBtn
 * Description	: on clicking the Edit Button
 * Parameters	: ID
 * Return value	: IBAction
 */
- (IBAction)onEditBtn:(id)sender
{
    if(self.eGordianViewContactsTableView.isEditing)
    {
        [self.eGordianViewContactsTableView setEditing:NO animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
        eGordianAddContactsViewController *addContactsVC = (eGordianAddContactsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGordianAddContactsViewController"];
        addContactsVC.isEditing = TRUE;
        addContactsVC.egContactDetail = self.eGContactDetail;
        addContactsVC.strJobOrderID = self.jobOrderId;
        [self.navigationController pushViewController:addContactsVC animated:NO];
    }
}

/*
 * Method Name	: onEmail
 * Description	: on eMail button click
 * Parameters	: ID
 * Return value	: IBAction
 */
-(IBAction) onEmail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients: [NSArray arrayWithObject: self.eGContactDetail.contactEMail]];
//        [controller setSubject:@"eGordian"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No email account is setup to send mail." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    /*
     MFMailComposeViewController* mailViewController = [[MFMailComposeViewController alloc] init];
     mailViewController.mailComposeDelegate = self;
     [mailViewController setToRecipients: [NSArray arrayWithObject: self.eGContact.eMail]];
     
     //    [[[[mailViewController.view.subviews objectAtIndex:3] subviews] objectAtIndex:2] becomeFirstResponder];
     
     if (mailViewController)
     [self presentViewController:mailViewController animated:YES completion:nil];
     */
}
/*
 * Method Name	: onPhone
 * Description	: on phone button click
 * Parameters	: ID
 * Return value	: IBAction
 */
-(IBAction) onPhone : (id)sender
{
    UIButton* phoneBtn = (UIButton*)sender;
    
    CGPoint buttonPosition = [phoneBtn convertPoint:CGPointZero toView:self.eGordianViewContactsTableView];
    NSIndexPath *indexPath = [self.eGordianViewContactsTableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        self.displayNameLabel.text = @"";
        self.phoneNumberLabel.text = @"";
        self.displayNameLabel.text = [NSString stringWithFormat:@"%@", self.eGContactDetail.contactDisplayName];
        NSMutableArray *sortedKeys = [[[self.dataDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
        
        if([[sortedKeys objectAtIndex:indexPath.row] isEqualToString:bPhoneMacro1]){
            self.phoneNumberLabel.text = self.eGContactDetail.contactBusinessPhone1;
        }
        else if([[sortedKeys objectAtIndex:indexPath.row] isEqualToString:mPhoneMacro]){
            self.phoneNumberLabel.text = self.eGContactDetail.contactMobilePhone;
        }
        
        
        if(self.phoneNumberLabel.text.length > 0){
            [self.callView setHidden:NO];
            [self.view bringSubviewToFront:self.callView];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                            message:@"No phone numbers were set for this contact"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }

    }
}

#pragma mark - mail delegate
/*
 * Method Name	: mailComposeController
 * Description	: mailComposeController delegate
 * Parameters	: nil
 * Return value	: nil
 */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        {
            break;
        }
        case MFMailComposeResultSaved:
        {
            break;
        }
        case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:
        {
            break;
        }
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
            break;
    }
    if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
    
}


#pragma mark - misc
/*
 * Method Name	: getContact
 * Description	: get the current contact details through API call
 * Parameters	: nil
 * Return value	: nil
 */
-(void) getContact
{
    [self startAnimating];
    
    NSString* userID = [AppConfig getUserId];
    
//    NSString *contactURLString = [NSString stringWithFormat:@"%@users/%@/Contacts/%@", APP_HEADER_URL, userID, self.eGContact.ID];
    [[DataDownloader sharedDownloader] setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader] getResponseFormUrl:self.contactDetailURL serviceType:EGORDIAN_API_CONTACTS_DETAIL];
}

/*
 * Method Name	: startAnimating
 * Description	: start Animating
 * Parameters	: nil
 * Return value	: nil
 */
-(void) startAnimating
{
    if(!self.spinner)
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 0, 0);
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

/*
 * Method Name	: stopAnimating
 * Description	: stop Animation
 * Parameters	: nil
 * Return value	: nil
 */
-(void) stopAnimating
{
    if(self.spinner)
    {
        [self.spinner stopAnimating];
        self.spinner.hidden = TRUE;
        self.spinner = nil;
    }
    
}

#pragma mark Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if (type == EGORDIAN_API_CONTACTS_DETAIL && [responseArray count]>0) {
        
        self.eGContactDetail = (EG_ContactDetail *)[responseArray objectAtIndex:0];
        
        self.contactDisplayNameLabel.text = self.eGContactDetail.contactDisplayName;
        
        self.companyNameLabel.text = self.eGContactDetail.contactCompanyName;
        
        //the below code is to create a dictionary with data which is present in the response and to avoid fields which are blank
        if(self.dataDict)
            self.dataDict = nil;
        
         self.dataDict = [[NSMutableDictionary alloc] init];
        
        if([self.eGContactDetail.contactAddress1 length] && ([self.eGContactDetail.contactCity length] || [self.eGContactDetail.contactState length] || [self.eGContactDetail.contactZipCode length]))
            [self.dataDict setObject:self.eGContactDetail.contactAddress1 forKey:addressMacro];
        
        if([self.eGContactDetail.contactEMail length])
            [self.dataDict setObject:self.eGContactDetail.contactEMail forKey:eMailMacro];
        
        if([self.eGContactDetail.contactBusinessPhone1 length])
            [self.dataDict setObject:self.eGContactDetail.contactBusinessPhone1 forKey:bPhoneMacro1];
        
//        if([self.eGContactDetail.BusinessPhone2 length])
//            [self.dataDict setObject:self.eGContactDetail.BusinessPhone2 forKey:bPhoneMacro2];
        
        if([self.eGContactDetail.contactMobilePhone length])
            [self.dataDict setObject:self.eGContactDetail.contactMobilePhone forKey:mPhoneMacro];
        
        [self.eGordianViewContactsTableView reloadData];
        
    }
    [self stopAnimating];
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    [self stopAnimating];
    
}

- (IBAction)callButtonTappedToRedirectDailer:(id)sender {
    
    NSString *cleanedString = [[self.phoneNumberLabel.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }

    
    [self.callView setHidden:YES];
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self.callView setHidden:YES];
}

@end
