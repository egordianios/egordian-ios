//
//  EGJobContactsAssignViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDownloader.h"

@interface EGJobContactsAssignViewController : UIViewController <DataDownloaderDelegate>

@property (weak, nonatomic) IBOutlet UITableView *assignContactsTableView;

@property (weak, nonatomic) IBOutlet UIView *loaderView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;

@property (weak, nonatomic) IBOutlet UITextField *assignContactSearchField;

@property (weak, nonatomic) IBOutlet UIImageView *searchImage;

@property (nonatomic, strong) NSString *jobOrderID;

@property (nonatomic, strong) NSString *jobOrderOwnerId;

@property (nonatomic, strong) NSString *rolesURl;

@property (weak, nonatomic) IBOutlet UIButton *eGSearchBtn;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)searchResultCancelTapped:(id)sender;

- (IBAction)assignBackTapped:(id)sender;

@end
