//
//  EGJobConatctAddARoleViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/4/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EGJobConatctAddARoleViewController.h"
#import "eGViewContactAddressCell.h"
#import "egViewContactOtherCell.h"
#import "eGMapViewController.h"
#import "eGordianTelephoneViewController.h"
#import "SortViewListCell.h"
#import "EGJobContactsViewController.h"

@interface EGJobConatctAddARoleViewController (){
    NSMutableArray *rolesArray;
    NSString *roleId;
}

@end

#define addressMacro @"A_Address1"
#define eMailMacro @"B_Email:"
#define bPhoneMacro1 @"C_Business Phone:"
//#define bPhoneMacro2 @"D_Business Phone2:"
#define mPhoneMacro @"E_Mobile:"

@implementation EGJobConatctAddARoleViewController

@synthesize eGContactDetail,dataDict,mySensitiveRect,strJobOrderId,strRolesURL,eGOwnerContact;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.popOverView.hidden = YES;
    
//    self.strContactId = self.eGContact.ID;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    [gestureRecognizer setNumberOfTapsRequired:1.0];
    
    [gestureRecognizer setDelegate:self];
    
    [self.popOverView addGestureRecognizer:gestureRecognizer];

    rolesArray = [[NSMutableArray alloc]init];
    
    [self.eGordianViewContactsTableView setEditing:NO];
    
    self.eGordianViewContactsTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    self.dataDict = nil;
    
    [self getContact];
}

#pragma UIGestureRecogniser Delegates

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.pickerTable]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    self.mySensitiveRect = self.actualPopOverView.frame;
    CGPoint p = [gestureRecognizer locationInView:self.popOverView];
    if (!CGRectContainsPoint(self.mySensitiveRect, p)) {
        self.popOverView.hidden = YES;
        NSLog(@"got a tap in the region i care about");
    } else {
        NSLog(@"got a tap, but not where i need it");
    }
}

#pragma mark - misc
/*
 * Method Name	: getContact
 * Description	: get the current contact details through API call
 * Parameters	: nil
 * Return value	: nil
 */
-(void) getContact
{
    [self actvityStart];
    
    NSString* userID = [AppConfig getUserId];
    
    NSString *contactURLString = [NSString stringWithFormat:@"%@users/%@/Contacts/%@", APP_HEADER_URL, userID, self.eGOwnerContact.contactId];
    
    [[DataDownloader sharedDownloader] setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader] getResponseFormUrl:contactURLString serviceType:EGORDIAN_API_CONTACTS_DETAIL];
    
}

-(void)fetchRoles{
    
//     NSString *assignRoleUrl = [NSString stringWithFormat:@"%@owners/%@/JobOrders/%@/Contacts/%@/roleId/%@",APP_HEADER_URL,self.strOwnerId,self.strJobOrderId,self.strContactId,roleId];
    
    NSString *rolesURL = [NSString stringWithFormat:@"%@owners/%@/Roles",APP_HEADER_URL,self.eGOwnerContact.contactOwnerID];
    
        [[DataDownloader sharedDownloader] setDataDownloaderDelegate:(id)self];
        [[DataDownloader sharedDownloader] getResponseFormUrl:rolesURL serviceType:EGORDIAN_API_OWNER_ROLES];

}

-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Table view data source
/*
 * Method Name	: numberOfSectionsInTableView
 * Description	: returns the number of sections in tableview
 * Parameters	: UITableView
 * Return value	: NSInteger (number of sections in tableview)
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/*
 * Method Name	: numberOfRowsInSection
 * Description	: returns the number of rows in a section
 * Parameters	: UITableView, NSInteger
 * Return value	: NSInteger (number of cells in the section)
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //dataDict contains email, bPHone, mPhone details. If any one of them is not present, no need to show that row.
    
    NSInteger rowCount = 0;
    if(tableView == self.pickerTable){
        rowCount = rolesArray.count;
    }
    else if(tableView == self.eGordianViewContactsTableView){
        rowCount = [[self.dataDict allKeys] count];
    }
    return rowCount;
}

/*
 * Method Name	: heightForRowAtIndexPath
 * Description	: variable height support
 * Parameters	: UITableView, NSIndexPath
 * Return value	: CGFloat
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0.0;
    
    if(tableView == self.pickerTable){
        rowHeight = 50.0;
    }
    else if(tableView == self.eGordianViewContactsTableView){
        if(indexPath.row == 0 && [[self.dataDict allKeys]containsObject:addressMacro])
            rowHeight = 80;
        else
            rowHeight =65;
    }
    return  rowHeight;
}

/*
 * Method Name	: cellForRowAtIndexPath
 * Description	: Customize the appearance of table view cells.
 * Parameters	: UITableView, NSIndexPath
 * Return value	: UITableViewCell
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    
    if(tableView == self.pickerTable){
        
        SortViewListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SortViewListCell"];
        
        if (!cell)
        {
            cell = [[SortViewListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SortViewListCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        if(rolesArray.count)
            cell.titleText = [[rolesArray objectAtIndex:indexPath.row] objectForKey:@"Name"];
        
        if([[[rolesArray objectAtIndex:indexPath.row] objectForKey:@"Name"] isEqualToString:self.roleLabel.text]){
            cell.selectionImage.image = [UIImage imageNamed:@"egSelectJobTick"];
        }
        else{
            cell.selectionImage.image = nil;
        }
        
        return cell;
        
    }
    else{
        if (indexPath.row == 0 && [[self.dataDict allKeys]containsObject:addressMacro])
        {
            static NSString *CellIdentifier = @"eGViewContactAddressCell";
            
            eGViewContactAddressCell *egContactAddressCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (!egContactAddressCell)
            {
                egContactAddressCell = [[eGViewContactAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                egContactAddressCell.shouldIndentWhileEditing = NO;
            }
            
            egContactAddressCell.contactsViewAddressTxtfld.text = self.eGContactDetail.contactAddress1?self.eGContactDetail.contactAddress1:@"";
            egContactAddressCell.contactsViewCityTxtfld.text = [NSString stringWithFormat:@"%@ %@ %@", [self.eGContactDetail.contactCity length]?[NSString stringWithFormat:@"%@,",self.eGContactDetail.contactCity]:@"", self.eGContactDetail.contactState?self.eGContactDetail.contactState:@"", self.eGContactDetail.contactZipCode?self.eGContactDetail.contactZipCode:@""];
            
            return egContactAddressCell;
        }
        else
        {
            static NSString *CellIdentifier = @"eGViewContactOtherCell";
            eGViewContactOtherCell *egViewOtherCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!egViewOtherCell)
            {
                egViewOtherCell = [[eGViewContactOtherCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                egViewOtherCell.shouldIndentWhileEditing = NO;
            }
            
            NSMutableArray *sortedKeys = [[[self.dataDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
            
            NSInteger currentIndex = 0;
            
            if([[self.dataDict allKeys]containsObject:addressMacro]){
                [sortedKeys removeObjectAtIndex:[sortedKeys indexOfObject:addressMacro]];
                currentIndex = indexPath.row-1;
            }
            else{
                currentIndex = indexPath.row;
            }
            
            NSString *headerLabelText = [sortedKeys objectAtIndex:currentIndex];
            
            NSString *headerLabelText1 = [headerLabelText substringWithRange:NSMakeRange(2, [headerLabelText length]-2)];
            
            egViewOtherCell.GViewContactOtherHeaderLbl.text = headerLabelText1;
            
            egViewOtherCell.eG_ViewContact_OtherTxtfld.text = [self.dataDict valueForKey:headerLabelText];//[[self.dataDict allValues] objectAtIndex:indexPath.row];
            
//            egViewOtherCell.GViewContactOtherHeaderLbl.text = [[self.dataDict allKeys] objectAtIndex:indexPath.row];
            
            if([[sortedKeys objectAtIndex:currentIndex] isEqualToString:eMailMacro])
            {
                [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:[UIImage imageNamed:@"eG_ContactView_EMail"] forState:UIControlStateNormal];
                [egViewOtherCell.eG_ViewContact_OtherBtn addTarget:self action:@selector(onEmail:) forControlEvents: UIControlEventTouchUpInside];
            }
            else
            {
                [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:nil forState:UIControlStateNormal];
                [egViewOtherCell.eG_ViewContact_OtherBtn setBackgroundImage:[UIImage imageNamed:@"eG_Contacts_Phone"] forState:UIControlStateNormal];
                [egViewOtherCell.eG_ViewContact_OtherBtn addTarget:self action:@selector(onPhone:) forControlEvents: UIControlEventTouchUpInside];
                egViewOtherCell.eG_ViewContact_OtherBtn.tag = indexPath.row;
            }
            
            return egViewOtherCell;
        }

    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == self.pickerTable){
        [self.popOverView setHidden:YES];
        self.roleLabel.text = [[rolesArray objectAtIndex:indexPath.row]objectForKey:@"Name"];
        roleId = [[rolesArray objectAtIndex:indexPath.row]objectForKey:@"Id"];
    }
}

/*
 * Method Name	: eGordianViewContactsAddressBtn
 * Description	: on clicking the Address Button
 * Parameters	: ID
 * Return value	: IBAction
 */
- (IBAction)eGordianViewContactsAddressBtn:(id)sender {
    
    if(self.eGContactDetail.contactZipCode.length || self.eGContactDetail.contactCity.length || self.eGContactDetail.contactAddress1.length){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
        eGMapViewController *mapsVC = (eGMapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGMapViewController"];
        
        NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] init];
        
        [addressDict setObject:[NSString stringWithFormat:@"%@", [self.eGContactDetail.contactDisplayName length]?[NSString stringWithFormat:@"%@",self.eGContactDetail.contactDisplayName]:@""] forKey:@"Name"] ;
        [addressDict setObject:[NSString stringWithFormat:@"%@", [self.eGContactDetail.contactAddress1 length]?[NSString stringWithFormat:@"%@",self.eGContactDetail.contactAddress1]:@""] forKey:@"Address"] ;
        
        [addressDict setObject:[NSString stringWithFormat:@"%@%@%@",self.eGContactDetail.contactCity.length?[self.eGContactDetail.contactCity stringByAppendingString:@","]:@"",self.eGContactDetail.contactState.length?[self.eGContactDetail.contactState stringByAppendingString:@","]:@"",self.eGContactDetail.contactZipCode.length?self.eGContactDetail.contactZipCode:@""] forKey:addressMacro] ;
        
        mapsVC.addressDict = addressDict;
        [self.navigationController pushViewController:mapsVC animated:NO];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                        message:@"Address not available for this contact"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

/*
 * Method Name	: onEmail
 * Description	: on eMail button click
 * Parameters	: ID
 * Return value	: IBAction
 */
-(IBAction) onEmail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients: [NSArray arrayWithObject: self.eGContactDetail.contactEMail]];
        //        [controller setSubject:@"eGordian"];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"No email account is setup to send mail." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
}
/*
 * Method Name	: onPhone
 * Description	: on phone button click
 * Parameters	: ID
 * Return value	: IBAction
 */
-(IBAction) onPhone : (id)sender
{
    UIButton* phoneBtn = (UIButton*)sender;
    
    CGPoint buttonPosition = [phoneBtn convertPoint:CGPointZero toView:self.eGordianViewContactsTableView];
    
    NSIndexPath *indexPath = [self.eGordianViewContactsTableView indexPathForRowAtPoint:buttonPosition];
    
    NSMutableArray *sortedKeys = [[[self.dataDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
    
    if (indexPath != nil)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
        eGordianTelephoneViewController *egTelViewController = (eGordianTelephoneViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianTelephoneViewController"];
        egTelViewController.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        
        if([[sortedKeys objectAtIndex:indexPath.row] isEqualToString:bPhoneMacro1]){
            egTelViewController.eGPhoneLbl.text = self.eGContactDetail.contactBusinessPhone1;
        }
        else if([[sortedKeys objectAtIndex:indexPath.row] isEqualToString:mPhoneMacro]){
            egTelViewController.eGPhoneLbl.text = self.eGContactDetail.contactMobilePhone;
        }

        egTelViewController.eGNameLbl.text = [NSString stringWithFormat:@"%@", self.eGContactDetail.contactDisplayName];
        egTelViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:egTelViewController animated:YES completion:nil]; ;
    }
}

#pragma mark - mail delegate
/*
 * Method Name	: mailComposeController
 * Description	: mailComposeController delegate
 * Parameters	: nil
 * Return value	: nil
 */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        {
            break;
        }
        case MFMailComposeResultSaved:
        {
            break;
        }
        case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:
        {
            break;
        }
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Email Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
            break;
    }
    if ([self respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self performSelector:@selector(dismissModalViewControllerAnimated:) withObject:[NSNumber numberWithBool:YES]];
    } else {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    switch (type) {
        
        case EGORDIAN_API_CONTACTS_DETAIL:{
         
            if( [responseArray count]>0){
                
                
                self.eGContactDetail = (EG_ContactDetail *)[responseArray objectAtIndex:0];
                
                self.companyNameLabel.text = self.eGContactDetail.contactCompanyName;
                
                //the below code is to create a dictionary with data which is present in the response and to avoid fields which are blank
                if(self.dataDict)
                    self.dataDict = nil;
                
                self.dataDict = [[NSMutableDictionary alloc] init];
                
                if([self.eGContactDetail.contactAddress1 length] && ([self.eGContactDetail.contactCity length] || [self.eGContactDetail.contactState length] || [self.eGContactDetail.contactZipCode length]))
                    [self.dataDict setObject:self.eGContactDetail.contactAddress1 forKey:addressMacro];
                
                if([self.eGContactDetail.contactEMail length])
                    [self.dataDict setObject:self.eGContactDetail.contactEMail forKey:eMailMacro];
                
                if([self.eGContactDetail.contactBusinessPhone1 length])
                    [self.dataDict setObject:self.eGContactDetail.contactBusinessPhone1 forKey:bPhoneMacro1];
                
//                if([self.eGContactDetail.contactBusinessPhone2 length])
//                    [self.dataDict setObject:self.eGContactDetail.contactBusinessPhone2 forKey:bPhoneMacro2];
                
                if([self.eGContactDetail.contactMobilePhone length])
                    [self.dataDict setObject:self.eGContactDetail.contactMobilePhone forKey:mPhoneMacro];
                
                [self.eGordianViewContactsTableView reloadData];
            }
            
            [self fetchRoles];
        }
            break;
        case EGORDIAN_API_OWNER_ROLES:{
            rolesArray = [responseArray copy];
            
            [self actvityStop];
        }
            break;
        case EGORDIAN_API_ASSIGNROLE:{
            
            [self actvityStop];
            
            NSArray *navigationStackControllers = [self.navigationController viewControllers];
            
            for (UIViewController *currVC in navigationStackControllers) {
                if([currVC isKindOfClass:[EGJobContactsViewController class]]){
                    [self.navigationController popToViewController:currVC animated:YES];
                }
            }
        }
            break;
        default:
            break;
    }
    
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    [self actvityStop];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)roleButtonTapped:(id)sender {
    
    [self.view bringSubviewToFront:self.popOverView];
    
    [self.pickerTable reloadData];
    
    self.popOverView.hidden = NO;
    
}

- (IBAction)addRoleBackButtonTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButtonTapped:(id)sender {
    
    if(roleId){
        
        [self actvityStart];
        
        NSString *assignRoleUrl = [NSString stringWithFormat:@"%@owners/%@/JobOrders/%@/Contacts/%@/roleId/%@",APP_HEADER_URL,self.eGContactDetail.contactOwnerID,self.strJobOrderId,self.eGContactDetail.contactId,roleId];
        
        [[DataDownloader sharedDownloader] setDataDownloaderDelegate:(id)self];
        [[DataDownloader sharedDownloader] getResponseFormUrl:assignRoleUrl serviceType:EGORDIAN_API_ASSIGNROLE];
    }
    else{
        UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                              message:@"Please assign a role to this contact."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil];
        
        [displayAlert show];
    }
}
@end
