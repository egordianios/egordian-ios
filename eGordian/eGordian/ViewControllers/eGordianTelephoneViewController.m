//
//  eGordianTelephoneViewController.m
//  eGordian-iOS
//
//  Created by Laila on 26/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianTelephoneViewController.h"

@interface eGordianTelephoneViewController ()

@end

@implementation eGordianTelephoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.eGCallView.layer setCornerRadius:8.f];
    [self.eGCallView.layer setMasksToBounds:YES];
    self.eGCallView.layer.opaque = YES;
    
    [self.egCancelBtn.layer setCornerRadius:8.f];
    [self.egCancelBtn.layer setMasksToBounds:YES];
    self.egCancelBtn.layer.opaque = YES;
    
    [self.egCallBtn.layer setCornerRadius:8.f];
    [self.egCallBtn.layer setMasksToBounds:YES];
    self.egCallBtn.layer.opaque = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 * Method Name	: onCallBtn
 * Description	: on Call Button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onCallBtn:(id)sender {
    
    NSString *cleanedString = [[self.eGPhoneLbl.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 * Method Name	: onCancelBtn
 * Description	: on Cancel Button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onCancelBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
