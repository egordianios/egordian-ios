//
//  eGordianContact.m
//  eGordian-iOS
//
//  Created by Laila on 28/08/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianContact.h"
#import "NSDictionary+Json.h"

@implementation eGordianContact

@synthesize ID = _ID;
@synthesize FirstName = _FirstName;
@synthesize LastName = _LastName;
@synthesize Name = _Name;
@synthesize Link = _Link;
@synthesize OwnerID = _OwnerID;
@synthesize Key = _Key;
@synthesize CompanyID = _CompanyID;
@synthesize CompanyName = _CompanyName;
@synthesize eMail = _eMail;
@synthesize Address1 = _address1;
@synthesize Address2 = _address2;
@synthesize City = _City;
@synthesize State = _State;
@synthesize ZipCode = _ZipCode;
@synthesize BusinessPhone1 = _BusinessPhone1;
@synthesize BusinessPhone2 = _BusinessPhone2;
@synthesize MobilePhone = _MobilePhone;
@synthesize Suffix = _Suffix;
@synthesize Role = _Role;
@synthesize ContactLink = _ContactLink;
@synthesize MiddleName = _MiddleName;
@synthesize DisplayName = _DisplayName;

#define contactID @"Id"
#define companyID @"CompanyID"
#define ownerID @"ownerId"
#define suffix @"Suffix"
#define firstName @"FirstName"
#define middleName @"MiddleName"
#define lastName @"LastName"
#define displayName @"DisplayName"
#define role @"Role"
#define BusinessTel @"TeleBusiness1"
#define Mobile @"MobilePhone"
#define email @"Email"
#define companyName @"CompanyName"
#define contactLink @"ContactLink"
#define company @"Company"
#define Fields @"Fields"
#define BusinessPh1 @"BusinessPhone1"
#define BusinessPh2 @"BusinessPhone2"
#define address1 @"Address1"
#define address2 @"Address2"
#define city @"City"
#define state @"State"
#define zipCode @"ZipCode"

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.ID = [dictionary objectForKey:@"ID"];
        self.FirstName = [dictionary objectForKey:@"FirstName"];
        self.LastName = [dictionary objectForKey:@"LastName"];
        self.Link = [dictionary objectForKey:@"Link"];
        self.Key = [dictionary objectForKey:@"Key"];
        self.CompanyID = [dictionary objectForKey:companyID];
    }
    
    return self;
}

/*/*
 * Method Name	: initWithDict
 * Description	: initialize the object with the dictionary that comes from server
 * Parameters	: ContactsDict - the dictionary object from the server.
 * Return value	: eGordianContact object
 */
/*-(id) initWithDict:(NSMutableDictionary*)ContactsDict
{
    self = [super init];
    
    if(self)
    {
        NSMutableDictionary *companyDict = [ContactsDict objectForKey:company];
        NSArray *contactArray = [ContactsDict objectForKey:Fields];
        
        NSMutableDictionary *contactDict = [[NSMutableDictionary alloc] init];
        for(NSDictionary* obj in contactArray)
        {
            if([obj valueForKey:@"Value"] == (NSString *)[NSNull null])
                [contactDict setObject:@"" forKey:[obj valueForKey:@"Name"]];
            else
                [contactDict setObject:[obj valueForKey:@"Value"] forKey:[obj valueForKey:@"Name"]];
        }
        
        
        self.CompanyID = [companyDict valueForKey:companyID];
        self.CompanyName = [companyDict valueForKey:companyName];
        self.ID = [ContactsDict valueForKey:contactID];
        self.ContactLink = [contactDict valueForKey:ownerID];
        self.FirstName = [contactDict valueForKey:firstName];
        self.LastName = [contactDict valueForKey:lastName];
        self.Suffix = [contactDict valueForKey:suffix];
        self.OwnerID = [contactDict valueForKey:ownerID];
        self.MiddleName = [contactDict valueForKey:middleName];
        self.DisplayName = [contactDict valueForKey:displayName];
        self.Role = [contactDict valueForKey:role];
        self.BusinessPhone = [contactDict valueForKey:BusinessTel];
        self.MobilePhone = [contactDict valueForKey:Mobile];
        self.eMail = [contactDict valueForKey:email];
        
        
    }
    return self;
}*/

/*
 * Method Name	: initWithArray
 * Description	: initialize the object with the array that comes from server
 * Parameters	: contactsArray - the array object from the server.
 * Return value	: nil
 */
-(id) initWithArray:(NSArray*)ContactsArray
{
    self = [super init];
    
    if(self)
    {
        NSMutableDictionary *contactDict = [[NSMutableDictionary alloc] init];
        
        for(NSDictionary* obj in ContactsArray)
        {
            if([obj valueForKey:@"Value"] == (NSString *)[NSNull null] || ![[obj valueForKey:@"Value"] length])
                [contactDict setObject:@"" forKey:[obj valueForKey:@"Name"]];
            else
                [contactDict setObject:[obj valueForKey:@"Value"] forKey:[obj valueForKey:@"Name"]];
        }
        self.ID = [contactDict objectForKey:contactID];
        self.OwnerID = [contactDict objectForKey:@"OwnerId"];
        self.FirstName = [contactDict objectForKey:@"FirstName"];
        self.LastName = [contactDict objectForKey:@"LastName"];
        self.MiddleName = [contactDict objectForKey:@"MiddleName"];
        self.DisplayName = [contactDict objectForKey:@"DisplayName"];
        self.Name        =  [contactDict objectForKey:@"DisplayName"];
        self.eMail = [contactDict objectForKey:@"Email"];
//        self.CompanyName = [contactDict objectForKey:@"CompanyName"];
        self.Address1 = [contactDict objectForKey:@"Address1"];
        self.Address2 = [contactDict objectForKey:@"Address2"];
        self.City = [contactDict objectForKey:@"City"];
        self.State = [contactDict objectForKey:@"State"];
        self.ZipCode = [contactDict objectForKey:@"ZipCode"];
        self.BusinessPhone2 = [contactDict objectForKey:@"BusinessPhone2"];
        self.BusinessPhone1 = [contactDict objectForKey:@"BusinessPhone1"];
        self.MobilePhone = [contactDict objectForKey:@"MobilePhone"];
//        self.eMail = [contactDict objectForKey:email];
    }
    return self;
}

/*
 * Method Name	: getContactDictionaryFromObject
 * Description	: return a dictionary from the current Contact Object
 * Parameters	: nil
 * Return value	: Dictionary from the current Contact Object
 */
-(NSDictionary*) getAsDictionary
{
    NSMutableDictionary* contactDictionary = [[NSMutableDictionary alloc] init];
    
    [contactDictionary setObject:self.ID forKey:contactID];
    [contactDictionary setObject:self.FirstName forKey:firstName];
    [contactDictionary setObject:self.LastName forKey:lastName];
    [contactDictionary setObject:self.Link forKey:contactLink];
    [contactDictionary setObject:self.CompanyID forKey:companyID];
    [contactDictionary setObject:self.eMail forKey:email];
    [contactDictionary setObject:self.Address1 forKey:address1];
    [contactDictionary setObject:self.City forKey:city];
    [contactDictionary setObject:self.State forKey:state];
    [contactDictionary setObject:self.ZipCode forKey:zipCode];
    [contactDictionary setObject:self.BusinessPhone1 forKey:BusinessTel];
    [contactDictionary setObject:self.MobilePhone forKey:Mobile];
    
    return contactDictionary;
}


+(NSMutableArray*) getContactArray:(NSArray*)contactsArray
{
    NSMutableArray* contactArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary* obj in contactsArray)
    {
        NSMutableDictionary *companyDict = [obj objectForKey:company];
        NSArray *tempArr = [obj objectForKey:Fields];
        
        NSMutableDictionary *contactDict = [[NSMutableDictionary alloc] init];
        
        for(NSDictionary* obj in tempArr)
        {
            if([obj valueForKey:@"Value"] == (NSString *)[NSNull null])
                [contactDict setObject:@"" forKey:[obj valueForKey:@"Name"]];
            else
                [contactDict setObject:[obj valueForKey:@"Value"] forKey:[obj valueForKey:@"Name"]];
        }
        
        eGordianContact* egContact = [[eGordianContact alloc] init];
        egContact.CompanyID = [companyDict valueForKey:companyID];
        egContact.CompanyName = [companyDict valueForKey:companyName];
        egContact.Name = [companyDict valueForKey:@"Name"];
        egContact.ID = [obj valueForKey:contactID];
        egContact.ContactLink = [obj valueForKey:contactLink];
        egContact.FirstName = [contactDict valueForKey:firstName];
        
        egContact.LastName = [contactDict valueForKey:lastName];//[contactDict getStringForKey:@"LastName"].length?[contactDict getStringForKey:@"LastName"]:@" ";
        egContact.Suffix = [contactDict valueForKey:suffix];
        egContact.OwnerID = [contactDict valueForKey:ownerID];
        egContact.MiddleName = [contactDict valueForKey:middleName];
        egContact.DisplayName = [contactDict valueForKey:displayName];
        egContact.Role = [contactDict valueForKey:role];
        egContact.BusinessPhone1 = [contactDict valueForKey:BusinessPh1];
        egContact.BusinessPhone2 = [contactDict valueForKey:BusinessPh2];
        egContact.MobilePhone = [contactDict valueForKey:Mobile];
        egContact.eMail = [contactDict valueForKey:email];
        egContact.OwnerID = [contactDict valueForKey:ownerID];
        
        
        if(egContact.FirstName.length || egContact.LastName.length){
            
            if(![egContact.LastName length]){
                egContact.LastName = @" ";
            }
            
            [contactArray addObject:egContact];
        }
    }
    
    return  contactArray;
}

//+(NSMutableDictionary*) getAsDictionary:(NSDictionary*)dictionary
//{
//    NSMutableDictionary* dict;
//    if([dictionary valueForKey:@"Value"] == (NSString *)[NSNull null])
//        [dict setObject:@"" forKey:[dictionary valueForKey:@"Name"]];
//    else
//        [dict setObject:[dictionary valueForKey:@"Value"] forKey:[dictionary valueForKey:@"Name"]];
//    
//    return dict;
//}
@end
