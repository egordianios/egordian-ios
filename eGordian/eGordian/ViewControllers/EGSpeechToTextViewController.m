//
//  EGSpeechToTextViewController.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/13/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EGSpeechToTextViewController.h"

@interface EGSpeechToTextViewController ()<UIGestureRecognizerDelegate>{
    
    UITapGestureRecognizer *tapBehindGesture;
}

@end
const unsigned char SpeechKitApplicationKey[] = {
    0x2e, 0xcb, 0x13, 0x3f, 0x16, 0xaa, 0xeb, 0x50, 0x62, 0xe7, 0x2c, 0x1f, 0x13, 0x07, 0xe0, 0x0b, 0x8d, 0xd5, 0x03, 0x78, 0xb6, 0x36, 0x18, 0x03, 0x39, 0xea, 0x59, 0xa1, 0x9a, 0x13, 0x09, 0xc8, 0xde, 0x1d, 0x07, 0xf5, 0xc7, 0xf2, 0x45, 0x29, 0xd9, 0x71, 0x48, 0xec, 0x78, 0x28, 0xf7, 0x05, 0xfd, 0xfa, 0x7d, 0xb3, 0x31, 0x95, 0xb7, 0x6a, 0x80, 0x10, 0x0c, 0x1a, 0x24, 0x15, 0xe2, 0x02};

@implementation EGSpeechToTextViewController
@synthesize recordButton,delegate;
@synthesize vuMeter,voiceSearch,recognitionType,recordingView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.resultsTextView.text =@"";
    
    [SpeechKit setupWithID:@"NMDPTRIAL_naresh_kalangi_marlabs_com20160111040343"
                      host:@"sandbox.nmdp.nuancemobility.net"
                      port:443
                    useSSL:NO
                  delegate:self];
    
    // Set earcons to play
    SKEarcon* earconStart	= [SKEarcon earconWithName:@"earcon_listening.wav"];
    SKEarcon* earconStop	= [SKEarcon earconWithName:@"earcon_done_listening.wav"];
    SKEarcon* earconCancel	= [SKEarcon earconWithName:@"earcon_cancel.wav"];
    
    [SpeechKit setEarcon:earconStart forType:SKStartRecordingEarconType];
    [SpeechKit setEarcon:earconStop forType:SKStopRecordingEarconType];
    [SpeechKit setEarcon:earconCancel forType:SKCancelRecordingEarconType];
    
    if(!tapBehindGesture) {
        tapBehindGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapBehind:)];
        [tapBehindGesture setNumberOfTapsRequired:1];
        tapBehindGesture.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view
        [self.view  addGestureRecognizer:tapBehindGesture];
        tapBehindGesture.delegate = self;
    }

}

-(void) viewWillAppear:(BOOL)animated
{
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    [super viewWillAppear:YES];
}

#pragma mark Tao Gesture Delegate
- (void)handleTapBehind:(UITapGestureRecognizer *)sender
{
    CGRect mySensitiveRect = recordingView.frame;
    CGPoint p = [tapBehindGesture locationInView:self.view];
    if (!CGRectContainsPoint(mySensitiveRect, p)) {
        NSLog(@"got a tap in the region i care about");
        [self.view removeGestureRecognizer:tapBehindGesture];
        if (self.delegate && [self.delegate respondsToSelector:@selector(handOutsideTouch:)]) {
            [self.delegate handOutsideTouch:self.resultsTextView.attributedText];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        tapBehindGesture=nil;
    } else {
        NSLog(@"got a tap, but not where i need it");
    }
}
#pragma mark -
#pragma mark Actions

- (IBAction)recordButtonAction: (id)sender {
    
    
    if (transactionState == TS_RECORDING) {
        [voiceSearch stopRecording];
    }
    else if (transactionState == TS_IDLE) {
        SKEndOfSpeechDetection detectionType;
        NSString* recoType;
        NSString* langType;
        
        transactionState = TS_INITIAL;
        
        self.resultsTextView.text =@"";
        
        if (recognitionType.selectedSegmentIndex == 0) {
            /* 'Search' is selected */
            detectionType = SKShortEndOfSpeechDetection; /* Searches tend to be short utterances free of pauses. */
            recoType = SKSearchRecognizerType; /* Optimize recognition performance for search text. */
        }
        else if (recognitionType.selectedSegmentIndex == 1){
            /* 'Dictation' is selected */
            detectionType = SKLongEndOfSpeechDetection; /* Dictations tend to be long utterances that may include short pauses. */
            recoType = SKDictationRecognizerType; /* Optimize recognition performance for dictation or message text. */
        }
        langType = @"en_US";
        
        /* Nuance can also create a custom recognition type optimized for your application if neither search nor dictation are appropriate. */
        
        NSLog(@"Recognizing type:'%@' Language Code: '%@' using end-of-speech detection:%d.", recoType, langType, detectionType);
        
        if (voiceSearch) voiceSearch =nil;
        
        voiceSearch = [[SKRecognizer alloc] initWithType:recoType
                                               detection:detectionType
                                                language:langType
                                                delegate:self];
    }
}

#pragma mark -
#pragma mark VU Meter

- (void)setVUMeterWidth:(float)width {
    if (width < 0)
        width = 0;
    
    CGRect frame = vuMeter.frame;
    frame.size.width = width+10;
    vuMeter.frame = frame;
}

- (void)updateVUMeter {
    float width = (90+voiceSearch.audioLevel)*5/2;
    
    [self setVUMeterWidth:width];
    [self performSelector:@selector(updateVUMeter) withObject:nil afterDelay:0.05];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark SpeechKitDelegate methods

- (void) audioSessionReleased {
    NSLog(@"audio session released");
}

- (void) destroyed {
    // Debug - Uncomment this code and fill in your app ID below, and set
    // the Main Window nib to MainWindow_Debug (in DMRecognizer-Info.plist)
    // if you need the ability to change servers in DMRecognizer
    //
    //[SpeechKit setupWithID:INSERT_YOUR_APPLICATION_ID_HERE
    //                  host:INSERT_YOUR_HOST_ADDRESS_HERE
    //                  port:INSERT_YOUR_HOST_PORT_HERE[[portBox text] intValue]
    //                useSSL:NO
    //              delegate:self];
    //
    // Set earcons to play
    //SKEarcon* earconStart	= [SKEarcon earconWithName:@"earcon_listening.wav"];
    //SKEarcon* earconStop	= [SKEarcon earconWithName:@"earcon_done_listening.wav"];
    //SKEarcon* earconCancel	= [SKEarcon earconWithName:@"earcon_cancel.wav"];
    //
    //[SpeechKit setEarcon:earconStart forType:SKStartRecordingEarconType];
    //[SpeechKit setEarcon:earconStop forType:SKStopRecordingEarconType];
    //[SpeechKit setEarcon:earconCancel forType:SKCancelRecordingEarconType];
}

#pragma mark -
#pragma mark SKRecognizerDelegate methods

- (void)recognizerDidBeginRecording:(SKRecognizer *)recognizer
{
    NSLog(@"Recording started.");
    
    transactionState = TS_RECORDING;
    [recordButton setTitle:@"Recording..." forState:UIControlStateNormal];
    [self performSelector:@selector(updateVUMeter) withObject:nil afterDelay:0.05];
}

- (void)recognizerDidFinishRecording:(SKRecognizer *)recognizer
{
    NSLog(@"Recording finished.");
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateVUMeter) object:nil];
    [self setVUMeterWidth:0.];
    transactionState = TS_PROCESSING;
    [recordButton setTitle:@"Processing..." forState:UIControlStateNormal];
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithResults:(SKRecognition *)results
{
    NSLog(@"Got results.");
    NSLog(@"Session id [%@].", [SpeechKit sessionID]); // for debugging purpose: printing out the speechkit session id
    
    long numOfResults = [results.results count];
    
    transactionState = TS_IDLE;
    [recordButton setTitle:@"Record" forState:UIControlStateNormal];
    
    if (numOfResults > 0)
        
        self.resultsTextView.text    =[results firstResult];
    
    //    if (numOfResults > 1)
    //        alternativesDisplay.text = [[results.results subarrayWithRange:NSMakeRange(1, numOfResults-1)] componentsJoinedByString:@"\n"];
    
    if (results.suggestion){
        
        if(SYSTEM_VERSION_GREATER_THAN(@"8")){
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Suggestion"
                                                                           message:results.suggestion
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];

        }else{
            
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"Suggestion"
                                                             message:results.suggestion
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil, nil];
            
            [dialog show];
        }
        
    }
    
    //voiceSearch = nil;
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithError:(NSError *)error suggestion:(NSString *)suggestion
{
    NSLog(@"Got error.");
    NSLog(@"Session id [%@].", [SpeechKit sessionID]); // for debugging purpose: printing out the speechkit session id
    
    transactionState = TS_IDLE;
    [recordButton setTitle:@"Record" forState:UIControlStateNormal];
    
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:[error localizedDescription]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    if (suggestion) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Suggestion"
                                                                       message:suggestion
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    voiceSearch = nil;
}

- (IBAction)cancelClicked:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(handOutsideTouch:)]) {
        [delegate handOutsideTouch:self.resultsTextView.attributedText];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okClicked:(id)sender {
    
    if (delegate && [delegate respondsToSelector:@selector(handleSuccessfulConversion:)]) {
        [delegate handleSuccessfulConversion:self.resultsTextView.attributedText];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
