//
//  APPhotolibrary.h
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ALAsset;
@class ALAssetsLibrary;

typedef NSComparisonResult (^APAssetComparator)(ALAsset *asset1, ALAsset *asset2);

@interface APPhotolibrary : NSObject

@property (nonatomic, readonly) ALAssetsLibrary *assetLibrary;
@property (nonatomic, assign) BOOL sortAscending;
@property (nonatomic, copy) APAssetComparator sortComparator;

+ (BOOL)isAuthorized;
- (void)loadPhotosAsynchronously:(void (^)(NSArray *assets, NSError *error))callbackBlock;
-(void) createPhotoLibrary;
-(void) saveImageToAlbum:(UIImage*) Image;
+(UIImage *) loadImageFromAssertByUrl:(NSURL *)url completion:(void (^)(UIImage*)) completion;
@end
