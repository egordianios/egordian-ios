//
//  eGordianPhotoViewController.h
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "EGContainerViewController.h"

@import Photos;

@class APPhotolibrary;

@interface eGordianPhotoViewController : UIViewController<egordianContainerDelegate>
{
    APPhotolibrary *photoLibrary;
    BOOL isSelectionMode;
    int deletedPhotoCount;
}

@property (weak, nonatomic) IBOutlet UICollectionView *eGPhotoCollectionView;
//@property (nonatomic, strong) NSArray *assets;
@property (weak, nonatomic) IBOutlet UIButton *eGAddPhotoBtn;
@property (weak, nonatomic) IBOutlet UIButton *eGCameraBtn;
@property (weak, nonatomic) IBOutlet UIButton *eGSelectBtn;
@property (strong, nonatomic) NSMutableArray* eGSynchedPhotoArray;
@property (strong, nonatomic) NSMutableArray* egSelectedIndices;//contains the index of the photos which are selected
@property (weak, nonatomic) IBOutlet UIView *egLoadView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *egActivityIndicator;

- (IBAction)onBackBtn:(id)sender;
- (IBAction)onSelectBtn:(id)sender;
- (void) cancelDeletePhoto;

@end
