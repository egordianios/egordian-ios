//
//  APPhotolibrary.m
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "APPhotolibrary.h"
#import "ALAssetsGroup+Assets.h"
#import <UIKit/UIKit.h>

@implementation APPhotolibrary

#pragma mark - life cycle

- (id)init
{
    self = [super init];
    if (self)
    {
        _assetLibrary = [[ALAssetsLibrary alloc] init];
        self.sortAscending = FALSE;
    }
    return self;
}

#pragma mark - public

+ (BOOL)isAuthorized
{
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    return (status == ALAuthorizationStatusAuthorized ||
            status == ALAuthorizationStatusNotDetermined);
}

- (void)loadPhotosAsynchronously:(void (^)(NSArray *assets, NSError *error))callbackBlock
{
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    
//    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@""
                                                                 ascending:self.sortAscending
                                                                comparator:self.sortComparator];
    [self.assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                     usingBlock:^(ALAssetsGroup *group, BOOL *stop)
     {
         if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString:eGordianAlbum])
             [assets addObjectsFromArray:group.assets];
         if (!group)
         {
             [assets sortUsingDescriptors:@[descriptor]];
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                callbackBlock ? callbackBlock([assets copy], nil) : nil;
                            });
         }
         
         
     } failureBlock:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            callbackBlock ? callbackBlock(nil, error) : nil;
                        });
     }];
}

#pragma mark - private

- (APAssetComparator)sortComparator
{
    if (!_sortComparator)
    {
        _sortComparator = ^NSComparisonResult(ALAsset *asset1, ALAsset *asset2)
        {
            NSDate *date1 = [asset1 valueForProperty:ALAssetPropertyDate];
            NSDate *date2 = [asset2 valueForProperty:ALAssetPropertyDate];
            return [date1 compare:date2];
        };
    }
    return _sortComparator;
}

-(void) createPhotoLibrary
{
    ALAssetsLibrary* libraryFolder = [[ALAssetsLibrary alloc] init];
    [libraryFolder addAssetsGroupAlbumWithName:eGordianAlbum resultBlock:^(ALAssetsGroup *group)
    {
        NSLog(@"AAlbum created");
    } failureBlock:^(NSError *error)
    {
        NSLog(@"Error: creating album");
    }];
}

-(void) saveImageToAlbum:(UIImage*) Image
{
    
    __weak ALAssetsLibrary *lib = self.assetLibrary;
    
    UIImage* image = Image;
    [self.assetLibrary addAssetsGroupAlbumWithName:eGordianAlbum resultBlock:^(ALAssetsGroup *group) {
        
        ///checks if group previously created
        if(group == nil){
            
            //enumerate albums
            [lib enumerateGroupsWithTypes:ALAssetsGroupAlbum
                               usingBlock:^(ALAssetsGroup *g, BOOL *stop)
             {
                 //if the album is equal to our album
                 if ([[g valueForProperty:ALAssetsGroupPropertyName] isEqualToString:eGordianAlbum]) {
                     
                     //save image
                     [lib writeImageDataToSavedPhotosAlbum:UIImagePNGRepresentation(image) metadata:nil
                                           completionBlock:^(NSURL *assetURL, NSError *error) {
                                               
                                               //then get the image asseturl
                                               [lib assetForURL:assetURL
                                                    resultBlock:^(ALAsset *asset) {
                                                        //put it into our album
                                                        [g addAsset:asset];
                                                    } failureBlock:^(NSError *error) {
                                                        
                                                    }];
                                           }];
                     
                 }
             }failureBlock:^(NSError *error){
                 
             }];
            
        }
        
    } failureBlock:^(NSError *error) {
        
    }];
}

+(UIImage *) loadImageFromAssertByUrl:(NSURL *)url completion:(void (^)(UIImage*)) completion{
    
    
    __block UIImage* img;
    
    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    
    [assetLibrary assetForURL:url resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        Byte *buffer = (Byte*)malloc(rep.size);
        NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
        NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        img = [UIImage imageWithData:data];
        completion(img);
//        NSLog(@"img ::: %@", img);
    } failureBlock:^(NSError *err) {
        NSLog(@"Error: %@",[err localizedDescription]);
    }];
    
    return img;
}
@end