//
//  eGordianPhotoDetailViewController.h
//  eGordian-iOS
//
//  Created by Laila Varghese on 9/22/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APPhotolibrary.h"
#import "EG_PhotoDetail.h"
#import "EG_Photo.h"
#import "EGContainerViewController.h"

@import Photos;
@interface eGordianPhotoDetailViewController : UIViewController<UIScrollViewDelegate, egordianContainerDelegate>
{
    
//    NSInteger previousPage;
}


@property (nonatomic, strong) NSArray* eGphotoArray;
@property (nonatomic, strong) EG_PhotoDetail* egPhotoDetail;
@property (nonatomic, strong) ALAsset* eGCenterPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *eGPhotoImageView;
@property (assign) NSInteger currentIndex;
@property (nonatomic, strong) UIImage* image;
@property (weak, nonatomic) IBOutlet UIView *egLoadView;
@property (nonatomic, strong) EG_Photo* egSynchedPhoto;

-(void)imageAtIndex:(NSInteger)inImageIndex;
-(void)loadImageWithPhoto:(EG_Photo*)photo;
- (IBAction)onBack:(id)sender;

@end