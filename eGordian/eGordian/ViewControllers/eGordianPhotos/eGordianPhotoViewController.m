//
//  eGordianPhotoViewController.m
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianPhotoViewController.h"
#import "eGordianPhotoCollectionViewCell.h"
#import "APPhotolibrary.h"
#import "ALAsset+Photo.h"
#import "eGordianPhotoHeaderView.h"
#import "eGordianPhotoDetailViewController.h"
#import "APPhotoViewController.h"
#import "EG_photo.h"
#import "APPhotolibrary.h"
#import "EGContainerViewController.h"
#import "UIImageView+WebCache.h"
#import "EGAddToJobViewController.h"
#import "DashBoardViewController.h"

@interface eGordianPhotoViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end

@implementation eGordianPhotoViewController

@synthesize eGSynchedPhotoArray, egSelectedIndices;

/*
 * Method Name	: viewDidLoad
 * Description	: Called after the view has been loaded.
 * Parameters	: nil
 * Return value	: void
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.eGPhotoCollectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    isSelectionMode = FALSE;
    [self.eGPhotoCollectionView setAllowsMultipleSelection:YES];
    
    
    self.egSelectedIndices = [[NSMutableArray alloc] init];
    [self fetchPicture];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [self loadPhotos];
    
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.delegate = self;
    
    if([EGordianUtilities getRefreshStatusForPage:APP_USERPHOTO_REFRESH_FLAG])
    {
        [self fetchPicture];
    }
    [self selectStatusChange];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionViewDataSource
/*
 * Method Name	: numberOfSectionsInCollectionView
 * Description	: number of sections in the collectionview
 * Parameters	: UICollectionView
 * Return value	: NSInteger (number of sections)
 */
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;

}

/*
 * Method Name	: numberOfItemsInSection
 * Description	: number of items in the section
 * Parameters	: UICollectionView, section
 * Return value	: NSInteger (number of items in section)
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    int synchedPhotoCount = 0;
    if(self.eGSynchedPhotoArray)
        synchedPhotoCount = (int)[self.eGSynchedPhotoArray count];
//    return [self.assets count] + synchedPhotoCount;
    return synchedPhotoCount;
}

/*
 * Method Name	: cellForItemAtIndexPath
 * Description	: create the cell at Indexpath
 * Parameters	: UICollectionView, IndexPath
 * Return value	: UICollectionViewCell
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const reuseIdentifier = @"eGordianPhotoCollectionViewCell";
    
    eGordianPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    // Configure the cell
    
    EG_Photo* photoObj = [self.eGSynchedPhotoArray objectAtIndex:indexPath.row];
    //        cell.eGPhotoCollectionImageView.image = [UIImage imageWithData:photoObj.Thumbnail];
    
    
    [cell.eGPhotoCollectionImageView sd_setImageWithURL:[NSURL URLWithString:photoObj.UserPictureLink]
                                       placeholderImage:[UIImage imageWithData:photoObj.Thumbnail]];
    
    
    if(isSelectionMode)
    {
        cell.eGSelectImgVw.hidden = cell.selected ? NO : YES;
        
    }
    else
    {
        cell.eGSelectImgVw.hidden = YES;
    }
    //    [self selectStatusChange];
    
    return cell;
}


/*
 * Method Name	: didDeselectItemAtIndexPath
 * Description	: called when an item in the list is deselected
 * Parameters	: UICollectionView, IndexPath
 * Return value	: void
 */
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    eGordianPhotoCollectionViewCell *cell = (eGordianPhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    //select button is pressed
    if(isSelectionMode)
    {
        cell.eGSelectImgVw.hidden = YES;
        [cell setSelected:NO];
        if(self.egSelectedIndices)
        {
            [self.egSelectedIndices removeObject:[NSNumber numberWithInt:(int)indexPath.row]];
            
            if(![self.egSelectedIndices count])
            {
                EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
                [containerVC.addtoJobButton setEnabled:FALSE];
                [containerVC.deleteButton setEnabled:FALSE];
            }
        }
        
    }
    
}

/*
 * Method Name	: didSelectItemAtIndexPath
 * Description	: called when an item in the list is selected
 * Parameters	: UICollectionView, IndexPath
 * Return value	: void
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    eGordianPhotoCollectionViewCell *cell = (eGordianPhotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    if(isSelectionMode)
    {
        cell.eGSelectImgVw.hidden = NO;
        [cell setSelected:YES];
        
        if(!self.egSelectedIndices)
            self.egSelectedIndices = [[NSMutableArray alloc] init];
        [self.egSelectedIndices addObject:[NSNumber numberWithInt: (int)indexPath.row]];
        EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
        [containerVC.addtoJobButton setEnabled:TRUE];
        [containerVC.deleteButton setEnabled:TRUE];
    }
    else
    {
        if(![AppDelegate isConnectedToInternet])
        {
            [EGordianAlertView showFeatureNotAvailableMessage];
            return;
        }
        
        [[((UINavigationController *)self.parentViewController) viewControllers] objectAtIndex:0].hidesBottomBarWhenPushed = YES;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
        eGordianPhotoDetailViewController *eGPhotoDetailVC = (eGordianPhotoDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGordianPhotoDetailViewController"];
        
        EG_Photo* photo = [self.eGSynchedPhotoArray objectAtIndex:indexPath.row];
        eGPhotoDetailVC.egSynchedPhoto = photo;
        
        [self.navigationController pushViewController:eGPhotoDetailVC animated:YES];
        
    }
}

#pragma mark - Misc
/*
 * Method Name	: loadPhotos
 * Description	: get the latest image from the album
 * Parameters	: void
 * Return value	: void
 */
- (void)loadPhotos
{
//    [self.activity startAnimating];
//    
//    photoLibrary = [[APPhotolibrary alloc] init];
//    
//    [photoLibrary createPhotoLibrary];
//    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    
//    [formatter setDateFormat:@"LLLL dd"];
//    
//    __weak __typeof(self) weakSelf = self;
//    [photoLibrary loadPhotosAsynchronously:^(NSArray *asset, NSError *error)
//     {
//         [weakSelf.activity stopAnimating];
//         if (!error)
//         {
//             weakSelf.assets = [[NSArray alloc] initWithArray:asset];
//             
//             [weakSelf.eGPhotoCollectionView reloadData];
//         }
//         else
//         {
//             NSLog(@"Error loading photos");
//         }
//     }];
}

/*
 * Method Name	: selectStatusChange
 * Description	: on select status change Select/DeSelect
 * Parameters	: nil
 * Return value	: nil
 */
-(void) selectStatusChange
{
    
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];

    if(isSelectionMode)
    {
        [self.eGSelectBtn setTitle:@"Deselect" forState:UIControlStateNormal];
        
        [containerVC updateFooterView:@"PhotoList"];
    }
    else
    {
        [self.eGSelectBtn setTitle:@"Select" forState:UIControlStateNormal];
        [containerVC resetFooterView];
        [containerVC.addtoJobButton setEnabled:FALSE];
        [containerVC.deleteButton setEnabled:FALSE];
        self.egSelectedIndices = nil;
        [self.eGPhotoCollectionView reloadData];
    }
    
}

-(void) deletePhoto
{
    if(!self.egSelectedIndices) return;
    
    if(!self.egSelectedIndices.count)
        return;
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self deleteSynchedPhotos];
}

//called when delete is canceled.
-(void) cancelDeletePhoto
{
//    isSelectionMode = FALSE;
//    [self selectStatusChange];
}

-(void)addToJob
{
    if(!self.egSelectedIndices)
        return;
    
    NSMutableArray* selectedSynchedPhotosArr = [[NSMutableArray alloc] init];
    
    for (NSNumber *number in self.egSelectedIndices) {
        [selectedSynchedPhotosArr addObject:[self.eGSynchedPhotoArray objectAtIndex:([number intValue])]];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    EGAddToJobViewController *addJobVC = [storyboard instantiateViewControllerWithIdentifier:@"EGAddToJobViewController"];
    addJobVC.isFromPhotoList = TRUE;
    addJobVC.photosToAttachArray = selectedSynchedPhotosArr;
    [self.navigationController pushViewController:addJobVC animated:YES];
    [self completedPushing];
//    [self performSelector:@selector(completedPushing) withObject:nil afterDelay:.3];
}

-(void)completedPushing{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.addtoJobButton.hidden = TRUE;
    containerVC.deleteButton.hidden = TRUE;
    [containerVC resetFooterView];
}

-(void) deleteSynchedPhotos
{
    NSString* deletePhotoURL;
    
    for (int i=0; i< self.egSelectedIndices.count; i++) {
        [self startAnimating];
        deletePhotoURL = [NSString stringWithFormat:@"%@Users/%@/Pictures/%@", APP_HEADER_URL, [AppConfig getUserId], ((EG_Photo*) [self.eGSynchedPhotoArray objectAtIndex:[[self.egSelectedIndices objectAtIndex:i] intValue]]).Id];
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        [[DataDownloader sharedDownloader]getResponseFormUrl:deletePhotoURL serviceType:EGORDIAN_API_PHOTO_DELETE];
    }
    
    //this was when DeletePicture and DeletePictures API was working
    
//    if([indexArray count]>1)//multiple synched photos delete
//    {
//        [self startAnimating];
//        NSString *deletePhotoURL = [NSString stringWithFormat:@"%@Users/%@/Pictures", APP_HEADER_URL, [AppConfig getUserId]];
//        
//        NSMutableArray* jsonDictArr = [[NSMutableArray alloc] init];
//        for (NSNumber *number in indexArray) {
//            
//            NSDictionary* jsonDict = @{@"id": ((EG_Photo*) [self.eGSynchedPhotoArray objectAtIndex:[number intValue] ]).Id};
//            [jsonDictArr addObject:jsonDict];
//        }
//        
//        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
//        
//        [[DataDownloader sharedDownloader] deleteData:jsonDictArr andServiceURl:deletePhotoURL andserviceType:EGORDIAN_API_PHOTO_DELETE];
//    }
//    else//single synched photo delete
//    {
//        [self startAnimating];
//        deletePhotoURL = [NSString stringWithFormat:@"%@Users/%@/Pictures/%@", APP_HEADER_URL, [AppConfig getUserId], ((EG_Photo*) [self.eGSynchedPhotoArray objectAtIndex:0]).Id];
//        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
//        [[DataDownloader sharedDownloader]getResponseFormUrl:deletePhotoURL serviceType:EGORDIAN_API_PHOTO_DELETE];
//    }
    
    
}

-(void) deleteLocalPhoto:(NSArray*) selectedIndexArray
{
//    for (NSNumber *number in selectedIndexArray)
//    {
//        if(self.assets.count < [number intValue])
//            continue;
//        
//        [self startAnimating];
//        ALAsset * asset = [self.assets objectAtIndex:[number intValue]];
//        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc]init];
//        NSURL *url = [asset valueForProperty:ALAssetPropertyAssetURL];
//        PHFetchResult *fetchResult = [PHAsset fetchAssetsWithALAssetURLs:@[url] options:fetchOptions];
//        PHAsset* phAsset = fetchResult.firstObject ;
//        
//        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
//            // Create a change request from the asset to be modified.
//            BOOL req = [phAsset canPerformEditOperation:PHAssetEditOperationDelete];
//            if (req)
//            {
//                NSLog(@"true");
//                [PHAssetChangeRequest deleteAssets:@[phAsset]];
//            }
//            
//        } completionHandler:^(BOOL success, NSError *error) {
//            [self loadPhotos];
//            isSelectionMode = FALSE;
//            [self selectStatusChange];
//            [self stopAnimating];
//        }];
//    }
}



#pragma mark - Button Handlers
/*
 * Method Name	: onBackBtn
 * Description	: on back button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onBackBtn:(id)sender
{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    [containerVC resetFooterView];
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    for (UIViewController *vc in self.navigationController.viewControllers)
//    {
//        if([vc isKindOfClass:[DashBoardViewController class]])
//        {
//            [self.navigationController popToViewController:vc animated:NO];
//        if([vc isKindOfClass:[eGordianPhotoViewController class]])
//            [self.navigationController popToViewController:vc animated:NO];
//        }
//        else
//        break;
//    }
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
* Method Name	: onSelectBtn
* Description	: on Select button click
* Parameters	: id - sender
* Return value	: IBAction
*/
- (IBAction)onSelectBtn:(id)sender
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    isSelectionMode =! isSelectionMode;
    
    [self selectStatusChange];
    
    [self.eGPhotoCollectionView reloadData];
    
}

-(void) fetchPicture
{
    [self startAnimating];
    NSString* photoURL = [NSString stringWithFormat:@"%@Users/%@/Pictures?pageSize=25&pageNumber=1", APP_HEADER_URL, [AppConfig getUserId]];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:photoURL serviceType:EGORDIAN_API_PHOTO_THUMBNAIL];
    
}

-(void)startAnimating{
    
    [self.egLoadView setHidden:NO];
    [self.egLoadView setUserInteractionEnabled:YES];
    [self.view bringSubviewToFront:self.egLoadView];
    [self.egActivityIndicator startAnimating];
}

-(void)stopAnimating{
    [self.egActivityIndicator stopAnimating];
    [self.egLoadView setHidden:YES];
}

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    switch (type) {
        case EGORDIAN_API_PHOTO_THUMBNAIL:
        {
            self.eGSynchedPhotoArray = [[NSMutableArray alloc] initWithArray:responseArray];
            [self.eGPhotoCollectionView reloadData];
            isSelectionMode = FALSE;
            [self selectStatusChange];
            [self stopAnimating];
            deletedPhotoCount = 0;
            self.egSelectedIndices = nil;
            [EGordianUtilities updateRefreshStatus:NO forPage:APP_USERPHOTO_REFRESH_FLAG];
        }
            break;
            
        case EGORDIAN_API_PHOTO_DELETE:
        {
            deletedPhotoCount++;
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_USERPHOTO_REFRESH_FLAG];
            
            if(deletedPhotoCount == self.egSelectedIndices.count)//for multiple photo delete. As multiple photo delete API is not working.
            {
                
                isSelectionMode = FALSE;
                [self selectStatusChange];
                [self stopAnimating];
                deletedPhotoCount = 0;
                self.egSelectedIndices = nil;
                [self fetchPicture];
            }
        }
            break;
            
        default:
            break;
    }
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type
{
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    [EGordianUtilities updateRefreshStatus:YES forPage:APP_USERPHOTO_REFRESH_FLAG];
    NSLog(@"Error Message---%@",alertMessage);
    
    if([alertMessage rangeOfString:@"No user pictures found for the User Id"].location != NSNotFound)
    {
        self.eGSynchedPhotoArray = nil;
        [self.eGPhotoCollectionView reloadData];
    }
    isSelectionMode = FALSE;
    [self selectStatusChange];
    [self stopAnimating];
}

@end
