//
//  ALAssetsGroup+Assets.m
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//
#import "ALAssetsGroup+Assets.h"

@implementation ALAssetsGroup (Assets)

- (NSArray *)assets
{
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    [self enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop)
    {
        if (result)
        {
            [assets addObject:result];
        }
    }];
    return [assets copy];
}



@end
