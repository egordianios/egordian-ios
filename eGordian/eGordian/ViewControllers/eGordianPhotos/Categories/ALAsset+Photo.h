//
//  ALAsset+Photo.h
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>

@interface ALAsset (Photo)

- (UIImage *)photoThumbnail;
- (void)loadPhotoInFullResolutionAsynchronously:(void (^)(UIImage *image))callbackBlock;
- (void)loadPhotoInFullScreenAsynchronously:(void (^)(UIImage *image))callbackBlock;
- (UIImage*)loadPhotoSynchronously:(BOOL)isFullResolution;
- (BOOL)isEqual:(id)obj ;
@end
