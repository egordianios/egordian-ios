//
//  ALAssetsGroup+LoadAsynchronously.h
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsGroup (Assets)

@property (nonatomic, readonly) NSArray *assets;

@end
