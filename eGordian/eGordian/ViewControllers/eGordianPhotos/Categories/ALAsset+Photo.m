//
//  ALAsset+Photo.m
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "ALAsset+Photo.h"

@implementation ALAsset (Photo)

- (UIImage *)photoThumbnail
{
    if (self.thumbnail)
    {
        return [UIImage imageWithCGImage:self.aspectRatioThumbnail scale:UIScreen.mainScreen.scale
                                                  orientation:UIImageOrientationUp];
    }
    return nil;
}

- (void)loadPhotoInFullResolutionAsynchronously:(void (^)(UIImage *image))callbackBlock
{
    [self loadPhotoAsynchronously:callbackBlock isFullResolution:YES];
}

- (void)loadPhotoInFullScreenAsynchronously:(void (^)(UIImage *image))callbackBlock
{
    [self loadPhotoAsynchronously:callbackBlock isFullResolution:NO];
}

#pragma mark - private

- (void)loadPhotoAsynchronously:(void (^)(UIImage *))callbackBlock
               isFullResolution:(BOOL)isFullResolution
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        ALAssetRepresentation *representation = self.defaultRepresentation;
        CGImageRef cgImage = isFullResolution ? representation.fullResolutionImage :
                             representation.fullScreenImage;
        UIImage *image = nil;
        if (cgImage)
        {
            image = [UIImage imageWithCGImage:cgImage scale:UIScreen.mainScreen.scale
                                  orientation:UIImageOrientationUp];
        }
        dispatch_async(dispatch_get_main_queue(), ^
        {
            callbackBlock ? callbackBlock(image) : nil;
        });
    });
}


- (UIImage*)loadPhotoSynchronously:(BOOL)isFullResolution
{
    
    ALAssetRepresentation *representation = self.defaultRepresentation;
    CGImageRef cgImage = isFullResolution ? representation.fullResolutionImage :
    representation.fullScreenImage;
    UIImage *image = nil;
    if (cgImage)
    {
        image = [UIImage imageWithCGImage:cgImage scale:UIScreen.mainScreen.scale
                              orientation:UIImageOrientationUp];
    }
    return image;
}


- (BOOL)isEqual:(id)obj {
    if(![obj isKindOfClass:[ALAsset class]])
        return NO;
    
    if([[[self defaultRepresentation].url absoluteString] isEqualToString:[[obj defaultRepresentation].url absoluteString]])
        return YES;
    else
        return NO;
}
@end
