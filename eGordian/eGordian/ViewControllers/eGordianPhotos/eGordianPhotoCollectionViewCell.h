//
//  eGordianPhotoCollectionViewCell.h
//  eGordian-iOS
//
//  Created by Laila on 14/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGordianPhotoCollectionViewCell : UICollectionViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *eGPhotoCollectionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *eGSelectImgVw;
//@property (weak, nonatomic) IBOutlet UIImageView *eGSyncImgVw;

@end
