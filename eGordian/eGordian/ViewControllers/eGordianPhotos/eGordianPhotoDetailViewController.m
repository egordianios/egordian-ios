//
//  eGordianPhotoDetailViewController.m
//  eGordian-iOS
//
//  Created by Laila Varghese on 9/22/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianPhotoDetailViewController.h"
#import "ALAsset+Photo.h"
#import <UIImageView+WebCache.h>
#import "EGContainerViewController.h"
#import "EGAddToJobViewController.h"

@interface eGordianPhotoDetailViewController ()

@end

@implementation eGordianPhotoDetailViewController

@synthesize eGphotoArray, eGCenterPhoto, egSynchedPhoto, currentIndex, image, egPhotoDetail;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.delegate = self;
    [containerVC.deleteButton setEnabled:TRUE];
    [containerVC.addtoJobButton setEnabled:TRUE];
    
    if(self.egSynchedPhoto)
        [self loadImageWithPhoto:self.egSynchedPhoto];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.delegate = self;
    [containerVC.addtoJobButton setEnabled:TRUE];
    [containerVC updateFooterView:@"PhotoDetail"];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    [containerVC resetFooterView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)imageAtIndex:(NSInteger)inImageIndex{
    
    ALAsset *asset = [self.eGphotoArray objectAtIndex:inImageIndex];
    self.image = [[UIImage alloc] init];
    self.image = [asset loadPhotoSynchronously:NO];
    self.eGPhotoImageView.image = self.image;
    
}

/*-(void)loadImageWithPhoto:(EG_Photo*)photo
{
    if(!photo) return;
    
    if (photo.UserPictureLink) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = self.eGPhotoImageView;
        [self.eGPhotoImageView sd_setImageWithURL:[NSURL URLWithString:photo.UserPictureLink]
                                           placeholderImage:[UIImage imageWithData:photo.Thumbnail]
                                                    options:SDWebImageProgressiveDownload
                                                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                       if (!activityIndicator) {
                                                           [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                           activityIndicator.center = weakImageView.center;
                                                           [activityIndicator startAnimating];
                                                       }
                                                   }
                                                  completed:^(UIImage *myImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                      [activityIndicator removeFromSuperview];
                                                      activityIndicator = nil;

                                                  }];
    }
}*/

-(void) loadImageWithPhoto:(EG_Photo *)photo
{
    [self startAnimating];
    NSString* photoURL = photo.UserPictureLink;
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:photoURL serviceType:EGORDIAN_API_PHOTO_DETAIL];
}

- (IBAction)onBack:(id)sender
{
    [[((UINavigationController *)self.parentViewController) viewControllers] objectAtIndex:0].hidesBottomBarWhenPushed = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) addToJob
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    EGAddToJobViewController *addJobVC = [storyboard instantiateViewControllerWithIdentifier:@"EGAddToJobViewController"];
    if(self.egSynchedPhoto)
    {
        addJobVC.isFromPhotoList = FALSE;
        addJobVC.currentPhoto = self.egSynchedPhoto;
        
        [self.navigationController pushViewController:addJobVC animated:YES];
        [self completedPushing];
//        [self performSelector:@selector(completedPushing) withObject:nil afterDelay:.3];
    }
}

-(void)completedPushing{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.addtoJobButton.hidden = TRUE;
    containerVC.deleteButton.hidden = TRUE;
    [containerVC resetFooterView];
}


-(void) deletePhoto
{
    [self startAnimating];
    if(self.egSynchedPhoto)
    {
        NSString* deletePhotoURL = [NSString stringWithFormat:@"%@Users/%@/Pictures/%@", APP_HEADER_URL, [AppConfig getUserId], ((EG_Photo*)self.egSynchedPhoto).Id];
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        [[DataDownloader sharedDownloader]getResponseFormUrl:deletePhotoURL serviceType:EGORDIAN_API_PHOTO_DELETE];
        
    }
    else
    {
        ALAsset * asset = [self.eGphotoArray objectAtIndex:currentIndex];
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc]init];
        NSURL *url = [asset valueForProperty:ALAssetPropertyAssetURL];
        PHFetchResult *fetchResult = [PHAsset fetchAssetsWithALAssetURLs:@[url] options:fetchOptions];
        PHAsset* phAsset = fetchResult.firstObject ;
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            // Create a change request from the asset to be modified.
            BOOL req = [phAsset canPerformEditOperation:PHAssetEditOperationDelete];
            if (req) {
                NSLog(@"true");
                [PHAssetChangeRequest deleteAssets:@[phAsset]];
            }
        } completionHandler:^(BOOL success, NSError *error) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
}

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    [self stopAnimating];
    switch (type) {
           
        case EGORDIAN_API_PHOTO_DETAIL:
        {
            self.egPhotoDetail = [responseArray objectAtIndex:0];
            
            self.eGPhotoImageView.image = [UIImage imageWithData:self.egPhotoDetail.Image];
        }
            break;
            
        case EGORDIAN_API_PHOTO_DELETE:
        {
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_USERPHOTO_REFRESH_FLAG];
            [[((UINavigationController *)self.parentViewController) viewControllers] objectAtIndex:0].hidesBottomBarWhenPushed = NO;
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}



-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type
{
    [self stopAnimating];
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message---%@",alertMessage);
    
    [self stopAnimating];
}

-(void)startAnimating{
    
    [self.egLoadView setHidden:NO];
    [self.egLoadView setUserInteractionEnabled:YES];
    [self.view bringSubviewToFront:self.egLoadView];
}

-(void)stopAnimating{
    [self.egLoadView setHidden:YES];
}
@end
