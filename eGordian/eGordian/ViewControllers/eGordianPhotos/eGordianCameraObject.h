//
//  eGordianCameraObject.h
//  eGordian-iOS
//
//  Created by Laila on 11/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OverlayView.h"

@interface eGordianCameraObject : UIImagePickerController
{
    
}

@property (strong, nonatomic)  OverlayView *overlay;
@property (assign, nonatomic) BOOL isFromJobDetails;
@property (assign,nonatomic)  BOOL isCalledFromOffline;

@end
