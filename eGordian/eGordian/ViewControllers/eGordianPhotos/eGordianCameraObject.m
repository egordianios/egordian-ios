//
//  eGordianCameraObject.m
//  eGordian-iOS
//
//  Created by Laila on 11/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "eGordianCameraObject.h"
#import "OverlayView.h"
#import "AppDelegate.h"

@implementation eGordianCameraObject

@synthesize overlay, isFromJobDetails,isCalledFromOffline;
#define CAMERA_TRANSFORM                    1.333333

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isFromJobDetails = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails;
    
    [self intializeCameraWithOverlay];
}



-(void) intializeCameraWithOverlay
{

    self.overlay = [[OverlayView alloc] initWithFrame:self.view.frame];
   
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        self.sourceType = UIImagePickerControllerSourceTypeCamera;
        //hide all controls
        self.showsCameraControls = NO;
        self.navigationBarHidden = YES;
        self.toolbarHidden = YES;
        self.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        
        [self setDelegate:self.overlay];
        
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0.0, 71.0); //This slots the preview exactly in the middle of the screen by moving it down 71 points
        self.cameraViewTransform = translate;
        
        CGAffineTransform scale = CGAffineTransformScale(translate, CAMERA_TRANSFORM, CAMERA_TRANSFORM);
        self.cameraViewTransform = scale;
        self.cameraOverlayView = overlay;
        self.cameraOverlayView.frame =self.view.frame;
        overlay.egImagePickerController = self;
    }
    else
    {
        
        // UIAlertView…
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
                                                        message:@"No Camera Available."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }
    

}



@end
