//
//  eGordianPhotoHeaderView.h
//  eGordian-iOS
//
//  Created by Laila Varghese on 9/17/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGordianPhotoHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *eG_PhotoCollection_DateLbl;

@end
