//
//  eGordianAddContactsViewController.h
//  eGordian-iOS
//
//  Created by Laila Varghese on 9/3/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_ContactDetail.h"
#import <TPKeyboardAvoidingScrollView.h>
#import "DataDownloader.h"

@interface eGordianAddContactsViewController : UIViewController<DataDownloaderDelegate, UIAlertViewDelegate, UITextFieldDelegate>
{
    CGFloat animatedDistance;
    BOOL keyboardIsShown;
}

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *eGordianAddContactScrollView;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactsName;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactsStreet;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactsCity;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactsState;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactZipCode;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactEMail;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactPhone;
@property (weak, nonatomic) IBOutlet UITextField *eGordianAddContactMobile;
@property (weak, nonatomic) IBOutlet UILabel *eGHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *eGordianEditContactLbl;
@property (nonatomic, strong) NSString *strJobOrderID;
@property (weak, nonatomic) UIView *activeTextView;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;


@property (assign, nonatomic) BOOL isEditing;//true if the view is in Edit Contact View mode
@property (strong, nonatomic) EG_ContactDetail* egContactDetail;

- (IBAction)onBackBtn:(id)sender;
- (IBAction)onSaveBtn:(id)sender;

@end
