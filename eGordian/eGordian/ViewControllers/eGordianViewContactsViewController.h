//
//  eGordianViewContactsViewController.h
//  eGordian-iOS
//
//  Created by Laila on 07/09/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "EG_ContactDetail.h"

@interface eGordianViewContactsViewController : UIViewController<MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *eGordianViewContactsTableView;

@property (weak, nonatomic) IBOutlet UILabel *contactDisplayNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;

@property (nonatomic, strong) EG_ContactDetail* eGContactDetail;

@property (nonatomic, strong) NSString *jobcontactURl;

@property (nonatomic, strong) NSString *jobcontactCompanyName;

@property (weak, nonatomic) IBOutlet UIView *callView;

@property (weak, nonatomic) IBOutlet UILabel *displayNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;

@property (nonatomic, strong) NSString *jobOrderId;

@property (nonatomic, strong) NSString *contactDetailURL;

@property (nonatomic, strong) NSString *redirectVia;


@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

- (IBAction)callButtonTappedToRedirectDailer:(id)sender;

- (IBAction)cancelButtonTapped:(id)sender;

@property (strong, nonatomic) UIActivityIndicatorView *spinner;
 // this dict is used especially for email, B. Phone and Mobile to accomodate the scenario when there is no email\b.phone\Mobile is available.
@property (strong, nonatomic) NSMutableDictionary* dataDict;

- (IBAction)onViewContactsBackBtn:(id)sender;
- (IBAction)eGordianViewContactsAddressBtn:(id)sender;
- (IBAction)onEditBtn:(id)sender;

@end
