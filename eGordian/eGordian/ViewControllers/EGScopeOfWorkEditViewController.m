//
//  EGScopeOfWorkEditViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/31/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGScopeOfWorkEditViewController.h"
#import "JobOrderDetail.h"
#import "JobOrderScope.h"
#import "EGJobDocumentsViewController.h"
#import "EGContainerViewController.h"
#import "AttachPhoto.h"
#import "AttachPhotoDAO.h"
#import "EGSpeechToTextViewController.h"
#import "UIViewController+MBOverCurrentContextModalPresenting.h"
@interface EGScopeOfWorkEditViewController ()<egordianContainerDelegate,SpeechToTextDelegate>


@end

@implementation EGScopeOfWorkEditViewController
@synthesize scopeOfWorkString,isCalledFromOffline,strDetailedScopeLink;
@synthesize selectedJobOrder;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setIntialUI];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    [super viewWillAppear:TRUE];
}

-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

-(void)setIntialUI{
    
    [self.scrollView contentSizeToFit];
    
    [self.eGScopeEditInputModeSeg addTarget:self
                         action:@selector(searchSegmentedControl:)
               forControlEvents:UIControlEventValueChanged];
    
    self.eGScopeEditTxtVw.attributedText =[[NSAttributedString alloc] initWithString:@""];
    
    if(isCalledFromOffline)
    {//Adding notification to get offline photo details
        //[self.eGScopeEditInputModeSeg setEnabled:NO forSegmentAtIndex:0];
        [[EGContainerViewController sharedContainerView]setDelegate:self];
        
        if ([selectedJobOrder isKindOfClass:[JobOrder class]])
        {
            JobOrder *selJobOrder =(JobOrder *)selectedJobOrder;
            JobOrderScope *scope =(JobOrderScope *)selJobOrder.jobScope;
            if (scope)
            {
                //Populate Data
                self.eGScopeEditTxtVw.attributedText =scope.jobScopeDescription;
            }
            return;
        }
    }
    
    self.eGScopeEditTxtVw.attributedText = self.scopeOfWorkString;
}
- (IBAction)searchSegmentedControl:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
//            [self redirectToSpeechToText];
            NSLog(@"First was selected");
            break;
        case 1:
            
            break;
                default:
            break;
    }
}
-(void)redirectToSpeechToText{
    
    
    if(![AppDelegate isConnectedToInternet]){
        [EGordianAlertView showFeatureNotAvailableMessage];
    }
    else{
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EGSpeechToTextViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"EGSpeechToTextViewController"];
        [vc setDelegate:self];
        if([self.eGScopeEditTxtVw isFirstResponder])
            [self.eGScopeEditTxtVw resignFirstResponder];
        
        
        [self MBOverCurrentContextPresentViewController:vc animated:YES completion:nil];

    }
    
    
}
-(void)takePhotoClickedOffline{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotOfflinePhoto:) name:EGOfflineImageCapturedNotification object:nil];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"isOpenedOffline"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSave:(id)sender{
    
    if(self.eGScopeEditTxtVw.text.length > 0)
    {
        if  (isCalledFromOffline){
            [self.eGScopeEditTxtVw resignFirstResponder];
            JobOrder *selJobOrder =(JobOrder *)selectedJobOrder;
            JobOrderScope *scope =(JobOrderScope *)selJobOrder.jobScope;
            if (scope) {
                //Populate Data
                scope.jobScopeDescription =self.eGScopeEditTxtVw.attributedText;
                //scope.scopeStatus =JT_ALREADYPRESENT;
                selJobOrder.syncStatus =JS_WAITINGTOSYNC;
                [CoreDataBase saveActiveContext];
                
                NSDictionary *dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:YES]};
            
                
                [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineRecordUpdatedNotification object:nil userInfo:dictToPass];
            }
            return;
        }
        else{
            
            if(![AppDelegate isConnectedToInternet])
            {
                [EGordianAlertView showFeatureNotAvailableMessage];
                return;
            }
            
            [self saveScopeOfWork];
        }
    }
    else{
        UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                              message:@"Scope of work can't be empty."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil];
        
        [displayAlert show];
    }
}

-(void)gotOfflinePhoto:(NSNotification *)notification{
    
    NSDictionary *inputParamss =[notification userInfo];
    UIImage *originalPic =[inputParamss valueForKey:@"originalImage"];
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
    NSString *fileName = [NSString stringWithFormat:@"MyImage %@",[dateFormatter stringFromDate:currentDate]];;
    NSData *imageData =UIImageJPEGRepresentation(originalPic, 0.1);//UIImagePNGRepresentation(originalPic);
    // Get the path of the application's documents directory.
    if (imageData){
        NSManagedObjectContext *ctx =[CoreDataBase activeContext];
    JobOrder *selJobOrder =(JobOrder *)selectedJobOrder;
    NSDictionary *dictToInsert =@{@"useriD":[AppConfig getUserId],
                                  @"jobId":selJobOrder.jobId,
                                  //@"filePath":[saveLocation absoluteString],
                                  @"pictureName":fileName,
                                  @"ownerId":selJobOrder.ownerId,
                                  @"imageData":imageData
                                  };
    AttachPhoto *row =[AttachPhotoDAO insertIfDoesNotExists:dictToInsert inContext:ctx];
    [CoreDataBase saveActiveContext];
        
//        dispatch_async(dispatch_get_main_queue(), ^{

            NSDictionary *dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:YES]};
            
            [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineRecordUpdatedNotification object:nil userInfo:dictToPass];
            
//        });
}
    
    
}

- (NSURL *)documentsDirectoryURL
{
    NSError *error = nil;
    NSURL *url = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                        inDomain:NSUserDomainMask
                                               appropriateForURL:nil
                                                          create:NO
                                                           error:&error];
    if (error) {
        // Figure out what went wrong and handle the error.
    }
    
    return url;
}



-(void)saveScopeOfWork
{
    [self actvityStart];
    
    NSDictionary *dictToPass =@{@"Text":self.eGScopeEditTxtVw.text,
                                @"Description":@""};
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictToPass
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:self.strDetailedScopeLink andserviceType:EGORDIAN_API_SCOPEOFWORK_UPDATE];
}


#pragma mark - Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_SCOPEOFWORK_UPDATE){
        
        [self actvityStop];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDOCUMENTS_REFRESH_FLAG];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSArray *navStackControllers = [self.navigationController viewControllers];
        
        for (UIViewController *currVC in navStackControllers) {
            if([currVC isKindOfClass:[EGJobDocumentsViewController class]]){
                [self.navigationController popToViewController:currVC animated:YES];
                break;
            }
        }
    }
}
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{

    [self actvityStop];
    
    NSString *alertMessage = @"";
    
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message--%@",alertMessage);
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

#pragma mark Speechto text Response Delegate

-(void)handleSuccessfulConversion:(NSAttributedString *)recievedString{
    if (recievedString ) {
        self.eGScopeEditTxtVw.attributedText =recievedString;
    }
        [self.eGScopeEditInputModeSeg setSelectedSegmentIndex:UISegmentedControlNoSegment];
}
- (void)handOutsideTouch:(NSAttributedString *)recievedString{
    if (recievedString ) {
        self.eGScopeEditTxtVw.attributedText =recievedString;
    }
    [self.eGScopeEditInputModeSeg setSelectedSegmentIndex:UISegmentedControlNoSegment];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
