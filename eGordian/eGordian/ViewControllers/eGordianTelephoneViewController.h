//
//  eGordianTelephoneViewController.h
//  eGordian-iOS
//
//  Created by Laila on 26/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGordianTelephoneViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *eGCallView;
@property (weak, nonatomic) IBOutlet UILabel *eGPhoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *eGNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *egCancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *egCallBtn;
- (IBAction)onCallBtn:(id)sender;
- (IBAction)onCancelBtn:(id)sender;

@end
