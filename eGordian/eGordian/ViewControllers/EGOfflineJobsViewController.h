//
//  OfflineJobsViewController.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/21/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreData;
@interface EGOfflineJobsViewController : UIViewController<NSFetchedResultsControllerDelegate>{
    
@private
    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;
}

@end
