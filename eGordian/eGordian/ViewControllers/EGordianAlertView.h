//
//  EGordianAlertView.h
//  eGordian-iOS
//
//  Created by Laila on 12/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EGordianAlertView : UIAlertView

+ (EGordianAlertView *)getSingletonInstance;

+ (void)alertWithMessage : (NSString *)message withDelegate:(id)delegate;
+ (void)alertWithMessage : (NSString *)message;
+ (void)showNetworkFailureMessage;
+ (void)showFeatureNotAvailableMessage;
@end
