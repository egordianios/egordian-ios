//
//  EGAddToJobViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/22/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrderDetail.h"
#import "EG_Photo.h"

@interface EGAddToJobViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    int pgNo;
    int previousPage;
    int pgSize;
    int _totalPages;
}

@property (weak, nonatomic) IBOutlet UITableView *egJobListTableView;
@property (weak, nonatomic) IBOutlet UILabel *egJobSearchLbl;
@property (weak, nonatomic) IBOutlet UITextField *egJobSearchTextField;
@property (weak, nonatomic) IBOutlet UIButton *egJobSearchBtn;
@property (weak, nonatomic) IBOutlet UIView *egSearchView;
//@property (assign, nonatomic) BOOL isSynchedPhoto;
@property (assign, nonatomic) BOOL isFromPhotoList;//this screen is called from PhotoList page and PhotoDetail page. isFromPhotoList = TRUE if called from Photo List page.
@property (strong, nonatomic) UIImage* egImage;//image to be synched to the server
@property (weak, nonatomic) IBOutlet UIView *egLoadView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIView *egAddToJobView;

@property (strong, nonatomic) UILabel *infoTextLabel;
@property (strong, nonatomic) NSMutableArray* jobListArray;
@property (strong, nonatomic) EG_JobOrder* selectedJob;
@property (strong, nonatomic) NSArray* photosToAttachArray;//contains array of dictionary both synched and unsynched photos to attach.
@property (weak, nonatomic) IBOutlet UIButton *eGSearchCancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *eGSearchBtn;
@property (strong, nonatomic) EG_Photo* currentPhoto;

- (IBAction)onSave:(id)sender;

- (IBAction)onSearchCancelBtn:(id)sender;

- (IBAction)addToJobBackTapped:(id)sender;

- (IBAction)onSearch:(id)sender;
- (IBAction)textChanged:(id)sender;

@end
