//
//  EGContainerViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/21/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGContainerViewController.h"
#import "DashBoardViewController.h"
#import "APIClient.h"
#import "AppDelegate.h"
#import "eGordianCameraObject.h"
#import "EGAddToJobViewController.h"
#import "eGordianPhotoViewController.h"
#import "EGWebViewController.h"
#import "EGContainerNavigationVC.h"
#import "DataDownloader.h"
#define LOGOUT_ALERT_TAG 999999
#define kStringArray [NSArray arrayWithObjects:@" About Us ",@" Support ", @" Logout ", nil]
#define kImageArray [NSArray arrayWithObjects:@"",@"", @"", nil]

@interface EGContainerViewController (){
    PopoverView *morePopOver;
    BOOL deleteButtonFlag;
    NSInteger previousSelectedIndex;
}

@end

@implementation EGContainerViewController

@synthesize delegate;

static EGContainerViewController *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (EGContainerViewController  *)sharedContainerView
{
    if(!_sharedInstance)
    {
        dispatch_once(&oncePredicate, ^
                      {
                          _sharedInstance = [[super allocWithZone:nil] init];
                      });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance = nil;
    oncePredicate = 0;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedContainerView];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
    return self;
}

- (unsigned)retainCount
{
    return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}
#endif
#pragma mark -
#pragma mark Custom Methods

- (id)init
{
    @synchronized(self)
    {
        if ((self = [super init]))
        {
        }
        return self;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMoreIcon:) name:EGOfflineRecordUpdatedNotification object:nil];
}

-(void) viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)eGMainFooterHomeTapped:(id)sender {
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
    UINavigationController *navController = nil;
    
    if(self.childViewControllers.count>0)
        navController = (UINavigationController*)[self.childViewControllers lastObject];
    
    [navController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)eGMainFooterCameraTapped:(id)sender {
    if(![AppDelegate isConnectedToInternet])//not connected to internet
    {
        //Show Alert
        if ( self.delegate && [self.delegate respondsToSelector:@selector(takePhotoClickedOffline)]) {
            //offline and from document edit
            [self loadCamera];
            [self.delegate takePhotoClickedOffline];
            return;
        }else{
            
            [EGordianAlertView showFeatureNotAvailableMessage];
            
        }
    }
    else
    {
        if ( self.delegate && [self.delegate respondsToSelector:@selector(takePhotoClickedOffline)]) {
            //offline and from document edit
            [self loadCamera];
            [self.delegate takePhotoClickedOffline];
        }
        else
        {//Online
            [self loadCamera];
        }
    }
}

-(void) loadCamera
{
    UINavigationController *navController = nil;
    if(self.childViewControllers.count>0)
        navController = (UINavigationController*)[self.childViewControllers lastObject];
    
    eGordianCameraObject *cameraVC =  [[eGordianCameraObject alloc] init];
    [navController presentViewController:cameraVC animated:YES completion:nil];
}

- (IBAction)eGMainFooterMoreTapped:(id)sender {
    
    UIButton *moreButton = (UIButton *)sender;
    
    if(morePopOver)
        [morePopOver dismiss:YES];
    
    morePopOver = [PopoverView showPopoverAtPoint:CGPointMake(moreButton.frame.origin.x+moreButton.frame.size.width/2, [[UIScreen mainScreen] applicationFrame].size.height-moreButton.frame.size.height/1.5)
                                  inView:self.view
                         withStringArray:kStringArray
                                delegate:self];
}

- (IBAction)eGFooterAddToJobTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIViewController* photoListVC = nil;
    UINavigationController *navController = nil;
    if(self.childViewControllers.count>0)
        navController = (UINavigationController*)[self.childViewControllers lastObject];
    
    
    photoListVC = [navController.viewControllers lastObject];
    if(photoListVC)
    {
        if(![photoListVC isKindOfClass:[EGAddToJobViewController class]])
        {
            if ([[self delegate] respondsToSelector:@selector(addToJob)]) {
                [[self delegate] addToJob];
            }
        }
    }
    else
        if ([[self delegate] respondsToSelector:@selector(addToJob)])
            [[self delegate] addToJob];
    
    //    [self resetFooterView];
    //
    //    UINavigationController *navController = nil;
    //    if(self.childViewControllers.count>0)
    //        navController = (UINavigationController*)[self.childViewControllers lastObject];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    //    EGAddToJobViewController *addJobVC = [storyboard instantiateViewControllerWithIdentifier:@"EGAddToJobViewController"];
    //    [navController pushViewController:addJobVC animated:YES];
}

-(void)onThumbnail
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [self resetFooterView];
    
    UINavigationController *navController = nil;
    if(self.childViewControllers.count>0)
        navController = (UINavigationController*)[self.childViewControllers lastObject];
    
    eGordianPhotoViewController *photosCollectionVC = nil;
    
    UIViewController* photoListVC = [navController.viewControllers lastObject];
    if(![photoListVC isKindOfClass:[eGordianPhotoViewController class]])
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
        photosCollectionVC = (eGordianPhotoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianPhotosViewController"];
        
        [navController pushViewController:photosCollectionVC animated:YES];
    }
    
}

- (IBAction)eGFooterDeleteTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    NSString* title = @"Are You Sure?";
    NSString* message = @"Please confirm that you wish to Delete these items from the eGordian.";
    NSString* cancelBtnText = @"Cancel Deletion";
    NSString* deleteBtnText = @"Delete Photo";
    
    if ([UIAlertController class])
    {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:title
                                   message:message
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:deleteBtnText style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       if ([[self delegate] respondsToSelector:@selector(deletePhoto)]) {
                                                           [[self delegate] deletePhoto];
                                                       }
                                                   }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelBtnText style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                          
                                                           if ([[self delegate] respondsToSelector:@selector(cancelDeletePhoto)])
                                                               [[self delegate] cancelDeletePhoto];
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           
                                                       }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        // use UIAlertView
        UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:cancelBtnText
                                               otherButtonTitles:deleteBtnText, nil];
        
        dialog.tag = 400;
        [dialog show];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == LOGOUT_ALERT_TAG){
        if (buttonIndex != [alertView cancelButtonIndex]) {
            [self forceLogout];
            return;
        }
    }
    
    if(alertView.tag == 400){
        if (buttonIndex == [alertView cancelButtonIndex])
        {
            if ([[self delegate] respondsToSelector:@selector(cancelDeletePhoto)]) {
                [[self delegate] cancelDeletePhoto];
            }
        }
        else
        {
            if ([[self delegate] respondsToSelector:@selector(deletePhoto)]) {
                [[self delegate] deletePhoto];
            }
        }
    }
}


-(void)updateMoreIcon:(NSNotification *)notification{
    
    NSDictionary *notificationDict =[notification userInfo];
    
    BOOL isUpdate = [[notificationDict valueForKey:EGOfflineRecordUpdateKey]boolValue];
    
    if(isUpdate){
        [self.moreButton setImage:[UIImage imageNamed:@"eG_Icon-More_Offline"] forState:UIControlStateNormal];
    }
    else{
        [self.moreButton setImage:[UIImage imageNamed:@"eG_Icon-More_Norm"] forState:UIControlStateNormal];
    }
    
    [self.view setNeedsLayout];
}

-(void) resetFooterView{
    
    [self.eGMainFooter setHidden:NO];
    [self.eGPhotoSelectionFooter setHidden:YES];
    [self.view bringSubviewToFront:self.eGMainFooter];
    
    [self.view setNeedsLayout];
}
-(void) updateFooterView:(NSString *)viaScreenName{
    
    if([viaScreenName isEqualToString:@"PhotoList"]){
        deleteButtonFlag = TRUE;
        [self.addtoJobButton setHidden:NO];
        [self.cameraButton setHidden:YES];
        [self.deleteButton setHidden:NO];
    }
    else if([viaScreenName isEqualToString:@"PhotoDetail"]){
        deleteButtonFlag = FALSE;
        [self.addtoJobButton setHidden:NO];
        [self.cameraButton setHidden:NO];
        [self.deleteButton setHidden:NO];
    }
    
    [self.eGMainFooter setHidden:YES];
    [self.eGPhotoSelectionFooter setHidden:NO];
    [self.view bringSubviewToFront:self.eGPhotoSelectionFooter];
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.5f];
    
    switch (index) {
        case 0:
        case 1:
        {
            UINavigationController *navController = nil;
            if(self.childViewControllers.count>0)
                navController = (UINavigationController*)[self.childViewControllers lastObject];
            
            if([[navController visibleViewController] isKindOfClass:[EGWebViewController class]]){
                
                if(index == previousSelectedIndex){
                    return;
                }
                else{
                  
                    NSDictionary *dictToPass = nil;
                    
                    previousSelectedIndex = index;
                    
                    if(index == 0){
                        dictToPass = @{@"SelectedFile":@"About"};
                    }
                    else{
                        dictToPass = @{@"SelectedFile":@"Support"};
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:EGWebViewReloadDataNotification object:nil userInfo:dictToPass];
                }
            }
            else{
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                EGWebViewController *webVC = (EGWebViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGWebViewController"];
                
                webVC.redirectVia      = @"More";
                
                webVC.resourceViaIndex = index;
                
                previousSelectedIndex = index;
                
                [navController pushViewController:webVC animated:YES];
            }
            
            break;
        }
        case 2:{
            
    
            [self logoutPressed];
            break;
        }
        default:
            break;
    }
}


-(void)logoutPressed{
    
    
    if ([[SyncManager sharedManager] isSyncing]) {
        
        if ([UIAlertController class])
        {
            // use UIAlertController
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:APP_NAME
                                       message:@"Logging out while sync is in progress will results in unsynced data being lost. Please try logout after sync  process completed."
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           //                                                               textField.text = @"";
                                                           
                                                       }];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            // use UIAlertView
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                             message:@"Logging out while sync is in progress will results in unsynced data being lost. Please try logout after sync process is completed."
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
            [dialog show];
            
        }
        return;
    }
    else if([[SyncManager sharedManager] syncOnNetworkAwake]){
        
        if ([UIAlertController class])
        {
            // use UIAlertController
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:APP_NAME
                                       message:@"There are pending items to sync back to server and unsynced data will be lost on logout.Would you like to Continue?"
                                       preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
            
            [alert addAction:cancel];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           //Do Some action here
                                                           [self forceLogout];
                                                           
                                                       }];
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            // use UIAlertView
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                             message:@"There are pending items to sync back to server and unsynced data will be lost on logout.Would you like to Continue?"
                                                            delegate:self
                                                   cancelButtonTitle:@"CANCEL"
                                                   otherButtonTitles:@"OK", nil];
            dialog.tag = LOGOUT_ALERT_TAG;
            [dialog show];
            
        }

        return;
    }

    [self forceLogout];
    
}

-(void)forceLogout{
    
    [APIClient cancelAllRequest];
    [CoreDataBase clearUserData];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_USERID];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_OWNERID];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_AUTHORIZATION_PREFERENCE];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:EGAppFirstLaunch];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [DataDownloader releaseInstance];
    
    [SyncManager releaseInstance];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate resetRootViewController];
}
- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EGOfflineRecordUpdatedNotification
                                                  object:nil];
}
@end
