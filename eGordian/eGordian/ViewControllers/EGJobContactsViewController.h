//
//  EGJobContactsViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/21/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobOrderDetail.h"

@interface EGJobContactsViewController : UIViewController<DataDownloaderDelegate>
@property (weak, nonatomic) IBOutlet UIView *jobOrderTitleInfoView;
@property (weak, nonatomic) IBOutlet UIView *JobContactsListView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) UILabel *jobClientNameLabel;
@property (weak, nonatomic) UILabel *jobTitleLabel;
@property (weak, nonatomic) UILabel *jobIdLabel;
@property (weak, nonatomic) IBOutlet UITableView *jobContactsTableView;
@property (weak, nonatomic) IBOutlet UITableView *assignContactsTableView;
@property (nonatomic, retain) EG_JobOrderDetail *currentJobOrder;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIView *assignPersonView;
@property (weak, nonatomic) IBOutlet UIView *callView;
@property (weak, nonatomic) IBOutlet UILabel *displayNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (nonatomic, strong) NSString *strRolesURL;
@property (nonatomic, strong) NSString *strJobOwnerId;
- (IBAction)jobConatctsBackButtonTapped:(id)sender;
- (IBAction)assignPersonBackButtonTapped:(id)sender;
- (IBAction)assignPersonTapped:(id)sender;
- (IBAction)onSearchTextChanged:(id)sender;
- (IBAction)callButtonTappedToRedirectDailer:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *contactSearchField;
@end
