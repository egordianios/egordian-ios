//
//  EGMoreViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/10/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGMoreViewController.h"
#import "APIClient.h"
#import "AppDelegate.h"
#import "CoreDataBase.h"
#import "EGWebViewController.h"

#define kStringArray [NSArray arrayWithObjects:@"Support",@"Help", @"Logout", nil]

@interface EGMoreViewController (){
    CGPoint point;
    PopoverView *pv;
}

@end

@implementation EGMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self showPopOver];
}

-(void)showPopOver{
    
    pv = [PopoverView showPopoverAtPoint:CGPointMake(270, 450)
                                  inView:self.view
                         withStringArray:kStringArray
                                delegate:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)logOutButtonTapped:(id)sender {
    
    [APIClient cancelAllRequest];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_USERID];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_OWNERID];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:APP_AUTHORIZATION_PREFERENCE];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    [CoreDataBase clearUserData];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate resetRootViewController];
    
}
#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
//    NSLog(@"%s item:%d", __PRETTY_FUNCTION__, index);
    
    
    switch (index) {
        case 0:
        case 1:
        {

            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            EGWebViewController *photosCollectionVC = (EGWebViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGWebViewController"];
            
            [self.navigationController pushViewController:photosCollectionVC animated:YES];
            
            break;
        }
        default:
            break;
    }
    
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.5f];
}
- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
