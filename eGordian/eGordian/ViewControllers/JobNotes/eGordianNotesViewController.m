//
//  eGordianNotesViewController.m
//  eGordian-iOS
//
//  Created by Laila on 30/11/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "eGordianNotesViewController.h"
#import "eGordianNotesEditViewController.h"


@interface eGordianNotesViewController (){

    UILabel *jobNoteCreatedLabel;
    UILabel *jobTitleLabel;
    UILabel *noteSubjectLabel;
    UILabel *noteCreatedByLabel;
    UITextView *noteDescriptionTextView;
    
}

@end

@implementation eGordianNotesViewController

@synthesize notes, jobTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.eGNotesTextView.text = self.notes.Value;
    
//    self.egNotesHeadingDateLbl.text = self.notes.CreatedDate;
//    self.eGNotesSubjectLbl.text = self.notes.Subject;
    self.eGNotesCreatedByLbl.text = self.notes.CreatedBy;
//    self.egNotesJobTitleLbl.text = self.jobTitle;
    
    //allow edit only if UserID is equal to logged in UserID
    self.egNotesEditBtn.enabled =  [[AppConfig getUserId] isEqualToString:self.notes.UserId] ? YES:NO;
    
    [self setIntialUI];
    
}

-(void)setIntialUI{
    CGFloat padding = 15.0f;
    
    CGFloat labelWidth = self.view.frame.size.width - (2*padding);
    
    CGFloat dy = 10;
    
    CGFloat defaultLabelHeight = 20.0f;
    
    NSString *jobTitleText = [NSString stringWithFormat:@"%@",self.jobTitle];
    
    NSString *noteCreatedDate = [NSString stringWithFormat:@"%@",self.notes.CreatedDate];
    
    NSString *noteSubject = [NSString stringWithFormat:@"%@",self.notes.Subject];
    
    NSString *noteCreatedBy = [NSString stringWithFormat:@"%@",self.notes.CreatedBy];
    
    NSString *notesDesciptionText = [NSString stringWithFormat:@"%@",self.notes.Value];
    
    [self.jobOrderTitleInfoView  setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if(jobTitleText.length > 0){
        
        jobTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobTitleLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:15.0]];
        
        [jobTitleLabel setNumberOfLines:0];
        
        [jobTitleLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobTitleLabel setText:jobTitleText];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobTitleLabel WithText:jobTitleLabel.text];
        
        [jobTitleLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobTitleLabel];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(noteCreatedDate.length > 0){
        
//        jobNoteCreatedLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
//        
//        [jobNoteCreatedLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
//        
//        [jobNoteCreatedLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [self.egNotesHeadingDateLbl setNumberOfLines:0];
                
        [self.egNotesHeadingDateLbl setTextColor:[EGordianUtilities labelTitleColor]];
        
        [self.egNotesHeadingDateLbl setTextAlignment:NSTextAlignmentCenter];
        
        [self.egNotesHeadingDateLbl setText:noteCreatedDate];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:self.egNotesHeadingDateLbl WithText:self.egNotesHeadingDateLbl.text];
        
        [self.egNotesHeadingDateLbl setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
//        [self.jobOrderTitleInfoView addSubview:jobNoteCreatedLabel];
        
        dy = dy+dynamicTextHeight+10;
    }
    
    
    [self.jobOrderTitleInfoView setFrame:CGRectMake(0, 62, self.view.frame.size.width, dy)];
    
    [self.JobNotesListView setFrame:CGRectMake(0, self.jobOrderTitleInfoView.frame.origin.y+self.jobOrderTitleInfoView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-62.0f-self.jobOrderTitleInfoView.frame.size.height - 62.0f)];
    
    dy = 10;
    
    if(noteSubject.length > 0){
        
        noteSubjectLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [noteSubjectLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [noteSubjectLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:14.0]];
        
        [noteSubjectLabel setNumberOfLines:0];
        
        [noteSubjectLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [noteSubjectLabel setTextAlignment:NSTextAlignmentLeft];
        
        [noteSubjectLabel setText:noteSubject];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:noteSubjectLabel WithText:noteSubjectLabel.text];
        
        [noteSubjectLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.JobNotesListView addSubview:noteSubjectLabel];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(noteCreatedBy.length > 0){
        
        noteCreatedByLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [noteCreatedByLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [noteCreatedByLabel setFont:[UIFont fontWithName:FONT_REGULAR size:13.0]];
        
        [noteCreatedByLabel setNumberOfLines:0];
        
        [noteCreatedByLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [noteCreatedByLabel setTextAlignment:NSTextAlignmentLeft];
        
        [noteCreatedByLabel setText:noteCreatedBy];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:noteCreatedByLabel WithText:noteCreatedByLabel.text];
        
        [noteCreatedByLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.JobNotesListView addSubview:noteCreatedByLabel];
        
        dy = dy+dynamicTextHeight;
    }
    
    if(notesDesciptionText.length > 0){
        
        noteDescriptionTextView = [[UITextView alloc]initWithFrame:CGRectMake(padding-3.0, dy, labelWidth-6.0, defaultLabelHeight)];
        
        [noteDescriptionTextView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [noteDescriptionTextView setFont:[UIFont fontWithName:FONT_REGULAR size:11.0]];
        
        [noteDescriptionTextView setTextColor:[EGordianUtilities labelTitleColor]];
        
        [noteDescriptionTextView setTextAlignment:NSTextAlignmentLeft];
        
        [noteDescriptionTextView setText:notesDesciptionText];
        
        [jobNoteCreatedLabel setFrame:CGRectMake(padding, dy, labelWidth, self.JobNotesListView.frame.size.height - dy - 10)];
        
        [self.JobNotesListView addSubview:noteDescriptionTextView];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 * Method Name	: onBackBtn
 * Description	: on back button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Method Name	: onEdit
 * Description	: on Edit button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onEdit:(id)sender {
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    eGordianNotesEditViewController *editNotesVC = (eGordianNotesEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianNotesEditViewController"];
    editNotesVC.note = notes;
    [self.navigationController pushViewController:editNotesVC animated:YES];
    
}



@end
