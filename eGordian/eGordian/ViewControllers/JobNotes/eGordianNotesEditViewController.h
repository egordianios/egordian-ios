//
//  eGordianNotesEditViewController.h
//  eGordian-iOS
//
//  Created by Laila on 03/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TPKeyboardAvoidingScrollView.h>
#import "eG_Notes.h"

@interface eGordianNotesEditViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *eGNotesEditInputModeSeg;
@property (weak, nonatomic) IBOutlet UITextField *eGNotesEditSubjectTxtFld;
@property (weak, nonatomic) IBOutlet UITextView *eGNotesEditNotesTxtVw;
@property (weak, nonatomic) IBOutlet UIButton *egNotesEditSaveBtn;
@property (nonatomic,strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (strong, nonatomic) eG_Notes* note;
@property (strong, nonatomic) NSString *strJobOrderId;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;

- (IBAction)onBack:(id)sender;
- (IBAction)onSave:(id)sender;

@end
