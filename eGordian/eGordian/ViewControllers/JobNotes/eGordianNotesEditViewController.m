//
//  eGordianNotesEditViewController.m
//  eGordian-iOS
//
//  Created by Laila on 03/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "eGordianNotesEditViewController.h"
#import "EGJobDetailViewController.h"
#import "EGSpeechToTextViewController.h"
#import "UIViewController+MBOverCurrentContextModalPresenting.h"
@interface eGordianNotesEditViewController ()<SpeechToTextDelegate>

@end

@implementation eGordianNotesEditViewController

@synthesize note,strJobOrderId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.scrollView contentSizeToFit];
    
    [self.loaderView setHidden:YES];
    
    self.eGNotesEditSubjectTxtFld.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    [self.eGNotesEditSubjectTxtFld setValue:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.eGNotesEditSubjectTxtFld.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    
    [self.eGNotesEditNotesTxtVw.layer setBorderColor:[EGordianUtilities headerColor].CGColor];

    if(self.note)
    {
        self.eGNotesEditSubjectTxtFld.text = self.note.Subject;
        self.eGNotesEditNotesTxtVw.text = self.note.Value;
        self.headerLabel.text = @"EDIT NOTE";
    }
    else{
        self.eGNotesEditSubjectTxtFld.text = self.note.Subject;
        self.eGNotesEditNotesTxtVw.text = self.note.Value;
        self.headerLabel.text = @"ADD NOTE";
    }
    
    [self.eGNotesEditInputModeSeg addTarget:self
                                     action:@selector(searchSegmentedControl:)
                           forControlEvents:UIControlEventValueChanged];
}


- (IBAction)searchSegmentedControl:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
//            [self redirectToSpeechToText];
            NSLog(@"First was selected");
            break;
        case 1:
            
            break;
        default:
            break;
    }
}
-(void)redirectToSpeechToText{
    
    if(![AppDelegate isConnectedToInternet]){
        [EGordianAlertView showFeatureNotAvailableMessage];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EGSpeechToTextViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"EGSpeechToTextViewController"];
        [vc setDelegate:self];
        if([self.eGNotesEditNotesTxtVw  isFirstResponder])
            [self.eGNotesEditNotesTxtVw resignFirstResponder];
        
        [self MBOverCurrentContextPresentViewController:vc animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 * Method Name	: onBackBtn
 * Description	: on back button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Method Name	: onSave
 * Description	: on save button click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSave:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    if(self.eGNotesEditNotesTxtVw.text.length > 0 && self.eGNotesEditSubjectTxtFld.text.length > 0){
        
        [self.egNotesEditSaveBtn setEnabled:FALSE];
        
        if(self.note){
            [self putNotes];
        }
        else{
            [self addJobNote];
        }
    }
    else{
        UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                              message:@"Subject and notes details are required."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil];
        
        [displayAlert show];
    }
}

-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

#pragma mark - TextField delegates
/*
 * Method Name	: textFieldDidEndEditing
 * Description	: delegate on editing text
 * Parameters	: UITextField - textField
 * Return value	: nil
 */
- (void)textFieldDidEndEditing:(UITextField *) textField
{
    self.egNotesEditSaveBtn.enabled = textField.text.length ? TRUE : FALSE;
}



#pragma mark - misc
-(void)addJobNote{
    
    if(self.eGNotesEditNotesTxtVw.text.length > 0 && self.eGNotesEditSubjectTxtFld.text.length > 0){
        
        NSString *addNoteUrl = [NSString stringWithFormat:@"%@Users/%@/JobOrders/%@/Notes",APP_HEADER_URL,[AppConfig getUserId],self.strJobOrderId];
        
        NSDictionary *dictToPass =@{@"JobOrderId": self.strJobOrderId,
                                    @"UserId": [AppConfig getUserId],
                                    @"Value":self.eGNotesEditNotesTxtVw.text,
                                    @"Subject":self.eGNotesEditSubjectTxtFld.text,
                                    @"IsHidden":[NSNumber numberWithBool:self.note.IsHidden]};
        
        NSError *error;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictToPass
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        
        [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:addNoteUrl andserviceType:EGORDIAN_API_JOBNOTES_ADD];
    }
    else{
        
    }
}

-(void)putNotes
{
    if ([AppDelegate isConnectedToInternet])
    {
        if(self.eGNotesEditNotesTxtVw.text.length > 0 && self.eGNotesEditSubjectTxtFld.text.length > 0){
            
            NSString *editNoteUrl = [NSString stringWithFormat:@"%@Users/%@/JobOrders/%@/Notes/%@",APP_HEADER_URL,[AppConfig getUserId], note.jobID, note.Id];
            
            NSDictionary *dictToPass =@{@"Value":self.eGNotesEditNotesTxtVw.text,
                                        @"Subject":self.eGNotesEditSubjectTxtFld.text,
                                        @"IsHidden":[NSNumber numberWithBool:self.note.IsHidden]};
            NSError *error;
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictToPass
                                                               options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                 error:&error];
            id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
            
            [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
            
            [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:editNoteUrl andserviceType:EGORDIAN_API_JOBNOTES_UPDATE];
        }
        else{
            
            UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                                  message:@"Internet connection not available, Please check the Connection and try again."
                                                                 delegate:nil
                                                        cancelButtonTitle:@"Ok"
                                                        otherButtonTitles:nil, nil];
            
            [displayAlert show];
        }
    }
    else{
        UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                              message:@"Internet connection not available, Please check the Connection and try again."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil];
        
        [displayAlert show];
    }
}

#pragma mark - Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_JOBNOTES_UPDATE || type == EGORDIAN_API_JOBNOTES_ADD){
        
        [self actvityStop];
        
        [self.egNotesEditSaveBtn setEnabled:TRUE];
        
        NSArray *navStackControllers = [self.navigationController viewControllers];
        
        for (UIViewController *currVC in navStackControllers) {
            if([currVC isKindOfClass:[EGJobDetailViewController class]]){
                [self.navigationController popToViewController:currVC animated:YES];
                break;
            }
        }
    }
}
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    [self actvityStop];
    
    [self.egNotesEditSaveBtn setEnabled:TRUE];
    
    NSString *alertMessage = @"";
    
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message--%@",alertMessage);
}

#pragma mark Speechto text Response Delegate

-(void)handleSuccessfulConversion:(NSAttributedString *)recievedString{
    if (recievedString ) {
        self.eGNotesEditNotesTxtVw.attributedText =recievedString;
    }
    [self.eGNotesEditInputModeSeg setSelectedSegmentIndex:UISegmentedControlNoSegment];
}
- (void)handOutsideTouch:(NSAttributedString *)recievedString{
    if (recievedString ) {
        self.eGNotesEditNotesTxtVw.attributedText =recievedString;
    }
    [self.eGNotesEditInputModeSeg setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
}

@end
