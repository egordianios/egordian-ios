//
//  eGordianNotesViewController.h
//  eGordian-iOS
//
//  Created by Laila on 30/11/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "eG_Notes.h"

@interface eGordianNotesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *jobOrderTitleInfoView;
@property (weak, nonatomic) IBOutlet UIView *JobNotesListView;
@property (strong, nonatomic) eG_Notes* notes;
@property (strong, nonatomic) NSString* jobTitle;
@property (weak, nonatomic) IBOutlet UIButton *egNotesEditBtn;

@property (weak, nonatomic) IBOutlet UITextView *eGNotesTextView;
@property (weak, nonatomic) IBOutlet UILabel *egNotesJobTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *egNotesHeadingDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *eGNotesCreatedByLbl;
@property (weak, nonatomic) IBOutlet UILabel *eGNotesSubjectLbl;
- (IBAction)onBack:(id)sender;
- (IBAction)onEdit:(id)sender;

@end
