//
//  EGJobListViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/25/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EGJobListViewController.h"
#import "JobOrderListCell.h"
#import "SortViewListCell.h"
#import "APIClient.h"
#import "EG_JobOrder.h"
#import "EGJobDetailViewController.h"
#import "EGRightMenuViewController.h"
#import "eGMapViewController.h"

#define PAGESIZE 50

#define EmptyKey @" "

#define sortByMostRecent 10000
#define sortByCity       10001
#define sortByJobTitle   10002
#define sortByOwner      10003
#define sortByZipCode    10004

//#define searchKeyRecent @"LastUpdatedDate"
#define searchKeyOwner @"ClientName"
#define searchKeyCities @"City"

@interface EGJobListViewController ()<rightMenuDelegate> {
    int currentPageIndex,sortRecentIndex;
    BOOL isSortOrder;
    BOOL isAppendData;
    UILabel *infoTextLabel;
    BOOL isSearch;
    EGRightMenuViewController *rightMenuVC;
    int selectedSearchBy;//contains the array index of the selected searchBy option - 0 by default

}
@property (nonatomic,retain) NSMutableArray *searchByArr;
@property (nonatomic,strong) NSMutableArray *jobListLoadArray;

@property (nonatomic) BOOL loading;
@property (nonatomic, assign) BOOL isSearchView;
// contains the contacts as got from the Search Contacts service request
@property (nonatomic, strong) NSMutableArray* searchContactsArr;

@end

@implementation EGJobListViewController
@synthesize searchByArr, sections, sortedJobListArray,isSearchView;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    self.eGSearchBtn.hidden = NO;
    
    self.cancelButton.hidden = YES;
    
    self.eGSearchByTblVw.backgroundColor = [UIColor colorWithRed:.95 green:.95 blue:.95 alpha:1.0];
    
    self.searchByArr = [NSMutableArray arrayWithObjects:@"Owners",@"Cities",nil];
    
    self.jobListLoadArray = [[NSMutableArray alloc]init];
    
    
    [self.searchImage.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
    
    self.jobListTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.eGSearchByBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(4.0, 4.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.eGSearchByBtn.layer.mask = maskLayer;
    
    [self.eGSearchByBtn setTitle:[self.searchByArr objectAtIndex:selectedSearchBy] forState:UIControlStateNormal];
    
    [self setFrame];
    
    self.loading = FALSE;
    
    [self.jobListSearchField setValue:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
//    self.sortListTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    [self.jobListSortView setFrame:CGRectMake(-(self.view.frame.size.width), 0.0f, self.jobListSortView.frame.size.width, self.jobListSortView.frame.size.height)];
    
    [self.jobListSortView setHidden:YES];
    
    [self.loaderView setHidden:YES];
    
    
    
//    self.sortListTableView.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0);
    
    [self.jobListTableView reloadData];

    sortKey = sortByMostRecent;
    //GTM Implementation
    [GTMLogger pushOpenScreenEventWithScreenName:@"job List"];
    sortKey = sortByMostRecent;
    self.eGSearchByView.hidden = TRUE;
}

/*
 * Method Name	: setFrame
 * Description	: set the frame of search tableview
 * Parameters	: nil
 * Return value	: nil
 */
-(void) setFrame
{
    [self.eGSearchByView setFrame:CGRectMake(self.jobListSearchField.frame.origin.x-self.eGSearchByBtn.frame.size.width-8, self.jobListSearchField.frame.origin.y, self.eGSearchByBtn.frame.size.width, 40*[self.searchByArr count])];//here 40 is the height of a tableview cell
    [self.eGSearchByTblVw setFrame:CGRectMake(0, 0, self.eGSearchByBtn.frame.size.width, self.eGSearchByView.frame.size.height)];
}

-(void)fetchSortedJobOrders:(NSString *)jobOrderSortKey andPageNumber:(int)pgNo andPageSize:(int)pgSize{
    
    NSString *jobListURl = [NSString stringWithFormat:@"%@Users/%@/JobOrders?pagesize=%d&pagenumber=%d&sortBy=%@",APP_HEADER_URL,[AppConfig getUserId],pgSize,pgNo,jobOrderSortKey];
    
    [self getJobOrderServiceResponse:jobListURl];
}

-(void)fetchSearchResults:(NSString *)queryFilter andSortOrder:(NSString *)jobOrderSortKey{
    
    NSString *jobListURL = [NSString stringWithFormat:@"%@Users/%@/JobOrders?filter=%@&sortBy=%@",APP_HEADER_URL,[AppConfig getUserId],queryFilter,jobOrderSortKey];
    
    [self getJobOrderServiceResponse:jobListURL];
}

-(void)viewWillAppear:(BOOL)animated{
   
    [super viewWillAppear:animated];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:APP_JOBLIST_REFRESH_FLAG] boolValue])
    {
        
        self.eGSearchBtn.hidden = NO;
        
        self.cancelButton.hidden = YES;
        
        self.jobListSearchField.text = @"";
        
        for (UIButton *btn in self.eGSortByBtnCollection) {
            if(btn.tag == 0){
                [btn setSelected:TRUE];
            }
            else{
                [btn setSelected:FALSE];
            }
        }
        
        currentPageIndex=1;
        
        sortRecentIndex = 1;
        
        isAppendData = FALSE;
        
        sortKey = sortByMostRecent;
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:APP_JOBLIST_REFRESH_FLAG];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSString *sortOrderKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortByMostRecent]];
    
        [self fetchSortedJobOrders:sortOrderKey andPageNumber:currentPageIndex andPageSize:PAGESIZE];
    }
    else if(self.jobListSearchField.text.length > 0){
        
        self.eGSearchBtn.hidden = NO;
        
        self.cancelButton.hidden = YES;
        
        self.jobListSearchField.text = @"";
        
        sortKey = sortByMostRecent;
        
        for (UIButton *btn in self.eGSortByBtnCollection) {
            if(btn.tag == 0){
                [btn setSelected:TRUE];
            }
            else{
                [btn setSelected:FALSE];
            }
        }
        
        [self getSortedRecords];
    }

}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

-(void)getJobOrderServiceResponse:(NSString *)jobOrderRequestURL{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader]getResponseFormUrl:jobOrderRequestURL serviceType:EGORDIAN_API_JOBORDERS_LIST];

}

-(void) getSortedRecords
{
    
    currentPageIndex=1;
    
    isAppendData = FALSE;
    
    NSString *jobOderSortKey = @"";
    
    if(sortKey == sortByMostRecent){
        jobOderSortKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortKey]];
    }
    else{
        jobOderSortKey = [NSString stringWithFormat:@"%@",[self sortKeyForSortNumber:sortKey]];
    }
    
    
    [self fetchSortedJobOrders:jobOderSortKey andPageNumber:currentPageIndex andPageSize:PAGESIZE];
}

/*
 * Method Name	: sortKeyForSortNumber
 * Description	: get the key for the sortnumber
 * Parameters	: NSInteger
 * Return value	: NSString (key to sort)
 */
-(NSString*)sortKeyForSortNumber:(NSInteger)sortNumber
{
    switch (sortNumber) {
        case sortByMostRecent:
            return @"LastUpdatedDate";
        
        case sortByJobTitle:
            return @"JobTitle";
            
        case sortByCity:
            return @"City";
            
        case sortByOwner:
            return @"ClientName";
            
        case sortByZipCode:
            return @"ZipCode";
            
        default:
            return @"LastUpdatedDate";
    }
    return @"LastUpdatedDate";
}

-(void) populateSortedData:(NSMutableArray *)inputArray
{
    BOOL found;
    
    if(![inputArray count]){
        self.sections =nil;
        return;
    }
    
    if(sortKey == sortByMostRecent)
        return;
    
    NSString* c = @"";
    self.sections = [[NSMutableDictionary alloc] init];
    
    // Loop through the jobs and create our keys
    for (EG_JobOrder *job in inputArray)
    {
        switch (sortKey) {
            case sortByCity:
            {
                c = job.City;
                if(!c || ![c length])
                    c = EmptyKey;
            }
                break;
                
            case sortByZipCode:
            {
                c = job.ZipCode;
                if(!c || ![c length])
                    c = EmptyKey;
            }
                break;
                
            case sortByOwner:
            {
                c = job.ClientName;
                if(!c || ![c length])
                    c = EmptyKey;
            }
                break;
                
            default:
                c = job.LastUpdatedDate;
                break;
        }
        found = NO;
        
        for (NSString *str in [self.sections allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [self.sections setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    switch (sortKey) {
        case sortByCity:
        {
            // Loop again and sort the jobs into their respective keys
            for (EG_JobOrder *job in self.jobListLoadArray)
            {
                if(!job.City || ![job.City length])
                    [[self.sections objectForKey:EmptyKey] addObject:job];
                else
                    [[self.sections objectForKey:job.City] addObject:job];
                
            }
            break;
        }
        case sortByZipCode:
        {
            // Loop again and sort the jobs into their respective keys
            for (EG_JobOrder *job in self.jobListLoadArray)
            {
                if(!job.ZipCode || ![job.ZipCode length])
                    [[self.sections objectForKey:EmptyKey] addObject:job];
                else
                    [[self.sections objectForKey:job.ZipCode] addObject:job];
                
            }
            break;
        }
            
        case sortByOwner:
        {
            // Loop again and sort the jobs into their respective keys
            for (EG_JobOrder *job in self.jobListLoadArray)
            {
                if(!job.ClientName || ![job.ClientName length])
                    [[self.sections objectForKey:EmptyKey] addObject:job];
                else
                    [[self.sections objectForKey:job.ClientName] addObject:job];
                
            }
            break;
        }
            
        default:
        {
            // Loop again and sort the jobs into their respective keys
            for (EG_JobOrder *job in self.jobListLoadArray)
            {
                if(!job.LastUpdatedDate || ![job.LastUpdatedDate length])
                    [[self.sections objectForKey:EmptyKey] addObject:job];
                else
                    [[self.sections objectForKey:job.LastUpdatedDate] addObject:job];
                
            }
            break;
        }
    }
    
//    // Sort each section array
//    for (NSString *key in [self.sections allKeys])
//    {
//        [[self.sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:sortKeyString ascending:YES]]];
//    }
}

-(void)onLocationBtn:(id)sender
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIButton* locationBtn = (UIButton*)sender;
     
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.jobListTableView];
    NSIndexPath *indexPath = [self.jobListTableView indexPathForRowAtPoint:buttonPosition];
    
//    NSIndexPath *indexPath = [self.jobListTableView indexPathForCell:((JobOrderListCell*)locationBtn.superview)];
    
    EG_JobOrder* jobOrderSelected = nil;
    
    if(sortKey == sortByMostRecent)
        jobOrderSelected = (EG_JobOrder *)[self.sortedJobListArray objectAtIndex:locationBtn.tag];
    else
        jobOrderSelected = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    if(!jobOrderSelected)
        return;
    
    //this is for job photo attach
    [EGordianUtilities writeJobDetails:jobOrderSelected.jobOrderId ownerID:jobOrderSelected.ownerID];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    eGMapViewController *mapsVC = (eGMapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGMapViewController"];
    
    NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] init];
    
    [addressDict setObject:jobOrderSelected.jobTitle.length?jobOrderSelected.jobTitle:@"" forKey:@"Name"];
    
    [addressDict setObject:jobOrderSelected.address1.length?jobOrderSelected.address1:@"" forKey:@"Address"];
    
    [addressDict setObject:[NSString stringWithFormat:@"%@%@%@",jobOrderSelected.City.length?[jobOrderSelected.City stringByAppendingString:@","]:@"",jobOrderSelected.State.length?[jobOrderSelected.State stringByAppendingString:@","]:@"",jobOrderSelected.ZipCode.length?jobOrderSelected.ZipCode:@""] forKey:@"A_Address1"] ;
    
    mapsVC.addressDict = addressDict;
    
    [self.navigationController pushViewController:mapsVC animated:NO];
}

/*
 * Method Name	: sortJobs
 * Description	: sort jobs
 * Parameters	: null
 * Return value	: null
 */
-(void) sortJobs
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    if(!sortKey)
        sortKey = sortByMostRecent;
    
    self.sortedJobListArray = [EG_JobOrder sortJobs:self.jobListLoadArray WithSortKey:[self sortKeyForSortNumber:sortKey]];
    
    [self populateSortedData:self.sortedJobListArray];
    
    [self.jobListTableView setContentOffset:CGPointMake(0, 0)];
    
    [self.jobListTableView reloadData];
}



#pragma mark Data Downloader Manager Delegates
- (void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_JOBORDERS_LIST){
        
        if(currentPageIndex > 1)
        {
            [self.jobListLoadArray addObjectsFromArray:responseArray];
        }
        else{
            [self.jobListLoadArray removeAllObjects];
            self.jobListLoadArray = [responseArray mutableCopy];
            [self.jobListTableView setContentOffset:CGPointMake(0, 0)];
        }
        
        NSLog(@"job load array count---%lu",(unsigned long)self.jobListLoadArray.count);
        
        isAppendData = FALSE;
        
        if (responseArray.count > 0) {
            self.loading = FALSE;
        }
        else{
            self.loading = TRUE;
        }
        
        self.sortedJobListArray = [[NSMutableArray alloc] initWithArray:self.jobListLoadArray] ;
        
        [self populateSortedData:self.sortedJobListArray];
        
        
        [self.jobListTableView reloadData];
        
        if(self.jobListLoadArray.count == 0){
            
            [self showNullRecords];
        }
        else{
            [infoTextLabel removeFromSuperview];
        }
        
        [self.loaderView setHidden:YES];
        
        [self.activityLoader stopAnimating];
    }
}

-(void)showNullRecords{
    if (!infoTextLabel)
        infoTextLabel = [[UILabel alloc] init];
    infoTextLabel.frame = CGRectMake(self.jobListTableView.frame.origin.x, self.view.frame.size.height/2, self.jobListTableView.frame.size.width, 60); //self.jobListTableView.bounds;
    infoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    infoTextLabel.backgroundColor = [UIColor clearColor];
    infoTextLabel.textAlignment = NSTextAlignmentCenter;
    infoTextLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    infoTextLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:20];
    infoTextLabel.text = @"No Items Found";
    [self.view addSubview:infoTextLabel];
    [self.view bringSubviewToFront:infoTextLabel];
}

- (void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    [self.loaderView setHidden:YES];
    
    [self.activityLoader stopAnimating];
    
    if(type == EGORDIAN_API_JOBORDERS_LIST && currentPageIndex > 1){
        currentPageIndex--;
    }
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    NSLog(@"Error Message--%@",alertMessage);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    NSInteger numberOfSections = 1;

    if(sortKey == sortByMostRecent)
        numberOfSections = 1;
    else if([self.sections count])
        numberOfSections = [[self.sections allKeys] count];
    
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = 0;
    
    if(tableView == self.eGSearchByTblVw){
        rowCount = [self.searchByArr count];
    }
    else if(tableView == self.jobListTableView){
        if(sortKey == sortByMostRecent)
        {
            if(![self.jobListLoadArray count])
                rowCount = 0;
            else
                rowCount = [self.jobListLoadArray count];
        }
        else
        {
            if(![self.sections count])
                rowCount = 0;
            else
                rowCount = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
        }
    }
    return rowCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cellToReturn=nil;
    
    if([tableView isEqual:self.eGSearchByTblVw]){
        UITableViewCell* searchByCell = [tableView dequeueReusableCellWithIdentifier:@"tableViewCell"];
        
        if(!searchByCell)
        {
            searchByCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tableViewCell"];
        }
        
        searchByCell.textLabel.text = [self.searchByArr objectAtIndex:indexPath.row];
        searchByCell.textLabel.textColor = [UIColor blackColor];
        searchByCell.textLabel.highlightedTextColor = [UIColor whiteColor];
        searchByCell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:13];
        [searchByCell.selectedBackgroundView setBackgroundColor: [UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:70/255.0 alpha:1.0]];
        [searchByCell.contentView setBackgroundColor: [UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:70/255.0 alpha:1.0]];
        [searchByCell.textLabel setBackgroundColor:[UIColor clearColor]];
        
        return searchByCell;
    }
    else  if(tableView == self.jobListTableView){
        
        JobOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobOrderListCell"];
        
        
        if (!cell)
        {
            cell = [[JobOrderListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JobOrderListCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        EG_JobOrder *currentJobOrder = nil;
        
        if(sortKey == sortByMostRecent)
            currentJobOrder = (EG_JobOrder *)[self.sortedJobListArray objectAtIndex:indexPath.row];
        else
            currentJobOrder = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        cell.jobTitleLabel.text         = currentJobOrder.jobTitle;
        
        cell.jobClientNameLabel.text    = currentJobOrder.ClientName;
        
        NSString *timeToDisplay =[AppConfig dateStringFromString:currentJobOrder.LastUpdatedDate sourceFormat:@"MM/dd/yyyy HH:mm:ss a" destinationFormat:@"MM/dd/yyyy HH:mm:ss a"];
        
        NSString *locationValue = [NSString stringWithFormat:@"%@%@%@%@",currentJobOrder.address1.length?[currentJobOrder.address1 stringByAppendingString:@","]:@"",currentJobOrder.City.length?[currentJobOrder.City stringByAppendingString:@","]:@"",currentJobOrder.State.length?[currentJobOrder.State stringByAppendingString:@","]:@"",currentJobOrder.ZipCode.length?currentJobOrder.ZipCode:@""];
        
        cell.jobLocationLabel.text      = locationValue;//currentJobOrder.locationName;
        
        cell.jobIdLabel.text            = currentJobOrder.jobOrderNumber?currentJobOrder.jobOrderNumber:@""; //[NSString stringWithFormat:@"ID: %@",timeToDisplay];//currentJobOrder.jobOrderId
        
        [cell.locationIconBtn setImage:[UIImage imageNamed:@"eG_JobList_Direction_Active"] forState:UIControlStateNormal];
        [cell.locationIconBtn addTarget:self action:@selector(onLocationBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.locationIconBtn.tag = indexPath.row;
        
        if(([currentJobOrder.address1 length]) && [currentJobOrder.City length] && ([currentJobOrder.State length] || [currentJobOrder.ZipCode length])){//if(![locationValue length])
            cell.locationIconBtn.hidden = FALSE;
            cell.directionsLabel.hidden = FALSE;
        }
        else{
            cell.locationIconBtn.hidden = TRUE;
            cell.directionsLabel.hidden = TRUE;
        }
    
        if(indexPath.row % 2){
            cell.jobListCellBackGroundImage.image = [UIImage imageNamed:@"eG_JobList_WhitePanelBG"];
        }
        else{
            cell.jobListCellBackGroundImage.image = [UIImage imageNamed:@"eG_JobList_GrayPanelBG"];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    
    return cellToReturn;
    
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if([tableView isEqual:self.eGSearchByTblVw])
    {
        [[tableView cellForRowAtIndexPath:indexPath].contentView setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:70/255.0 alpha:1.0]];
        selectedSearchBy = (int)indexPath.row;//setting the current searchBy index.
        
        self.jobListSearchField.text = @"";
        
        [tableView reloadData];
        [self.eGSearchByBtn setTitle:[self.searchByArr objectAtIndex:selectedSearchBy] forState:UIControlStateNormal] ;
        self.eGSearchByView.hidden = TRUE;
        [self.view bringSubviewToFront:self.eGSearchByBtn];
        self.eGSearchByBtn.hidden = FALSE;
        self.isSearchView = FALSE;
        
        searchKey = indexPath.row;
        
        //        if(selectedSearchBy==1)//company - udjust the text and image to fit properly
        //        {
        //            [self.eGSearchByBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -6.0, 0.0, 0.0)];
        //            [self.eGSearchByBtn setImageEdgeInsets:UIEdgeInsetsMake(3.0, self.eGSearchByBtn.frame.size.width - image.size.width-5, 0.0, 0.0)];
        //        }
        //        else
        //        {//set it back to default
        //            [self.eGSearchByBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
        //            [self.eGSearchByBtn setImageEdgeInsets:UIEdgeInsetsMake(3.0, self.eGSearchByBtn.frame.size.width - 2*image.size.width, 0.0, 0.0)];
        //        }
    }
    else if(tableView == self.jobListTableView){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EGJobDetailViewController *jobDetailVC = (EGJobDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGJobDetailViewController"];
        
        if(sortKey == sortByMostRecent)
            jobDetailVC.currentJobOrder = (EG_JobOrder *)[self.jobListLoadArray objectAtIndex:indexPath.row];
        else
            jobDetailVC.currentJobOrder = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:jobDetailVC animated:YES];
    }
}

-(void) updateUI
{
    switch (sortKey) {
        case sortByMostRecent:
//            self.
            break;
            
        default:
            break;
    }
}

/*
 * Method Name	: onSortBtn
 * Description	: on sort button collection click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSortBtn:(id)sender
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    NSInteger prevSortKey = sortKey;
    
    UIButton* sortBtn = (UIButton*)sender;
    
    switch (sortBtn.tag) {
        case 0:
            sortKey  = sortByMostRecent;
            break;
        case 1:
            sortKey  = sortByOwner;
            break;
            
        case 2:
            sortKey  = sortByCity;
            break;
            
        default:
            sortKey  = sortByMostRecent;
            break;
    }
    
    if(prevSortKey == sortKey) //don't do anything if there is no change in the sortkey
        return;
    
    sortBtn.selected = TRUE;
    
    for (UIButton* btn in self.eGSortByBtnCollection)
    {
        if(![btn isEqual:sortBtn])
        {
            btn.selected = FALSE;
        }
    }
    
    if(self.jobListSearchField.text.length){
       [self sortJobs];
    }
    else{
        [self getSortedRecords];
    }
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 0.0;
    
    //    if (tableView == self.sortListTableView){
    //        return height = 1.0f;
    //    }
    //    else
    if(tableView == self.jobListTableView){
        return height = 30.0f;
    }
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.eGSearchByTblVw)
        return (80/[self.searchByArr count]); //80 is the tableview height
    
    return 95;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView == self.jobListTableView){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor colorWithRed:156.0/255.0 green:182.0/255.0 blue:197.0/255.0 alpha:1.0]];
        
        UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,0,tableView.bounds.size.width-30,30)];
        tempLabel.backgroundColor=[UIColor clearColor];
        tempLabel.textColor = [UIColor whiteColor];
        tempLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:15];
        NSString *headerTitle = @"";

        if(sortKey == sortByMostRecent)
            headerTitle = @"Most Recent";
        else
            headerTitle = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        
        //sort by city and the city is blank...
        if(!([headerTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) && [self.sections count] && sortKey == sortByCity)
        {
                headerTitle = @"No City Available";
        }
        
        tempLabel.text=headerTitle;//@"Most Recent";
        [headerView addSubview:tempLabel];
        
        return headerView;
    }
    else{
        return nil;
    }
}

/*
 * Method Name	: onSearchByBtn
 * Description	: on Search By button click ( decides on which parameter the search must happen)
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSearchByBtn:(id)sender
{
    if(![self.jobListLoadArray count]) return;
    
    self.eGSearchByView.hidden = FALSE; //don't hide the searchby tableview
    self.isSearchView = TRUE;
    [self.eGSearchByTblVw reloadData];
    self.eGSearchByBtn.hidden = TRUE;
    [self.view bringSubviewToFront:self.eGSearchByTblVw];
    
}

#pragma mark - TextField Delegate

- (IBAction)onSearchTextChanged:(id)sender {
    
    if(self.jobListSearchField.text.length >= 2)
    {
        self.cancelButton.hidden = FALSE;
        self.eGSearchBtn.hidden = TRUE;
    }
    else
    {
        self.cancelButton.hidden = TRUE;
        self.eGSearchBtn.hidden = FALSE;
    }
}

/*
 * Method Name	: onTextFieldEndEditing
 * Description	: on editing did end
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onTextFieldEndEditing:(id)sender {
    
    UITextField* textField = sender;
    
    if(textField.text.length >= 2){
        [self.eGSearchBtn setHidden:YES];
    }
    else if (textField.text.length == 0){
        [self.cancelButton setHidden:YES];
        [self.eGSearchBtn setHidden:YES];
    }
    else{
        [self.eGSearchBtn setHidden:NO];
        [self.cancelButton setHidden:YES];
    }
}

/*
 * Method Name	: onSearch
 * Description	: on search button click.
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSearch:(id)sender {
    
    if(!self.jobListSearchField.text.length)
        return;
    
    self.isSearchView = FALSE;
    
    self.eGSearchByView.hidden = TRUE;
    
    [self.eGSearchByTblVw reloadData];
    
    if(self.searchContactsArr)
        self.searchContactsArr = NULL;
    
    self.searchContactsArr = [[NSMutableArray alloc] init];
    
    isSearch = TRUE;
    
    isAppendData = FALSE;
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    if(self.jobListSearchField.text.length > 0){
        
        NSArray *subStrings = [self.jobListSearchField.text componentsSeparatedByString:@":"];
        
        if(!subStrings.count){
            return;
        }
        
        NSString *firstString = @"";
        NSString *lastString = @"";
        
        if(subStrings.count == 1){
            lastString = [subStrings objectAtIndex:0];
        }
        else if(subStrings.count > 1){
            
            firstString = [subStrings objectAtIndex:0];
            
            firstString = [firstString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            lastString = [subStrings objectAtIndex:1];
            
            lastString = [lastString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
        }
        
        if ([firstString caseInsensitiveCompare:@"City"] == NSOrderedSame){
            firstString = @"City";
        }
        else if([firstString caseInsensitiveCompare:@"Owner"] == NSOrderedSame){
            firstString = @"ClientName";
        }
        else{
            firstString = @"ClientName";
        }

        if(lastString.length > 0)
        {
            NSString *strParamValue = [NSString stringWithFormat:@"%@ eq %@",firstString, lastString];
            
            strParamValue = [AppConfig urlencode:strParamValue];
            
            NSString *sortOrderKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortByMostRecent]];
            
            [self fetchSearchResults:strParamValue andSortOrder:sortOrderKey];
        }
        else{
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                             message:@"Invalid Search"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil, nil];
            
            [dialog show];
        }
    
    }
    else{
        
        NSString *sortOrderKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortKey]];
        
        [self fetchSortedJobOrders:sortOrderKey andPageNumber:currentPageIndex andPageSize:PAGESIZE];
    }
}

- (IBAction)searchResultCancelTapped:(id)sender {
    
    self.eGSearchBtn.hidden = NO;
    
    self.cancelButton.hidden = YES;
    
    self.jobListSearchField.text = @"";
    
    isSearch = FALSE;
    
//    currentPageIndex = 1;
    sortKey = sortByMostRecent;
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    //done to change the color of Sort by MostRecent to Selected Color.
    for (UIButton* btn in self.eGSortByBtnCollection)
    {
        if(btn.tag == 0)
            btn.selected = TRUE;
        else
            btn.selected = FALSE;
    }
    
    currentPageIndex = 1;
    
    NSString *sortOrderKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortKey]];
    
    [self fetchSortedJobOrders:sortOrderKey andPageNumber:1 andPageSize:(sortRecentIndex*PAGESIZE)];
}

#pragma mark - TextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length > 0){
        
        [self onSearch:nil];
    }
    else
    {
        isSearch = FALSE;
        
        currentPageIndex = 1;
        
        NSString *sortOrderKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortByMostRecent]];
        
        [self fetchSortedJobOrders:sortOrderKey andPageNumber:currentPageIndex andPageSize:PAGESIZE];
        
//        NSString *jobListURl = [NSString stringWithFormat:@"%@Users/%@/JobOrders?pagesize=%d&pagenumber=%d&sortBy=LastUpdatedDate",APP_HEADER_URL,[AppConfig getUserId],PAGESIZE,currentPageIndex];
//        
//        [self getJobOrderServiceResponse:jobListURl];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.isSearchView)
        return;
    
    if (!self.loading) {
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        
        if (endScrolling >= scrollView.contentSize.height)
        {
            [self performSelector:@selector(loadDataDelayed) withObject:nil afterDelay:1.0];
        }
    }
}

-(void)loadDataDelayed{
    
    if(self.jobListSearchField.text.length > 0)
        return;
    
    currentPageIndex++;
    
    NSString *sortOrderKey = @"";
    
    if(sortKey==sortByMostRecent){
        
        sortRecentIndex = currentPageIndex;
        
        sortOrderKey = [NSString stringWithFormat:@"-%@",[self sortKeyForSortNumber:sortKey]];
    }
    else{
        sortOrderKey = [NSString stringWithFormat:@"%@",[self sortKeyForSortNumber:sortKey]];
    }
    
    [self fetchSortedJobOrders:sortOrderKey andPageNumber:currentPageIndex andPageSize:PAGESIZE];
    
//    NSString *jobListURl = [NSString stringWithFormat:@"%@Users/%@/JobOrders?&pagesize=%d&pagenumber=%d&sortBy=%@",APP_HEADER_URL,[AppConfig getUserId],PAGESIZE,currentPageIndex, [self sortKeyForSortNumber:sortKey]];
//    
//    [self getJobOrderServiceResponse:jobListURl];
    
}

- (IBAction)jobsBackButtonTapped:(id)sender {
    
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)rightMenuTapped:(id)sender {
    
    if(rightMenuVC==nil)
        rightMenuVC= (EGRightMenuViewController *)[EGRightMenuViewController instantiateRightMenuController];//[storyboard instantiateViewControllerWithIdentifier:@"EGRightMenuViewController"];
    rightMenuVC.parentView =(id)self;
    rightMenuVC.rightMenuDelegate = self;
    
    rightMenuVC.selectedModuleIndex = EGORDIAN_JOBORDER_MODULE;
    
    [self addChildViewController:rightMenuVC];
    
    [self.view addSubview:rightMenuVC.view];
    
    CGRect rightMenuRect = rightMenuVC.view.frame;
    
    rightMenuVC.view.frame = CGRectMake(self.view.frame.size.width, 0, rightMenuRect.size.width, rightMenuRect.size.height);
    
    rightMenuRect.origin.x=self.view.frame.size.width-rightMenuVC.view.frame.size.width;
    
    [UIView animateWithDuration:0.40f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         rightMenuVC.view.frame = rightMenuRect;
                     }
                     completion:nil];
}

- (void)slideMenuCloseButtonTapped:(EGRightMenuViewController *)rightController
{
    [UIView animateWithDuration:0.40f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         rightController.view.frame = CGRectMake(self.view.frame.size.width, 0, rightMenuVC.view.frame.size.width, rightMenuVC.view.frame.size.height);
                         rightMenuVC.parentView =nil;
                     }
                     completion:^(BOOL finished)
     {
         [rightController removeFromParentViewController];
         [rightController.view removeFromSuperview];
     }];
    
}

@end
