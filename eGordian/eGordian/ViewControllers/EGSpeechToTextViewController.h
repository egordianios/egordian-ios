//
//  EGSpeechToTextViewController.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/13/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpeechKit/SpeechKit.h>
@protocol SpeechToTextDelegate <NSObject>
@optional
- (void)handleSuccessfulConversion:(NSAttributedString *)recievedString;
- (void)handOutsideTouch:(NSAttributedString *)recievedString;
@end

@interface EGSpeechToTextViewController : UIViewController<SpeechKitDelegate, SKRecognizerDelegate>{
    SKRecognizer* voiceSearch;
    enum {
        TS_IDLE,
        TS_INITIAL,
        TS_RECORDING,
        TS_PROCESSING,
    } transactionState;

}
@property(strong, nonatomic) id<SpeechToTextDelegate> delegate;
@property(nonatomic,retain) IBOutlet UIView* vuMeter;
@property(readonly) SKRecognizer* voiceSearch;
@property(nonatomic,retain) IBOutlet UIButton* recordButton;
@property(nonatomic,retain) IBOutlet UITextView* resultsTextView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *recognitionType;
@property (strong, nonatomic) IBOutlet UIView *recordingView;

- (IBAction)cancelClicked:(id)sender;
- (IBAction)okClicked:(id)sender;
- (IBAction)recordButtonAction: (id)sender;
@end
