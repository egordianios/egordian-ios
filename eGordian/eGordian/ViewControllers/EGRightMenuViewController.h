//
//  EGRightMenuViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/25/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    EGORDIAN_HOME_MODULE            = 0,
    EGORDIAN_JOBORDER_MODULE        = 1,
    EGORDIAN_CONTACT_MODULE         = 2,
    EGORDIAN_PHOTOS_MODULE          = 3,
    EGORDIAN_ABOUT_MODULE           = 4,
    EGORDIAN_SUPPORT_MODULE         = 5,
    EGORDIAN_LOGOUT_MODULE          = 6,
} selectedModule;

@class EGRightMenuViewController;

@protocol rightMenuDelegate <NSObject>
@optional
- (void)slideMenuCloseButtonTapped:(EGRightMenuViewController*) rightController;
@end

@interface EGRightMenuViewController : UIViewController

+ (id)instantiateRightMenuController;
@property (weak, nonatomic) IBOutlet UITableView *rightMenuTable;
@property (nonatomic,assign) NSUInteger selectedModuleIndex;
@property (nonatomic,assign) id parentView;
@property (nonatomic, weak) id <rightMenuDelegate> rightMenuDelegate;
@property (weak, nonatomic) IBOutlet UIView *logoutView;
- (IBAction)logoutCancelButtonTapped:(id)sender;
- (IBAction)logoutConfirmButtonTapped:(id)sender;
- (IBAction)rightMenuCloseButtonTapped:(id)sender;

@end

