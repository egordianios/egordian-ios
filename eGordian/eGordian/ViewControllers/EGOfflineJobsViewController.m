//
//  OfflineJobsViewController.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/21/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGOfflineJobsViewController.h"
#import "JobOrderListCell.h"
#import "JobOrder.h"
#import "CoreDataBase.h"
#import "JobOrderDAO.h"
#import "EGOfflineJobDetailViewController.h"
#import "NSDate+DisplayFormat.h"
#import "EGScopeOfWorkEditViewController.h"
#import "EGContainerViewController.h"
@interface EGOfflineJobsViewController (){
    
    
}
@property (nonatomic,strong) IBOutlet UITableView *jobsTable;
@property (nonatomic, strong) UILabel* filterInfoTextLabel;
@end

@implementation EGOfflineJobsViewController
@synthesize jobsTable = _jobsTable, filterInfoTextLabel=_filterInfoTextLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSError *error = nil;
    
//    NSArray *toDisplay =[JobOrderDAO fetchAllDataInContext:[CoreDataBase activeContext]];
//    [[EGContainerViewController sharedContainerView]setDelegate:self];
    if (![ [ self fetchedResultsController ] performFetch:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
#ifdef DEBUG
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
#endif
    }
    else{
        //reload Data
        [self.jobsTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }
       // [collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark fetchedresults Controller

- (NSFetchedResultsController*)fetchedResultsController
{
    if (fetchedResultsController == nil) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:TBL_JOBORDERS
                                                  inManagedObjectContext:[CoreDataBase activeContext]];
        [fetchRequest setEntity:entity];

        // Edit the sort key as appropriate.
//        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastUpdatedDate"
                                                                           ascending:NO];
//        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastUpdatedDate" ascending:NO];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        [fetchRequest setFetchLimit:10];

        NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                              managedObjectContext:[CoreDataBase activeContext]
                                                                                sectionNameKeyPath:nil
                                                                                         cacheName:nil];
        frc.delegate = self;
        fetchedResultsController = frc;
    }
    return fetchedResultsController;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    NSInteger numberOfSections = [[self.fetchedResultsController sections] count];
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
    
    if(rowCount == 0)
    {
        [self showNullRecords];
    }
    else
        [self.filterInfoTextLabel removeFromSuperview];
    return rowCount;
}

-(void)showNullRecords{
    if (!self.filterInfoTextLabel)
        self.filterInfoTextLabel = [[UILabel alloc] init];
    self.filterInfoTextLabel.frame = self.jobsTable.bounds;
    self.filterInfoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.filterInfoTextLabel.backgroundColor = [UIColor clearColor];
    self.filterInfoTextLabel.textAlignment = NSTextAlignmentCenter;
    self.filterInfoTextLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    self.filterInfoTextLabel.font = [UIFont boldSystemFontOfSize:16.0];
    self.filterInfoTextLabel.text = @"No Items Found";;
    [self.jobsTable addSubview:self.filterInfoTextLabel];
}

-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    JobOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobOrderListCell"];
    
    
    if (!cell)
    {
        cell = [[JobOrderListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JobOrderListCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.accessoryType =UITableViewCellAccessoryNone;
    }
    
    JobOrder *currentJobOrder = (JobOrder *)[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    
    
    cell.jobTitleLabel.text         = currentJobOrder.jobTitle;
    cell.jobClientNameLabel.text    =currentJobOrder.clientName;
    cell.jobLocationLabel.text      = currentJobOrder.city;
    NSString *locationValue = [NSString stringWithFormat:@"%@%@%@%@",currentJobOrder.address1.length?[currentJobOrder.address1 stringByAppendingString:@","]:@"",currentJobOrder.city.length?[currentJobOrder.city stringByAppendingString:@","]:@"",currentJobOrder.state.length?[currentJobOrder.state stringByAppendingString:@","]:@"",currentJobOrder.zipCode.length?currentJobOrder.zipCode:@""];
    cell.jobLocationLabel.text      = locationValue;
    
    NSTimeInterval timeInterval = currentJobOrder.lastUpdatedDate;
    NSDate *startDate = nil;
    if (timeInterval) {
        startDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    }
    NSString *dateStr =@"";
    if (startDate) {
        dateStr = [startDate userDisplayDateAndTime];
    }
    cell.jobIdLabel.text            = [NSString stringWithFormat:@"ID: %@",currentJobOrder.jobId];//[NSString stringWithFormat:@"ID: %@",dateStr];//currentJobOrder.jobId
    [cell.locationIconBtn setImage:[UIImage imageNamed:@"eG_JobList_Direction_Active"] forState:UIControlStateNormal];
    cell.locationIconBtn.tag = indexPath.row;
    
    if(([currentJobOrder.address1 length]) && ([currentJobOrder.city length] || [currentJobOrder.state length] || [currentJobOrder.zipCode length])){//if(![locationValue length])
        cell.locationIconBtn.hidden = FALSE;
    }
    else{
        cell.locationIconBtn.hidden = TRUE;
    }
    
    
    if(indexPath.row % 2){
        cell.jobListCellBackGroundImage.image = [UIImage imageNamed:@"eG_JobList_WhitePanelBG"];
    }
    else{
        cell.jobListCellBackGroundImage.image = [UIImage imageNamed:@"eG_JobList_GrayPanelBG"];
    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    JobOrder *selJobOrder =(JobOrder *)[self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EGScopeOfWorkEditViewController *editScopeVC = (EGScopeOfWorkEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGScopeOfWorkEditViewController"];
    editScopeVC.isCalledFromOffline =true;
    if  (selJobOrder)
    editScopeVC.selectedJobOrder =selJobOrder;
    [self.navigationController pushViewController:editScopeVC animated:YES];
}

#pragma mark Containerviwdelegate Methods
//-(void)takePhotoClickedOffline{
//    
//    
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
