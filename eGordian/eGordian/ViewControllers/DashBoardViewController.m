//
//  DashBoardViewController.m
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "DashBoardViewController.h"
#import "AFNetworking.h"
#import "APIClient.h"
#import "Owner.h"
#import "EGRightMenuViewController.h"
#import "RightSlideMenuCell.h"
#import "EGJobListViewController.h"
#import "eGordianContactsViewController.h"
#import "eGordianPhotoViewController.h"
#import "OverlayView.h"
#import "eGordianCameraObject.h"
#import "DashboardCustomCell.h"
#import "EGOfflineJobsViewController.h"
#import "JobOrderDAO.h"
#import "AttachPhotoDAO.h"

//transform values for full screen support
#define CAMERA_TRANSFORM_X 1
#define CAMERA_TRANSFORM_Y 1.12412

@interface DashBoardViewController ()<rightMenuDelegate,SyncManagerDelegate>{
    
    EGRightMenuViewController *rightMenuVC;
    NSUInteger statusCount;
    
}
@property (nonatomic,assign) BOOL shouldDeleteRecords;
@property (nonatomic,assign) BOOL isJobScopeUpdated;
@property (nonatomic,strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) NSString *openJobsCount;
@property (nonatomic,strong) NSString *contactsCount;
@property (nonatomic,strong) NSString *photosCount;
@property (nonatomic,strong) NSString *OfflineJobsCount;
@property (nonatomic,assign) BOOL shouldStopAnimation;


@end

//static int count=0;

@implementation DashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    photoCount = 0;
//    [self getAllPictures];
    
    [self callGTMTrackerForHomeScreen];
    
    self.shouldStopAnimation = FALSE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushingOfflineData) name:CTSyncOutBoundStartedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDashboard) name:CTDASHBOARDTABLERELOAD object:nil];
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:EGAppFirstLaunch] && [AppDelegate isConnectedToInternet]){
        
        [self performSelectorOnMainThread:@selector(performSync) withObject:nil waitUntilDone:NO];
    }
    
    //[self initalisePullToRefresh];
}


-(void)performSync{
    
    [CoreDataBase clearUserData];
    
    self.shouldStopAnimation = TRUE;
    
    [[SyncManager sharedManager] setStatusDelegate:self];
    
    [[SyncManager sharedManager] fullSync];
    
    [self.dashBoardTable reloadData];
    _shouldDeleteRecords =NO;
    
}


-(void)reloadDashboard{
    [self.dashBoardTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

-(void)initalisePullToRefresh{
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor darkGrayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    
    NSString *s = @"Pull to Refresh";
    
    NSMutableAttributedString *a = [[NSMutableAttributedString alloc] initWithString:s];
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: [UIColor colorWithRed:255/255.0 green:184/255.0 blue:0/255.0 alpha:1],
                                        NSFontAttributeName:[UIFont fontWithName:@"Georgia" size:17]
                                        };
    [a setAttributes:refreshAttributes range:NSMakeRange(0, a.length)];
    self.refreshControl.attributedTitle = a;
    [self.refreshControl addTarget:self
                            action:@selector(updateOfflineJobs:)
                  forControlEvents:UIControlEventValueChanged];
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.dashBoardTable;
    tableViewController.refreshControl = self.refreshControl;
    // Here is the thing ! Just change the contentInset to push down the UITableView content to 64 pixels (StatusBar + NavigationBar)
    //self.dashBoardTable.contentInset = UIEdgeInsetsMake(64.f, 0.f, 0.f, 0.f);
}

-(void)updateOfflineJobs:(id)sender{

    __weak UIRefreshControl *refreshControl = (UIRefreshControl *)sender;
    if(refreshControl.refreshing) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            /* (... refresh code ...) */
            //Sync
            if (![[SyncManager sharedManager] isSyncing]) {
                [[SyncManager sharedManager] fullSync];
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                // Update network activity UI
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [refreshControl endRefreshing];
                //reload the table here, too
                [self.dashBoardTable reloadData];
            });
        });
    }
   // [self.refreshControl endRefreshing];
}

//update the photo count from the Album
//-(void)getAllPictures
//{
//    photoCount = 0;
//}
//{
//    [self.loaderToUpdatePhotoCount startAnimating];
//    
//    __block int countPhoto;
//    ALAssetsLibrary *al = [[ALAssetsLibrary alloc]init];
//    [al enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop)
//     {
//         if ([[group valueForProperty:ALAssetsGroupPropertyName]isEqualToString:eGordianAlbum]) {
//             
//             countPhoto=0;
//             [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop)
//              {
//                  if([[asset valueForProperty:ALAssetPropertyType]isEqualToString:ALAssetTypePhoto]){
//                      countPhoto++;
//                  }
//                  photoCount = countPhoto;
//              }];
//         }
//     }
//                    failureBlock:^(NSError *error) { NSLog(@"Boom!!!");}
//     ];
//}


-(void)getOwnersandContractors{
    
    NSString *userID = [AppConfig getUserId];
    
    NSString *getOwnersURL = [NSString stringWithFormat:@"%@GetOwnersAndContractors?callback=JSON_CALLBACK&userId=%@",APP_BASE_URL,userID];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:getOwnersURL]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *responseData = [operation.responseString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSRange range = [jsonString rangeOfString:@"("];
        range.location++;
        range.length = [jsonString length] - range.location - 2; // removes parens and trailing semicolon
        jsonString = [jsonString substringWithRange:range];
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *jsonError = nil;
        NSArray *ownersRawArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
        NSMutableArray *ownersFinalArray = [NSMutableArray array];
        
        if (ownersRawArray.count) {
            // process jsonResponse as needed
            for (int i=0;
                 i<1;//i<ownersRawArray.count;
                 i++)
            {
                Owner *currentOwner = [Owner getOwnerAndContractorJsonDictionary:[ownersRawArray objectAtIndex:i]];
                
                [ownersFinalArray addObject:currentOwner];
                
                NSLog(@"Fetching Owners completed---\n%@",ownersFinalArray);
//                [self getJoblist:currentOwner.OwnerID];
            }
            
        } else {
            NSLog(@"Unable to parse JSON data: %@", jsonError);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"error: %@",  operation.responseString);
        [self.activityLoader stopAnimating];
    }];    
    [operation start];
}

/*
 * Method Name	: getCounts
 * Description	: get count of photos, contacts and jobs from the API
 * Parameters	: nil
 * Return value	: nil
 */
-(void) getCounts
{
    [self.activityLoader startAnimating];
    NSString *getCountURL = [NSString stringWithFormat:@"%@Users/%@",APP_HEADER_URL,[AppConfig getUserId]];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:getCountURL serviceType:EGORDIAN_API_DASHBOARD_COUNT];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if([AppDelegate isConnectedToInternet])
    {
        [self.activityLoader startAnimating];
//        [self performSelectorInBackground:@selector(getAllPictures) withObject:nil];
        [self performSelectorInBackground:@selector(getCounts) withObject:nil];
    }
    else
    {
        [self.dashBoardTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        
        NSDictionary *dictToPass = nil;
        
        if([self pendingActionToSync]){
            dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:YES]};
        }
        else{
            dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:NO]};
        }
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineRecordUpdatedNotification object:nil userInfo:dictToPass];
    }
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
}

-(void)callGTMTrackerForHomeScreen{
    
    //GTM Implementation

    [GTMLogger pushOpenScreenEventWithScreenName:@"Home Screen"];
    
}

- (IBAction)openJobsButtonTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self manualSyncButtonTapped:nil];

    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EGJobListViewController *contactListVC = (EGJobListViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGJobListViewController"];
    [self.navigationController pushViewController:contactListVC animated:YES];
    
}

- (IBAction)goToCameraTapped:(id)sender {
    
    //GTM Implementation
    NSDictionary *pushDict  = @{@"event": @"takePhoto",// openScreen// Event, name of
                                };
    [GTMLogger logEventToGTM:@"takePhoto" andinputParams:pushDict];
}


/*
 * Method Name	: onAllContacts
 * Description	: calls the global contacts view screen
 * Parameters	: id - sender
 * Return value	: IBACTION
 */
- (IBAction)onAllContacts:(id)sender
{
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    [self openContactsList];
    
}

/*
 * Method Name	: onUnattachedPhotosBtn
 * Description	: on click of Unattached photos button
 * Parameters	: id - sender
 * Return value	: IBACTION
 */
- (IBAction)onUnattachedPhotosBtn:(id)sender
{
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self openUnattachedPhotos];
    
}

#pragma mark misc
/*
 * Method Name	: openContactsList
 * Description	: open the contacts screen
 * Parameters	: nil
 * Return value	: nil
 */
-(void) openContactsList
{
    UIStoryboard *contactsStoryboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    eGordianContactsViewController *contactListVC = (eGordianContactsViewController *)[contactsStoryboard instantiateViewControllerWithIdentifier:@"eGordianContactsViewController"];
    [self.navigationController pushViewController:contactListVC animated:YES];
}

/*
 * Method Name	: openUnattachedPhotos
 * Description	: open the unattached photos
 * Parameters	: nil
 * Return value	: nil
 */
-(void) openUnattachedPhotos
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    eGordianPhotoViewController *photosCollectionVC = (eGordianPhotoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianPhotosViewController"];
    [self.navigationController pushViewController:photosCollectionVC animated:YES];
}

#pragma mark Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{

    switch (type) {
        case EGORDIAN_API_DASHBOARD_COUNT:{
            if(responseArray.count > 0){
                photoCount = 0;
//                int photoCount = [self.unAttachedPhotoCountLabel.text intValue];
                
                NSDictionary *countDict = [responseArray objectAtIndex:0];
                photoCount = [[countDict objectForKey:@"PictureCount"] intValue];
                _openJobsCount = [NSString stringWithFormat:@"%@",[countDict objectForKey:@"JobOrderCount"]];
                _photosCount = [NSString stringWithFormat:@"%d",photoCount];
                _contactsCount = [NSString stringWithFormat:@"%@",[countDict objectForKey:@"ContactCount"]];
                
                [self.dashBoardTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
               
                NSDictionary *dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:YES]};
                
                if([self pendingActionToSync]){
                    dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:YES]};
                }
                else{
                    dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:NO]};
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineRecordUpdatedNotification object:nil userInfo:dictToPass];
            }
        }
            break;
        default:
            break;
    }
}

-(void)serviceRequestFinishedWithDict:(NSMutableDictionary *)responseDict serviceType:(APIKey)type
{
    switch (type) {
        case EGORDIAN_API_DASHBOARD_COUNT:
        {
            photoCount = [[responseDict objectForKey:@"PictureCount"] intValue];
            self.jobCountLabel.text = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"JobOrderCount"]];
            self.unAttachedPhotoCountLabel.text = [NSString stringWithFormat:@"%d",photoCount];
            self.contactCountLabel.text = [NSString stringWithFormat:@"%@",[responseDict objectForKey:@"ContactCount"]];
        }
            break;
            
        default:
            break;
    }
    [self.activityLoader stopAnimating];
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type
{
    [self.activityLoader stopAnimating];
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    
    NSLog(@"Error Message---%@",alertMessage);
    
}

-(void)redirectToOfflineJobs{
    
    if([AppDelegate isConnectedToInternet]){
        UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                         message:@"Offline feature can't be accessed when the application is online."
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [dialog show];
    }
    else{
        UIStoryboard *contactsStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EGOfflineJobsViewController *egOffline = (EGOfflineJobsViewController *)[contactsStoryboard instantiateViewControllerWithIdentifier:@"EGOfflineJobsViewController"];
        [self.navigationController pushViewController:egOffline animated:YES];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
   return  1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return DM_COUNT;
}

#pragma mark UITableview delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return tableView.bounds.size.height/DM_COUNT;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        DashboardCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DashboardCustomCell"];
        if (!cell)
        {
            cell = [[DashboardCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DashboardCustomCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
    switch (indexPath.row) {
        case DM_OPEN_JOBS:
            cell.logoImgView.image =[UIImage imageNamed:@"eG_DashBoard_OpenJobs"];
            cell.titleLabel.text =@"Open Jobs";
            cell.countLabel.text =_openJobsCount;
            break;
        case DM_CONTACTS:
            cell.logoImgView.image =[UIImage imageNamed:@"eG_DashBoard_Contacts"];
            cell.titleLabel.text =@"Contacts";
            cell.countLabel.text =_contactsCount;

            break;
        case DM_PHOTOS:
            cell.logoImgView.image =[UIImage imageNamed:@"eG_DashBoard_Photos"];
            cell.titleLabel.text =@"Photos";
            cell.countLabel.text =_photosCount;

            break;
        case DM_OFFLINE_JOBS:{
            cell.logoImgView.image =[UIImage imageNamed:@"eG_DashBoard_OfflineJobs"];
            cell.titleLabel.text =@"Offline";
            NSArray *offlineJobOrdersArray = [JobOrderDAO fetchAllDataInContext:[CoreDataBase getTemporaryContext]];
            cell.countLabel.text =[NSString stringWithFormat:@"%lu",(unsigned long)offlineJobOrdersArray.count];
            
            if(self.shouldStopAnimation){
                [cell syncStarted];
            }
            else{
                if([self pendingActionToSync]){
                    
                    [cell.manualSyncView.imageView setImage:[[UIImage imageNamed:@"SyncAnimationFrame"]imageWithColor:[UIColor redColor]]];
                }
                else{
                    [cell.manualSyncView.imageView setImage:[[UIImage imageNamed:@"SyncAnimationFrame"]imageWithColor:[UIColor darkGrayColor]]];
                }
            }
            
            break;
        }
            
        default:
            break;
    }
    
    cell.manualSyncView.hidden = !(indexPath.row == DM_OFFLINE_JOBS);
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.titleLabel.textColor =cell.countLabel.textColor=[UIColor colorWithRed:70/255.0 green:81/255.0 blue:88/255.0 alpha:1];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    switch (indexPath.row) {
        case DM_OPEN_JOBS:
            [self openJobsButtonTapped:nil];
            break;
        case DM_CONTACTS:
            [self onAllContacts:nil];
            break;
        case DM_PHOTOS:
            [self onUnattachedPhotosBtn:nil];
            break;
        case DM_OFFLINE_JOBS:
            [self redirectToOfflineJobs];
            break;
            
        default:
            
            break;
    }
}

#pragma custom button delegates

-(void)buttonDidPressed:(CustomButtonView *)sender{
    if([sender isKindOfClass:[CustomButtonView class]]){
        [self manualSyncButtonTapped:nil];
    }
}

-(IBAction)manualSyncButtonTapped:(id)sender{
    
    NSLog(@"Should do Manual Sync");
    statusCount=0;
    NSManagedObjectContext *ctx =[CoreDataBase activeContext];
    NSArray *jobOrdersToSync = [JobOrderDAO fetchItemsForSyncJobOrdersInContext:ctx];
    NSArray *jobPhotostoSYnc= [AttachPhotoDAO fetchItemsForSyncJobOrdersInContext:ctx];
    if (jobOrdersToSync.count>0 |jobPhotostoSYnc.count >0 && [AppDelegate isConnectedToInternet]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Sync
            if (![[SyncManager sharedManager] isSyncing]) {
                [[SyncManager sharedManager] setStatusDelegate:self];
                [[SyncManager sharedManager] syncOut];
                _shouldDeleteRecords =YES;
                
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                // Update network activity UI
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                //reload the table here, too
            });
        });
    }else{
        if ([AppDelegate isConnectedToInternet]) {
            NSLog(@"No records to update to server so refreshing the local Ones");
            if (![[SyncManager sharedManager] isSyncing]) {
                [CoreDataBase clearUserData];//temp fix till USer recent records are fetched correctly
                [[SyncManager sharedManager] setStatusDelegate:self];
                [[SyncManager sharedManager] fullSync];
            }

        }
    }
    return;
}

-(void)pushingOfflineData{
    
    NSManagedObjectContext *ctx =[CoreDataBase activeContext];
    NSArray *jobOrdersToSync = [JobOrderDAO fetchItemsForSyncJobOrdersInContext:ctx];
    NSArray *jobPhotostoSYnc= [AttachPhotoDAO fetchItemsForSyncJobOrdersInContext:ctx];

    if (jobOrdersToSync.count == 0) {
        [CoreDataBase removeAllObjectFromTable:TBL_JOBORDERS inContext:ctx];
        
        [CoreDataBase removeAllObjectFromTable:TBL_JOBDETAILEDSCOPE inContext:ctx];
    }
    else if(jobPhotostoSYnc.count == 0){
        [CoreDataBase removeAllObjectFromTable:TBL_ATTACHPHOTO inContext:ctx];
    }
    if (![[SyncManager sharedManager] isSyncing] && _shouldDeleteRecords) {
        //[[SyncManager sharedManager] setStatusDelegate:self];
        [[SyncManager sharedManager] fullSync];
        _shouldDeleteRecords=NO;
        
    }
    
}

#pragma mark SyncStatus Delegates

-(void)syncDidStart{
    NSLog(@"Started Full Sync");
    [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONSTART object:nil];
}
-(void)syncDidFinsh{
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        if([[NSUserDefaults standardUserDefaults]boolForKey:EGAppFirstLaunch]){
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:EGAppFirstLaunch];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        if(self.shouldStopAnimation){
            self.shouldStopAnimation = FALSE;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONEND object:nil];
        
        [self.dashBoardTable reloadData];

        NSDictionary *dictToPass = nil;
        
        if([self pendingActionToSync]){
            [dictToPass setValue:[NSNumber numberWithBool:YES] forKey:EGOfflineRecordUpdateKey];
        }
        else{
            [dictToPass setValue:[NSNumber numberWithBool:NO] forKey:EGOfflineRecordUpdateKey];
        }
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineRecordUpdatedNotification object:nil userInfo:dictToPass];
        
        
        
        if (![self pendingActionToSync] && _shouldDeleteRecords ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CTSyncOutBoundStartedNotification object:nil];
        }
    });
    
    
    
    
}
-(void)syncDidStartForAction:(SyncManagerAction)status{
    if (status == SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT) {
//        statusCount ++;
            self.isJobScopeUpdated =NO;
    }
    
}
-(void)syncDidFinshForAction:(SyncManagerAction)status withError:(NSError *)error{
    
    statusCount --;
    if (error) {
        return;
    }
    if (status == SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT && !error) {
        self.isJobScopeUpdated =YES;        
    }
}

-(BOOL)pendingActionToSync{
    
    BOOL pendingToSync = NO;
    
    NSManagedObjectContext *ctx =[CoreDataBase activeContext];
    NSArray *jobOrdersToSync = [JobOrderDAO fetchItemsForSyncJobOrdersInContext:ctx];
    NSArray *jobPhotostoSYnc= [AttachPhotoDAO fetchItemsForSyncJobOrdersInContext:ctx];
    if (jobOrdersToSync.count>0 | jobPhotostoSYnc.count >0){
        pendingToSync = YES;
    }
    
    return pendingToSync;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CTSyncOutBoundStartedNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CTSYNCICONSTART
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CTSYNCICONEND
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CTDASHBOARDTABLERELOAD
                                                  object:nil];

}


@end
