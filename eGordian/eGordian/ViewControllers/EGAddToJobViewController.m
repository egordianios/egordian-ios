//
//  EGAddToJobViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/22/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGAddToJobViewController.h"
#import "egJobListForPhotoTableViewCell.h"
#import "EG_Photo.h"
#import "EGContainerViewController.h"
#import "eGordianPhotoViewController.h"

@interface EGAddToJobViewController ()

@end

@implementation EGAddToJobViewController
@synthesize  jobListArray, infoTextLabel, selectedJob, egImage, isFromPhotoList, photosToAttachArray, currentPhoto;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.egJobSearchLbl setTextColor:[UIColor colorWithRed:70.0/255 green:81.0/255 blue:88.0/255 alpha:1.0]];
    
    [self.egSearchView setBackgroundColor:[UIColor colorWithRed:233.0/255 green:239.0/255 blue:242.0/255 alpha:1.0]];
    [self.egSearchView.layer setBorderColor:[UIColor colorWithRed:150.0/255 green:179.0/255 blue:193.0/255 alpha:1.0].CGColor];
    [self.egJobSearchTextField setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
    
    pgNo = 1;
    pgSize = 100;
    previousPage = 1;
    self.jobListArray = nil;
    self.selectedJob = nil;
    [self fetchSortedJobOrders];
}

-(void)viewWillAppear:(BOOL)animated
{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    containerVC.addtoJobButton.hidden = TRUE;
    containerVC.deleteButton.hidden = TRUE;
    [containerVC resetFooterView];
    
    self.eGSearchBtn.hidden = FALSE;
    self.eGSearchCancelBtn.hidden = TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)fetchSearchResults{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    NSArray *subStrings = [self.egJobSearchTextField.text componentsSeparatedByString:@":"];
    
    if(!subStrings.count){
        return;
    }
    
    NSString *firstString = @"";
    NSString *lastString = @"";
    
    if(subStrings.count == 1){
        lastString = [subStrings objectAtIndex:0];
    }
    else if(subStrings.count > 1){
        
        firstString = [subStrings objectAtIndex:0];
        
        firstString = [firstString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        lastString = [subStrings objectAtIndex:1];
        
        lastString = [lastString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
    }
    
    if ([firstString caseInsensitiveCompare:@"City"] == NSOrderedSame){
        firstString = @"City";
    }
    else if([firstString caseInsensitiveCompare:@"Owner"] == NSOrderedSame){
        firstString = @"ClientName";
    }
    
    if(lastString.length > 0)
    {
        NSString *strParamValue = [NSString stringWithFormat:@"%@ eq %@",firstString, lastString];
        
        strParamValue = [AppConfig urlencode:strParamValue];
        
        NSString *jobListURL = [NSString stringWithFormat:@"%@Users/%@/JobOrders?filter=%@&sortBy=LastUpdatedDate",APP_HEADER_URL, [AppConfig getUserId], strParamValue];
        
        [self startAnimating];
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        
        [[DataDownloader sharedDownloader]getResponseFormUrl:jobListURL serviceType:EGORDIAN_API_JOBORDERS_LIST];
    }
    else{
        UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                         message:@"Invalid Search"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil, nil];
        
        [dialog show];
    }

}


-(void)fetchSortedJobOrders{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    NSString *jobListURl = [NSString stringWithFormat:@"%@Users/%@/JobOrders?&pagesize=%d&pagenumber=%d&sortBy=LastUpdatedDate",APP_HEADER_URL,[AppConfig getUserId],pgSize,pgNo];
    
    [self startAnimating];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader]getResponseFormUrl:jobListURl serviceType:EGORDIAN_API_JOBORDERS_LIST];
}

- (IBAction)onSave:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    if(!self.selectedJob)
        return;
    
    if(!self.isFromPhotoList)//this is from the photo detail screen
    {
        [self attachPhotoFromPhotoDetailScreen];
    }
    else
    {
        [self attachPhotoFromPhotoListScreen];
    }
}

- (IBAction)onSearchCancelBtn:(id)sender {
    self.eGSearchBtn.hidden = NO;
    
    self.eGSearchCancelBtn.hidden = YES;
    
    self.egJobSearchTextField.text = @"";
    [self fetchSortedJobOrders];
}

//attach photo from the photo detail screen. only one photo at a time
-(void)attachPhotoFromPhotoDetailScreen
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    
    
    NSString *photoToJobURLString = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/AttachPictures",APP_HEADER_URL,self.selectedJob.ownerID,self.selectedJob.jobOrderId];
    
    NSDictionary* jsonDict = @{@"id": currentPhoto.Id,
                               @"UserID":([AppConfig getUserId])?[AppConfig getUserId]:@"",
                               @"PictureName":currentPhoto.PictureName };
    
    [self startAnimating];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]updateData:[NSArray arrayWithObjects:jsonDict, nil] andServiceURl:photoToJobURLString andserviceType:EGORDIAN_API_PHOTOS_ATTACH_TO_JOB ];
    
}

//attach photos from photo detail screen. Multiple photos at a time
//this is to attach local photos and not used currently
-(void)attachPhotoFromPhotoListScreen
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    if(!self.photosToAttachArray.count || !self.selectedJob)
        return;
    
    [self startAnimating];
    
    NSString *photoToJobURLString = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/AttachPictures",APP_HEADER_URL,self.selectedJob.ownerID,self.selectedJob.jobOrderId];
    
    NSMutableArray *photosToAttachDictArray = [[NSMutableArray alloc] init];

    for (EG_Photo* photo in self.photosToAttachArray)
    {

        NSDictionary* jsonDict = @{@"id": photo.Id,
                                   @"UserID":([AppConfig getUserId])?[AppConfig getUserId]:@"",
                                   @"PictureName":photo.PictureName };
        
        [photosToAttachDictArray addObject:jsonDict];
    }
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]updateData:photosToAttachDictArray andServiceURl:photoToJobURLString andserviceType:EGORDIAN_API_PHOTOS_ATTACH_TO_JOB ];
}

- (IBAction)addToJobBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSearch:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    self.jobListArray = nil;
    
    [self fetchSearchResults];
}

- (IBAction)textChanged:(id)sender {
     UITextField* textField = (UITextField*)sender;
    if(textField.text.length >= 2)
    {
        self.eGSearchCancelBtn.hidden = FALSE;
        self.eGSearchBtn.hidden = TRUE;
    }
    else
    {
        self.eGSearchCancelBtn.hidden = TRUE;
        self.eGSearchBtn.hidden = FALSE;
    }
}

#pragma mark - UITextField delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    [self.egJobSearchBtn setHidden:TRUE];
    
    if(textField.text.length >= 2)
    {
        self.eGSearchCancelBtn.hidden = FALSE;
        self.eGSearchBtn.hidden = TRUE;
    }
    else
    {
        self.eGSearchCancelBtn.hidden = TRUE;
        self.eGSearchBtn.hidden = FALSE;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    [self.egJobSearchBtn setHidden:FALSE];
    
    if(!textField.text.length)
    {
        pgNo=1;
        previousPage = 1;
        [self fetchSortedJobOrders];
        [textField resignFirstResponder];
    }
    
//    UITextField* textField = (UITextField*)sender;
    
    if(textField.text.length >= 2){
        [self.eGSearchBtn setHidden:YES];
    }
    else if (textField.text.length == 0){
        [self.eGSearchCancelBtn setHidden:YES];
        [self.eGSearchBtn setHidden:YES];
    }
    else{
        [self.eGSearchCancelBtn setHidden:NO];
        [self.eGSearchBtn setHidden:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.jobListArray = nil;
    [self.egJobSearchBtn setHidden:FALSE];
    [textField resignFirstResponder];
    [self fetchSearchResults];
    return YES;
}

#pragma mark - tableview datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(self.jobListArray)
        return [self.jobListArray count];
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"egJobListTableViewCell";
    egJobListForPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[egJobListForPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    EG_JobOrder *jobOrder = nil;
    
    jobOrder = (EG_JobOrder *)[self.jobListArray objectAtIndex:indexPath.row];
    cell.egJobLabel.text = jobOrder.jobTitle;
    cell.egJobOwnerLbl.text = jobOrder.ClientName;
    cell.egJobIDLbl.text = [NSString stringWithFormat:@"ID: %@", jobOrder.jobOrderId];
    
    if([jobOrder.jobOrderId isEqualToString:self.selectedJob.jobOrderId])
      [ cell.egSelectedJobImgView setImage:[UIImage imageNamed:@"egSelectJobTick"]];
    else
        [cell.egSelectedJobImgView setImage:[UIImage imageNamed:@"egSelectJob"]];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    EG_JobOrder *jobOrder = [self.jobListArray objectAtIndex:indexPath.row];
    self.selectedJob = jobOrder;
    [self.egJobListTableView reloadData];
}

#pragma mark - Data Downloader Manager Delegates
- (void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    switch (type) {
        case EGORDIAN_API_JOBORDERS_LIST:
        {
            
            if(self.jobListArray)
            {
                [self.jobListArray addObjectsFromArray:responseArray];
            }
            else{
                self.jobListArray = [responseArray mutableCopy];
                [self.egJobListTableView setContentOffset:CGPointMake(0, 0)];
            }
            
            [self.egJobListTableView reloadData];
            
            [self stopAnimating];
            
            if(self.jobListArray.count == 0){
                
                [self showNullRecords];
            }
            else{
                [self.infoTextLabel removeFromSuperview];
            }
        }
            break;
            
        case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
        {
            [self stopAnimating];
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_USERPHOTO_REFRESH_FLAG];
            NSArray *viewCtrlrs=self.navigationController.viewControllers;
            for (UIViewController *currVC in viewCtrlrs) {
                if ([currVC isKindOfClass:[eGordianPhotoViewController class]]) {
                    [self.navigationController popToViewController:currVC animated:YES];
                    break;
                }
            }

        }
            break;
            
        default:
            break;
    }
}

- (void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    self.jobListArray = nil;
    [self.egJobListTableView reloadData];
    [self stopAnimating];
    
    [self showNullRecords];
    
}

-(void)showNullRecords{
    if (!self.infoTextLabel)
        self.infoTextLabel = [[UILabel alloc] init];
    self.infoTextLabel.frame = CGRectMake(self.egJobListTableView.frame.origin.x, self.view.frame.size.height/2, self.egJobListTableView.frame.size.width, 60); //self.jobListTableView.bounds;
    self.infoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.infoTextLabel.backgroundColor = [UIColor clearColor];
    self.infoTextLabel.textAlignment = NSTextAlignmentCenter;
    self.infoTextLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    self.infoTextLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:20];
    self.infoTextLabel.text = @"No Items Found";
    [self.view addSubview:self.infoTextLabel];
    [self.view bringSubviewToFront:self.infoTextLabel];
    [self.egJobListTableView reloadData];
}

-(void)startAnimating{
    
    [self.egLoadView setHidden:NO];
    [self.view bringSubviewToFront:self.egLoadView];
}

-(void)stopAnimating{
    [self.egLoadView setHidden:YES];
}

#pragma mark - Scroll Delegates

/*
 * Method Name	: scrollViewDidEndDecelerating
 * Description	: called on scrolling end.
 * Parameters	: UIScrollView
 * Return value	: nil
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    if(self.egJobSearchTextField.text.length) return;
    
    CGFloat bottomInset = scrollView.contentInset.bottom;
    CGFloat bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height - bottomInset;
    if (bottomEdge == scrollView.contentSize.height) {
        // Scroll view is scrolled to bottom
        previousPage = pgNo;
        pgNo++;
        [self performSelector:@selector(fetchSortedJobOrders) withObject:nil afterDelay:1];
    }
    
}


@end
