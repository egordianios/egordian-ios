//
//  eGordianJobPhotoDetailViewController.m
//  eGordian-iOS
//
//  Created by Laila on 05/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "eGordianJobPhotoDetailViewController.h"
#import <UIImageView+WebCache.h>
#import "EGContainerViewController.h"

@interface eGordianJobPhotoDetailViewController ()

@end

@implementation eGordianJobPhotoDetailViewController

@synthesize currentPhoto, currentJobOrder;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(self.currentPhoto)
        [self loadImageWithPhoto];
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    [containerVC updateFooterView:@"PhotoDetail"];
    containerVC.addtoJobButton.hidden = TRUE;
    [containerVC.deleteButton setEnabled:TRUE];
    containerVC.delegate = self;
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

-(void) viewDidDisappear:(BOOL)animated
{
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    [containerVC resetFooterView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) loadImageWithPhotoAPI//:(EG_Photo *)photo
{
    [self startAnimating];
    NSString* photoURL = currentPhoto.DownloadLink;
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:photoURL serviceType:EGORDIAN_API_PHOTO_DETAIL];
}

-(void)loadImageWithPhoto
{
    if(!self.currentPhoto) return;

    if (self.currentPhoto.DownloadLink) {
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = self.eGJobPhotoImageView;
        [self.eGJobPhotoImageView sd_setImageWithURL:[NSURL URLWithString:self.currentPhoto.fullSizeLink]
                                    placeholderImage:nil
                                             options:SDWebImageProgressiveDownload
                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                if (!activityIndicator) {
                                                    [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                    activityIndicator.center = weakImageView.center;
                                                    [activityIndicator startAnimating];
                                                }
                                            }
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                               [activityIndicator removeFromSuperview];
                                               activityIndicator = nil;
                                           }];
    }
    
}

-(void) deletePhoto
{
    
    if(!self.currentPhoto || !self.currentJobOrder)
        return;
    
    NSString *photoID= @"";
    
    if([currentPhoto.DownloadLink rangeOfString:@"Pictures/"].location != NSNotFound){
        NSString *originalString = currentPhoto.DownloadLink;
        NSRange end = [originalString rangeOfString:@"Pictures/"];
        photoID = [originalString substringFromIndex:(end.location+@"Pictures/".length)];
        end = [photoID rangeOfString:@"/"];
        photoID = [photoID substringToIndex:end.location];
    }
    
    NSString *deleteJobPhotoURL = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/Pictures/%@", APP_HEADER_URL, currentJobOrder.ownerID, currentJobOrder.jobOrderId, photoID];
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:deleteJobPhotoURL serviceType:EGORDIAN_API_JOB_PHOTO_DELETE];

}

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    [self stopAnimating];
    switch (type) {
            
        case EGORDIAN_API_PHOTO_DETAIL:
        {
            self.eGJobPhotoImageView.image = [UIImage imageWithData:[responseArray objectAtIndex:0]];
            [EGordianUtilities updateRefreshStatus:NO forPage:APP_JOBPHOTO_REFRESH_FLAG];
        }
            break;
            
        case EGORDIAN_API_JOB_PHOTO_DELETE:
        {
            
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_JOBPHOTO_REFRESH_FLAG];
            [[((UINavigationController *)self.parentViewController) viewControllers] objectAtIndex:0].hidesBottomBarWhenPushed = NO;
            [self.navigationController popViewControllerAnimated:YES];
            
        }
            break;
            
        default:
            break;
    }
}

-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type
{
    [EGordianUtilities updateRefreshStatus:NO forPage:APP_JOBPHOTO_REFRESH_FLAG];
    
}

- (IBAction)onBack:(id)sender {
    [[((UINavigationController *)self.parentViewController) viewControllers] objectAtIndex:0].hidesBottomBarWhenPushed = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)startAnimating{
    
    [self.egLoadView setHidden:NO];
    [self.egLoadView setUserInteractionEnabled:YES];
    [self.view bringSubviewToFront:self.egLoadView];
}

-(void)stopAnimating{
    [self.egLoadView setHidden:YES];
}
@end
