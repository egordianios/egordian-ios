//
//  eGordianContactsViewController.m
//  eGordian-iOS
//
//  Created by Laila on 25/08/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "eGordianContactsViewController.h"
#import "eGordianContactsCell.h"
#import "APIClient.h"
#import "SortViewListCell.h"
#import "eGordianContact.h"
#import "AFNetworking.h"
#import "eGordianViewContactsViewController.h"
#import "eGordianTelephoneViewController.h"

#define searchKeyFirstName @"FirstName"
#define searchKeyLastName @"LastName"
#define searchKeyCompany @"CompanyName"
#define searchKeyDisplayName @"DisplayName"
#define PAGESIZE 400


@interface eGordianContactsViewController ()<DataDownloaderDelegate>
{
}
//sort on which (Alphabetical, owner etc)
@property (nonatomic, strong) NSArray* searchByArr;
//boolean to know if the searchBy selection view is currently visible.
@property (nonatomic, assign) BOOL isSearchView;
//boolean variable to know if the view is in alphabetical sort mode.
@property (nonatomic, assign) BOOL isAlphabeticalSort;
// contains the contacts as got from the service request
@property (nonatomic, strong) NSMutableArray* contactsArr;
// to keep track of what is the next page to load
@property (nonatomic, assign) int currentPage;
// contains the contacts as got from the Search Contacts service request
@property (nonatomic, strong) NSMutableArray* searchContactsArr;

@property (nonatomic) BOOL alreadyAppeared;

@end

@implementation eGordianContactsViewController

@synthesize isSearchView, searchByArr, contactsArr, currentPage, searchContactsArr;
@synthesize spinner, sectionDict, isAlphabeticalSort;
@synthesize filteredContacts;

#pragma mark - view load
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.egLoadView setHidden:YES];
    
    [self.searchImage.layer setBorderColor:[EGordianUtilities headerColor].CGColor];
   
    self.eGSearchByTblVw.backgroundColor = [UIColor colorWithRed:.95 green:.95 blue:.95 alpha:1.0];
    [self performInit];
    
}

/*
 * Method Name	: viewWillAppear
 * Description	: Called when the view is about to made visible. Default does nothing
 * Parameters	: BOOL
 * Return value	: nil
 */
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(!self.alreadyAppeared){
        
        self.alreadyAppeared = YES;
        
        self.eGSearchBtn.hidden = NO;
        
        self.cancelButton.hidden = YES;
    }
    else{
        if(self.contactsSearchTF.text.length > 0){
            self.cancelButton.hidden = NO;
            self.eGSearchBtn.hidden = YES;
        }
        else{
            self.cancelButton.hidden = YES;
            self.eGSearchBtn.hidden = NO;
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma end

#pragma mark - Table view data source

/*
 * Method Name	: numberOfSectionsInTableView
 * Description	: returns the number of sections in tableview
 * Parameters	: UITableView
 * Return value	: NSInteger (number of sections in tableview)
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if([tableView isEqual:self.eGSearchByTblVw])
        return 1;
    
    if(!self.sectionDict)
        return 1;
    
    return [[self.sectionDict allKeys] count];
}

/*
 * Method Name	: numberOfRowsInSection
 * Description	: returns the number of rows in a section
 * Parameters	: UITableView, NSInteger
 * Return value	: NSInteger (number of cells in the section)
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    if([tableView isEqual:self.eGSearchByTblVw])
    {
        if(![self.searchByArr count])
            return 0;
        
        return [self.searchByArr count];
    }
    
    if(![self.sectionDict count])
        return 0;
    
    return [[self.sectionDict valueForKey:[[[self.sectionDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}


/*
 * Method Name	: cellForRowAtIndexPath
 * Description	: Row display.
 * Parameters	: UITableView, NSIndexPath
 * Return value	: UITableViewCell (the cell to be displayed)
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([tableView isEqual:self.eGSearchByTblVw]){
        UITableViewCell* searchByCell = [tableView dequeueReusableCellWithIdentifier:@"tableViewCell"];
        
        if(!searchByCell)
        {
            searchByCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tableViewCell"];
        }
        
        searchByCell.textLabel.text = [self.searchByArr objectAtIndex:indexPath.row];
        searchByCell.textLabel.textColor = [UIColor blackColor];
        searchByCell.textLabel.highlightedTextColor = [UIColor whiteColor];
        searchByCell.textLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:13];
        [searchByCell.selectedBackgroundView setBackgroundColor: [UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:70/255.0 alpha:1.0]];
        [searchByCell.contentView setBackgroundColor: [UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:70/255.0 alpha:1.0]];
        [searchByCell.textLabel setBackgroundColor:[UIColor clearColor]];
        
        return searchByCell;
    }
    else if([tableView isEqual:self.contactsTableView])
    
    {
        static NSString *cellIdentifier = @"eGordianContactsCell";
        
        eGordianContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            
            cell = [[eGordianContactsCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                               reuseIdentifier:cellIdentifier];
        }
        
        
        eGordianContact *contact = [[self.sectionDict valueForKey:[[[self.sectionDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        
        cell.addressLbl.text = [NSString stringWithFormat:@"%@ %@",contact.FirstName?contact.FirstName:@"",contact.LastName?contact.LastName:@""];
        
//        cell.addressLbl.font = [UIFont fontWithName:FONT_REGULAR size:13];
        
        if(isAlphabeticalSort){
            cell.companyLbl.text = contact.CompanyName;
//            cell.companyLbl.font = [UIFont fontWithName:FONT_REGULAR size:10];
//            cell.companyLbl.textColor = [UIColor colorWithRed:50.0/255.0 green:53.0/255.0 blue:55.0/255.0 alpha:1.0];
        }
        else{
            cell.companyLbl.text = @"";
        }
        return cell;
    }
    return nil;
}

/*
 * Method Name	: heightForRowAtIndexPath
 * Description	: variable height support
 * Parameters	: UITableView, NSIndexPath
 * Return value	: CGFloat
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([tableView isEqual:self.eGSearchByTblVw])
        return self.eGSearchByTblVw.frame.size.height/[self.searchByArr count];
    
    if([tableView isEqual:self.contactsTableView])
    {
        if(!self.isAlphabeticalSort)//company name not shown, so cell height can be less
            return 50;
        else
            
            return 60;
    }
    
    return 30;
}

/*
 * Method Name	: heightForHeaderInSection
 * Description	: variable section height support
 * Parameters	: UITableView, section
 * Return value	: CGFloat
 */
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if([tableView isEqual:self.contactsTableView])
        return 30;
    
    return 0;
}

/*
 * Method Name	: willDisplayCell
 * Description	: for display customisation
 * Parameters	: UITableView, uitableviewcell, NSIndexPath
 * Return value	: void
 */
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect frame = cell.frame;
    frame.size.width = tableView.frame.size.width;
    cell.frame = frame;
    
    if(indexPath.row == selectedSearchBy)
        cell.textLabel.textColor = [UIColor whiteColor];
    else
        cell.textLabel.textColor = [UIColor blackColor];
}

/*
 * Method Name	: sectionIndexTitlesForTableView
 * Description	: return list of section titles to display in section index view (e.g. "ABCD...Z#")
 * Parameters	: UITableView
 * Return value	: NSArray
 */
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(![tableView isEqual:self.eGSearchByTblVw] && isAlphabeticalSort)
    {
        return [[self.sectionDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }

    return nil;
}

/*
 * Method Name	: viewForHeaderInSection
 * Description	: title for the header
 * Parameters	: NSInteger
 * Return value	: NSString (title)
 */
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(![tableView isEqual:self.eGSearchByTblVw])
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor colorWithRed:156.0/255.0 green:182.0/255.0 blue:197.0/255.0 alpha:1.0]];
        
        UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,0,tableView.bounds.size.width-30,30)];
        tempLabel.backgroundColor=[UIColor clearColor];
        tempLabel.textColor = [UIColor whiteColor];
        tempLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:15];
        tempLabel.text = [[[self.sectionDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        [headerView addSubview:tempLabel];
        
        return headerView;
        
//        return [[[self.sectionDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    }
    
    return nil;
}

/*
 * Method Name	: didSelectRowAtIndexPath
 * Description	: Called after the user changes the selection.
 * Parameters	: NSIndexPath
 * Return value	: nil
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    
    if([tableView isEqual:self.eGSearchByTblVw])
        
    {
        [[tableView cellForRowAtIndexPath:indexPath].contentView setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:185.0/255.0 blue:70/255.0 alpha:1.0]];
        selectedSearchBy = (int)indexPath.row;//setting the current searchBy index.
        self.contactsSearchTF.text = @"";
        [tableView reloadData];
        [self.eGSearchByBtn setTitle:[self.searchByArr objectAtIndex:selectedSearchBy] forState:UIControlStateNormal] ;
        self.eGSearchByView.hidden = TRUE;
        [self.view bringSubviewToFront:self.eGSearchByBtn];
        self.eGSearchByBtn.hidden = FALSE;
        self.isSearchView = FALSE;

        UIImage *image = [UIImage imageNamed:@"eG_Contacts_DownArrow"];
        if(selectedSearchBy==1)//company - udjust the text and image to fit properly
        {
            [self.eGSearchByBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -6.0, 0.0, 0.0)];
            [self.eGSearchByBtn setImageEdgeInsets:UIEdgeInsetsMake(3.0, self.eGSearchByBtn.frame.size.width - image.size.width-5, 0.0, 0.0)];
        }
        else
        {//set it back to default
            [self.eGSearchByBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
            [self.eGSearchByBtn setImageEdgeInsets:UIEdgeInsetsMake(3.0, self.eGSearchByBtn.frame.size.width - 2*image.size.width, 0.0, 0.0)];
        }
    }
    else
    {
        eGordianContact* theContact = [[self.sectionDict valueForKey:[[[self.sectionDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        if(theContact.ID)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
            eGordianViewContactsViewController* contactViewController = (eGordianViewContactsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"eGordianViewContactsViewController"];
            contactViewController.redirectVia = @"GlobalContacts";
            
            contactViewController.contactDetailURL = theContact.ContactLink;//[[eGordianContact alloc] init];
            [self.navigationController pushViewController:contactViewController animated:YES];
            
        }
        
    }
}

#pragma mark - Scroll Delegates

/*
 * Method Name	: scrollViewDidEndDecelerating
 * Description	: called on scrolling end.
 * Parameters	: UIScrollView
 * Return value	: nil
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.contactsSearchTF.text.length) return;
    
    if(self.isSearchView)
        return;
    
    if(isSearch)
        return;
    
    CGFloat bottomInset = scrollView.contentInset.bottom;
    CGFloat bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height - bottomInset;
    if (bottomEdge == scrollView.contentSize.height) {
        // Scroll view is scrolled to bottom
        previousPage = pgNo;
        pgNo++;
        [self performSelector:@selector(fetchContacts) withObject:nil afterDelay:1];
    }
    
}


#pragma mark - ButtonHandlers

/*
 * Method Name	: onBackBtn
 * Description	: on Back button click
 * Parameters	: id - Sender
 * Return value	: IBAction
 */
- (IBAction)onBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
 * Method Name	: onSearch
 * Description	: on search button click.
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSearch:(id)sender {
    
    if(!self.contactsSearchTF.text.length)
        return;
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    self.isSearchView = FALSE;
    
    self.eGSearchByView.hidden = TRUE;
    
    [self.eGSearchByTblVw reloadData];
    
    if(self.searchContactsArr)
        self.searchContactsArr = NULL;
    
    self.searchContactsArr = [[NSMutableArray alloc] init];
    
    isSearch = TRUE;
    
    [self setSelectedButton:0];//set the selected sort button to sortByRecent
    
    if(self.contactsSearchTF.text.length > 0){
        
        NSArray *subStrings = [self.contactsSearchTF.text componentsSeparatedByString:@":"];
        
        if(!subStrings.count){
            return;
        }
        
        NSString *firstString = @"";
        NSString *lastString = @"";
        
        if(subStrings.count == 1){
            lastString = [subStrings objectAtIndex:0];
        }
        else if(subStrings.count > 1){
            
            firstString = [subStrings objectAtIndex:0];
            
            firstString = [firstString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            lastString = [subStrings objectAtIndex:1];
            
            lastString = [lastString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
        }
        
        if ([firstString caseInsensitiveCompare:@"Company"] == NSOrderedSame){
            firstString = @"Company";
        }
        else if([firstString caseInsensitiveCompare:@"Name"] == NSOrderedSame){
            firstString = @"LastName";
        }
        else{
            firstString = @"LastName";
        }
        
        if(lastString.length > 0){
            
            [self startAnimating];
            
            NSString *strParamValue = [NSString stringWithFormat:@"%@ eq %@",firstString, lastString];
            
            strParamValue = [AppConfig urlencode:strParamValue];
            
            NSString *contactSearchURl = [NSString stringWithFormat:@"%@Users/%@/Contacts?filter=%@",APP_HEADER_URL,[AppConfig getUserId],strParamValue];
            
            [[DataDownloader sharedDownloader]setDataDownloaderDelegate:self];
            
            [[DataDownloader sharedDownloader]getResponseFormUrl:contactSearchURl serviceType:EGORDIAN_API_CONTACTS_SEARCH];
        }
        else{
            UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                             message:@"Invalid Search"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil, nil];
            
            [dialog show];
        }
        
    }
}

- (IBAction)searchResultCancelTapped:(id)sender {
    
    [self startAnimating];
    
    self.eGSearchBtn.hidden = NO;
    
    self.cancelButton.hidden = YES;
    
    self.contactsSearchTF.text = @"";
    
    isSearch = FALSE;
    currentPage = 1;
    self.isAlphabeticalSort = TRUE;
    
    //done to change the color of Sort by MostRecent to Selected Color.
    [self setSelectedButton:0];
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self fetchContacts];
    
    [self stopAnimating];
}

/*
 * Method Name	: onSortBtn
 * Description	: on sort button collection click
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSortBtn:(id)sender
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    BOOL prevSortKey = self.isAlphabeticalSort;
    
    UIButton* sortBtn = (UIButton*)sender;
    
    if([sortBtn.currentTitle isEqualToString: @"Name"])
        self.isAlphabeticalSort = TRUE;
    else if ([sortBtn.currentTitle isEqualToString: @"Company"])
        self.isAlphabeticalSort = FALSE;
    
    if(prevSortKey == self.isAlphabeticalSort)//don't do anything if there is no change in the sortkey
        return;
    
    sortBtn.selected = TRUE;
    [self setSelectedButton:sortBtn.tag]; //set the color of the selected button
    
    if(self.contactsSearchTF.text.length)
        [self indexSortInitializers:self.searchContactsArr];
    else
    {
        pgNo = 1;
        currentPage = 1;
        self.contactsArr = nil;
        self.searchContactsArr = nil;
        [self fetchContacts];
    }
}

/*
 * Method Name	: onSearchByBtn
 * Description	: on Search By button click ( decides on which parameter the search must happen)
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onSearchByBtn:(id)sender
{
    if(![self.contactsArr count]) return;
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    self.eGSearchByView.hidden = FALSE; //don't hide the searchby tableview
    self.isSearchView = TRUE;
    [self.eGSearchByTblVw reloadData];
    self.eGSearchByBtn.hidden = TRUE;
    [self.view bringSubviewToFront:self.eGSearchByTblVw];
    
}

#pragma mark - TextField Delegate

/*
 * Method Name	: onSearchTextChanged
 * Description	: called when the TextField is edited
 * Parameters	: id - Sender
 * Return value	: IBAction
 */
- (IBAction)onSearchTextChanged:(id)sender
{
    if(self.contactsSearchTF.text.length >= 2)
    {
        self.cancelButton.hidden = FALSE;
        self.eGSearchBtn.hidden = TRUE;
    }
    else
    {
        self.cancelButton.hidden = TRUE;
        self.eGSearchBtn.hidden = FALSE;
    }
}



/*
 * Method Name	: onTextFieldEndEditing
 * Description	: on editing did end
 * Parameters	: id - sender
 * Return value	: IBAction
 */
- (IBAction)onTextFieldEndEditing:(id)sender {
    
    UITextField* textField = sender;
    
    if(textField.text.length >= 2){
        [self.eGSearchBtn setHidden:YES];
    }
    else if (textField.text.length == 0){
        [self.cancelButton setHidden:YES];
        [self.eGSearchBtn setHidden:YES];
    }
    else{
        [self.eGSearchBtn setHidden:NO];
        [self.cancelButton setHidden:YES];
    }
}

/*
 * Method Name	: textFieldShouldReturn
 * Description	: called when 'return' key pressed. return NO to ignore.
 * Parameters	: UITextField - textField
 * Return value	: BOOL
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*
 * Method Name	: textFieldDidBeginEditing
 * Description	: called when typing the keybord.
 * Parameters	: UITextField - textField
 * Return value	: void
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    [self.eGSearchBtn setHidden:YES];
//    [self.cancelButton setHidden:NO];
    
    
}

/*
 * Method Name	: textFieldDidEndEditing
 * Description	: called when 'return' key pressed. return NO to ignore.
 * Parameters	: UITextField - textField
 * Return value	: void
 */

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length > 0){
        
        [self onSearch:nil];
    }
    else
    {
        isSearch = FALSE;
        
        pgNo = 1;
        
        [self fetchContacts];
    }
}


/*
 * Method Name	: textFieldShouldEndEditing
 * Description	: called when focus is lost.
 * Parameters	: UITextField - textField
 * Return value	: BOOL
 */
- (BOOL) textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*
 * Method Name	: showNullRecords
 * Description	: show null records label when there are no records to be displayed
 * Parameters	: nil
 * Return value	: nil
 */
-(void) showNullRecords
{
    if (!infoTextLabel)
        infoTextLabel = [[UILabel alloc] init];
    infoTextLabel.frame = CGRectMake(self.contactsTableView.frame.origin.x, self.view.frame.size.height/2, self.contactsTableView.frame.size.width, 60);
    infoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    infoTextLabel.backgroundColor = [UIColor clearColor];
    infoTextLabel.textAlignment = NSTextAlignmentCenter;
    infoTextLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    infoTextLabel.font = [UIFont fontWithName:FONT_SEMIBOLD size:20];
    infoTextLabel.text = @"No Items Found";
    [self.view addSubview:infoTextLabel];
    [self.view bringSubviewToFront:infoTextLabel];
}

#pragma mark - Data Downloader Delegates
/*
 * Method Name	: serviceRequestFinished
 * Description	: on successful completion of the service request call
 * Parameters	: NSArray - responseArray APIKey - service type
 * Return value	: nil
 */
-(void) serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    if(type == EGORDIAN_API_GLOBAL_CONTATCS){
        
        NSArray *formattedArray = [eGordianContact getContactArray:responseArray];
        
        if(previousPage!=pgNo){
            [self.contactsArr addObjectsFromArray:formattedArray];//[eGordianContact getContactArray:responseArray];
        }
        else
        {
            [self.contactsArr removeAllObjects];
            self.contactsArr = [formattedArray mutableCopy];
        }
        
        if(![self.contactsArr count])
        {
            [self.contactsTableView reloadData];
            self.sectionDict = nil;
            [self showNullRecords];
        }
        else
        {
            [infoTextLabel removeFromSuperview];
            [self indexSortInitializers:self.contactsArr];
        }
    
        [self stopAnimating];
        
    }
    else if(type == EGORDIAN_API_CONTACTS_SEARCH){
        
        self.eGSearchBtn.hidden = YES;
        
        self.cancelButton.hidden = NO;
        
        NSLog(@"search Contacts Result--\n%@",responseArray);
        
        [self.searchContactsArr removeAllObjects];
        
        self.searchContactsArr = [eGordianContact getContactArray:responseArray];
        
        if(![self.searchContactsArr count])
        {
            self.sectionDict = nil;
            [self.contactsTableView reloadData];
            [self showNullRecords];
        }
        else
        {
            [infoTextLabel removeFromSuperview];
            [self indexSortInitializers:self.searchContactsArr];
        }
        
        [self stopAnimating];
    }
}

/*
 * Method Name	: serviceRequestFailed
 * Description	: on failure of the service request call
 * Parameters	: NSError - error APIKey - service type
 * Return value	: nil
 */
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    [self stopAnimating];
   
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    NSLog(@"Error Message--%@",alertMessage);
}


#pragma mark - UIResponder delegates

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    [self.contactsSearchTF resignFirstResponder];
    
    
    //hide the searchby tableview if clicked somewhere else than the searchby tableview, while this tableview was not hidden
    if(!self.eGSearchByView.hidden){ // check if searchby tableview is visible
        
        UITouch* touch = [touches anyObject];
        CGPoint touchLocation = [touch locationInView:self.view];
        
        if(!CGRectContainsPoint(self.eGSearchByTblVw.frame, touchLocation)){
            // hide the tableview
            self.eGSearchByView.hidden = TRUE;
            self.eGSearchByBtn.hidden = FALSE;
            self.isSearchView = FALSE;
        }
    }
    
        [super touchesBegan:touches withEvent:event];
}

#pragma mark - Misc
/*
 * Method Name	: setSelectedButton
 * Description	: among the sort buttons, set the current selection
 * Parameters	: selected button tag
 * Return value	: nil
 */
-(void) setSelectedButton:(NSInteger)selectedTag
{
    for (UIButton* btn in self.eGSortByBtnCollection)
    {
        if(btn.tag == selectedTag)
            btn.selected = TRUE;
        else
            btn.selected = FALSE;
    }
    
}

/*
 * Method Name	: fetchContacts
 * Description	: fetch all contacts from sever
 * Parameters	: nil
 * Return value	: nil
 */
- (void)fetchContacts {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self startAnimating];
    
    //    [APIClient cancelAllRequest];
    
    if(!self.contactsArr)
        self.contactsArr = [[NSMutableArray alloc] init];
    
    NSString* userID = [AppConfig getUserId];
    
    NSString *contactsURLString = @"";
    self.sectionDict = nil;
    
    if(self.isAlphabeticalSort)
        contactsURLString = [NSString stringWithFormat:@"%@users/%@/Contacts?sortBy=LastName&pageSize=%d&pageNumber=%d", APP_HEADER_URL,userID,pgSize, pgNo];
    else
        contactsURLString = [NSString stringWithFormat:@"%@users/%@/Contacts?sortBy=Company&pageSize=%d&pageNumber=%d",APP_HEADER_URL,userID,pgSize, pgNo];
    
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader]getResponseFormUrl:contactsURLString serviceType:EGORDIAN_API_GLOBAL_CONTATCS];
    
    [self.cancelButton setHidden:YES];
    
    [self.eGSearchBtn setHidden:NO];
    
    
}



/*
 * Method Name	: updateCount
 * Description	: updates the count of records on the label
 * Parameters	: nil
 * Return value	: nil
 */
-(void) updateCount
{
    NSString* contactsCountStr =nil;
    if(isSearch)
        contactsCountStr = [NSString stringWithFormat:@"%lu Contacts", (unsigned long)[self.searchContactsArr count]];
    else
        contactsCountStr = [NSString stringWithFormat:@"%lu Contacts", (unsigned long)[self.contactsArr count]];
    self.eGordianContactsNumberLbl.text = contactsCountStr;
}

/*
 * Method Name	: performInit
 * Description	: initialize the view positions and variables
 * Parameters	: nil
 * Return value	: nil
 */
-(void) performInit
{
    previousPage = 1;
    
    selectedSearchBy = 0;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.eGSearchByBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(4.0, 4.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.eGSearchByBtn.layer.mask = maskLayer;
    
    
    [self.contactsSearchTF setValue:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    self.searchByArr = [[NSMutableArray alloc] initWithObjects:@"Name", @"Company", nil];
    [self.eGSearchByBtn setTitle:[self.searchByArr objectAtIndex:selectedSearchBy] forState:UIControlStateNormal];
    
    [self setFrame];
    self.isSearchView = NO;
    self.isAlphabeticalSort = YES;
    
    self.contactsTableView.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    //tableview
    self.contactsTableView.dataSource = self;
    self.contactsTableView.delegate = self;
    
    pgNo = 1;
    pgSize = PAGESIZE;
    
    self.filteredContacts = nil;
    self.eGSearchByView.hidden = TRUE;
    
    [self fetchContacts];
}

/*
 * Method Name	: setFrame
 * Description	: set the frame of search tableview
 * Parameters	: nil
 * Return value	: nil
 */
-(void) setFrame
{
    [self.eGSearchByView setFrame:CGRectMake(self.eGSearchByBtn.frame.origin.x, self.eGSearchByBtn.frame.origin.y, self.eGSearchByBtn.frame.size.width, self.eGSearchByBtn.frame.size.height*[self.searchByArr count])];
    [self.eGSearchByTblVw setFrame:CGRectMake(0, 0, self.eGSearchByBtn.frame.size.width, self.eGSearchByBtn.frame.size.height*[self.searchByArr count])];
}

/*
 * Method Name	: indexSortInitializers
 * Description	: initialize the variables which are needed for alphabetical sorting
 * Parameters	: nil
 * Return value	: nil
 */
-(void) indexSortInitializers:(NSArray*) array
{
    if(![array count])
        return;
    
    self.sectionDict = nil;
    
    self.sectionDict = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    // Loop through the Contacts and create our keys
    for (eGordianContact *contact in array)
    {
        NSString *c = nil;
        
        if(isAlphabeticalSort)
        {
            if(![contact.LastName length])
                continue;
            
            c = [[contact.LastName uppercaseString] substringToIndex:1];
        }
        else
        {
            if(![contact.CompanyName length])
                continue;
            
            c = contact.CompanyName ;
        }
        
        found = NO;
        
        for (NSString *str in [self.sectionDict allKeys])
        {
            if ([str caseInsensitiveCompare:c] == NSOrderedSame)
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [self.sectionDict setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the contacts into their respective keys
    for (eGordianContact *contact in array)
    {
        if(isAlphabeticalSort)
        {
            if(![contact.LastName length])
                continue;
            
            [[self.sectionDict objectForKey:[[contact.LastName substringToIndex:1] uppercaseString] ] addObject:contact];
        }
        else
        {
            if(![contact.CompanyName length])
                continue;
            
            [[self.sectionDict objectForKey:contact.CompanyName] addObject:contact];
        }
    }
    
    // Sort each section array
    for (NSString *key in [self.sectionDict allKeys])
    {

        [[self.sectionDict objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:searchKeyLastName ascending:YES]]];
    }
    
    [self.contactsTableView setContentOffset:CGPointMake(0, 0)];
    
    [self.contactsTableView reloadData];
}

-(void)startAnimating{
    
    [self.egLoadView setHidden:NO];
    [self.view bringSubviewToFront:self.egLoadView];
    [self.spinner startAnimating];
}

-(void)stopAnimating{
    [self.spinner stopAnimating];
    [self.egLoadView setHidden:YES];
}

/*
 * Method Name	: startAnimating
 * Description	: start Animating
 * Parameters	: nil
 * Return value	: nil
 */
//-(void) startAnimating
//{
//    if(!self.spinner)
//        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    self.spinner.frame = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 0, 0);
//    [self.view addSubview:self.spinner];
//    [self.view bringSubviewToFront:self.spinner];
//    [self.spinner startAnimating];
//}
//
///*
// * Method Name	: stopAnimating
// * Description	: stop Animation
// * Parameters	: nil
// * Return value	: nil
// */
//-(void) stopAnimating
//{
//    if(self.spinner)
//    {
//        [self.spinner stopAnimating];
//        self.spinner.hidden = TRUE;
//        self.spinner = nil;
//    }
//    
//}


@end
