//
//  EGHomeNavigationController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/10/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGHomeNavigationController.h"

@interface EGHomeNavigationController ()

@end

@implementation EGHomeNavigationController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.navigationItem.backBarButtonItem.title = @"";
        self.navigationItem.backBarButtonItem.image = [UIImage imageNamed:@"eGSystem_BackArrow"];
        
        UIImage *NavigationPortraitBackground = [[UIImage imageNamed:@"eG_Dashboard_Header"]
                                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
//        UIImage *NavigationLandscapeBackground = [[UIImage imageNamed:@"nav-image-landscape"]
//                                                  resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];
//        [[UINavigationBar appearance] setBackgroundImage:NavigationLandscapeBackground forBarMetrics:UIBarMetricsCompact];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.barTintColor = [UIColor colorWithRed:155/255.0 green:179/255.0 blue:193/255.0 alpha:1.0];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
