//
//  EGOfflineJobDetailViewController.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGOfflineJobDetailViewController.h"
#import "JobOrderDetail.h"
@interface EGOfflineJobDetailViewController ()

@end

@implementation EGOfflineJobDetailViewController
@synthesize selJobOrder;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshUIControls];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backButtonTapped:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)refreshUIControls{
    self.documentsLabelCount.text       = @"";
    self.photosLabelcount.text          = @"";
    self.contactsLabelCount.text        = @"";

    self.jobClientNameLabel.text =selJobOrder.clientName;
    self.jobTitleLabel.text =selJobOrder.jobTitle;
    self.jobIdLabel.text =selJobOrder.jobId;
    
    //Updating Counts for Documents ,Photos,Contacts
    self.documentsLabelCount.text =@"12";//[NSString stringWithFormat:@"%d",selJobOrder.jobOrderDetail.];
    JobOrderDetail *selJOBDetail =  (JobOrderDetail *)selJobOrder.jobOrderDetail;
    self.documentsLabelCount.text =[NSString stringWithFormat:@"%lld",selJOBDetail.totalDocumentsCount];
    self.contactsLabelCount.text =[NSString stringWithFormat:@"%lld",selJOBDetail.totalContactsCount];
    self.photosLabelcount.text =[NSString stringWithFormat:@"%lld",selJOBDetail.totalPhotosCount];
    
    NSArray *locationsArray =[selJOBDetail.jobLocations allObjects];
    NSLog(@"Locations %@",locationsArray);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
