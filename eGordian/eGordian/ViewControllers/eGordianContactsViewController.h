//
//  eGordianContactsViewController.h
//  eGordian-iOS
//
//  Created by Laila on 25/08/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eGordianContactsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    UIActivityIndicatorView *spinner;
    int pgNo;
    int pgSize;
    int _totalPages;
    BOOL isPageRefreshing;
    BOOL isSearch;
    int previousPage;
    int selectedSearchBy;//contains the array index of the selected searchBy option - 0 by default
    UILabel *infoTextLabel;
}



//@property (weak, nonatomic) IBOutlet UITableView *eGordianContactsSortTblVw;
@property (weak, nonatomic) IBOutlet UILabel *eGordianContactsNumberLbl;
@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;
@property (weak, nonatomic) IBOutlet UITextField *contactsSearchTF;
@property (strong, nonatomic) NSMutableDictionary* sectionDict;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) NSArray *filteredContacts;
@property (weak, nonatomic) IBOutlet UIButton *eGSearchBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *eGSortByBtnCollection;
@property (weak, nonatomic) IBOutlet UITableView *eGSearchByTblVw;
@property (weak, nonatomic) IBOutlet UIView *eGSearchByView;
@property (weak, nonatomic) IBOutlet UIButton *eGSearchByBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchImage;
@property (weak, nonatomic) IBOutlet UIView *egLoadView;

- (IBAction)onTextFieldEndEditing:(id)sender;
- (IBAction)onBackBtn:(id)sender;
- (IBAction)onSearchTextChanged:(id)sender;
- (IBAction)onSearch:(id)sender;
- (IBAction)searchResultCancelTapped:(id)sender;
- (IBAction)onSortBtn:(id)sender;
- (IBAction)onSearchByBtn:(id)sender;

@end
