//
//  eGordianJobPhotoDetailViewController.h
//  eGordian-iOS
//
//  Created by Laila on 05/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EG_JobPhoto.h"
#import "EGContainerViewController.h"

@interface eGordianJobPhotoDetailViewController : UIViewController<egordianContainerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *eGJobPhotoImageView;
@property (nonatomic, strong) EG_JobPhoto* currentPhoto;
@property (nonatomic, strong) EG_JobOrder* currentJobOrder;
@property (weak, nonatomic) IBOutlet UIView *egLoadView;

- (IBAction)onBack:(id)sender;

@end
