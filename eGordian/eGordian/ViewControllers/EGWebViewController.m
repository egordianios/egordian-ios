//
//  EGWebViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/5/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EGWebViewController.h"

@interface EGWebViewController ()

@end

@implementation EGWebViewController

@synthesize resourceViaIndex,redirectVia;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.loaderView setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWebView:) name:EGWebViewReloadDataNotification object:nil];
    
    [self setIntialUI];
}



-(void)reloadWebView:(NSNotification *)notification{
    
    NSDictionary *notificationDict =[notification userInfo];
    
    NSString *htmlFile = @"";
    
    if([[notificationDict objectForKey:@"SelectedFile"]isEqualToString:@"About"]){
        
        htmlFile = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"];
        self.titleLabel.text = @"ABOUT US";
    }
    else if([[notificationDict objectForKey:@"SelectedFile"]isEqualToString:@"Support"]){
        
        htmlFile = [[NSBundle mainBundle] pathForResource:@"support" ofType:@"html"];
        self.titleLabel.text = @"SUPPORT";
    }
    
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    [self.webViewToLoadContent loadHTMLString:htmlString baseURL:baseURL];
    self.webViewToLoadContent.scalesPageToFit = false;
    self.webViewToLoadContent.scrollView.bounces = false;
    
//    [self.webViewToLoadContent performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
//    [self.webViewToLoadContent reload];
    
    [self.view setNeedsLayout];
    
    
    
}


-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if([self.redirectVia isEqualToString:@"Documents"]){
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    }
}

-(void)setIntialUI{
    
    if([self.redirectVia isEqualToString:@"More"])
    {
        NSString *htmlFile = @"";
        
        switch (self.resourceViaIndex) {
            case 0:{
                htmlFile = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"];
                self.titleLabel.text = @"ABOUT US";
            }
                break;
            case 1:{
                htmlFile = [[NSBundle mainBundle] pathForResource:@"support" ofType:@"html"];
                self.titleLabel.text = @"SUPPORT";
            }
                break;
                default:
                break;
        }
        
        
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        NSString *path = [[NSBundle mainBundle] bundlePath];
        NSURL *baseURL = [NSURL fileURLWithPath:path];
        
        [self.webViewToLoadContent loadHTMLString:htmlString baseURL:baseURL];
        self.webViewToLoadContent.scalesPageToFit = false;
        self.webViewToLoadContent.scrollView.bounces = false;
    }
    else{
        
        self.titleLabel.text = @"DOCUMENTS";
        self.webViewToLoadContent.scalesPageToFit = true;
        [self.webViewToLoadContent loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webViewLoadURl]]];
        ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma WebView Delegates
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        NSLog(@"Cliked Link--%@",request.URL);
        
//        if([[request.URL absoluteString] isEqualToString:@"https://www.facebook.com/jobordercontracting"] || [[request.URL absoluteString]isEqualToString:@"https://twitter.com/jobordercont"]){
        
            if([[UIApplication sharedApplication]canOpenURL:request.URL]){
                [[UIApplication sharedApplication]openURL:request.URL];
//            }
            
            return NO;
        }
    }
    
    return YES;
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self actvityStart];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self actvityStop];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    [self actvityStop];
}

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:EGWebViewReloadDataNotification
                                                  object:nil];
}

@end
