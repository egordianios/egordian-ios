//
//  EGMapDirectionsViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 10/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EGMapDirectionsViewController : UIViewController
- (IBAction)directionsBackButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *mapDirectionsWebView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (nonatomic, strong) NSString *formattedAddress;
@property (nonatomic, assign) CGFloat destinationLatitude;
@property (nonatomic, assign) CGFloat destinationLongitude;
@end
