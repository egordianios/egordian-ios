//
//  eGMapViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 10/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "eGMapViewController.h"
#import "EGMapDirectionsViewController.h"

@import GoogleMaps;

@interface eGMapViewController (){
    GMSMapView *mapView_;
    GMSMarker *marker_;
    NSString *markerFormattedAddress;
}
@property (nonatomic) BOOL alreadyAppeared;
@end

@implementation eGMapViewController
@synthesize destinationLat,destinationLong, addressDict;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.alreadyAppeared = FALSE;
    [self setIntialUI];
}

-(void)setIntialUI
{
    
    self.infoLabel.text = [self.addressDict valueForKey:@"Name"];
    
    self.address1Label.text = [self.addressDict valueForKey:@"Address"];
    
    self.address2Label.text = [self.addressDict valueForKey:@"A_Address1"];
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    if(!self.alreadyAppeared){

        self.alreadyAppeared = TRUE;
        
        NSString *addressValue = [NSString stringWithFormat:@"%@,%@",self.address1Label.text,self.address2Label.text];
        
        addressValue = [addressValue stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        [self fetchCoOrdinatesFromAddress:addressValue];
        
    }
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

-(void)fetchCoOrdinatesFromAddress:(NSString *)jobOrderLocation{
    
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:34.074706
                                                            longitude:-118.2040366
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = YES;
    
    [self.mapView addSubview:mapView_];
    
//    http://maps.google.com/maps/api/geocode/json?address=3501%20N%20Broadway,%20Los%20Angeles,%20CA%2090031&sensor=true
    
    NSString *loadMapURL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=%@&address=%@&sensor=true",APP_GOOGLE_MAPS_SERVER_KEY,jobOrderLocation];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader]getResponseFormUrl:loadMapURL serviceType:EGORDIAN_API_GET_COORDINATES_FROMADDRESS];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)directionsButtonTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"eGordian" bundle:nil];
    EGMapDirectionsViewController *directionMapVC = (EGMapDirectionsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGMapDirectionsViewController"];
    directionMapVC.destinationLatitude = self.destinationLat;
    directionMapVC.destinationLongitude = self.destinationLong;
    directionMapVC.formattedAddress = markerFormattedAddress;
    [self.navigationController pushViewController:directionMapVC animated:NO];
}

- (IBAction)resetButtonTapped:(id)sender {
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(self.destinationLat, self.destinationLong);
    mapView_.camera = [GMSCameraPosition cameraWithTarget:target zoom:15];
    
    marker_ = [[GMSMarker alloc] init];
    marker_.position = CLLocationCoordinate2DMake(self.destinationLat, self.destinationLong);
    marker_.title = markerFormattedAddress;
    marker_.map = mapView_;
    
}

#pragma mark Data Downloader Manager Delegates
- (void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_GET_COORDINATES_FROMADDRESS){
        
        if(responseArray.count > 0){
            
            NSDictionary *addressDictNew = (NSDictionary *)[responseArray objectAtIndex:0];
            
            markerFormattedAddress = [addressDictNew objectForKey:@"formatted_address"];
            
            NSDictionary *geometryDict = [addressDictNew objectForKey:@"geometry"];
            
            NSDictionary *locationDict = [geometryDict objectForKey:@"location"];
            
            self.destinationLat = [[locationDict objectForKey:@"lat"] floatValue];
            
            self.destinationLong = [[locationDict objectForKey:@"lng"] floatValue];
            
            CLLocationCoordinate2D target =
            CLLocationCoordinate2DMake(self.destinationLat, self.destinationLong);
            mapView_.camera = [GMSCameraPosition cameraWithTarget:target zoom:15];
            
            marker_ = [[GMSMarker alloc] init];
            marker_.position = CLLocationCoordinate2DMake(self.destinationLat, self.destinationLong);
            marker_.title = markerFormattedAddress;
            marker_.map = mapView_;
            
        }
    }
}

- (void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    NSString *alertMessage = @"";
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    NSLog(@"Error Message--%@",alertMessage);
}


@end
