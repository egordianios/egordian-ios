//
//  EGLocationManager.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 10/30/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGLocationManager.h"

@implementation EGLocationManager
@synthesize locationManagerDelegate = _locationManagerDelegate;
static EGLocationManager *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (EGLocationManager*)sharedLocationManager
{
    if(!_sharedInstance)
    {
        dispatch_once(&oncePredicate, ^{
            _sharedInstance = [[super allocWithZone:nil] init];
        });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance=nil;
    oncePredicate=0;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedLocationManager];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
    return self;
}

- (unsigned)retainCount
{
    return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (id)init {
    self = [super init];
    if(self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 100; // meters
        self.locationManager.delegate = self;
    }
    
    return self;
}
-(void)locationStartUpdate{
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}
-(void)locationStopUpdate{
    
    [self.locationManager startUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSString *errorMessage = @"";
    
    if([CLLocationManager locationServicesEnabled])
    {
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            errorMessage = @"Failed to get your location as location services are disabled at the device level.";
        }
    }
    else
    {
        errorMessage = @"Failed to get your location as location services are disabled for eGordian application.";
    }
    
    [self.locationManager stopUpdatingLocation];
    
    if(_locationManagerDelegate && [_locationManagerDelegate respondsToSelector:@selector(locationDidUpdateFailed:)]){
        [_locationManagerDelegate locationDidUpdateFailed:errorMessage];
    }
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * currentLocation = (CLLocation *)[locations lastObject];
    NSLog(@"Location: %@", currentLocation);
    
    CGFloat currentLattitude = 0.0;
    CGFloat currentLongitude = 0.0;
    
    if (currentLocation != nil)
    {
        currentLattitude = self.locationManager.location.coordinate.latitude;
        currentLongitude = self.locationManager.location.coordinate.longitude;
    }
    
    [self.locationManager stopUpdatingLocation];
    
    if(_locationManagerDelegate && [_locationManagerDelegate respondsToSelector:@selector(locationDidUpdateFinishedWithLatitude:andLongitude:)]){
        [_locationManagerDelegate locationDidUpdateFinishedWithLatitude:currentLattitude andLongitude:currentLongitude];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    CGFloat currentLattitude = 0.0;
    CGFloat currentLongitude = 0.0;
    
    if (currentLocation != nil)
    {
        currentLattitude = self.locationManager.location.coordinate.latitude;
        currentLongitude = self.locationManager.location.coordinate.longitude;
    }
    
    [self.locationManager stopUpdatingLocation];
    
    if(_locationManagerDelegate && [_locationManagerDelegate respondsToSelector:@selector(locationDidUpdateFinishedWithLatitude:andLongitude:)]){
        [_locationManagerDelegate locationDidUpdateFinishedWithLatitude:currentLattitude andLongitude:currentLongitude];
    }
}

@end
