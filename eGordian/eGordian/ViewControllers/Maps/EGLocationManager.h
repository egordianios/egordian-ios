//
//  EGLocationManager.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 10/30/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@protocol LocationManagerDelegate <NSObject>
@optional
- (void)locationDidUpdateLocations:(NSArray *)locationsArray;
- (void)locationDidUpdateFinishedWithLatitude:(CGFloat)currentLat andLongitude:(CGFloat)currentLong;
- (void)locationDidUpdateFailed:(NSString *)errorMessage;
@end

@interface EGLocationManager : NSObject <CLLocationManagerDelegate>

@property(strong, nonatomic) id<LocationManagerDelegate> locationManagerDelegate;
@property (strong, nonatomic) CLLocationManager *locationManager;

+ (EGLocationManager*)sharedLocationManager;
-(void)locationStartUpdate;
-(void)locationStopUpdate;
@end
