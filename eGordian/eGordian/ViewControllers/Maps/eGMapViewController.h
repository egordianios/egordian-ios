//
//  eGMapViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 10/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "eGordianContact.h"
@import GoogleMaps;

@interface eGMapViewController : UIViewController

- (IBAction)backButtonTapped:(id)sender;
- (IBAction)directionsButtonTapped:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *address1Label;
@property (weak, nonatomic) IBOutlet UILabel *address2Label;
@property (nonatomic, retain) NSMutableDictionary *addressDict;
@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (nonatomic, assign) CGFloat destinationLat;
@property (nonatomic, assign) CGFloat destinationLong;

@end
