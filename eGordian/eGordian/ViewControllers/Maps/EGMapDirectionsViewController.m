//
//  EGMapDirectionsViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 10/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGMapDirectionsViewController.h"
#import "EGLocationManager.h"
@interface EGMapDirectionsViewController ()<UIWebViewDelegate,LocationManagerDelegate>

@end


@implementation EGMapDirectionsViewController
@synthesize destinationLatitude,destinationLongitude,formattedAddress;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.view bringSubviewToFront:self.activityLoader];
    
    [self.activityLoader startAnimating];
    
    self.formattedAddress = [self.formattedAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        [[EGLocationManager sharedLocationManager] setLocationManagerDelegate:(id)self];
        
        [[EGLocationManager sharedLocationManager]locationStartUpdate];

    }
    else{
        NSString *urlString = [NSString stringWithFormat:@"https://www.google.com/maps/dir//%@/@%f,%f", self.formattedAddress, self.destinationLatitude,self.destinationLongitude];
        
        [self.mapDirectionsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    }
    
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

- (IBAction)directionsBackButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIWebview Delegates

- (void)webViewDidStartLoad:(UIWebView *)webView{
 
    [self.activityLoader startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self.activityLoader stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    
    [self.activityLoader stopAnimating];
}

#pragma mark - Location Manager Delegates

-(void)locationDidUpdateFinishedWithLatitude:(CGFloat)currentLat andLongitude:(CGFloat)currentLong{
    
    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f", currentLat, currentLong, self.destinationLatitude,self.destinationLongitude];
    
    [self.mapDirectionsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

-(void)locationDidUpdateFailed:(NSString *)errorMessage{
    
    [[EGLocationManager sharedLocationManager] setLocationManagerDelegate:(id)self];
    
    [[EGLocationManager sharedLocationManager]locationStopUpdate];
    
    [self.activityLoader stopAnimating];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"eGordian"
//                                                    message:errorMessage
//                                                   delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
}

-(void)locationDidUpdateLocations:(NSArray *)locationsArray{
    
    NSLog(@"Fired--\n%@",locationsArray);
}

@end
