//
//  EGContainerViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/21/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopoverView.h"

@protocol egordianContainerDelegate <NSObject>
@optional
-(void) deletePhoto;
-(void) cancelDeletePhoto;
-(void) addToJob;
-(void) takePhotoClickedOffline;
@end

@interface EGContainerViewController : UIViewController <PopoverViewDelegate>

+ (EGContainerViewController *)sharedContainerView;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *eGMainFooter;
@property (weak, nonatomic) IBOutlet UIView *eGPhotoSelectionFooter;

@property (weak, nonatomic) IBOutlet UIButton *addtoJobButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@property (weak) id <egordianContainerDelegate> delegate;


- (IBAction)eGMainFooterHomeTapped:(id)sender;
- (IBAction)eGMainFooterCameraTapped:(id)sender;
- (IBAction)eGMainFooterMoreTapped:(id)sender;
- (IBAction)eGFooterAddToJobTapped:(id)sender;
- (IBAction)eGFooterDeleteTapped:(id)sender;

-(void) resetFooterView;
-(void) updateMoreIcon:(NSNotification *)notification;
-(void) updateFooterView:(NSString *)viaScreenName;
-(void) onThumbnail;
@end
