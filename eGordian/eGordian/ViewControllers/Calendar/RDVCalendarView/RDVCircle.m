//
//  RDVCircle.m
//  RDVCalendarView
//
//  Created by Laila on 07/12/15.
//  Copyright © 2015 Robert Dimitrov. All rights reserved.
//

#import "RDVCircle.h"

@implementation RDVCircle

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
//        self.backgroundColor = [UIColor yellowColor];
        self.opaque = NO;
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
//    CGContextRef cacheContext = CGBitmapContextCreate (cacheBitmap, size.width, size.height, 8, bitmapBytesPerRow, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedLast);
//    CGContextSetRGBFillColor(cacheContext, 0, 0, 0, 0);
//    CGContextFillRect(cacheContext, (CGRect){CGPointZero, size});
    
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(ctx, 3.0);
//    CGContextSetRGBFillColor(ctx, 1.0, 0.0, 0.0, 1.0);
//    CGContextSetRGBStrokeColor(ctx, 0, 0, 1.0, 1.0);
    
//    CGContextFillEllipseInRect(ctx, self.bounds);
    
//    CGContextSetRGBFillColor(ctx, 255.0/255.0, 115.0/255.0, 155.0/255.0, 0.7);
//    CGContextFillEllipseInRect(ctx, self.bounds);
    
//    CGContextSetRGBStrokeColor(ctx, 255.0/255.0, 191.0/255.0, 21.0/255.0, 0.7);
//    CGContextStrokeEllipseInRect(ctx, self.bounds);
    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    UIColor *grayColor = [UIColor colorWithRed: 255.0/255.0 green: 191.0/255.0 blue: 21.0/255.0 alpha: 1.0];
//    
//    [grayColor set];
//    
//    CGContextSetLineWidth(context, 2.0);
//    CGContextStrokeEllipseInRect(context, self.bounds);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *grayColor = [UIColor colorWithRed: 255.0/255.0 green: 191.0/255.0 blue: 21.0/255.0 alpha: 1.0];
    CGFloat lineWidth = 2;
    CGRect borderRect = CGRectInset(rect, lineWidth * 0.5, lineWidth * 0.5);
    [grayColor set];
    
    CGContextSetLineWidth(context, lineWidth);
    CGContextStrokeEllipseInRect(context, borderRect);
    
}


@end

