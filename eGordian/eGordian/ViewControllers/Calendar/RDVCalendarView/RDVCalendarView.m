// RDVCalendarView.m
// RDVCalendarView
//
// Copyright (c) 2013 Robert Dimitrov
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "RDVCalendarView.h"
#import "RDVCalendarDayCell.h"

@interface RDVCalendarView () {
    NSMutableArray *_visibleCells;
    NSMutableArray *_dayCells;
    
    NSInteger _numberOfDays;
    NSInteger _numberOfWeeks;
    
    NSMutableArray *_separators;
    NSMutableArray *_visibleSeparators;
    
    RDVCalendarDayCell *_selectedDayCell;
    
    NSArray *_weekDays;
    
    Class _dayCellClass;
    
    UIInterfaceOrientation _orientation;
}

@property (atomic, strong) NSDateComponents *selectedDay;
@property (atomic, strong, readwrite) NSDateComponents *month;
@property (atomic, strong) NSDateComponents *currentDay;
@property (atomic, strong) NSDate *firstDay;

@end

#define cellHeight 42;
@implementation RDVCalendarView

@synthesize dateTableView, egPlannedDate, headerDateButton, egAdjustedDate, egActualDate, gestureRecognizer, transactionDate;
@synthesize jobID, activityLoader, dateHeaderView;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        selectedIndex = 0;
        
        _dayCells = [[NSMutableArray alloc] initWithCapacity:31];
        _visibleCells = [[NSMutableArray alloc] initWithCapacity:31];
        
        _visibleSeparators = [[NSMutableArray alloc] initWithCapacity:12];
        _separators = [[NSMutableArray alloc] initWithCapacity:12];
        
        // Setup defaults
        
        _currentDayColor = [UIColor clearColor];
        _selectedDayColor = [UIColor clearColor];
        _separatorColor = [UIColor grayColor];
        
        _separatorEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0, 0.0);;
        _dayCellEdgeInsets =  UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        
        _dayCellClass = [RDVCalendarDayCell class];
        
        _weekDayHeight = 30.0f;
        
        // Setup header view
        int space = 10;
        
        UIView* headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 62)];
        [headerView setBackgroundColor:[EGordianUtilities headerColor]];
        [self addSubview:headerView];
        
        UIImage* backImage = [UIImage imageNamed:@"eGSystem_BackArrow"];
        UIButton *prevScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [prevScreenButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [prevScreenButton setImage:backImage forState:UIControlStateNormal];
        prevScreenButton.frame = CGRectMake(space, (headerView.frame.size.height-backImage.size.height)/2, backImage.size.width, backImage.size.height);
        [headerView addSubview:prevScreenButton];
        [self addSubview:headerView];
        
        int width = 4* backImage.size.width;
        int height = backImage.size.height;
        
        UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [saveButton addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
        [saveButton setTitle:@"Save" forState:UIControlStateNormal];
        saveButton.frame = CGRectMake(self.frame.size.width - width, (headerView.frame.size.height-height)/2, width, height);
        saveButton.tintColor = [UIColor colorWithRed:70.0/255 green:81.0/255 blue:88.0/255 alpha:1.0];
        [saveButton.titleLabel setTextColor:[UIColor colorWithRed:70.0/255 green:81.0/255 blue:88.0/255 alpha:1.0]];
        [saveButton.titleLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:14 ]];
        [headerView addSubview:saveButton];
        [self addSubview:headerView];
        
        height += 3;
        UILabel* headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, (headerView.frame.size.height - height)/2, self.frame.size.width, height)];
        [headerLabel setText:@"CALENDAR"];
        [headerLabel setFont:[UIFont fontWithName:FONT_STRATUM_BOLD size:20]];
        [headerLabel setTextColor:[UIColor colorWithRed:70.0/255 green:81.0/255 blue:88.0/255 alpha:1.0]];
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setTextAlignment:NSTextAlignmentCenter];
        [headerView addSubview:headerLabel];
        
        _monthLabel = [[UILabel alloc] init];
        [_monthLabel setFont:[UIFont systemFontOfSize:16]];
        [_monthLabel setTextColor:[UIColor blackColor]];
        [_monthLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_monthLabel];
        
        _backButton = [[UIButton alloc] init];
        [_backButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_backButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_backButton setTitle:@"<" forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(showPreviousMonth)
              forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_backButton];
        
        _forwardButton = [[UIButton alloc] init];
        [_forwardButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_forwardButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_forwardButton setTitle:@">" forState:UIControlStateNormal];
        [_forwardButton addTarget:self action:@selector(showNextMonth)
                 forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_forwardButton];
        
        [self setupWeekDays];

        // Setup calendar
        
        NSCalendar *calendar = [self calendar];
        
        _currentDay = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
        
        NSDate *currentDate = [NSDate date];
        
        _month = [calendar components:NSYearCalendarUnit|
                                      NSMonthCalendarUnit|
                                      NSDayCalendarUnit|
                                      NSWeekdayCalendarUnit|
                                      NSCalendarCalendarUnit
                             fromDate:currentDate];
        _month.day = 1;
        
        [self updateMonthLabelMonth:_month];
        
        [self updateMonthViewMonth:_month];
        
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        
        [defaultCenter addObserver:self
                         selector:@selector(currentLocaleDidChange:)
                             name:NSCurrentLocaleDidChangeNotification
                           object:nil];
        
//        [defaultCenter addObserver:self
//                          selector:@selector(deviceDidChangeOrientation:)
//                              name:UIDeviceOrientationDidChangeNotification
//                            object:nil];
        
        _orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        [self createCustomDatePicker:CGRectMake(0, headerView.frame.origin.y+headerView.frame.size.height+space, frame.size.width, headerView.frame.size.height/2)];
    }

    return self;
}

-(void) createDatePicker
{

}

-(void) createCustomDatePicker:(CGRect)frame
{
    pickerData= [[NSMutableArray alloc] initWithObjects:@"Planned",@"Adjusted",@"Actual", nil];
    
    self.dateHeaderView = [[UIView alloc] initWithFrame:frame];
    [self.dateHeaderView setBackgroundColor:[UIColor colorWithRed:212.0/255 green:216.0/255 blue:220.0/255 alpha:1.0]];
    [self addSubview:self.dateHeaderView];
    
    self.headerDateButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.dateHeaderView.frame.size.width-20, self.dateHeaderView.frame.size.height)];
    [self.headerDateButton setBackgroundColor:[UIColor clearColor]];
    [self.headerDateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.headerDateButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.headerDateButton addTarget:self action:@selector(onDateSelection)
         forControlEvents:UIControlEventTouchUpInside];
    [self.dateHeaderView addSubview:self.headerDateButton];
    
    int width = 20;
    int height = 20;
    UIButton* downButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-width, (self.headerDateButton.frame.size.height - height)/2, width, height)];
    [downButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [downButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [downButton setImage:[UIImage imageNamed:@"eG_Contacts_DownArrow"] forState:UIControlStateNormal];
    [downButton addTarget:self action:@selector(onDateSelection)
          forControlEvents:UIControlEventTouchUpInside];
    [self.dateHeaderView addSubview:downButton];

    int space = 10;
    self.dateTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.dateHeaderView.frame.origin.y+self.dateHeaderView.frame.size.height+space, self.frame.size.width, 180) style:UITableViewStylePlain];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    self.dateTableView.delegate = self;
    self.dateTableView.dataSource = self;
    self.dateTableView.hidden = TRUE;
    [self.dateTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.dateTableView setBackgroundColor:[UIColor blackColor]];
//    self.dateTableView.opaque = FALSE;
    [self addSubview:self.dateTableView];
    
    NSString* dateStr = @"";
    if(self.egPlannedDate)
        dateStr = [AppConfig stringFromDate:self.egPlannedDate withFormat:@"dd-MM-yyyy"];
    [self.headerDateButton setTitle:[NSString stringWithFormat:@"%@ %@", [pickerData objectAtIndex:0], dateStr] forState:UIControlStateNormal ];
}


#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [pickerData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"DateCell";
    
    // Similar to UITableViewCell, but
    UITableViewCell *cell = (UITableViewCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor colorWithRed:105.0/255 green:114.0/255 blue:119.0/255 alpha:1.0];
    }

    NSString* dateString = @"";
    switch (indexPath.row) {
        case 0:
            if(self.egPlannedDate)
                dateString = [AppConfig stringFromDate:self.egPlannedDate withFormat:@"dd-MM-yyyy"];
            break;
            
        case 1:
            if(self.egAdjustedDate)
                dateString = [AppConfig stringFromDate:self.egAdjustedDate withFormat:@"dd-MM-yyyy"];
            break;
            
        case 2:
            if(self.egActualDate)
                dateString = [AppConfig stringFromDate:self.egActualDate withFormat:@"dd-MM-yyyy"];
            break;
            
        default:
            break;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [pickerData objectAtIndex:indexPath.row], dateString]; ;
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:FONT_BOLD size:14]];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    [cell.textLabel setOpaque:FALSE];
    return cell;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* theSelectedCell = [theTableView cellForRowAtIndexPath:indexPath];
//    [self.headerDateLabel.titleLabel setText:theSelectedCell.textLabel.text];
    [self.headerDateButton setTitle:theSelectedCell.textLabel.text forState:UIControlStateNormal];
    theTableView.hidden = TRUE;
    selectedIndex = (int)indexPath.row;
    
    switch (selectedIndex) {
        case 0:
            if(self.egPlannedDate)
                [self setSelectedDate:self.egPlannedDate];
            else
                [self deselectDayCellAtIndex:[self indexForSelectedDayCell] animated:YES];
            break;
            
        case 1:
            if(self.egAdjustedDate)
                [self setSelectedDate:self.egAdjustedDate];
            else
                [self deselectDayCellAtIndex:[self indexForSelectedDayCell] animated:YES];
            break;
            
        case 2:
            if(self.egActualDate)
                [self setSelectedDate:self.egActualDate];
            else
                [self deselectDayCellAtIndex:[self indexForSelectedDayCell] animated:YES];
            break;
            
        default:
            break;
    }
}

-(void) onDateSelect:(NSIndexPath*) indexpath
{
    UITableViewCell* theSelectedCell = [self.dateTableView cellForRowAtIndexPath:indexpath];
//    [self.headerDateLabel.titleLabel setText:theSelectedCell.textLabel.text];
    [self.headerDateButton setTitle:theSelectedCell.textLabel.text forState:UIControlStateNormal];
    
    self.dateTableView.hidden = TRUE;
    selectedIndex = (int)indexpath.row;
    
    switch (selectedIndex) {
        case 0:
            if(self.egPlannedDate)
                [self setSelectedDate:self.egPlannedDate];
            
            break;
            
        case 1:
            if(self.egAdjustedDate)
                [self setSelectedDate:self.egAdjustedDate];
            
            break;
            
        case 2:
            if(self.egActualDate)
                [self setSelectedDate:self.egActualDate];
            
            break;
            
        default:
            break;
    }
}
//Columns in picker views


- (void)layoutSubviews {
    CGSize viewSize = self.frame.size;
    CGSize headerSize = CGSizeMake(viewSize.width, self.dateHeaderView.frame.origin.y + self.dateHeaderView.frame.size.height + 100 + self.frame.size.height/7.89);
    CGFloat backButtonWidth = MAX([[self backButton] sizeThatFits:CGSizeMake(20, 50)].width, 44);
    CGFloat forwardButtonWidth = MAX([[self forwardButton] sizeThatFits:CGSizeMake(20, 50)].width, 44);
    
    CGSize previousMonthButtonSize = CGSizeMake(backButtonWidth, 50);
    CGSize nextMonthButtonSize = CGSizeMake(forwardButtonWidth, 50);
    CGSize titleSize = CGSizeMake(viewSize.width - previousMonthButtonSize.width - nextMonthButtonSize.width - 10 - 10,
                                  50);
    
    // Layout header view
    
    [[self backButton] setFrame:CGRectMake(10, roundf(headerSize.height / 2 - previousMonthButtonSize.height / 2),
                                         previousMonthButtonSize.width, previousMonthButtonSize.height)];
    
    [[self monthLabel] setFrame:CGRectMake(roundf(headerSize.width / 2 - titleSize.width / 2),
                                         roundf(headerSize.height / 2 - titleSize.height / 2),
                                         titleSize.width, titleSize.height)];
    
    [[self forwardButton] setFrame:CGRectMake(headerSize.width - 10 - nextMonthButtonSize.width,
                                            roundf(headerSize.height / 2 - nextMonthButtonSize.height / 2),
                                            nextMonthButtonSize.width, nextMonthButtonSize.height)];
    
    // Calculate sizes and distances
    
    CGFloat rowCount = 6; // 6 is the maximum number of weeks in a month
    
    CGFloat dayWidth = 0;
    if ([[self delegate] respondsToSelector:@selector(widthForDayCellInCalendarView:)]) {
        dayWidth = [[self delegate] widthForDayCellInCalendarView:self];
    } else if ([self dayCellWidth]) {
        dayWidth = [self dayCellWidth];
    } else {
        if (viewSize.width > viewSize.height) {
            dayWidth = roundf(viewSize.width / 10);
        } else {
            dayWidth = roundf(viewSize.width / 7);
        }
    }
    
    CGFloat weekDayLabelsEndY = CGRectGetMaxY([[self monthLabel] frame]) + [self weekDayHeight];
    
    CGFloat dayHeight = 0;
    if ([[self delegate] respondsToSelector:@selector(heightForDayCellInCalendarView:)]) {
        dayHeight = [[self delegate] heightForDayCellInCalendarView:self];
    } else if ([self dayCellHeight]) {
        dayHeight = [self dayCellHeight];
    } else {
        if (viewSize.width > viewSize.height) {
            dayHeight = roundf((viewSize.height - weekDayLabelsEndY) / 6) -
            [self dayCellEdgeInsets].top - [self dayCellEdgeInsets].bottom;
        } else {
            dayHeight = dayWidth;
        }
    }
    
    CGFloat elementHorizonralDistance = roundf((viewSize.width - [self dayCellEdgeInsets].left -
                                        [self dayCellEdgeInsets].right - dayWidth * 7) / 6);
    
    // Week days layout
    
    NSInteger column = 0;
    for (UILabel *weekDayLabel in [self weekDayLabels]) {
        CGFloat labelXPosition = [self dayCellEdgeInsets].left + (column * dayWidth) + (column * elementHorizonralDistance);
        [weekDayLabel setFrame:CGRectMake(labelXPosition, CGRectGetMaxY([[self monthLabel] frame]), dayWidth, [self weekDayHeight])];
        column++;
    }
    
    [[self backButton] setFrame:CGRectMake(self.monthLabel.frame.origin.x  - previousMonthButtonSize.width, roundf(headerSize.height / 2 - previousMonthButtonSize.height / 2),
                                           previousMonthButtonSize.width, previousMonthButtonSize.height)];
    [[self forwardButton] setFrame:CGRectMake(headerSize.width - 10 - nextMonthButtonSize.width,
                                              roundf(headerSize.height / 2 - nextMonthButtonSize.height / 2),
                                              nextMonthButtonSize.width, nextMonthButtonSize.height)];
    // Calendar grid layout
    
    CGFloat startigCalendarY = CGRectGetMaxY([[self weekDayLabels][0] frame]);
    CGFloat elementVerticalDistance = 0;
    
    column = 7 - [self numberOfDaysInFirstWeek];
    
    NSInteger row = 0;
    int separatorYPos = 0;
    for (NSInteger dayIndex = 0; dayIndex < [self numberOfDays]; dayIndex++) {
        RDVCalendarDayCell *dayCell = [self dayCellForIndex:dayIndex];
        
        
        if (![[self visibleCells] containsObject:dayCell]) {
            [_visibleCells addObject:dayCell];
            [self addSubview:dayCell];
        }
        
        if ([self selectedDay] && (dayIndex + 1 == [self selectedDay].day &&
                                   [self month].month == [self selectedDay].month &&
                                   [self month].year == [self selectedDay].year)) {
            
            
            _selectedDayCell = dayCell;
            [_selectedDayCell setSelected:YES];
        }
        
        if ([[self delegate] respondsToSelector:@selector(calendarView:configureDayCell:atIndex:)]) {
            [[self delegate] calendarView:self configureDayCell:dayCell atIndex:dayIndex];
        }
        
        CGFloat dayCellXPosition = [self dayCellEdgeInsets].left + (column * dayWidth) + (column * elementHorizonralDistance);
        CGFloat dayCellYPosition = [self dayCellEdgeInsets].top + (row * dayHeight) + 2*(row * elementVerticalDistance);
        
        [dayCell setFrame:CGRectMake(dayCellXPosition, startigCalendarY + dayCellYPosition, dayWidth, dayHeight)];
        
        if ([dayCell superview] != self) {
            [self addSubview:dayCell];
        }
        
        // Layout separators
        
        if (dayIndex == 0 || column == 0) {
            if ([self separatorStyle] & RDVCalendarViewDayCellSeparatorTypeHorizontal) {
                if (dayIndex < [self numberOfDays]+1) {
                    UIView *separator = [self dayCellSeparator];
                    
                        [separator setFrame:CGRectMake([self separatorEdgeInsets].left, CGRectGetMinY(dayCell.frame) +
                                                       ([self separatorEdgeInsets].top - [self separatorEdgeInsets].bottom),
                                                       viewSize.width - [self separatorEdgeInsets].left -
                                                       [self separatorEdgeInsets].right, 1)];
                    
                    separatorYPos = CGRectGetMinY(dayCell.frame) + ([self separatorEdgeInsets].top - [self separatorEdgeInsets].bottom);
                }
            }
        }
    
        int verticalLineHt = (row * dayHeight)*(rowCount-1);
        if(row == 5)
            verticalLineHt = (dayHeight)*(rowCount);
        
        if ((row == 1 || row==5) && column < [[self weekDayLabels] count]) {
            if ([self separatorStyle] & RDVCalendarViewDayCellSeparatorTypeVertical) {
                UIView *separator = [self dayCellSeparator];
                
                [separator setFrame:CGRectMake([self separatorEdgeInsets].left + CGRectGetMaxX(dayCell.frame) +
                                               roundf(elementHorizonralDistance / 2),
                                               weekDayLabelsEndY + ([self separatorEdgeInsets].top -
                                                                               [self separatorEdgeInsets].bottom),
                                               1, verticalLineHt )];
                
            }
        }
        
        if (column == 6) {
            column = 0;
            
            row++;
        } else {
            column++;
        }
    }
    //to draw the last line
    UIView *separator = [self dayCellSeparator];
    
    [separator setFrame:CGRectMake([self separatorEdgeInsets].left, separatorYPos + dayHeight,
                                   viewSize.width - [self separatorEdgeInsets].left -
                                   [self separatorEdgeInsets].right, 1)];
    
    
}

#pragma mark - Creating Calendar View Day Cells

- (void)registerDayCellClass:(Class)cellClass {
    _dayCellClass = cellClass;
}

- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier {
    UIView *dayCell = nil;
    
    for (RDVCalendarDayCell *calendarDayCell in _dayCells) {
        if ([[calendarDayCell reuseIdentifier] isEqualToString:identifier]) {
            dayCell = calendarDayCell;
            break;
        }
    }
    
    if (dayCell) {
        [_dayCells removeObject:dayCell];
    }
    
    return dayCell;
}

#pragma mark - Set up a calendar view

- (NSCalendar *)calendar {
    static NSCalendar *calendar = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        calendar = [NSCalendar autoupdatingCurrentCalendar];
    });
    return calendar;
}

- (void)setupWeekDays {
    NSCalendar *calendar = [self calendar];
    NSInteger firstWeekDay = [calendar firstWeekday] - 1;
    
    // We need an NSDateFormatter to have access to the localized weekday strings
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.veryShortWeekdaySymbols = @[@"Su", @"Mo", @"Tu", @"We", @"Th", @"Fr", @"Sa"];
    
     NSArray *weekSymbols = [formatter veryShortWeekdaySymbols];
    
    // weekdaySymbols returns and array of strings
    NSMutableArray *weekDays = [[NSMutableArray alloc] initWithCapacity:[weekSymbols count]];
    for (NSInteger day = firstWeekDay; day < [weekSymbols count]; day++) {
        [weekDays addObject:[weekSymbols objectAtIndex:day]];
    }
    
    if (firstWeekDay != 0) {
        for (NSInteger day = 0; day < firstWeekDay; day++) {
            [weekDays addObject:[weekSymbols objectAtIndex:day]];
        }
    }
    
    _weekDays = [NSArray arrayWithArray:weekDays];
    
    if (![_weekDayLabels count]) {
        NSMutableArray *weekDayLabels = [[NSMutableArray alloc] initWithCapacity:[_weekDays count]];
        
        for (NSString *weekDayString in _weekDays) {
            UILabel *weekDayLabel = [[UILabel alloc] init];
            [weekDayLabel setFont:[UIFont boldSystemFontOfSize:15]];
            [weekDayLabel setTextColor:[UIColor blackColor]];
            [weekDayLabel setTextAlignment:NSTextAlignmentCenter];
            [weekDayLabel setText:weekDayString];
            [weekDayLabels addObject:weekDayLabel];
            [self addSubview:weekDayLabel];
        }
        
        _weekDayLabels = [NSArray arrayWithArray:weekDayLabels];
    } else {
        NSInteger index = 0;
        for (NSString *weekDayString in _weekDays) {
            UILabel *weekDayLabel = [self weekDayLabels][index];
            [weekDayLabel setText:weekDayString];
            index++;
        }
    }
}

- (void)updateMonthLabelMonth:(NSDateComponents*)month {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"LLL yyyy";
    
    NSDate *date = [month.calendar dateFromComponents:month];
    self.monthLabel.text = [[formatter stringFromDate:date] capitalizedString];
}

- (void)updateMonthViewMonth:(NSDateComponents *)month {
    [self setFirstDay:[month.calendar dateFromComponents:month]];
    [self reloadData];
}

#pragma mark - Reloading the Calendar view

- (void)reloadData {
    for (RDVCalendarDayCell *visibleCell in [self visibleCells]) {
        [visibleCell removeFromSuperview];
        [visibleCell prepareForReuse];
        [_dayCells addObject:visibleCell];
    }
    
    for (UIView *separator in _visibleSeparators) {
        [_separators addObject:separator];
        [separator removeFromSuperview];
    }
    
    [_visibleSeparators removeAllObjects];
    [_visibleCells removeAllObjects];
}

#pragma mark - Separators

- (UIView *)dayCellSeparator {
    UIView *separator = nil;
    if ([_separators count]) {
        separator = [_separators lastObject];
        [_separators removeObject:separator];
        [_visibleSeparators addObject:separator];
    } else {
        separator = [[UIView alloc] init];
        [_visibleSeparators addObject:separator];
    }
    
    [separator setBackgroundColor:[self separatorColor]];
    
    if ([separator superview] != self) {
        [self addSubview:separator];
    }
    
    return separator;
}

#pragma mark - Date selection

- (void)setSelectedDate:(NSDate *)selectedDate {
    
    if(!selectedDate)
        return;
    
    NSDate *oldDate = [self selectedDate];
    
    if (![oldDate isEqualToDate:selectedDate]) {
        NSCalendar *calendar = [self calendar];
       
        _selectedDay = [calendar components:NSYearCalendarUnit|
                                           NSMonthCalendarUnit|
                                              NSDayCalendarUnit
                                   fromDate:selectedDate];
        
        self.month = [calendar components:NSYearCalendarUnit|
                                          NSMonthCalendarUnit|
                                          NSDayCalendarUnit|
                                          NSWeekdayCalendarUnit|
                                          NSCalendarCalendarUnit
                                 fromDate:selectedDate];
        self.month.day = 1;
        [self updateMonthLabelMonth:self.month];
        
        [self updateMonthViewMonth:self.month];
    }
}

- (NSDate *)selectedDate {
    if ([self selectedDay]) {
        return [[self calendar] dateFromComponents:[self selectedDay]];
    }
    return nil;
}

#pragma mark - Accessing day cells

- (NSArray *)visibleCells {
    return _visibleCells;
}

- (RDVCalendarDayCell *)dayCellForIndex:(NSInteger)index {
    static NSString *DayIdentifier = @"DayCell";
    
    RDVCalendarDayCell *dayCell = nil;
    
    if ([[self visibleCells] count] == [self numberOfDays]) {
        dayCell = [self visibleCells][index];
    } else {
        dayCell = [self dequeueReusableCellWithIdentifier:DayIdentifier];
        if (!dayCell) {
            dayCell = [[_dayCellClass alloc] initWithReuseIdentifier:DayIdentifier];
        }
    }
    
    if (![[self visibleCells] containsObject:dayCell]) {
        [dayCell prepareForReuse];
        [dayCell.textLabel setText:[NSString stringWithFormat:@"%d", (int)index + 1]];
        
        if (index + 1 == [self currentDay].day &&
            [self month].month == [self currentDay].month &&
            [self month].year == [self currentDay].year) {
            [[dayCell backgroundView] setBackgroundColor:[self currentDayColor]];
        } else {
            [[dayCell backgroundView] setBackgroundColor:[self normalDayColor]];
        }
        dayCell.textLabel.textColor = [UIColor blackColor];

        [dayCell setNeedsLayout];
    }
    
    return dayCell;
}

- (NSInteger)indexForDayCell:(RDVCalendarDayCell *)cell {
    return [[self visibleCells] indexOfObject:cell];
}

- (NSInteger)indexForDayCellAtPoint:(CGPoint)point {
    RDVCalendarDayCell *cell = [self viewAtLocation:point];
    
    if (cell) {
        return [self indexForDayCell:cell];
    }
    
    return 0;
}

- (NSDate *)dateForIndex:(NSInteger)index {
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:[[self month] year]];
    [dateComponents setMonth:[[self month] month]];
    [dateComponents setDay:(index + 1)];
    
    NSDate *date = [[self calendar] dateFromComponents:dateComponents];
    
    return date;
}

#pragma mark - Managing selections

- (NSInteger)indexForSelectedDayCell {
    if (_selectedDayCell) {
        return [_visibleCells indexOfObject:_selectedDayCell];
    }
    return 0;
}

- (void)selectDayCellAtIndex:(NSInteger)index animated:(BOOL)animated {
    if ([[self visibleCells] count] > index) {
        if ([[self delegate] respondsToSelector:@selector(calendarView:willSelectDate:)]) {
            [[self delegate] calendarView:self willSelectDate:[self dateForIndex:index]];
        }
        
        if ([[self delegate] respondsToSelector:@selector(calendarView:willSelectCellAtIndex:)]) {
            [[self delegate] calendarView:self willSelectCellAtIndex:index];
        }
        
        _selectedDayCell = [self dayCellForIndex:index];
        [_selectedDayCell setSelected:YES animated:animated];
        
        if (![self selectedDay]) {
            [self setSelectedDay:[[NSDateComponents alloc] init]];
        }
        
        [self.selectedDay setMonth:[[self month] month]];
        [self.selectedDay setYear:[[self month] year]];
        [self.selectedDay setDay:index + 1];
        
        if ([[self delegate] respondsToSelector:@selector(calendarView:didSelectDate:)]) {
            [[self delegate] calendarView:self didSelectDate:[self dateForIndex:index]];
        }
        
        if ([[self delegate] respondsToSelector:@selector(calendarView:didSelectCellAtIndex:)]) {
            [[self delegate] calendarView:self didSelectCellAtIndex:index];
        }
    }
}

- (void)deselectDayCellAtIndex:(NSInteger)index animated:(BOOL)animated {
    if ([[self visibleCells] count] > index) {
        RDVCalendarDayCell *dayCell = [self visibleCells][index];
        [dayCell setSelected:NO animated:animated];
        if ([dayCell isSelected]) {
            [dayCell setSelected:NO animated:animated];
        } else if ([dayCell isHighlighted]) {
            [dayCell setHighlighted:NO animated:animated];
        }
        
        if (_selectedDayCell == dayCell) {
            _selectedDayCell = nil;
        }
        
        [self setSelectedDay:nil];
    }
}

#pragma mark - Helper methods

- (NSInteger)numberOfWeeks {
    return [[self calendar] rangeOfUnit:NSDayCalendarUnit
                                 inUnit:NSWeekCalendarUnit
                                forDate:[self firstDay]].length;
}

- (NSInteger)numberOfDays {
    return [[self calendar] rangeOfUnit:NSDayCalendarUnit
                                 inUnit:NSMonthCalendarUnit
                                forDate:[self firstDay]].length;
}

- (NSInteger)numberOfDaysInFirstWeek {
    return [[self calendar] rangeOfUnit:NSDayCalendarUnit
                                 inUnit:NSWeekCalendarUnit
                                forDate:[self firstDay]].length;
}

- (RDVCalendarDayCell *)viewAtLocation:(CGPoint)location {
    RDVCalendarDayCell *view = nil;
    
    for (RDVCalendarDayCell *dayView in [self visibleCells]) {
        if (CGRectContainsPoint(dayView.frame, location)) {
            view = dayView;
        }
    }
    
    return view;
}

#pragma mark - Navigation

- (void)setDisplayedMonth:(NSDateComponents *)month {
    [self updateMonthLabelMonth:[self month]];
    [self updateMonthViewMonth:[self month]];
    
    if ([[self delegate] respondsToSelector:@selector(calendarView:didChangeMonth:)]) {
        [[self delegate] calendarView:self didChangeMonth:self.month];
    }
}

- (void)showCurrentMonth {
    [[self month] setMonth:[[self currentDay] month]];
    [[self month] setYear:[[self currentDay] year]];
 
    [self setDisplayedMonth:[self month]];
}

- (void)showPreviousMonth {
    NSCalendar *calendar = [self calendar];
    NSDateComponents *inc = [[NSDateComponents alloc] init];
    inc.month = -1;
    
    NSDate *date = [calendar dateFromComponents:self.month];
    NSDate *newDate = [calendar dateByAddingComponents:inc toDate:date options:0];
    
    self.month = [calendar components:NSYearCalendarUnit|
                  NSMonthCalendarUnit|
                  NSDayCalendarUnit|
                  NSWeekdayCalendarUnit|
                  NSCalendarCalendarUnit fromDate:newDate];   
 
    [self setDisplayedMonth:[self month]];
}

- (void)showNextMonth {

    NSCalendar *calendar = [self calendar];
    NSDateComponents *inc = [[NSDateComponents alloc] init];
    inc.month = 1;
    
    NSDate *date = [calendar dateFromComponents:self.month];
    NSDate *newDate = [calendar dateByAddingComponents:inc toDate:date options:0];
    
    self.month = [calendar components:NSYearCalendarUnit|
                  NSMonthCalendarUnit|
                  NSDayCalendarUnit|
                  NSWeekdayCalendarUnit|
                  NSCalendarCalendarUnit fromDate:newDate];
    
    [self setDisplayedMonth:[self month]];
}

#pragma mark - Locale change handling

- (void)currentLocaleDidChange:(NSNotification *)notification {
    [self setupWeekDays];
    [self updateMonthLabelMonth:[self month]];
    [self setNeedsLayout];
}

#pragma mark - Orientation cnahge handling

- (void)deviceDidChangeOrientation:(NSNotification *)notification {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    // orientation has changed to a new one
    if (UIInterfaceOrientationIsLandscape(orientation) != UIInterfaceOrientationIsLandscape(_orientation)) {
        _orientation = orientation;
        [self reloadData];
    }
}

#pragma mark - Touch handling


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    CGPoint locationPoint = [[touches anyObject] locationInView:self];
    CGPoint viewPoint = [self.dateTableView convertPoint:locationPoint fromView:self];
    if ([self.dateTableView pointInside:viewPoint withEvent:event] && !self.dateTableView.hidden) {
        
        NSIndexPath *indexPath = [self.dateTableView indexPathForRowAtPoint:viewPoint];
        
        [self onDateSelect:indexPath];
        return;
    }
    
    CGPoint touchLocation = [touch locationInView:self];
    
    if (touchLocation.y >= CGRectGetMaxY([[self weekDayLabels][0] frame])) {
        RDVCalendarDayCell *selectedDayCell = [self viewAtLocation:touchLocation];
        
        if (selectedDayCell && selectedDayCell != _selectedDayCell) {
            NSInteger cellIndex = [self indexForDayCell:selectedDayCell];
            
            if ([[self delegate] respondsToSelector:@selector(calendarView:shouldSelectDate:)]) {
                if (![[self delegate] calendarView:self shouldSelectDate:[self dateForIndex:cellIndex]]) {
                    return;
                }
            }
            
            if ([[self delegate] respondsToSelector:@selector(calendarView:shouldSelectCellAtIndex:)]) {
                if (![[self delegate] calendarView:self shouldSelectCellAtIndex:cellIndex]) {
                    return;
                }
            }
            
            [self deselectDayCellAtIndex:[self indexForDayCell:_selectedDayCell]
                                animated:NO];
            _selectedDayCell = selectedDayCell;
            [_selectedDayCell setHighlighted:YES];
        }
    }
    [self hideDateTableView:YES];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {

    UITouch *touch = [[event allTouches] anyObject];
    
    CGPoint touchLocation = [touch locationInView:self];
    
    if (touchLocation.y >= CGRectGetMaxY([[self weekDayLabels][0] frame])) {
        RDVCalendarDayCell *selectedDayCell = [self viewAtLocation:touchLocation];
        
        if (selectedDayCell != _selectedDayCell) {
            [self deselectDayCellAtIndex:[self indexForDayCell:_selectedDayCell]
                                animated:NO];
        }
    } else if ([_selectedDayCell isHighlighted]) {
        [_selectedDayCell setHighlighted:NO];
        _selectedDayCell = nil;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([_selectedDayCell isHighlighted]) {
        [_selectedDayCell setHighlighted:YES];
        _selectedDayCell = nil;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    CGPoint locationPoint = [[touches anyObject] locationInView:self];
    CGPoint viewPoint = [self.dateTableView convertPoint:locationPoint fromView:self];
    if ([self.dateTableView pointInside:viewPoint withEvent:event]) {
        
    }
    
    RDVCalendarDayCell *selectedDayCell = [self viewAtLocation:[touch locationInView:self]];
    
    if (selectedDayCell) {
        if (selectedDayCell == _selectedDayCell) {
            NSInteger cellIndex = [self indexForDayCell:selectedDayCell];
            
            [self selectDayCellAtIndex:cellIndex animated:NO];
        } else {
            [self deselectDayCellAtIndex:[self indexForDayCell:_selectedDayCell]
                                animated:NO];
        }
    }
    
    switch (selectedIndex) {
        case 0:
            self.egPlannedDate = [self selectedDate];
            
            
            [self.headerDateButton setTitle:[NSString stringWithFormat:@"%@ %@", [pickerData objectAtIndex:0], [AppConfig stringFromDate:self.egPlannedDate withFormat:@"dd-MM-yyyy"]] forState:UIControlStateNormal];
            break;
            
        case 1:
            self.egAdjustedDate = [self selectedDate];
            
            [self.headerDateButton setTitle:[NSString stringWithFormat:@"%@ %@", [pickerData objectAtIndex:1], [AppConfig stringFromDate:self.egAdjustedDate withFormat:@"dd-MM-yyyy"]] forState:UIControlStateNormal];
            break;
            
        case 2:
            self.egActualDate = [self selectedDate];
            
            [self.headerDateButton setTitle:[NSString stringWithFormat:@"%@ %@", [pickerData objectAtIndex:2], [AppConfig stringFromDate:self.egActualDate withFormat:@"dd-MM-yyyy"]] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
    
}

-(void) onDateSelection
{
//    self.dateTableView.hidden = FALSE;
    if(self.dateTableView.hidden)
        [self hideDateTableView:NO];
    else
        [self hideDateTableView:YES];

    NSInteger numberOfCells = 0;
    
    numberOfCells = [pickerData count];
    
    CGFloat height = numberOfCells * cellHeight;
    
    CGRect tableFrame = self.dateTableView.frame;
    tableFrame.size.height = height;
    self.dateTableView.frame = tableFrame;
    
    //then the tableView must be redrawn
    [self.dateTableView setNeedsDisplay];
    
    [self.dateTableView reloadData];

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if(self.dateTableView.hidden)
        return  YES;
    
    if ([touch.view isDescendantOfView:self.dateTableView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

- (void)handleGesture:(UIGestureRecognizer *)myGestureRecognizer {
    
    CGRect mySensitiveRect = self.dateTableView.frame;
    CGPoint p = [myGestureRecognizer locationInView:self];
    if (!CGRectContainsPoint(mySensitiveRect, p)) {
//        self.dateTableView.hidden = YES;
        [self hideDateTableView:YES];
        [self removeGestureRecognizer:self.gestureRecognizer];
    }
}


-(void)backButtonPressed:(id)sender
{
    if ([[self delegate] respondsToSelector:@selector(calendarView:backButton:)]) {
        [[self delegate] calendarView:self backButton:sender];
    }
}

-(void) onSave:(id)sender
{
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [self.activityLoader startAnimating];
    NSString* ownerID = self.transactionDate.trackingDateLink;
    
    NSString *str1 = @"Owners/";
    
    NSRange firstInstance = [self.transactionDate.trackingDateLink rangeOfString:str1];
    NSRange secondInstance = [[self.transactionDate.trackingDateLink substringFromIndex:firstInstance.location + firstInstance.length] rangeOfString:@"/"];
    NSRange finalRange = NSMakeRange(firstInstance.location + str1.length, secondInstance.location);
    ownerID = [self.transactionDate.trackingDateLink substringWithRange:finalRange];

    NSString *trackingDatesURL = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/TrackingDates/%@", APP_HEADER_URL, ownerID, self.jobID,  self.transactionDate.transcationId];
    
    NSString *jsonString = [NSString stringWithFormat:@"{\"Id\": \"%@\",\"Description\": \"TestTrackingDate\",\"PlannedDate\": \"%@\",\"AdjustedDate\": \"%@\",\"ActualDate\": \"%@\"}",self.transactionDate.transcationId, [AppConfig stringFromDate:self.egPlannedDate withFormat:@"yyyy-MM-dd"] , [AppConfig stringFromDate:self.egAdjustedDate withFormat:@"yyyy-MM-dd"], [AppConfig stringFromDate:self.egActualDate withFormat:@"yyyy-MM-dd"]];
    
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    id jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    
    [[DataDownloader sharedDownloader] updateData:jsonDict andServiceURl:trackingDatesURL andserviceType:EGORDIAN_API_TRACKING_DATES_UPDATE];
}

//this was done to solve the issue of date not comming while loading the screen
-(void)setEgPlannedDate:(NSDate *)egPlannedDate_ {
    if (egPlannedDate == egPlannedDate_) return; 

    egPlannedDate = [egPlannedDate_ copy];
    
    NSString* dateStr = @"";
    if(egPlannedDate)
        dateStr = [AppConfig stringFromDate:self.egPlannedDate withFormat:@"dd-MM-yyyy"];
    
//    if([self.headerDateButton.titleLabel.text containsString: [pickerData objectAtIndex:0]])
    
    NSRange range = [self.headerDateButton.titleLabel.text rangeOfString:[pickerData objectAtIndex:0]];
    if(range.location != NSNotFound)
        [self.headerDateButton setTitle:[NSString stringWithFormat:@"%@ %@", [pickerData objectAtIndex:0], dateStr] forState:UIControlStateNormal];
}

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    [self.activityLoader stopAnimating];
    if ([[self delegate] respondsToSelector:@selector(calendarView:backButton:)]) {
        [[self delegate] calendarView:self backButton:nil];
    }

}

-(void)serviceRequestFailed:(NSArray *)responseArray serviceType:(APIKey)type
{
    [self.activityLoader stopAnimating];
}

-(void) hideDateTableView:(BOOL) shouldHide
{
    if(shouldHide)
    {
        [UIView animateWithDuration:.5
                         animations: ^ {
                             [self.dateTableView setAlpha:0];
                         }
                         completion: ^ (BOOL finished) {
                             [self.dateTableView setHidden:TRUE];
                         }];
    }
    else{
        [UIView animateWithDuration:.5
                         animations: ^ {
                             [self.dateTableView setAlpha:1.0];
                         }
                         completion: ^ (BOOL finished) {
                             [self.dateTableView setHidden:FALSE];
                         }];
    }
}

@end
