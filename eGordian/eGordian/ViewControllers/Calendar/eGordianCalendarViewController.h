//
//  eGordianCalendarViewController.h
//  eGordian-iOS
//
//  Created by Laila Varghese on 12/17/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVCalendarViewController.h"
#import "EG_TransactionDate.h"

@interface eGordianCalendarViewController : RDVCalendarViewController<RDVCalendarViewDelegate>

@property (nonatomic, strong) EG_TransactionDate* transactionDate;
@property (nonatomic, strong) NSString* jobID;

@end
