//
//  eGordianCalendarViewController.m
//  eGordian-iOS
//
//  Created by Laila Varghese on 12/17/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "eGordianCalendarViewController.h"
#import "RDVCalendarDayCell.h"

@interface eGordianCalendarViewController ()

@end

@implementation eGordianCalendarViewController

@synthesize transactionDate, jobID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSDate* theDate = nil;
    
    
    if(self.transactionDate.jobPlannedDate)
        theDate = [AppConfig dateFromString:self.transactionDate.jobPlannedDate withFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    [self calendarView].egPlannedDate = theDate;
    if(theDate)
        [[self calendarView] setSelectedDate:[self calendarView].egPlannedDate ];
    
    theDate = nil;
    if(self.transactionDate.jobActualDate)
        theDate = [AppConfig dateFromString:self.transactionDate.jobActualDate withFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [self calendarView].egActualDate = theDate;
    
    theDate = nil;
    if(self.transactionDate.jobAdjustedDate)
        theDate = [AppConfig dateFromString:self.transactionDate.jobAdjustedDate withFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [self calendarView].egAdjustedDate = theDate;

    [self calendarView].transactionDate = self.transactionDate;
    [self calendarView].jobID = self.jobID;
    
    [[self calendarView] registerDayCellClass:[RDVCalendarDayCell class]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)calendarView:(RDVCalendarView *)calendarView backButton:(id)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
