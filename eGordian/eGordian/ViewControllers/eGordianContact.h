//
//  eGordianContact.h
//  eGordian-iOS
//
//  Created by Laila on 28/08/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface eGordianContact : NSObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *FirstName;
@property (nonatomic, copy) NSString *LastName;
@property (nonatomic, copy) NSString *Name;
@property (nonatomic, copy) NSString *MiddleName;
@property (nonatomic, copy) NSString *DisplayName;
@property (nonatomic, copy) NSString *Link;
@property (nonatomic, copy) NSString *OwnerID;
@property (nonatomic, copy) NSString *Key;
@property (nonatomic, copy) NSString *CompanyID;
@property (nonatomic, copy) NSString *CompanyName;
@property (nonatomic, copy) NSString *eMail;
@property (nonatomic, copy) NSString *Address1;
@property (nonatomic, copy) NSString *Address2;
@property (nonatomic, copy) NSString *City;
@property (nonatomic, copy) NSString *State;
@property (nonatomic, copy) NSString *ZipCode;
@property (nonatomic, copy) NSString *MobilePhone;
@property (nonatomic, copy) NSString *Suffix;
@property (nonatomic, copy) NSString *Role;
@property (nonatomic, copy) NSString *ContactLink;
@property (nonatomic, copy) NSString *BusinessPhone1;
@property (nonatomic, copy) NSString *BusinessPhone2;


- (id) initWithDictionary:(NSDictionary *)dictionary;
//-(id) initWithDict:(NSDictionary*)ContactsDict;
- (id) initWithArray:(NSArray*)ContactsArray;
- (NSDictionary*) getAsDictionary;
+(NSMutableArray*) getContactArray:(NSArray*)contactsArray;
@end