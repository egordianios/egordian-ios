//
//  EGJobDocumentsViewController.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EGJobDocumentsViewController.h"
#import "SortViewListCell.h"
#import "EG_JobDocument.h"
#import "EGScopeOfWorkDetailViewController.h"
#import "EGScopeOfWorkEditViewController.h"
#import "EGWebViewController.h"

@interface EGJobDocumentsViewController ()

@property(nonatomic,retain) NSMutableArray *documentDataArray;

@end

@implementation EGJobDocumentsViewController

@synthesize currentJobOrder,strJobOrderLink, jobClientNameLabel, jobTitleLabel, jobIdLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scopeOfworkAddButton.hidden = YES;
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:APP_JOBDOCUMENTS_REFRESH_FLAG];
    [[NSUserDefaults standardUserDefaults]synchronize];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    
    [self setIntialUI];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = TRUE;
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:APP_JOBDOCUMENTS_REFRESH_FLAG] boolValue]){
        
         [self actvityStart];
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:APP_JOBDOCUMENTS_REFRESH_FLAG];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        self.scopeOfworkAddButton.hidden = YES;
        self.scopeOfWorkButton.enabled = YES;
        [self fetchscopeOfWork:[NSString stringWithFormat:@"%@/DetailedScope",self.strJobOrderLink]];
        
    }
    else{
        if(![currentJobOrder.detailedScopeLink isEqual:[NSNull null]] && currentJobOrder.detailedScopeLink.length > 0){
            self.scopeOfworkAddButton.hidden = YES;
            self.scopeOfWorkButton.enabled = YES;
            [self fetchscopeOfWork:currentJobOrder.detailedScopeLink];
        }
        else
        {
            self.scopeOfworkAddButton.hidden = NO;
            self.scopeOfWorkButton.enabled = NO;
            self.detailedScopeDataLabel.text = @"Detailed scope of work not available for the job";
            if(![currentJobOrder.documentsLink isEqual:[NSNull null]] && currentJobOrder.documentsLink.length > 0){
                [self fetchDocuments:currentJobOrder.documentsLink];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)documentsBackButtonTapped:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)scopeOfworkTapped:(id)sender {
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EGScopeOfWorkDetailViewController *scopeVC = (EGScopeOfWorkDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGScopeOfWorkDetailViewController"];
    scopeVC.currentJobOrder = self.currentJobOrder;
    scopeVC.strScopeOfWorkUrl = [NSString stringWithFormat:@"%@/DetailedScope",self.strJobOrderLink];
    scopeVC.scopeOfWorkString = self.detailedScopeDataLabel.attributedText;
    [self.navigationController pushViewController:scopeVC animated:YES];
}

- (IBAction)addScopeOfWorkTapped:(id)sender {
  
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
   
    EGScopeOfWorkEditViewController *addScopeVC = (EGScopeOfWorkEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGScopeOfWorkEditViewController"];

    addScopeVC.isCalledFromOffline = FALSE;
    
    addScopeVC.strDetailedScopeLink = [NSString stringWithFormat:@"%@/DetailedScope",self.strJobOrderLink];
    
    addScopeVC.scopeOfWorkString = [[NSAttributedString alloc]initWithString:@""];
    
    [self.navigationController pushViewController:addScopeVC animated:YES];
    
}


-(void)setIntialUI{
    
    self.documentsTable.tableFooterView = [[UIView alloc] initWithFrame : CGRectZero];
    
    self.documentDataArray              = [[NSMutableArray alloc]init];
    
    self.jobTitleLabel.text             = self.currentJobOrder.jobTitle;
    
//    [self.jobTitleLabel sizeToFit];
    
    self.jobIdLabel.text                = [NSString stringWithFormat:@"ID:%@",self.currentJobOrder.jobOrderId];
    
    self.jobClientNameLabel.text        = self.currentJobOrder.clientName;
    
    [self setJobOrderInfoUI];
}

-(void)setJobOrderInfoUI{
    
    CGFloat padding = 15.0f;
    
    CGFloat labelWidth = self.view.frame.size.width - (2*padding);
    
    CGFloat dy = padding;
    
    CGFloat defaultLabelHeight = 20.0f;
    
    NSString *jobTitleText = [NSString stringWithFormat:@"%@",currentJobOrder.jobTitle];
    
    NSString *jobClientName = [NSString stringWithFormat:@"%@",currentJobOrder.clientName];
    
    NSString *jobOrderId = [NSString stringWithFormat:@"ID:%@",currentJobOrder.jobOrderNumber];
    
    [self.jobOrderTitleInfoView  setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if(jobTitleText.length > 0){
        
        jobTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobTitleLabel setFont:[UIFont fontWithName:FONT_SEMIBOLD size:15.0]];
        
        [jobTitleLabel setNumberOfLines:0];
        
        [jobTitleLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobTitleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobTitleLabel setText:jobTitleText];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobTitleLabel WithText:jobTitleLabel.text];
        
        [jobTitleLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobTitleLabel];
        
        dy = dy+dynamicTextHeight+3.0;
    }
    
    if(jobClientName.length > 0){
        
        jobClientNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobClientNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobClientNameLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobClientNameLabel setNumberOfLines:0];
        
        [jobClientNameLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobClientNameLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobClientNameLabel setText:jobClientName];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobClientNameLabel WithText:jobClientNameLabel.text];
        
        [jobClientNameLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobClientNameLabel];
        
        dy = dy+dynamicTextHeight+(jobOrderId.length?3.0:padding);
    }
    
    if(jobOrderId.length > 0){
        
        jobIdLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding, dy, labelWidth, defaultLabelHeight)];
        
        [jobIdLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        
        [jobIdLabel setFont:[UIFont fontWithName:FONT_REGULAR size:12.0]];
        
        [jobIdLabel setNumberOfLines:0];
        
        [jobIdLabel setTextColor:[EGordianUtilities labelTitleColor]];
        
        [jobIdLabel setTextAlignment:NSTextAlignmentCenter];
        
        [jobIdLabel setText:jobOrderId];
        
        CGFloat dynamicTextHeight = [EGordianUtilities heightForLabel:jobIdLabel WithText:jobIdLabel.text];
        
        [jobIdLabel setFrame:CGRectMake(padding, dy, labelWidth, dynamicTextHeight)];
        
        [self.jobOrderTitleInfoView addSubview:jobIdLabel];
        
        dy = dy+dynamicTextHeight+padding;
    }
    
    
    [self.jobOrderTitleInfoView setFrame:CGRectMake(0, 62.0f, self.view.frame.size.width, dy)];
    [self.scopeOfWorkView setFrame:CGRectMake(0, self.jobOrderTitleInfoView.frame.origin.y+self.jobOrderTitleInfoView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-62.0f-self.jobOrderTitleInfoView.frame.size.height - 62.0f)];
    
}

-(void)actvityStart{
    [self.loaderView setHidden:NO];
    [self.view bringSubviewToFront:self.loaderView];
    [self.activityLoader startAnimating];
}

-(void)actvityStop{
    [self.activityLoader stopAnimating];
    [self.loaderView setHidden:YES];
}

-(void)fetchscopeOfWork:(NSString *)scopeOfWorkURL{

    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:scopeOfWorkURL serviceType:EGORDIAN_API_JOBORDER_DETAILEDSCOPE];
}

-(void)fetchDocuments:(NSString *)documentsURL{
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]getResponseFormUrl:documentsURL serviceType:EGORDIAN_API_JOBORDER_DOCUMENTS];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return [self.documentDataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SortViewListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SortViewListCell"];
    
    if (!cell)
    {
        cell = [[SortViewListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SortViewListCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    EG_JobDocument *currentDocument = (EG_JobDocument *)[self.documentDataArray objectAtIndex:indexPath.row];
    
    cell.titleText = currentDocument.fileName;
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(![AppDelegate isConnectedToInternet])
    {
        [EGordianAlertView showFeatureNotAvailableMessage];
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EG_JobDocument *currentDocument = (EG_JobDocument *)[self.documentDataArray objectAtIndex:indexPath.row];
    
    EGWebViewController *webVC = (EGWebViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGWebViewController"];
    
    webVC.redirectVia      = @"Documents";
    
    webVC.webViewLoadURl   = currentDocument.fileUrl;
    
    [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - Data Downloader Delegate Methods

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    switch (type) {
        case EGORDIAN_API_JOBORDER_DETAILEDSCOPE:{
            if(responseArray.count > 0){
                NSDictionary *detailedScopeDict = [responseArray objectAtIndex:0];
                
                if([detailedScopeDict objectForKey:@"Data"])
                {
                    self.scopeOfWorkButton.enabled = YES;
                    
                    NSError *error ;
                    
                    NSString *detailedScopeString = [detailedScopeDict objectForKey:@"Data"];
                    
                    NSData *data = [detailedScopeString dataUsingEncoding:NSUTF8StringEncoding];
                    
                    NSAttributedString *str = [[NSAttributedString alloc] initWithData:data
                                               
                                                                               options:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType,
                                                                                         NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}
                                                                    documentAttributes:nil error:&error];
                    
                    if(str.length>0){
                        self.detailedScopeDataLabel.attributedText = str;
                        
                    }
                    else{
                        
                        self.detailedScopeDataLabel.attributedText = [[NSAttributedString alloc]initWithString:detailedScopeString];
                    }
                }
                else{
                    
                    self.scopeOfWorkButton.enabled = NO;
                    
                    self.detailedScopeDataLabel.text = @"Detailed scope of work not available for the job";
                }
            }
            
            
            if(![currentJobOrder.documentsLink isEqual:[NSNull null]] && currentJobOrder.documentsLink.length > 0){
                [self fetchDocuments:currentJobOrder.documentsLink];
            }
            
            break;
        }
        case EGORDIAN_API_JOBORDER_DOCUMENTS:{
            
            self.documentDataArray = [responseArray copy];
            
            [self.documentsTable reloadData];
            
            if(self.documentDataArray.count == 0){
                [self.infoTextLabel setHidden:FALSE];
                [self.view bringSubviewToFront:self.infoTextLabel];
                self.infoTextLabel.text = @"File(s) Not Found for this job Order";
            }
            else{
                [self.infoTextLabel setHidden:TRUE];
            }
            
            [self actvityStop];
            
            break;
        }
        default:
            break;
    }
}
-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
    
    
    [self actvityStop];
    
    NSString *alertMessage = @"";
    
    if(type == EGORDIAN_API_JOBORDER_DETAILEDSCOPE){
        self.detailedScopeDataLabel.text = @"Detailed scope of work not available for the job";
    }
    
    if(error.localizedDescription.length && error.code){
        alertMessage = error.localizedDescription;
    }
    NSLog(@"Error Message--%@",alertMessage);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
