//
//  DashBoardViewController.h
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <AssetsLibrary/AssetsLibrary.h> 
#import "OverlayView.h"
#import "CustomButtonView.h"

typedef enum
{
    DM_OPEN_JOBS    ,
    DM_CONTACTS     ,
    DM_PHOTOS       ,
    DM_OFFLINE_JOBS ,
    DM_COUNT
}DashBoardMenuItem;
@interface DashBoardViewController : UIViewController<DataDownloaderDelegate,ButtonDelegate>{
    ALAssetsLibrary *library;
    NSArray *imageArray;
    NSMutableArray *mutableArray;
    int photoCount;
}
@property (weak, nonatomic) IBOutlet UITableView *dashBoardTable;
@property (weak, nonatomic) IBOutlet UILabel *unAttachedPhotoCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactCountLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderToUpdatePhotoCount;
@property (weak, nonatomic) IBOutlet UIButton *openJobsButton;
- (IBAction)RightMenuButtonTapped:(id)sender;
- (IBAction)openJobsButtonTapped:(id)sender;
- (IBAction)onAllContacts:(id)sender;
- (IBAction)onUnattachedPhotosBtn:(id)sender;
//- (void)onThumbnailImage;
@end
