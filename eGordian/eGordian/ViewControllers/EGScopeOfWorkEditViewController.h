//
//  EGScopeOfWorkEditViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/31/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TPKeyboardAvoidingScrollView.h>
#import "EG_JobOrderDetail.h"
#import "DataDownloader.h"
#import "JobOrder.h"
#import "EGContainerViewController.h"

@interface EGScopeOfWorkEditViewController : UIViewController <DataDownloaderDelegate,egordianContainerDelegate>

@property (nonatomic, strong) NSString *strDetailedScopeLink;
@property (nonatomic,strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *eGScopeEditInputModeSeg;
@property (weak, nonatomic) IBOutlet UITextView *eGScopeEditTxtVw;
@property (nonatomic, assign) BOOL isCalledFromOffline;
@property (nonatomic, retain) NSAttributedString *scopeOfWorkString;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;
@property (nonatomic ,retain) id selectedJobOrder;

- (IBAction)onBack:(id)sender;
- (IBAction)onSave:(id)sender;

@end
