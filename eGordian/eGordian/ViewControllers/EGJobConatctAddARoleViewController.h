//
//  EGJobConatctAddARoleViewController.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/4/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "EG_ContactDetail.h"
#import "EG_OwnerContact.h"
#import "DataDownloader.h"

@interface EGJobConatctAddARoleViewController : UIViewController<MFMailComposeViewControllerDelegate,DataDownloaderDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *eGordianViewContactsTableView;

@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *roleLabel;

@property (nonatomic, strong) EG_ContactDetail* eGContactDetail;

@property (nonatomic, strong) EG_OwnerContact* eGOwnerContact;

@property (weak, nonatomic) IBOutlet UIView *loaderView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoader;

@property (weak, nonatomic) IBOutlet UIView *actualPopOverView;

@property (weak, nonatomic) IBOutlet UITableView *pickerTable;

@property (weak, nonatomic) IBOutlet UIView *popOverView;

@property (assign, nonatomic) CGRect mySensitiveRect;

@property (nonatomic,strong) NSString *strJobOrderId;

@property (nonatomic,strong) NSString *strRolesURL;

- (IBAction)roleButtonTapped:(id)sender;

- (IBAction)addRoleBackButtonTapped:(id)sender;

- (IBAction)saveButtonTapped:(id)sender;


// this dict is used especially for email, B. Phone and Mobile to accomodate the scenario when there is no email\b.phone\Mobile is available.
@property (strong, nonatomic) NSMutableDictionary* dataDict;

@end
