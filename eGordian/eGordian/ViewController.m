//
//  ViewController.m
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DashBoardViewController.h"
#import "AppDelegate.h"
#import "TAGContainer.h"
#import "TAGDataLayer.h"
#import "TAGManager.h"
#import "AppConfig.h"
#import "JobOrderDAO.h"
#import "EGWebViewController.h"


@interface ViewController ()<SyncManagerDelegate> {
    
    id firstResponder ; 
}
@property(nonatomic, assign) AppDelegate *appDelegate;

@end

@implementation ViewController

@synthesize mySensitiveRect;
@synthesize appDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.loaderView.hidden = YES;
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
}

-(void) viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:APP_USERID] && [[NSUserDefaults standardUserDefaults]objectForKey:APP_AUTHORIZATION_PREFERENCE])
    {
        [self redirectToDashboard];
    }
    
    self.userNameTextField.text = @"";
    
    self.passwordTextField.text = @"";
    
    //white text color for placeholder - fix Mobile - 366
    [self.userNameTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    self.forgotPasswordView.hidden = YES;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    [gestureRecognizer setNumberOfTapsRequired:1.0];
    
    [gestureRecognizer setDelegate:self];
    
    [self.forgotPasswordView addGestureRecognizer:gestureRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonTapped:(id)sender {
    
    if ([AppDelegate isConnectedToInternet])
    {
        if(self.userNameTextField.text.length > 0 && self.passwordTextField.text.length > 0){
            
            self.loaderView.hidden = NO;
            
            [self.actvityLoader startAnimating];
            
            [self.userNameTextField resignFirstResponder];
            [self.passwordTextField resignFirstResponder];
            
            [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
            [[DataDownloader sharedDownloader]loginWithUserID:self.userNameTextField.text password:self.passwordTextField.text];
            //GTM Implementation
            NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
            
            [self.appDelegate.tagManager.dataLayer pushValue:self.userNameTextField.text.length?self.userNameTextField.text:@"" forKey:@"appUserName"];
            [self.appDelegate.tagManager.dataLayer pushValue:appBuildString forKey:@"appVersion"];
            
            [GTMLogger pushOpenScreenEventWithScreenName:@"Login Screen"];
            
//            NSDictionary *pushDict  = @{@"event": @"openScreen",// openScreen// Event, name of
//                                        @"screenName": @"Login Screen",
//                                        @"appUserName":self.userNameTextField.text.length?self.userNameTextField.text:@"",
//                                        @"appVersion":appBuildString};
//            [GTMLogger logEventToGTM:@"loginEvent" andinputParams:pushDict];
                        
        }
        else{
            
            UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"Could Not Login?" message:@"Please provide your username and password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [displayAlert show];
        }
    }else{
        UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                              message:@"Internet connection not available, Please check the Connection and try again."
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil];
        
        [displayAlert show];
    }
}

-(void)redirectToDashboard{
    
    self.loaderView.hidden = YES;
    
    [self.actvityLoader stopAnimating];
        
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DashBoardViewController *dashBoardVC = (DashBoardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
    
    [self.navigationController pushViewController:dashBoardVC animated:YES];
}
- (void)displayComingSoonAlert{
   
    UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"Coming Soon" message:@"This page is under development and will be updated soon." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [displayAlert show];
}

- (IBAction)aboutButtonTapped:(id)sender {
//    [self displayComingSoonAlert];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EGWebViewController *webVC = (EGWebViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGWebViewController"];
    
    webVC.redirectVia      = @"More";
    
    webVC.resourceViaIndex = 0;
    
    [self.navigationController pushViewController:webVC animated:YES];
}

- (IBAction)forgotPasswordButtonTapped:(id)sender {
    [self.forgotPasswordView setHidden:NO];
}

- (IBAction)helpButtonTapped:(id)sender {
//    [self.forgotPasswordView setHidden:NO];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EGWebViewController *webVC = (EGWebViewController *)[storyboard instantiateViewControllerWithIdentifier:@"EGWebViewController"];
    
    webVC.redirectVia      = @"More";
    
    webVC.resourceViaIndex = 1;
    
    [self.navigationController pushViewController:webVC animated:YES];
}

- (IBAction)callSupportRepresentativeButtonTapped:(id)sender {
    
    [self.forgotPasswordView setHidden:YES];
}

- (IBAction)emailRepresentativebuttonTapped:(id)sender {
    
    [self.forgotPasswordView setHidden:YES];
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    mySensitiveRect = self.actualForgotPassowrdView.frame;
    CGPoint p = [gestureRecognizer locationInView:self.forgotPasswordView];
    if (!CGRectContainsPoint(mySensitiveRect, p)) {
        self.forgotPasswordView.hidden = YES;
        NSLog(@"got a tap in the region i care about");
    } else {
        NSLog(@"got a tap, but not where i need it");
    }
}

#pragma mark - UITextField Delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    firstResponder = textField;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.text = @"";
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    firstResponder = nil;
    if (textField == self.userNameTextField) {
        if(self.userNameTextField.text.length > 0){
            [self.passwordTextField becomeFirstResponder];
        }
        else{
            [self showAlertWithMessage:@"Please enter the username."];
            
            return NO;
        }
    }
    else if (textField == self.passwordTextField)
    {
        if(!self.userNameTextField.text.length){
            [self showAlertWithMessage:@"Please enter the username."];
            return NO;
        }
        else if(self.passwordTextField.text.length == 0){
            [self showAlertWithMessage:@"Please enter the password."];
            return NO;
        }
        
        [textField resignFirstResponder];
    }
    
    if(self.userNameTextField.text.length > 0 && self.passwordTextField.text.length > 0)
        [self loginButtonTapped:nil];

    
    return YES;
}

-(void)showAlertWithMessage:(NSString *)alertMessage{
    
    UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:APP_NAME
                                                          message:alertMessage
                                                         delegate:nil
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles:nil, nil];
    
    [displayAlert show];
}

#pragma mark API Manager Delegates

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type{
    
    if(type == EGORDIAN_API_LOGIN){
        
        [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
        
        [[DataDownloader sharedDownloader]eGordianAuthorization];
    }
    else if(type == EGORDIAN_API_AUTHORIZATION){
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:EGAppFirstLaunch];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
//        [self performSelectorOnMainThread:@selector(performSync) withObject:nil waitUntilDone:NO];
        
        [self.appDelegate resetRootViewController];
    }
}

-(void)performSync{
    
    [CoreDataBase clearUserData];
    
    [[SyncManager sharedManager] setStatusDelegate:self];
    
    [[SyncManager sharedManager] fullSync];
    
}


-(void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type{
   
    self.loaderView.hidden = YES;
    
    [self.actvityLoader stopAnimating];
    
    NSString *alertMessage = @"";
    NSString *alertTitle = @"";
    
    if(type == EGORDIAN_API_LOGIN){
        
        if([error.localizedDescription containsString:@"timed out"]){
            alertMessage = @"The username or password was incorrect. Please try again.";
            alertTitle = @"Could Not Login?";
        }
        else if(error.localizedDescription.length && error.code){
            alertMessage = error.localizedDescription;
        }
        else{
            alertMessage = @"The username or password was incorrect. Please try again.";
            alertTitle = @"Could Not Login?";
        }
    }
    else if(type == EGORDIAN_API_AUTHORIZATION){
        
        if(error.localizedDescription.length && error.code){
            alertMessage = error.localizedDescription;
            alertTitle = @"Error";
        }
    }
    
    UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [displayAlert show];
}

#pragma Sync Manager Delegate Methods

-(void)syncDidStart{
    [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONSTART object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:CTDASHBOARDTABLERELOAD object:nil];
}
-(void)syncDidFinshForAction:(SyncManagerAction) status withError:(NSError *)error{
    
}
-(void)syncDidFinsh{
    [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONEND object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:CTDASHBOARDTABLERELOAD object:nil];
}


@end
