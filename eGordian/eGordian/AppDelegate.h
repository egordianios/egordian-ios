//
//  AppDelegate.h
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "EG_JobOrderDetail.h"
#import "SyncManager.h"

@class TAGManager;
@class TAGContainer;
@interface AppDelegate : UIResponder <UIApplicationDelegate,SyncManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *objUINavigationController;
- (void)resetRootViewController;
//GTM Implementation
@property (nonatomic, strong) TAGManager *tagManager;
@property (nonatomic, strong) TAGContainer *container;
+ (BOOL)isConnectedToInternet;

@property (nonatomic, assign) BOOL isFromJobDetails;// TRUE if the current camera screen is called from any of the job details screen
//@property (nonatomic, assign) EG_JobOrderDetail* jobOrder;//this is required to attach photo to this job, if invoking camera from any of these screens.
@end

