//
//  OverlayView.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/10/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "OverlayView.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "APPhotolibrary.h"
#import "ALAsset+Photo.h"
#import "CustomTabBarViewController.h"
#import "EGContainerViewController.h"

@implementation OverlayView
@synthesize egImagePickerController, photoLibrary, loadView, cameraButton, homeButton, backButton;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //clear the background color of the overlay
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        
        //Adding TopBar
        int width = 100;
        int height = 44;
        int space = 10;
        
        UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 62)];
        [headerView setBackgroundColor:[EGordianUtilities headerColor]];
        UILabel *headerLabel =[[UILabel alloc] initWithFrame:CGRectMake((headerView.frame.size.width-width)/2, (headerView.frame.size.height-height)/2, width, height)];
        [headerLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin];
        [headerLabel setText:@"CAMERA"];
        [headerLabel setFont:[UIFont fontWithName:FONT_STRATUM_BOLD size:20]];
        [headerLabel setTextAlignment:NSTextAlignmentCenter];
        [headerView addSubview:headerLabel];
        [headerLabel setTextColor:[UIColor darkGrayColor]];
        
        //BackButton
        
        UIImage* backImage = [UIImage imageNamed:@"eGSystem_BackArrow"];
        int backButtonHeight = 2*backImage.size.height;
        self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.backButton setImage:backImage forState:UIControlStateNormal];
        self.backButton.frame = CGRectMake(space, (headerView.frame.size.height-backButtonHeight)/2, 2*backImage.size.width, backButtonHeight);
//        backButton.tintColor = self.tintColor;
        [headerView addSubview:self.backButton];
        [self addSubview:headerView];
        
        //eGSystem_BackArrow
        height = 62;
        UIView *footerView=[[UIView alloc] initWithFrame:CGRectMake(0, (self.frame.size.height-height), self.frame.size.width, height )];
        [footerView setBackgroundColor:[UIColor colorWithRed:53.0/255 green:62.0/255 blue:68.0/255 alpha:1.0]];
        UIImage* cameraImage = [UIImage imageNamed:@"eG_Icon-Camera_Norm"];
        width = footerView.frame.size.width/3;
        self.cameraButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.cameraButton addTarget:self action:@selector(cameraButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.cameraButton setImage:cameraImage forState:UIControlStateNormal];
        [self.cameraButton setImage:cameraImage forState:UIControlStateSelected];
        self.cameraButton.showsTouchWhenHighlighted = YES;
        self.cameraButton.frame = CGRectMake(footerView.frame.size.width/3, 0, footerView.frame.size.width/3, footerView.frame.size.height);
        self.cameraButton.tintColor = [UIColor whiteColor];
        [footerView addSubview:self.cameraButton];
        
        UIImage* homeImage = [UIImage imageNamed:@"eG_Icon-Home_Norm"];
        self.homeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.homeButton addTarget:self action:@selector(homeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.homeButton setImage:homeImage forState:UIControlStateNormal];
        [self.homeButton setImage:homeImage forState:UIControlStateHighlighted];
        self.homeButton.showsTouchWhenHighlighted = YES;
        self.homeButton.frame = CGRectMake(0, 0, footerView.frame.size.width/3, footerView.frame.size.height);
        self.homeButton.tintColor = [UIColor whiteColor];
        [footerView addSubview:self.homeButton];
        
        height = footerView.frame.size.height - 10;
        width = height;
        [self addSubview:footerView];
        
        self.photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.origin.x, headerView.frame.size.height, self.frame.size.width, self.frame.size.height-headerView.frame.size.height-footerView.frame.size.height)];
        self.photoImageView.hidden = TRUE;
        self.photoImageView.backgroundColor = [UIColor clearColor];
        [self.photoImageView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.photoImageView];
        
        self.loadView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y+headerView.frame.size.height, self.frame.size.width, self.frame.size.height-headerView.frame.size.height-footerView.frame.size.height)];
        [self.loadView setBackgroundColor:[UIColor clearColor]];
        self.loadView.hidden = TRUE;
        [self addSubview:self.loadView];
        
        UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.alpha = 1.0;
//        activityIndicator.center = CGPointMake((self.frame.size.width - activityIndicator.frame.size.width) / 2.0f, (self.frame.size.height - activityIndicator.frame.size.height) / 2.0f);
        activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin);
        activityIndicator.center = CGPointMake(([UIScreen mainScreen].bounds.size.width-activityIndicator.frame.size.width)/2.0, ([UIScreen mainScreen].bounds.size.height-footerView.frame.size.height-activityIndicator.frame.size.height)/2.0);
        [activityIndicator startAnimating];
        [self.loadView addSubview:activityIndicator];
        
        [self stopAnimating];
        [self loadThumbnailPhoto];
    }

    return self;
}


-(void) didMoveToSuperview
{
//    self.photoImageView.hidden = TRUE;
    [self loadThumbnailPhoto];
}

-(IBAction)cameraButtonTapped:(id)sender{
    
    if (self.egImagePickerController) {
        [self.egImagePickerController takePicture];
//        [self.fullLoadView setHidden:FALSE];
        [self deactivateButtons];
        [self stopAnimating];
        self.cameraButton.enabled = FALSE;
    }
}

-(IBAction)backButtonPressed:(id)sender{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"isOpenedOffline"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (self.egImagePickerController) {
        [self.egImagePickerController dismissViewControllerAnimated:YES completion:nil];
    }
   ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
}

-(IBAction)homeButtonPressed:(id)sender
{
    ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
    if (self.egImagePickerController) {
        [self.egImagePickerController dismissViewControllerAnimated:YES completion:nil];
    }
    
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    
    [containerVC eGMainFooterHomeTapped:nil];
}

-(void)onThumbnail
{
   ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
    if (self.egImagePickerController) {
        [self.egImagePickerController dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    EGContainerViewController *containerVC = [EGContainerViewController sharedContainerView];
    [containerVC onThumbnail];
}

- (void)cameraButtonTappedOverLay{
    
    
}


#pragma mark Image Picker DelegateMethods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //    ALAssetsLibrary* alassetsLib = [[ALAssetsLibrary alloc] init];
    
    //Image picker for nil value
    UIImage* selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    //    NSLog(@"my Info :: %@", info);
    
    //here there was a problem where selectedImage is null... so do additional task to get the image
    if(NULL == selectedImage){
        selectedImage =  [APPhotolibrary loadImageFromAssertByUrl:[info objectForKey:@"UIImagePickerControllerReferenceURL"]
                                                       completion:^(UIImage * selectedImage){
                                                           
                                                       }];
    }
    
    if(selectedImage)
    {
        //        [self.fullLoadView setHidden:TRUE];
        [self activateButtons];
        BOOL isInvokedOffline =[[NSUserDefaults standardUserDefaults] boolForKey:@"isOpenedOffline"];
        
        if (isInvokedOffline) {
            
            NSURL *myassetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
            NSLog(@"Got image for Offline:%@",myassetURL);
            //selectedImage = UIImagePNGRepresentation(selectedImage);
            NSDictionary* dict = @{@"originalImage":selectedImage };
            [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineImageCapturedNotification object:nil userInfo:dict];
            return;
        }
        
        
        [self.photoImageView setImage:selectedImage];
        self.photoImageView.hidden = FALSE;
        self.backgroundColor = [UIColor clearColor];
        [self bringSubviewToFront:self.photoImageView];
        
        [self startAnimating];
        
        NSData* imageData = UIImageJPEGRepresentation(selectedImage, (CGFloat)0.1);
        
        NSString *photoStr = [imageData  base64EncodedString];
        
        //photo is taken from a viewcontroller which has a jobID, so attach the photo to the job.
        if(((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails)
        {
            [self attachPhotoToJob:photoStr];
        }
        else //if camera is not opened from a viewcontroller related to job
        {
            [self uploadPhoto:photoStr];
        }
    }
    
}


-(void)attachPhotoToJob:(NSString*)selectedImage
{
    
    NSString* jobOrderID = [EGordianUtilities getJobOrderID];
    NSString* ownerID = [EGordianUtilities getOwnerID];
    
    if(!jobOrderID || !ownerID)
        return;
    
    NSString *photoToJobURLString = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/UploadPictures",APP_HEADER_URL, ownerID, jobOrderID];
    
//    NSString *photoToJobURLString = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/Pictures",APP_HEADER_URL, ownerID, jobOrderID];

    time_t unixTime = (time_t) [[NSDate date] timeIntervalSince1970];
    NSString *timestamp=[NSString stringWithFormat:@"%ld",unixTime];
    NSDictionary* jsonDict = @{@"id": @0,
                               @"UserID":([AppConfig getUserId])?[AppConfig getUserId]:@"",
                               @"PictureName":[NSString stringWithFormat:@"%@_%@", [AppConfig getUserId], timestamp],
                               @"Thumbnail":@"",
                               @"Image":(selectedImage)?selectedImage:@"" };
    
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]updateData:[NSArray arrayWithObjects:jsonDict, nil] andServiceURl:photoToJobURLString andserviceType:EGORDIAN_API_PHOTOS_ATTACH_TO_JOB ];
    
}

-(void) uploadPhoto:(NSString*)selectedImage
{
    NSString *photoToUserURLString = [NSString stringWithFormat:@"%@Users/%@/Pictures",APP_HEADER_URL, [AppConfig getUserId]];
    
    time_t unixTime = (time_t) [[NSDate date] timeIntervalSince1970];
    NSString *timestamp=[NSString stringWithFormat:@"%ld",unixTime];
    NSDictionary* jsonDict = @{@"id": @0,
                               @"UserID":([AppConfig getUserId])?[AppConfig getUserId]:@"",
                               @"PictureName":[NSString stringWithFormat:@"%@_%@", [AppConfig getUserId], timestamp],
                               @"Thumbnail":@"",
                               @"Image":(selectedImage)?selectedImage:@"" };
    NSArray* pictureArray = [NSArray arrayWithObjects:jsonDict, nil];
    NSMutableDictionary* pictureArrayDict = [[NSMutableDictionary alloc]init];
    [pictureArrayDict setObject:pictureArray forKey:@""];
    [pictureArrayDict setObject:@"" forKey:@"Description"];
    [[DataDownloader sharedDownloader]setDataDownloaderDelegate:(id)self];
    [[DataDownloader sharedDownloader]updateData:[NSArray arrayWithObjects:jsonDict, nil] andServiceURl:photoToUserURLString andserviceType:EGORDIAN_API_UPLOAD_USER_PHOTO ];
}

-(void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type
{
    switch (type) {
        case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
            case EGORDIAN_API_UPLOAD_USER_PHOTO:
        {
            [self stopAnimating];
            self.cameraButton.enabled = TRUE;
            
            self.photoImageView.hidden = TRUE;
            
            self.egImagePickerController.view.hidden = FALSE;
            self.egImagePickerController.view.backgroundColor = [UIColor clearColor];
            [self sendSubviewToBack:self.photoImageView];
            
            if (self.egImagePickerController) {
                [self.egImagePickerController dismissViewControllerAnimated:YES completion:nil];
            }
            
            ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
            if(type == EGORDIAN_API_UPLOAD_USER_PHOTO)
                [EGordianUtilities updateRefreshStatus:YES forPage:APP_USERPHOTO_REFRESH_FLAG];
            if(type == EGORDIAN_API_PHOTOS_ATTACH_TO_JOB)
                [EGordianUtilities updateRefreshStatus:YES forPage:APP_JOBPHOTO_REFRESH_FLAG];
        }
            break;
            
//        case EGORDIAN_API_UPLOAD_USER_PHOTO:
//        {
//            
//        }
//            break;
        
        default:
            break;
    }
}

- (void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type;
{
    self.cameraButton.enabled = TRUE;
    switch (type) {
        case EGORDIAN_API_UPLOAD_USER_PHOTO:
        {
            if(error.code==400 && ([error.localizedDescription rangeOfString:@"User Picture Limit"].location != NSNotFound) )
            {
                UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"User Picture Limit" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                
                [displayAlert show];
            }
            
            [self stopAnimating];
            [self backButtonPressed:nil];
            
            self.photoImageView.hidden = TRUE;
            
            self.egImagePickerController.view.hidden = FALSE;
            self.egImagePickerController.view.backgroundColor = [UIColor clearColor];
            [self sendSubviewToBack:self.photoImageView];
            [self stopAnimating];
            ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
        }
            break;
            
        case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
        {
            [self stopAnimating];
            [self backButtonPressed:nil];
            
            self.photoImageView.hidden = TRUE;
            
            self.egImagePickerController.view.hidden = FALSE;
            self.egImagePickerController.view.backgroundColor = [UIColor clearColor];
            [self sendSubviewToBack:self.photoImageView];
            [self stopAnimating];
            ((AppDelegate*)[[UIApplication sharedApplication] delegate]).isFromJobDetails = FALSE;
        }
            break;
            
        default:
            break;
    }
}

-(void) loadThumbnailPhoto
{
    
//    self.photoLibrary = [[APPhotolibrary alloc] init];
//    
//    [self.photoLibrary createPhotoLibrary];
//    
//    [self.photoLibrary loadPhotosAsynchronously:^(NSArray *asset, NSError *error)
//     {
//         if (!error && [asset count])
//         {
//             
//             self.thumbnailImageView.image = ((ALAsset*)[asset objectAtIndex:0 ]).photoThumbnail;
//             
//         }
//         else
//         {
//             NSLog(@"Error loading photos");
//         }
//     }];
}

-(void)startAnimating{
    
    [self.loadView setHidden:NO];
    [self bringSubviewToFront:self.loadView];
}

-(void)stopAnimating{
    [self.loadView setHidden:YES];
}

//to avoid any button clicks till the photo is taken
-(void) activateButtons
{
    self.backButton.enabled = TRUE;
    self.homeButton.enabled = TRUE;
}

-(void) deactivateButtons
{
    self.backButton.enabled = FALSE;
    self.homeButton.enabled = FALSE;
}
@end
