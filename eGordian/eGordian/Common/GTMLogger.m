//
//  GTMLogger.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 10/26/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "GTMLogger.h"
#import "TAGDataLayer.h"
#import "TAGManager.h"
#import "AppDelegate.h"
@implementation GTMLogger
+ (void)logEventToGTM:(NSString *)eventName andinputParams:(NSDictionary *)params{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    TAGDataLayer *dataLayer=   delegate.tagManager.dataLayer;
    if (params && params.count >0) {
        NSLog(@"Calling GTM %@",eventName);
        [dataLayer push:params];
    }
    return;
    // NOTE: A GTM container should already have been opened, otherwise events
    //     pushed to the DataLayer will not fire tags in that container.
    
    //TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    
    
//    NSDictionary *pushDict  = @{@"event":@"openScreen",// openScreen// Event, name of
//                                @"navBarLinkTitle":@"query",
//                                @"screenName":@"new event test"
//                                };
    [dataLayer pushValue:@"takePhoto" forKey:@"event"];
    
//    [dataLayer push:@{
//                      @"event" : @"navLink",
//                      @"navLink": @"mynewNavLink"
//                      }];
//    [dataLayer push:pushDict];
    return;
   

//    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
//    [delegate.tagManager.dataLayer push:@{@"event":@"navBarLink",
//                                            @"screenName":@"Home Screen"}];

}

+ (void)pushOpenScreenEventWithScreenName:(NSString *)screenName{
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.tagManager.dataLayer push:@{@"event": @"openScreen",
                                          @"screenName": screenName}];
}
@end
