//
//  eGordianUtilities.h
//  eGordian-iOS
//
//  Created by Laila on 14/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALAsset+Photo.h"

@interface EGordianUtilities : NSObject
extern NSString *const FONT_REGULAR;
extern NSString *const FONT_REGULAR;
extern NSString *const FONT_LIGHT;
extern NSString *const FONT_LIGHT_ITALIC;
extern NSString *const FONT_ITALIC;
extern NSString *const FONT_BOLD;
extern NSString *const FONT_BOLD_ITALIC;
extern NSString *const FONT_EXTRA_BOLD;
extern NSString *const FONT_EXTRA_BOLD_ITALIC;
extern NSString *const FONT_SEMIBOLD;
extern NSString *const FONT_SEMIBOLD_ITALIC;
extern NSString *const FONT_IPAD;

extern NSString *const FONT_STRATUM;
extern NSString *const FONT_STRATUM_REGULAR;
extern NSString *const FONT_STRATUM_BOLD;

//notes

extern NSString *const NOTES_DESCRIPTION;
extern NSString *const NOTES_COUNT;
extern NSString *const NOTES_DATA;
extern NSString *const NOTES_COLLECTION;


//+(void) attachPhoto:(NSArray*)imageArray withOwnerID:(NSString*)ownerID toJobWithID:(NSString*)job withDelegate:(id)delegate;

+(UIColor*) headerColor;
+(UIColor*) labelTitleColor;
+(void) writeJobDetails:(NSString*)jobOrderID ownerID:(NSString*)ownerID;
+(NSString*) getJobOrderID;
+(NSString*) getOwnerID;
+(void) updateRefreshStatus:(BOOL)status forPage:(NSString*)pageKey;
+(BOOL) getRefreshStatusForPage:(NSString*)pageKey;
+(CGFloat)heightForLabel:(UILabel *)label WithText:(NSString *)text;
//+(UIAlertView*) showNetworkNotAvailableAlert;
@end
