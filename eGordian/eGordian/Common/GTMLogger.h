//
//  GTMLogger.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 10/26/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTMLogger : NSObject
+ (void)logEventToGTM:(NSString *)eventName andinputParams:(NSDictionary *)params;
/**
 * Push an "openScreen" event with the given screen name. Tags that
 * match that event will fire.
 */

+ (void)pushOpenScreenEventWithScreenName:(NSString *)screenName;
@end
