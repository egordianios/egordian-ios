//
//  OverlayView.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/10/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APPhotolibrary.h"

@interface OverlayView : UIView<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

//@property (strong,nonatomic) UIImageView* thumbnailImageView;
@property (nonatomic, strong) UIImagePickerController* egImagePickerController;
@property (nonatomic, strong) UIImageView* photoImageView;
@property (nonatomic, strong) APPhotolibrary* photoLibrary;
@property (nonatomic, strong) UIView* loadView;
//@property (nonatomic, strong) UIView* fullLoadView;
@property (nonatomic, strong) UIButton* cameraButton;
@property (nonatomic, strong) UIButton *homeButton;
@property (nonatomic, strong) UIButton *backButton;
@end
