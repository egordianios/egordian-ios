//
//  SyncButton.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 13/01/16.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SyncButton : UIButton
@property (nonatomic, readonly) BOOL isAnimating;
- (void)startAnimating;
- (void)stopAnimating;
@end
