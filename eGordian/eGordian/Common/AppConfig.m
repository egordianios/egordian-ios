//
//  AppConfig.m
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "AppConfig.h"
NSString *const APP_NAME        = @"eGordian";

// Dev Login
// NSString *const APP_BASE_URL    = @"https://headerdev.egordian.com/Services/HeaderSVC.svc/";

// QA Login
//NSString *const APP_BASE_URL    = @"https://headertest.egordian.com/Services/HeaderSVC.svc/";

// Stage Login
// NSString *const APP_BASE_URL    = @"https://headerstage.egordian.com/Services/HeaderSVC.svc/";

// Production Login
 NSString *const APP_BASE_URL    = @"https://header.egordian.com/Services/HeaderSVC.svc/";


// Header Dev
//NSString *const APP_HEADER_URL  = @"https://jocservicedev.egordian.com/v1/";

// Header QA
//NSString *const APP_HEADER_URL  = @"https://jocservicetest.egordian.com/v1/";

// Header Stage
//NSString *const APP_HEADER_URL  = @"https://jocservicestage.egordian.com/v1/";

// Header Production
NSString *const APP_HEADER_URL  = @"https://jocservice.egordian.com/v1/";


// Authorization Header Dev
//NSString *const APP_AUTHENTICATION_URL = @"https://jocservicedev.egordian.com/api/";

// Authorization Header Qa
//NSString *const APP_AUTHENTICATION_URL = @"https://jocservicetest.egordian.com/api/";

// Authorizaton Header Stage
//NSString *const APP_AUTHENTICATION_URL = @"https://jocservicestage.egordian.com/api/";

// Authorization Production
NSString *const APP_AUTHENTICATION_URL = @"https://jocservice.egordian.com/api/";

NSString *const APP_TEST_URL    = @"http://192.168.33.128/v1/";

NSString *const APP_USERID      = @"APP_USERID";

NSString *const APP_OWNERID     = @"APP_OWNERID";

NSString *const APP_DATABASE    = @"DATABSENAME";

//NSString *const APP_APIKEY      = @"4ScInDjUzHKtf30pIuO8NMZOJmaI3z90LrtGgCZBzbA="; //authorization change

NSString *const APP_APIKEY      = @"A93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=";

NSString *const APP_PRODUCTKEY  = @"A93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=";

NSString *const APP_XVERSION    = @"1";

NSString *const APP_LOGINCOOKIE_PREFERENCE = @"loggedIN_Cookie";

NSString *const APP_AUTHORIZATION_PREFERENCE = @"authorizationToken";

NSString *const APP_TGG_REFERER         = @"egordian.com";

NSString *const eGordianAlbum           = @"eGordian";

NSString *const ALLOW_OFFLINEACCESS     = @"allowOfflineAccess";

NSString *const APP_VERSION_NUMBER      = @"Version Number 0.30.7e910ea";

NSString *const APP_GTM_CONTAINER_ID =@"GTM-TDSLTP";   //Mobile Coe Account GTM-NPPCXB ---  TGG Account GTM-TDSLTP

NSString *const APP_GOOGLE_MAPS_API_KEY = @"AIzaSyC8HXpo0brFf3MAymDxS2i0JoDgTLCQxmo"; // Client Key

//NSString *const APP_GOOGLE_MAPS_API_KEY = @"AIzaSyCc2s4jGCREw49OhypRGHhKWnO6oADDc4M"; //My Key


NSString *const APP_GOOGLE_MAPS_SERVER_KEY = @"AIzaSyCVUlb4tYPNt20IlLs4dZc7QeaNJRPjzS8";

NSString *const API_DATETIME_FORMAT=@"MM/dd/yyyy hh:mm:ss a";//11/6/2015 1:44:46 PM

NSString *const API_DATETIME_FORMAT_UTC=@"MM/dd/yyyy hh:mm:ss a";

NSString *const API_DATE_FORMAT=@"yyyy-MM-dd";//2014-10-28

NSString *const API_TIME_FORMAT=@"HH:mm";//@"HH:mm a"

NSString *APP_DATETIME_FORMAT = @"MMM dd, yyyy hh:mma";

NSString *APP_DATE_FORMAT = @"M/d/yy";

NSString *APP_TIME_FORMAT = @"h:mm a";

NSString *UNSYNCHEDPHOTOS = @"UnSynchedPhotos";

NSString *SYNCHEDPHOTOS = @"SynchedPhotos";

NSString *const APP_JOBDETAIL_REFRESH_FLAG  = @"jobDetailRefreshFlag";

NSString *const APP_JOBLIST_REFRESH_FLAG    = @"jobListRefreshFlag";

NSString *const APP_JOBDOCUMENTS_REFRESH_FLAG = @"jobDocumentRefreshFlag";

NSString *const APP_JOBPHOTO_REFRESH_FLAG    = @"jobPhotoRefreshFlag";

NSString *const APP_USERPHOTO_REFRESH_FLAG    = @"userPhotoRefreshFlag";

NSString *const APP_JOBORDER_ID = @"JobOrderID";

NSString *const APP_OWNER_ID = @"OwnerID";

@implementation AppConfig

+(void)enableAppOffline:(BOOL)isOffline{
    if (isOffline) {
        [[NSUserDefaults standardUserDefaults] setBool:isOffline forKey:ALLOW_OFFLINEACCESS];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ALLOW_OFFLINEACCESS];
    }
    
}
+(BOOL)isOfflineAccessAvailable{
    BOOL isAppOnline=NO;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ALLOW_OFFLINEACCESS]) {
        isAppOnline=YES;
    }else{
        
        isAppOnline=NO;
    }
    
    return isAppOnline;
}
+ (NSString*)appName
{
    NSString *appName = [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"];
    
    if(!appName)
        appName = [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleName"];
    
    if(!appName)
        appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    
    if(!appName)
        appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    
    if(!appName)
        appName = [[NSProcessInfo processInfo] processName];
    
    return appName;
}
+(NSString *)getUserId{
    
    NSString *userID = [[NSUserDefaults standardUserDefaults]objectForKey:APP_USERID];
    
    return userID;
}

+(NSString *)getOwnerId{
    
    NSString *ownerID = [[NSUserDefaults standardUserDefaults]objectForKey:APP_OWNERID];
    
    return ownerID;
}


+(NSString *)getAuthorizationToken{
    
    NSString *authorizationToken = [[NSUserDefaults standardUserDefaults]objectForKey:APP_AUTHORIZATION_PREFERENCE];
    
    return authorizationToken;
}
+(NSString *)getDataBase{
    NSString *databaseName = [[NSUserDefaults standardUserDefaults]objectForKey:APP_DATABASE];
    return databaseName;
}

- (id)init
{
    //Do not allow the create objects, Acts like static class
    self=nil;
    //    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

+(NSString *) urlencode: (NSString *) url
{
    NSArray *escapeChars = [NSArray arrayWithObjects:@";" , @"/" , @"?" , @":" ,
                            @"@" , @"&" , @"=" , @"+" ,
                            @"$" , @"," , @"[" , @"]",
                            @"#", @"!", @"'", @"(",
                            @")", @"*", @" ", @"\"",nil];
    
    NSArray *replaceChars = [NSArray arrayWithObjects:@"%3B" , @"%2F" , @"%3F" ,
                             @"%3A" , @"%40" , @"%26" ,
                             @"%3D" , @"%2B" , @"%24" ,
                             @"%2C" , @"%5B" , @"%5D",
                             @"%23", @"%21", @"%27",
                             @"%28", @"%29", @"%2A", @"%20",@"%22",nil];
    
    NSUInteger len = [escapeChars count];
    
    NSMutableString *temp = [url mutableCopy];
    
    int i;
    for(i = 0; i < len; i++)
    {
        
        [temp replaceOccurrencesOfString: [escapeChars objectAtIndex:i]
                              withString:[replaceChars objectAtIndex:i]
                                 options:NSLiteralSearch
                                   range:NSMakeRange(0, [temp length])];
    }
    return temp;
}

+(void)createFolder:(NSString*)dirName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Fetch path for document directory
    NSMutableString* dataPath = (NSMutableString *)[documentsDirectory stringByAppendingPathComponent:dirName];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]) {
        NSLog(@"Couldn't create directory error: %@", error);
    }
    else {
        NSLog(@"directory created!");
    }
    
    NSLog(@"dataPath : %@ ",dataPath); // Path of folder created
}

+(NSString *)dateStringFromString:(NSString *)sourceString
                destinationFormat:(NSString *)destinationFormat{
    if(!sourceString)
        return @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSDate *date = [dateFormatter dateFromString:sourceString];
    [dateFormatter setDateFormat:destinationFormat];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)dateStringFromString:(NSString *)sourceString
                      sourceFormat:(NSString *)sourceFormat
                 destinationFormat:(NSString *)destinationFormat
{
    if(!sourceString)
        return @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:sourceFormat];
    NSDate *date = [dateFormatter dateFromString:sourceString];
    [dateFormatter setDateFormat:destinationFormat];
    return [dateFormatter stringFromDate:date];
}

+(NSDate*) dateFromString:(NSString*)dateString withFormat:(NSString*)format
{
    if(!dateString)
        return nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    return dateFromString;
}

+(NSString*) stringFromDate:(NSDate*)dateToConvert withFormat:(NSString*)format
{
    if(!dateToConvert)
        return @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *stringDate = [dateFormatter stringFromDate:dateToConvert];
    return stringDate;
}


@end
