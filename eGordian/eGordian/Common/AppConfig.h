//
//  AppConfig.h
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_IPHONE_4 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)

#define OVERLAY_HOMEBUTTON 1101
#define OVERLAY_THUMBNAIL 1101

#pragma mark - API Config
extern NSString *const APP_NAME;
extern NSString *const APP_BASE_URL;
extern NSString *const APP_HEADER_URL;
extern NSString *const APP_AUTHENTICATION_URL;
extern NSString *const APP_TEST_URL;
extern NSString *const APP_USERID;
extern NSString *const APP_OWNERID;
extern NSString *const APP_DATABASE;
extern NSString *const APP_APIKEY;
extern NSString *const APP_PRODUCTKEY;
extern NSString *const APP_AUTHORIZATION_PREFERENCE;
extern NSString *const APP_LOGINCOOKIE_PREFERENCE;
extern NSString *const APP_XVERSION;
extern NSString *const APP_TGG_REFERER;
extern NSString *const eGordianAlbum;
extern NSString *const ALLOW_OFFLINEACCESS;
extern NSString *const APP_VERSION_NUMBER;
extern NSString *const APP_GTM_CONTAINER_ID;
extern NSString *const APP_GOOGLE_MAPS_API_KEY;
extern NSString *const APP_GOOGLE_MAPS_SERVER_KEY;
extern NSString *const API_DATETIME_FORMAT;
extern NSString *const API_DATETIME_FORMAT_UTC;
extern NSString *const API_DATE_FORMAT;
extern NSString *const API_TIME_FORMAT;

extern NSString *APP_DATETIME_FORMAT;
extern NSString *APP_DATE_FORMAT;
extern NSString *APP_TIME_FORMAT;

extern NSString *UNSYNCHEDPHOTOS;
extern NSString *SYNCHEDPHOTOS;

extern NSString *const APP_JOBDETAIL_REFRESH_FLAG;
extern NSString *const APP_JOBLIST_REFRESH_FLAG;
extern NSString *const APP_JOBDOCUMENTS_REFRESH_FLAG;
extern NSString *const APP_JOBORDER_ID;
extern NSString *const APP_OWNER_ID;
extern NSString *const APP_JOBPHOTO_REFRESH_FLAG;
extern NSString *const APP_USERPHOTO_REFRESH_FLAG;

@interface AppConfig : NSObject

+(NSString *)getUserId;
+(NSString *)getOwnerId;
+(NSString *)getAuthorizationToken;
+(NSString *)getDataBase;
+(NSString *) urlencode: (NSString *) url;
+(void)createFolder:(NSString*)dirName;
+(NSString *)dateStringFromString:(NSString *)sourceString
                     sourceFormat:(NSString *)sourceFormat
                destinationFormat:(NSString *)destinationFormat;
+(NSString *)dateStringFromString:(NSString *)sourceString
                destinationFormat:(NSString *)destinationFormat;
+(NSDate*) dateFromString:(NSString*)dateString withFormat:(NSString*)format;
+(NSString*) stringFromDate:(NSDate*)dateToConvert withFormat:(NSString*)format;
+(void)enableAppOffline:(BOOL)allowOl;
+(BOOL)isOfflineAccessAvailable;
+ (NSString*)appName;
@end
