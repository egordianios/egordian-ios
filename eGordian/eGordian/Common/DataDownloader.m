//
//  DataDownloader.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/5/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "DataDownloader.h"
#import "APIRequest.h"
#import "eG_Notes.h"
#import "EG_Photo.h"
#import "Eg_JobPhoto.h"
#import "EG_PhotoDetail.h"
#import "EG_OwnerContact.h"
#import "EG_ContactDetail.h"
#import "JobOrderDAO.h"
#import "eGordianJobPhotoViewController.h"
#import "NSDictionary+Json.h"

@implementation DataDownloader

@synthesize dataDownloaderDelegate=_dataDownloaderDelegate;
static DataDownloader *_sharedInstance;
static dispatch_once_t oncePredicate;

+ (DataDownloader  *)sharedDownloader
{
    if(!_sharedInstance)
    {
        dispatch_once(&oncePredicate, ^
                      {
                          _sharedInstance = [[super allocWithZone:nil] init];
                      });
    }
    return _sharedInstance;
}

+(void) releaseInstance
{
    _sharedInstance = nil;
    oncePredicate = 0;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedDownloader];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
    return self;
}

- (unsigned)retainCount
{
    return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}
#endif
#pragma mark -
#pragma mark Custom Methods

- (id)init
{
    @synchronized(self)
    {
        if ((self = [super init]))
        {
        }
        return self;
    }
}

- (void)loginWithUserID:(NSString *)userID password:(NSString*)pwd{
    
    APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_LOGIN];
    request.delegate = (id<APIRequestDelegate>)self;
    
    NSDictionary *parametersDictionary = @{@"username":userID,@"password":pwd};
    
    [request invokeWithParameter:parametersDictionary];
    
}
- (void)eGordianAuthorization{
    
    APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_AUTHORIZATION];
    
    request.delegate = (id<APIRequestDelegate>)self;
    
//    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:[AppConfig getUserId],@"JocUserId",
//                                                                            APP_APIKEY,@"ApiKey",
//                                                                            nil]; // New authorization change
    
    NSDictionary *dictParams=[[NSDictionary alloc] initWithObjectsAndKeys:[AppConfig getUserId],@"UserId",
                                                                            APP_APIKEY,@"ApiKey", APP_PRODUCTKEY,@"ProductKey", nil]; // Old One
    
    [request invokeWithParameter:nil andAditionalHeaders:dictParams];
    
}
- (void)getResponseFormUrl:(NSString *)serviceUrl serviceType:(APIKey)apiKey{
    
    
    APIRequest *request=[APIRequest apiRequestWithKey:apiKey];
    
    request.delegate = (id<APIRequestDelegate>)self;
    
    NSDictionary *headerDictionary = @{@"Authorization":[AppConfig getAuthorizationToken],
                                       @"X-Version":APP_XVERSION,
                                       @"X-Database":[AppConfig getDataBase],
                                       @"Referer":APP_TGG_REFERER
                                       };
    
    [request invokeWithURL:serviceUrl andAditionalHeaders:headerDictionary];

}

- (void)updateData:(id)updateData andServiceURl:(NSString *)serviceUrl andserviceType:(APIKey)apiKey{
    
    APIRequest *request=[APIRequest apiRequestWithKey:apiKey];
    
    request.delegate = (id<APIRequestDelegate>)self;
    
    NSDictionary *headerDictionary = @{@"Authorization":[AppConfig getAuthorizationToken],
                                       @"X-Version":APP_XVERSION,
                                       @"X-Database":[AppConfig getDataBase],
                                       @"Referer":APP_TGG_REFERER
                                       };
    
    [request invokeWithURL:serviceUrl withData:updateData andAditionalHeaders:headerDictionary];
}



- (void)deleteData:(id)deleteDict andServiceURl:(NSString *)serviceUrl andserviceType:(APIKey)apiKey{
    
    APIRequest *request=[APIRequest apiRequestWithKey:apiKey];
    
    request.delegate = (id<APIRequestDelegate>)self;
    
    NSDictionary *headerDictionary = @{@"Authorization":[AppConfig getAuthorizationToken],
                                       @"X-Version":APP_XVERSION,
                                       @"X-Database":[AppConfig getDataBase],
                                       @"Referer":APP_TGG_REFERER
                                       };
    
    [request invokeWithURL:serviceUrl withData:deleteDict andAditionalHeaders:headerDictionary];
}

#pragma mark - ASIHTTPRequestDelegate

- (void)requestFinished:(APIRequest *)request
{
    NSInteger statustCode = request.operation.response.statusCode;
    
    NSURL *URL = request.operation.request.URL;
    
    NSDictionary *header = [request.operation.request allHTTPHeaderFields];
    
    NSString *body = [[NSString alloc] initWithData:[request.operation.request HTTPBody] encoding:NSUTF8StringEncoding];
    
    NSLog(@"\nREQUEST\n  URL: %@\n  HEADERS: %@\n STATUS CODE: %ld\n BODY: %@",URL,header,(long)statustCode,body);
    
    URL = request.operation.response.URL;
    
    header = [request.operation.response allHeaderFields];
    
    switch (request.apiKey) {
            
        case EGORDIAN_API_LOGIN:{
            
            NSLog(@"Login Success:\n%@",request.responseObject);
            
            NSMutableDictionary *userDataDict = (NSMutableDictionary *)request.responseObject;
            
            NSLog(@"User ID---%@\nOwner ID---%@",[userDataDict objectForKey:@"userid"],[userDataDict objectForKey:@"ownerid"]);
            
            if([userDataDict objectForKey:@"ValidCredentials"]){
                
                [[NSUserDefaults standardUserDefaults] setObject:[userDataDict objectForKey:@"userid"] forKey:APP_USERID];
                
                [[NSUserDefaults standardUserDefaults] setObject:[userDataDict objectForKey:@"ownerid"] forKey:APP_OWNERID];
                
                [[NSUserDefaults standardUserDefaults] setObject:[userDataDict objectForKey:@"dbname"] forKey:APP_DATABASE];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
            
            /*
             NSArray* cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[request.operation.response allHeaderFields] forURL:request.operation.request.URL];
             
             NSMutableDictionary* cookieDictionary = [[NSMutableDictionary alloc]init];
             
             for (NSHTTPCookie* cookie in cookies)
             {
             [cookieDictionary setValue:cookie.value forKey:cookie.name];
             }
             
             [[NSUserDefaults standardUserDefaults] setObject:cookieDictionary forKey:APP_LOGINCOOKIE_PREFERENCE];
             
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             */
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_LOGIN];
            }
            break;
        }
        case EGORDIAN_API_AUTHORIZATION:{
            
            NSLog(@"Authorization Success\n: %@", request.responseObject);
            
            [[NSUserDefaults standardUserDefaults]setObject:request.responseObject forKey:APP_AUTHORIZATION_PREFERENCE];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_AUTHORIZATION];
            }
            break;
        }
        case EGORDIAN_API_OPENJOBS:{
            
            NSLog(@"Open Job Count Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderDict = (NSMutableDictionary *)request.responseObject;
            
            
            
            
            
            NSString *openJobcount = [[jobOrderDict objectForKey:@"Count"] stringValue];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:[NSArray arrayWithObject:openJobcount] serviceType:EGORDIAN_API_OPENJOBS];
            }
            break;
        }
        case EGORDIAN_API_JOBSOFFLINE:{
            NSLog(@"Job Order List For Offline:\n%@",request.responseObject);
            
            dispatch_queue_t backgroundQueue = dispatch_queue_create("com.eGordian.OfflineSync", 0);
            dispatch_async(backgroundQueue, ^{
                NSMutableDictionary *jobOrderDict = (NSMutableDictionary *)request.responseObject;
                NSArray *jobOrderArray = [jobOrderDict objectForKey:@"Data"];
                
                if([jobOrderDict objectForKey:@"Data"] != [NSNull null] && [jobOrderArray count]>0){
                    NSManagedObjectContext *ctx = [CoreDataBase getTemporaryContext];
                    for (int i=0; i<jobOrderArray.count; i++) {
                        [JobOrderDAO insertIfDoesNotExists:[jobOrderArray objectAtIndex:i] inContext:ctx];
                        
                        
                    }
                    [CoreDataBase saveTemporaryContextAndRelease:ctx];
                    
                }
                
                
            });
            
            // [CoreDataBase saveTemporaryContextAndRelease:ctx];
            break;
        }
        case EGORDIAN_API_JOBORDERS_LIST:
        {
            
            NSLog(@"Job Order List Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *jobOrderArray = [jobOrderDict objectForKey:@"Data"];
            
            NSMutableArray *jobOrdersTempArray = [NSMutableArray array];
            
            
            
            if([jobOrderDict objectForKey:@"Data"] != [NSNull null] && [jobOrderArray count]>0){
                
                for (int i=0; i<jobOrderArray.count; i++) {
                    EG_JobOrder *currJobOrder = [EG_JobOrder getJobOrdersJsonDictionary:[jobOrderArray objectAtIndex:i]];
                    
                    [jobOrdersTempArray addObject:currJobOrder];
                    
                    {//this is for getting ownerID
                        NSString* jobOrderLink = currJobOrder.jobOrderLink;
                        if([jobOrderLink rangeOfString:@"Owners"].location != NSNotFound){
                            NSRange end = [jobOrderLink rangeOfString:@"Owners/"];
                            NSString *ownerID = [jobOrderLink substringFromIndex:end.location+end.length];
                            end = [ownerID rangeOfString:@"/"];
                            ownerID =[ownerID substringToIndex:end.location];
                            currJobOrder.ownerID =ownerID;
                            
                        }
                        
                    }
                }
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:jobOrdersTempArray serviceType:request.apiKey];
            }
            
            break;
        }
        case EGORDIAN_API_JOBORDER_DETAIL:{
            
            NSLog(@"Job Order Deatil Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderDict = (NSMutableDictionary *)request.responseObject;
            
            EG_JobOrderDetail *jobDetailsDict = [EG_JobOrderDetail getJobOrdersJsonDictionary:[jobOrderDict objectForKey:@"Data"]];
            
            {//this is for getting ownerID
                NSString* contactsLink = jobDetailsDict.contactsLink;
                if([contactsLink rangeOfString:@"Owners"].location != NSNotFound){
                    NSRange end = [contactsLink rangeOfString:@"Owners/"];
                    NSString *ownerID = [contactsLink substringFromIndex:end.location+end.length];
                    end = [ownerID rangeOfString:@"/"];
                    ownerID =[ownerID substringToIndex:end.location];
                    jobDetailsDict.ownerID =ownerID;
                    
                }
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinishedWithDict:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinishedWithDict:(NSMutableDictionary*)jobDetailsDict serviceType:EGORDIAN_API_JOBORDER_DETAIL];
            }
            break;
        }
        case EGORDIAN_API_JOBORDER_STATUS:{
            
            NSLog(@"Job Order Status Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderStatusDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *jobOrderStatusesArray = [jobOrderStatusDict objectForKey:@"Data"];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:jobOrderStatusesArray serviceType:EGORDIAN_API_JOBORDER_STATUS];
            }
            
            break;
        }
        case EGORDIAN_API_JOBSTATUS_UPDATE:{
            
            NSLog(@"JobStatus Update--%@",request.responseObject);
            
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_JOBSTATUS_UPDATE];
            }
            
             break;
        
        }
           
        case EGORDIAN_API_TRACKING_DATES:{
            
            NSLog(@"Tracking Dates Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderTrackingDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *transactionsArray = [jobOrderTrackingDict objectForKey:@"Data"];
            
            NSMutableArray *transactionsTempArray = [NSMutableArray array];
            
            if([jobOrderTrackingDict objectForKey:@"Data"] != [NSNull null] && [transactionsArray count]>0){
                
                for (int i=0; i<transactionsArray.count; i++) {
                    
                    EG_TransactionDate *currTranscationDate = [EG_TransactionDate getTranscationsJsonDictionary:[transactionsArray objectAtIndex:i]];
                    
                    [transactionsTempArray addObject:currTranscationDate];
                }
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:transactionsTempArray serviceType:EGORDIAN_API_TRACKING_DATES];
            }
            break;
        }
        case EGORDIAN_API_JOBORDER_CONTACTS:{
            
            NSLog(@"Job Order Contacts List Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderConatcDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *contactArray = [jobOrderConatcDict objectForKey:@"Data"];
            
            NSMutableArray *contactsTempArray = [NSMutableArray array];
            
            if([jobOrderConatcDict objectForKey:@"Data"] != [NSNull null] && [contactArray count]>0){
                for (int i=0; i<contactArray.count; i++) {
                    
                    EG_JobConatcts *currContact = [EG_JobConatcts getJobContactJsonDictionary:[contactArray objectAtIndex:i]];
                    
                    [contactsTempArray addObject:currContact];
                    
                }
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:contactsTempArray serviceType:EGORDIAN_API_JOBORDER_CONTACTS];
            }
            
            break;
        }
        case EGORDIAN_API_JOBORDER_DOCUMENTS:{
            
            NSLog(@"Job Order Documents List Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderDocumentDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *documentsArray = [jobOrderDocumentDict objectForKey:@"Data"];
            
            NSMutableArray *documentsTempArray = [NSMutableArray array];
            
            if([jobOrderDocumentDict objectForKey:@"Data"] != [NSNull null] && [documentsArray count]>0){
                
                for (int i=0; i<documentsArray.count; i++) {
                    
                    EG_JobDocument   *currentDocument = [EG_JobDocument getJobDocumentsJsonDictionary:[documentsArray objectAtIndex:i]];
                    
                    [documentsTempArray addObject:currentDocument];
                }
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:documentsTempArray serviceType:EGORDIAN_API_JOBORDER_DOCUMENTS];
            }
            
            break;
        }
        case EGORDIAN_API_JOBORDER_DETAILEDSCOPE:{
            
            NSLog(@"Job Order Detailed Scope Success:\n%@",request.responseObject);
            
            NSMutableDictionary *jobOrderDetailScopeDict = (NSMutableDictionary *)request.responseObject;
            
            NSMutableArray *arrayDetailScopeDict = [[NSMutableArray alloc]initWithObjects:jobOrderDetailScopeDict, nil];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:arrayDetailScopeDict serviceType:EGORDIAN_API_JOBORDER_DETAILEDSCOPE];
            }
            
            break;
        }
        case EGORDIAN_API_GLOBAL_CONTATCS:{
            
            NSLog(@"Global Contacts List:\n%@",request.responseObject);
            
            NSMutableDictionary *contactsDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *contactArray = [contactsDict objectForKey:@"Data"];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:contactArray serviceType:EGORDIAN_API_GLOBAL_CONTATCS];
            }
            
            break;
        }
        case EGORDIAN_API_OWNER_CONTACTS:{
            
            NSLog(@"Owner Contacts List:\n%@",request.responseObject);
            
            NSMutableDictionary *ownerConatcDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *ownerContactsArray = [ownerConatcDict objectForKey:@"Data"];
            
            NSMutableArray *contactTempArray = [NSMutableArray array];
            
            if([ownerConatcDict objectForKey:@"Data"] != [NSNull null] && [ownerContactsArray count]>0){
                
                for (int i=0; i<ownerContactsArray.count; i++) {
                    
                    EG_OwnerContact *currContact = [EG_OwnerContact getOwnerContactJsonDictionary:[ownerContactsArray objectAtIndex:i]];
                    
                    [contactTempArray addObject:currContact];
                }
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:contactTempArray serviceType:EGORDIAN_API_OWNER_CONTACTS];
            }
            
            break;
        }
            
        case EGORDIAN_API_CONTACTS_DETAIL:{
            
            NSLog(@"Contact Detail success:\n%@",request.responseObject);
            
            NSMutableDictionary *responseDict = (NSMutableDictionary *)request.responseObject;
            
            EG_ContactDetail *contactDict = [EG_ContactDetail getContactDetailJsonDictionary:[responseDict objectForKey:@"Data"]];
            
            /*
            NSMutableDictionary *responseDict = (NSMutableDictionary *)request.responseObject;
            
            NSMutableDictionary *contactDict = [responseDict objectForKey:@"Data"];
            
            NSArray* fieldsArr = [[NSArray alloc]initWithArray:[contactDict objectForKey:@"Fields"]];
            
            eGordianContact *eGContact = [[eGordianContact alloc] initWithArray:fieldsArr];
            
            eGContact.ID = [contactDict getStringForKey:@"Id"];
            
            if([contactDict hasValidJSONKey:@"Company"])//[contactDict objectForKey:@"Company"]valueForKey:@"CompanyName"]
            {
                eGContact.CompanyName = [[contactDict objectForKey:@"Company"] valueForKey:@"CompanyName"];
            }
            */
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:[NSArray arrayWithObject:contactDict] serviceType:EGORDIAN_API_CONTACTS_DETAIL];
            }
        }
            break;
            
        case EGORDIAN_API_OWNER_ROLES:{
            
            NSLog(@"Owner Role List:\n%@",request.responseObject);
            
            NSMutableDictionary *responseDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *rolesArray = [responseDict objectForKey:@"Data"];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:rolesArray serviceType:EGORDIAN_API_OWNER_ROLES];
            }
        }
            break;
            
        case EGORDIAN_API_ASSIGNROLE:{
            
            NSLog(@"Assign Role Success:\n%@",request.responseObject);
            
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSMutableDictionary *responseDict = (NSMutableDictionary *)request.responseObject;
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_ASSIGNROLE];
            }
            
            
        }
            break;
            
        case EGORDIAN_API_CONTACTS_SEARCH:{
            
            NSLog(@"Global Contacts List:\n%@",request.responseObject);
            
            NSMutableDictionary *contactsDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *contactArray = [contactsDict objectForKey:@"Data"];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:contactArray serviceType:EGORDIAN_API_CONTACTS_SEARCH];
            }
            
        }
            break;
        case EGORDIAN_API_GET_COORDINATES_FROMADDRESS:{
            
            NSArray *coordinatesArray = [request.responseObject objectForKey:@"results"];
            
            NSLog(@"coordinatesArray---:\n%@",coordinatesArray);
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:coordinatesArray serviceType:EGORDIAN_API_GET_COORDINATES_FROMADDRESS];
            }
        }
            break;
            
        case EGORDIAN_API_NOTES:
        {
            
            NSLog(@"Job Notes List Success:\n:\n%@",request.responseObject);
            
            NSMutableDictionary *notesDict = (NSMutableDictionary *)request.responseObject;
            
            NSArray *notesArray = [notesDict objectForKey:@"Data"];
            
            NSMutableArray *notesTempArray = [NSMutableArray array];
            
            if([notesDict objectForKey:@"Data"] != [NSNull null] && [notesArray count]>0){
                
                for (int i=0; i<notesArray.count; i++) {
                    
                    eG_Notes   *currentNote = [eG_Notes getJobNotesJsonDictionary:[notesArray objectAtIndex:i]];
                    
                    [notesTempArray addObject:currentNote];
                }
            }

            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)]){
                [_dataDownloaderDelegate serviceRequestFinished:notesTempArray serviceType:EGORDIAN_API_NOTES];
            }
        }
            break;
            
        case EGORDIAN_API_JOBNOTES_UPDATE:{
            
            NSLog(@"Note Update--%@",request.responseObject);
            
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_JOBNOTES_UPDATE];
        }
            break;
        case EGORDIAN_API_JOBNOTES_ADD:{
            
            NSLog(@"Note Update--%@",request.responseObject);
            
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_JOBNOTES_ADD];
        }
            break;
        case EGORDIAN_API_SCOPEOFWORK_UPDATE:{
            
            NSLog(@"Scope Update--%@",request.responseObject);
        
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)] && statustCode==200)
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_SCOPEOFWORK_UPDATE];
            
            break;
        }
            
        case EGORDIAN_API_TRACKING_DATES_UPDATE:
        {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBLIST_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)] && statustCode==200)
            {
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_TRACKING_DATES_UPDATE];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            }
            //            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFailed:serviceType:)] && statustCode!=200)
            //                [_dataDownloaderDelegate serviceRequestFailed:nil serviceType:EGORDIAN_API_TRACKING_DATES_UPDATE];
        }
            break;
            
        case EGORDIAN_API_JOBORDER_CONTACT_UPDATE:{
            
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:APP_JOBDETAIL_REFRESH_FLAG];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)] && statustCode==200)
            {
                [_dataDownloaderDelegate serviceRequestFinished:nil serviceType:EGORDIAN_API_JOBORDER_CONTACT_UPDATE];
            }
            
        }
            break;
        case EGORDIAN_API_DASHBOARD_COUNT:
        {
            
            NSLog(@"Dashboard Count :\n%@",request.responseObject);
            
            NSMutableDictionary *dashboardCountDict = (NSMutableDictionary *)request.responseObject;
            
            NSMutableArray *arrayCountDict = [[NSMutableArray alloc]initWithObjects:dashboardCountDict, nil];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
                [_dataDownloaderDelegate serviceRequestFinished:arrayCountDict serviceType:EGORDIAN_API_DASHBOARD_COUNT];
        }
            break;
            
        case EGORDIAN_API_PHOTO_THUMBNAIL:
        {
//            NSLog(@"Picture response :\n%@",request.responseObject);
            NSLog(@"Picture response :Successful");
            
            NSMutableDictionary *pictureDict = (NSMutableDictionary *)request.responseObject;
            
            int photoCount = [[pictureDict objectForKey:@"Count"] intValue];
            
            NSArray *photoArray = [pictureDict objectForKey:@"Data"];
            
            NSMutableArray* eGPhotoArray = [[NSMutableArray alloc] init];
            for (int i=0; i<photoCount; i++) {
                EG_Photo* photo = [[EG_Photo alloc] initWithPhotoDict:[photoArray objectAtIndex:i]];
                [eGPhotoArray addObject:photo];
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
                [_dataDownloaderDelegate serviceRequestFinished:eGPhotoArray  serviceType:EGORDIAN_API_PHOTO_THUMBNAIL];
            
        }
            break;
            
        case EGORDIAN_API_PHOTO_DETAIL:
        {
//            NSLog(@"Picture response :\n%@",request.responseObject);
            
            EG_PhotoDetail* photoDetail = [[EG_PhotoDetail alloc] initWithPhotoDict:request.responseObject];
            
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
                [_dataDownloaderDelegate serviceRequestFinished:[NSArray arrayWithObjects:photoDetail, nil] serviceType:EGORDIAN_API_PHOTO_DETAIL];
        }
            break;
            
        case EGORDIAN_API_JOB_PHOTO_THUMBNAIL:
        {
//            NSLog(@"Job Thumbnail response :\n%@",request.responseObject);
            NSLog(@"Picture response :Successful");
            
            NSMutableDictionary *pictureDict = (NSMutableDictionary *)request.responseObject;
            
            NSInteger jobPhotoCount = [[pictureDict objectForKey:@"Count"] integerValue];
            
            NSArray *jobPhotoArray = [pictureDict objectForKey:@"Data"];
            
            NSMutableArray* eGJobPhotoArray = [[NSMutableArray alloc] init];
            for (int i=0; i<jobPhotoArray.count; i++) {
                EG_JobPhoto* photo = [[EG_JobPhoto alloc] initWithPhotoDict:[jobPhotoArray objectAtIndex:i]];
                [eGJobPhotoArray addObject:photo];
            }
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
            {
                if([_dataDownloaderDelegate isKindOfClass:[eGordianJobPhotoViewController class]])
                {
                    eGordianJobPhotoViewController* egJobPhotoVC = (eGordianJobPhotoViewController*)_dataDownloaderDelegate;
                    egJobPhotoVC.totalPhotoCount = jobPhotoCount;
                }
                [_dataDownloaderDelegate serviceRequestFinished:eGJobPhotoArray  serviceType:EGORDIAN_API_JOB_PHOTO_THUMBNAIL];
            }
            
        }
            break;
            
        case EGORDIAN_API_PHOTO_DELETE:
        case EGORDIAN_API_JOB_PHOTO_DELETE:
        case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
        case EGORDIAN_API_UPLOAD_USER_PHOTO:
        {

            NSLog(@"Picture response :Successful");
            
            [EGordianUtilities updateRefreshStatus:YES forPage:APP_JOBDETAIL_REFRESH_FLAG];
            
            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
                [_dataDownloaderDelegate serviceRequestFinished:nil  serviceType:request.apiKey];
        }
            break;
            
            //        case EGORDIAN_API_JOB_PHOTO_DELETE:
            //        {
            //            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
            //                [_dataDownloaderDelegate serviceRequestFinished:nil  serviceType:EGORDIAN_API_JOB_PHOTO_DELETE];
            //        }
            //            break;
            //
            //        case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
            //        {
            //            NSLog(@"Picture response :\n%@",request.responseObject);
            //            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
            //                [_dataDownloaderDelegate serviceRequestFinished:nil  serviceType:EGORDIAN_API_PHOTOS_ATTACH_TO_JOB];
            //        }
            //            break;
            //
            //        case EGORDIAN_API_UPLOAD_USER_PHOTO:
            //        {
            //            NSLog(@"Picture response :\n%@",request.responseObject);
            //            if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFinished:serviceType:)])
            //                [_dataDownloaderDelegate serviceRequestFinished:nil  serviceType:request.apiKey];
            //        }
            //            break;
            
        default:
            break;
            
    }
}

- (void)requestFailed:(APIRequest*)request withError:(NSError*)error{
    
    NSInteger statustCode = request.operation.response.statusCode;
    NSURL *URL = request.operation.request.URL;
    NSDictionary *header = [request.operation.request allHTTPHeaderFields];
    NSString *body = [[NSString alloc] initWithData:[request.operation.request HTTPBody] encoding:NSUTF8StringEncoding];
    NSLog(@"\nREQUEST\n  URL: %@\n  HEADERS: %@\n  BODY: %@",URL,header,body);
    
    URL = request.operation.response.URL;
    header = [request.operation.response allHeaderFields];
//    NSLog(@"\nRESPONSE\n  URL: %@\n  HEADERS: %@\n  STATUS CODE: %i\n  BODY: %@",URL,header,statustCode,request.responseObject);
    
    
    if(request.apiKey == EGORDIAN_API_PHOTO_THUMBNAIL)
    {
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        NSString* errMsg = @"";
        if ([[request.responseObject allKeys] containsObject:@"responseText"]) {
            errMsg = [[request.responseObject valueForKey:@"responseText"] valueForKey:@"Message"];
            if([errMsg rangeOfString:@"No user pictures found for the User Id"].location != NSNotFound)
            {
                errMsg = [NSString stringWithFormat:@"No user pictures found for the User Id"];
                [errorDetail setValue:errMsg forKey:NSLocalizedDescriptionKey];
                NSError *customError = [NSError errorWithDomain:@"myDomain" code:statustCode userInfo:errorDetail];
                NSLog(@"%@",errMsg);
                if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFailed:serviceType:)]){
                    [_dataDownloaderDelegate serviceRequestFailed:customError serviceType:request.apiKey];
                }
                return;
            }
        }
    }
    
    if(request.apiKey == EGORDIAN_API_UPLOAD_USER_PHOTO)
    {
        //An error occurred
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        NSString* errMsg = @"";
        
        if ([[request.responseObject allKeys] containsObject:@"responseText"]) {
            errMsg = [[request.responseObject valueForKey:@"responseText"] valueForKey:@"Message"];
            if([errMsg rangeOfString:@"User Picture Limit"].location != NSNotFound)
                errMsg = [NSString stringWithFormat:@"User Picture Limit Reached. Please delete some to upload"];
        }
        [errorDetail setValue:errMsg forKey:NSLocalizedDescriptionKey];
        NSError *customError = [NSError errorWithDomain:@"myDomain" code:statustCode userInfo:errorDetail];
        
        if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFailed:serviceType:)]){
            [_dataDownloaderDelegate serviceRequestFailed:customError serviceType:request.apiKey];
        }
    }
    
    else
    {
        if(request.apiKey != EGORDIAN_API_LOGIN || request.apiKey != EGORDIAN_API_PHOTO_DELETE || request.apiKey != EGORDIAN_API_JOB_PHOTO_DELETE)
        {
            [EGordianAlertView showNetworkFailureMessage];
        }
        
        if(_dataDownloaderDelegate && [_dataDownloaderDelegate respondsToSelector:@selector(serviceRequestFailed:serviceType:)]){
            [_dataDownloaderDelegate serviceRequestFailed:error serviceType:request.apiKey];
        }
        
    }
    
}

@end
