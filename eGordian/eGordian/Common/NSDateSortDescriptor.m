//
//  NSDateSortDescriptor.m
//  ClearTouch
//
//  Created by Shineeth Hamza on 7/2/15.
//  Copyright (c) 2015 ClearPoint. All rights reserved.
//

//#define NULL_OBJECT(a) ((a) == nil || [(a) isEqual:[NSNull null]])
#define NULL_OBJECT(a) ((a) == nil || [(a) isEqual:[NSNull null]] || [(a) isEqual:[NSDate dateWithTimeIntervalSinceReferenceDate:0]])
#import "NSDateSortDescriptor.h"

@implementation NSDateSortDescriptor

- (id)copyWithZone:(NSZone*)zone
{
    return [[[self class] alloc] initWithKey:[self key] ascending:[self ascending] selector:[self selector]];
}

- (NSComparisonResult)compareObject:(id)object1 toObject:(id)object2
{
    if (NULL_OBJECT([object1 valueForKeyPath:[self key]])) {
        if (NULL_OBJECT([object2 valueForKeyPath:[self key]]))
            return NSOrderedSame; // If both objects have no awardedOn field, they are in the same "set"
        return NSOrderedDescending; // If the first one has no awardedOn, it is sorted after
    }
    if (NULL_OBJECT([object2 valueForKeyPath:[self key]])) {
        return NSOrderedAscending; // If the second one has no awardedOn, it is sorted after
    }
    return [[object1 valueForKeyPath:[self key]] compare:[object2 valueForKeyPath:[self key]]];
//    return NSOrderedSame; // If they both have an awardedOn field, they are in the same "set"
}

@end
