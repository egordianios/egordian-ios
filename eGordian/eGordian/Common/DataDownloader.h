//
//  DataDownloader.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/5/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enums.h"
#import "APIRequest.h"
#import "EG_JobOrder.h"
#import "EG_JobOrderDetail.h"
#import "EG_JobDocument.h"
#import "EG_TransactionDate.h"
#import "EG_JobConatcts.h"
#import "eGordianContact.h"
#import "CoreDataBase.h"

@protocol DataDownloaderDelegate <NSObject>
@optional
- (void)serviceRequestFinished:(NSArray *)responseArray serviceType:(APIKey)type;
- (void)serviceRequestFailed:(NSError *)error serviceType:(APIKey)type;
- (void)serviceRequestFinishedWithDict:(NSMutableDictionary *)responseDict serviceType:(APIKey)type;
@end

@interface DataDownloader : NSObject{
    NSTimeInterval startInterval, stopInterval, elapsedTime;
}
@property(strong, nonatomic) id<DataDownloaderDelegate> dataDownloaderDelegate;
+ (DataDownloader *)sharedDownloader;
+ (void) releaseInstance;
- (void)loginWithUserID:(NSString *)userID password:(NSString*)pwd;
- (void)eGordianAuthorization;
- (void)getResponseFormUrl:(NSString *)serviceUrl serviceType:(APIKey)apiKey;
- (void)updateData:(id )updateData andServiceURl:(NSString *)serviceUrl andserviceType:(APIKey)apiKey;
- (void)deleteData:(id)deleteDict andServiceURl:(NSString *)serviceUrl andserviceType:(APIKey)apiKey;
@end
