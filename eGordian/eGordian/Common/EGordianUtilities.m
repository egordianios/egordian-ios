//
//  eGordianUtilities.m
//  eGordian-iOS
//
//  Created by Laila on 14/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EGordianUtilities.h"


@implementation EGordianUtilities
//FontNames
NSString *const FONT_REGULAR                 = @"OpenSans";
NSString *const FONT_LIGHT                   = @"OpenSans-Light";
NSString *const FONT_LIGHT_ITALIC            = @"OpenSansLight-Italic";
NSString *const FONT_ITALIC                  = @"OpenSans-Italic";
NSString *const FONT_BOLD                    = @"OpenSans-Bold";
NSString *const FONT_BOLD_ITALIC             = @"OpenSans-BoldItalic";
NSString *const FONT_EXTRA_BOLD              = @"OpenSans-Extrabold";
NSString *const FONT_EXTRA_BOLD_ITALIC       = @"OpenSans-ExtraboldItalic";
NSString *const FONT_SEMIBOLD                = @"OpenSans-Semibold";
NSString *const FONT_SEMIBOLD_ITALIC         = @"OpenSans-SemiboldItalic";

//Header fonts
NSString *const FONT_STRATUM                 = @"Stratum2";
NSString *const FONT_STRATUM_REGULAR         = @"Stratum2-Regular";
NSString *const FONT_STRATUM_BOLD            = @"Stratum2-Bold";

//notes ... these are related to notes collection and not a note object
NSString *const NOTES_DESCRIPTION = @"Description";
NSString *const NOTES_COUNT = @"Count";
NSString *const NOTES_DATA = @"Data";
NSString *const NOTES_COLLECTION = @"NotesCollection";

#define LABELDEFAULTHEIGHT 22.0f

+(UIColor*) headerColor
{
    return  [UIColor colorWithRed:150.5/255 green:179.5/255 blue:193.5/255 alpha:1.0];
}

+(UIColor*) labelTitleColor{
    return  [UIColor colorWithRed:70.0/255 green:81.0/255 blue:88.0/255 alpha:1.0];
}

+(void) writeJobDetails:(NSString*)jobOrderID ownerID:(NSString*)ownerID
{
    [[NSUserDefaults standardUserDefaults] setObject:jobOrderID forKey:APP_JOBORDER_ID];
    [[NSUserDefaults standardUserDefaults] setObject:ownerID forKey:APP_OWNER_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString*) getJobOrderID
{
    NSString *jobOrderID = nil;
    jobOrderID = [[NSUserDefaults standardUserDefaults] objectForKey:APP_JOBORDER_ID];
    return jobOrderID;
}

+(NSString*) getOwnerID
{
    NSString *ownerID = nil;
    ownerID = [[NSUserDefaults standardUserDefaults] objectForKey:APP_OWNER_ID];
    return ownerID;
}

+(void) updateRefreshStatus:(BOOL)status forPage:(NSString*)pageKey
{
    [[NSUserDefaults standardUserDefaults]setBool:status forKey:pageKey];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

+(BOOL) getRefreshStatusForPage:(NSString*)pageKey
{
    BOOL status = TRUE;
    status = [[[NSUserDefaults standardUserDefaults]objectForKey:pageKey] boolValue];
    return status;
}

+(CGFloat)heightForLabel:(UILabel *)label WithText:(NSString *)text
{
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, FLT_MAX);
    if (label.font) {
        CGRect textRect = [text boundingRectWithSize:maximumLabelSize
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:label.font}
                                             context:nil];
        CGSize expectedLabelSize = textRect.size;
        return expectedLabelSize.height< LABELDEFAULTHEIGHT ? LABELDEFAULTHEIGHT :expectedLabelSize.height;
    }
    return LABELDEFAULTHEIGHT;
}


//+(void) attachPhoto:(NSArray*)imageArray withOwnerID:(NSString*)ownerID toJobWithID:(NSString*)job withDelegate:(id)delegate
//{
//    
//}

//+ (UIAlertView*) showNetworkNotAvailableAlert
//{
//    
//    UIAlertView *alertView = [[self alloc] initWithTitle:@"eGordian"
//                                                 message:@"Network Not Available"
//                                                delegate:nil
//                                       cancelButtonTitle:@"OK"
//                                       otherButtonTitles:nil];
//    
//    alertView.alertViewStyle = UIAlertViewStyleDefault;
//    
//    return alertView;
//
//}
@end
