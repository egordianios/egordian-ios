//
//  SyncManager.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/22/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "SyncManager.h"
#import "JobOrderDAO.h"
#import "APPConfig.h"
#import "AppDelegate.h"
#import "APIRequest.h"
#import "NSDictionary+JSON.h"
#import "JobOrderDetailsDAO.h"
#import "JobOrder.h"
#import "JobOrderDetail.h"
#import "JobOrderStatus.h"
#import "JobOrderStatusDAO.h"
#import "JobOrderScopeDAO.h"
#import "NSURL+Utilities.h"
#import "JobOrderScope.h"
#import "AttachPhotoDAO.h"
#import "AttachPhoto.h"
@interface SyncManager ()<APIRequestDelegate>
{
    NSArray *jobOrderDetailsToFetch;
    NSUInteger pageSize;
    NSUInteger currentPage;
    
    NSUInteger jobStatusCount;
    NSUInteger jobOrdersCount;

   
}
@property (nonatomic,retain) NSString *currentJobId;
@end
@implementation SyncManager
@synthesize pendingActions = _pendingActions;
@synthesize currentStatus = _currentStatus;
@synthesize statusDelegate;

static SyncManager *_sharedManager;
static dispatch_once_t oncePredicate;
static dispatch_queue_t syncQueue;

+ (dispatch_queue_t)getSyncQueue
{
    static dispatch_once_t onceToken;
    
    if (!syncQueue) {
        dispatch_once(&onceToken, ^{
            syncQueue = dispatch_queue_create("com.cleartouch.syncManager", DISPATCH_QUEUE_SERIAL);
            //syncQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
        });
    }
    return syncQueue;
}

+ (instancetype)sharedManager
{
    if(!_sharedManager)
    {
        dispatch_once(&oncePredicate, ^
                      {
                          _sharedManager = [[super allocWithZone:nil] init];
                      });
    }
    return _sharedManager;
}

+ (void)releaseInstance
{
    //syncQueue = 0;
    _sharedManager = nil;
    oncePredicate = 0;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedManager];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#if (!__has_feature(objc_arc))

- (id)retain
{
    return self;
}

- (unsigned)retainCount
{
    return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}
#endif

#pragma mark -
#pragma mark Custom Methods

- (instancetype)init
{
    @synchronized(self)
    {
        self = [super init];
        if (self) {
            pageSize = 1;
            currentPage = 0;
            jobOrdersCount = 0;
            jobStatusCount =0;
            jobOrderDetailsToFetch = [NSMutableArray new];
        }
        return self;
    }
}

- (void)dealloc
{
    

}
- (BOOL)APIInvocationPrecheck
{
    return ([AppDelegate isConnectedToInternet]);
}
- (BOOL)isSyncing
{
    return (_currentStatus != SMA_IDLE && [AppDelegate isConnectedToInternet]);
}
- (void)getResponseFormUrl:(NSString *)serviceUrl serviceType:(APIKey)apiKey{
    
    APIRequest *request=[APIRequest apiRequestWithKey:apiKey];
    
    request.delegate = (id<APIRequestDelegate>)self;
    
    NSDictionary *headerDictionary = @{@"Authorization":[AppConfig getAuthorizationToken],
                                       @"X-Version":APP_XVERSION,
                                       @"X-Database":[AppConfig getDataBase],
                                       @"Referer":APP_TGG_REFERER
                                       };
    [request invokeWithURL:serviceUrl andAditionalHeaders:headerDictionary];
}
- (void)moveToNextStep
{
    if ([AppDelegate isConnectedToInternet]) {
        dispatch_queue_t backgroundQueue = [SyncManager getSyncQueue];
        dispatch_async(backgroundQueue, ^{
            
            if (_currentStatus != SMA_IDLE) {
                return ;
            }
            
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            
            if((_pendingActions & SMA_OPENJOBS) == SMA_OPENJOBS)
            {
                [self downloadOpenJobOrders];
                return;
            }
            if((_pendingActions & SMA_JOBSCOPE) == SMA_JOBSCOPE)
            {
                [self downloadJobOrderScope];
                return;
            }
            
            if((_pendingActions & SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT) == SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT)
            {
                [self updateJobDetailedScope];
                return;
            }
            if((_pendingActions & SMA_JOBPHOTOS_SYNC_OUT) == SMA_JOBPHOTOS_SYNC_OUT)
            {
                [self updateJobOrderPhotos];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:CTSyncStartedDidEndNotification object:nil];
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinsh)]) {
                    [statusDelegate syncDidFinsh];
                }
            });
            
            
        });
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            _currentStatus = SMA_IDLE;
            _pendingActions = SMA_IDLE;
            [[NSNotificationCenter defaultCenter] postNotificationName:CTSyncStartedDidEndNotification object:nil];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinsh)]) {
                [statusDelegate syncDidFinsh];
            }
        });
    }
    
}

#pragma mark JobOrder Download SyncIn Step 1
-(void)downloadOpenJobOrders{
    if(![self APIInvocationPrecheck])
        return;
    NSLog(@"SMA_OPENJOBS Started");
    _currentStatus = SMA_OPENJOBS;
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStartForAction:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStartForAction:_currentStatus];
        });
    }
    NSString *jobListURl = [NSString stringWithFormat:@"%@Users/%@/JobOrders?&pagesize=%d&pagenumber=%d&sortBy=-LastUpdatedDate",APP_HEADER_URL,[AppConfig getUserId],10,1];
    [self getResponseFormUrl:jobListURl serviceType:EGORDIAN_API_JOBSOFFLINE];
    
    
}
- (void)downloadOpenJobsWithError:(NSError*)error
{
    NSLog(@"SMA_OPENJOBS Ended");
    if((_pendingActions & SMA_OPENJOBS) == SMA_OPENJOBS)
        _pendingActions=_pendingActions - SMA_OPENJOBS;
    
    _currentStatus = SMA_IDLE;
    
    [self moveToNextStep];
    
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinshForAction:withError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidFinshForAction:SMA_OPENJOBS withError:error];
        });
    }
    
}

#pragma mark DownloadJOBScope Of Work SyncIn Step 2
-(void)downloadJobOrderScope{
    if(![self APIInvocationPrecheck])
        return;
    NSLog(@"SMA_JOBSCOPE Started");
    _currentStatus = SMA_JOBSCOPE;
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStartForAction:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStartForAction:_currentStatus];
        });
    }
    //Loop through JOBS and Call JOB ORDER Scope Service
    NSArray *itemsForFetch = nil;
    NSArray *jobOrdersArray =[JobOrderDAO fetchAllDataInContext:[CoreDataBase getTemporaryContext]];
    jobOrderDetailsToFetch=jobOrdersArray;
    itemsForFetch =jobOrdersArray;
    NSString *jobDetailUrl =@"";
    
    jobStatusCount = 0;
    
    if (jobOrdersArray && jobOrdersArray.count >0)
    {
        APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_JOBORDER_DETAILEDSCOPE];
        request.delegate = (id<APIRequestDelegate>)self;
        for (JobOrder *currentJob in itemsForFetch)
        {
            jobDetailUrl =[currentJob.jobOrderLink stringByAppendingString:@"/DetailedScope"];
            self.currentJobId =currentJob.jobId;
            NSLog(@"Calling Service for Job SCOPE for URL:%@",jobDetailUrl);
            [self getResponseFormUrl:jobDetailUrl serviceType:EGORDIAN_API_JOBORDER_DETAILEDSCOPE];
            jobStatusCount ++;
        }
        
    }
    else
        [self downloadJobScopeCompletedWithError:nil];

    
}

-(void)downloadJobScopeCompletedWithError:(NSError *)error{
    
    NSLog(@"SMA_JOBSCOPE Ended");
    if((_pendingActions & SMA_JOBSCOPE) == SMA_JOBSCOPE)
        _pendingActions = _pendingActions - SMA_JOBSCOPE;
    _currentStatus = SMA_IDLE;
    currentPage = 0;
    [self moveToNextStep];
    
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinshForAction:withError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidFinshForAction:SMA_JOBSCOPE withError:error];
        });
    }

}

#pragma mark JobOrderDetail Download
-(void)downloadJobOrderDetails{
    if(![self APIInvocationPrecheck])
        return;
    NSLog(@"SMA_JOBDETAILS Started Page");
    _currentStatus = SMA_JOBDETAILS;
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStartForAction:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStartForAction:_currentStatus];
        });
    }
    NSArray *itemsForFetch = nil;
    //Loop Through Job Orders and Call the Individual API Call
    NSArray *jobOrdersArray =[JobOrderDAO fetchAllDataInContext:[CoreDataBase getTemporaryContext]];
    jobOrderDetailsToFetch=jobOrdersArray;
    itemsForFetch =jobOrdersArray;
    NSString *jobDetailUrl =@"";
    if (jobOrdersArray && jobOrdersArray.count >0)
    {
        APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_JOBORDER_DETAIL];
        request.delegate = (id<APIRequestDelegate>)self;
        for (JobOrder *currentJob in itemsForFetch)
        {
            jobDetailUrl =currentJob.jobOrderLink;
            NSLog(@"Calling Service for Job Oeder Detail for URL:%@",jobDetailUrl);
            [self getResponseFormUrl:jobDetailUrl serviceType:EGORDIAN_API_JOBORDER_DETAIL];
            jobOrdersCount ++;
        }

    }
    else
        [self downloadJobOrderDetailsCompletedWithError:nil];
    
}
- (void)downloadJobOrderDetailsCompletedWithError:(NSError*)error
{
    NSLog(@"SMA_JOBDETAILS Ended");
    if((_pendingActions & SMA_JOBDETAILS) == SMA_JOBDETAILS)
            _pendingActions = _pendingActions - SMA_JOBDETAILS;
        _currentStatus = SMA_IDLE;
        currentPage = 0;
        [self moveToNextStep];
        
        if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinshForAction:withError:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [statusDelegate syncDidFinshForAction:SMA_JOBDETAILS withError:error];
            });
        }
}

#pragma mark JobOrderDetail Statuses Download
-(void)downloadStatusForJobOrders{
    if(![self APIInvocationPrecheck])
        return;
    NSLog(@"SMA_JOBSTATUS Started Page");
    _currentStatus = SMA_JOBSTATUS;
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStartForAction:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStartForAction:_currentStatus];
        });
    }
    NSArray *itemsForFetch = nil;
    //Loop Through Job Orders and Call the Individual API Call
    NSArray *jobOrdersArray =[JobOrderDetailsDAO fetchAllDataInContext:[CoreDataBase getTemporaryContext]];
    NSString *statusURL =@"";
    itemsForFetch=jobOrdersArray;
    if (jobOrdersArray && jobOrdersArray.count >0)
    {
        APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_JOBORDER_STATUS];
        request.delegate = (id<APIRequestDelegate>)self;
        for (JobOrderDetail *currentJob in itemsForFetch)
        {
            statusURL =currentJob.statusUrl;
            NSLog(@"Calling Service for Job Statuses for URL:%@",statusURL);
            [self getResponseFormUrl:statusURL serviceType:EGORDIAN_API_JOBORDER_STATUS];
            jobStatusCount ++;
        }
        
    }
    else
        [self downloadStatusForJobOrdersCompletedWithError:nil];
    
}
- (void)downloadStatusForJobOrdersCompletedWithError:(NSError*)error{
    
    NSLog(@"SMA_JOBSTATUS Ended");
    if((_pendingActions & SMA_JOBSTATUS) == SMA_JOBSTATUS)
        _pendingActions = _pendingActions - SMA_JOBSTATUS;
    _currentStatus = SMA_IDLE;
    currentPage = 0;
    [self moveToNextStep];
    
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinshForAction:withError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidFinshForAction:SMA_JOBDETAILS withError:error];
        });
    }

}

#pragma mark syncout step Updating Job Detailedscope SyncOut Step 1

-(void)updateJobDetailedScope{
    
    if(![self APIInvocationPrecheck])
        return;
    NSLog(@"SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT Started");
    _currentStatus = SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT;
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStartForAction:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStartForAction:_currentStatus];
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        NSManagedObjectContext *ctx = [CoreDataBase activeContext];
        NSArray *jobOrdersToSync = [JobOrderDAO fetchItemsForSyncJobOrdersInContext:ctx];
        if (jobOrdersToSync.count) {
            for (JobOrder *currentJob in jobOrdersToSync) {
                
                JobOrderScope *scopeToPass =(JobOrderScope *)currentJob.jobScope;
                NSDictionary *dictToPass =@{@"Text":[scopeToPass.jobScopeDescription string],
                                             @"Description":@""};
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictToPass
                                                                   options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                     error:&error];
                id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
                APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_SCOPEOFWORK_UPDATE];
                request.delegate = (id<APIRequestDelegate>)self;
                NSDictionary *headerDictionary = @{@"Authorization":[AppConfig getAuthorizationToken],
                                                   @"X-Version":APP_XVERSION,
                                                   @"X-Database":[AppConfig getDataBase],
                                                   @"Referer":APP_TGG_REFERER
                                                   };
                NSString *detailedScopeLink =[NSString stringWithFormat:@"%@/DetailedScope",currentJob.jobOrderLink];
                [request invokeWithURL:detailedScopeLink withData:jsonDict andAditionalHeaders:headerDictionary];
                jobStatusCount++;
            }
            
            
        }
        else {
            [self updateJobDetailScopeCompletedWithError:nil];
        }

    });
    

}
-(void)updateJobDetailScopeCompletedWithError:(NSError *)error{
    if((_pendingActions & SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT) == SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT)
        _pendingActions=_pendingActions - SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT;
    NSLog(@"SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT Ended");
    _currentStatus = SMA_IDLE;
    
    if (error) {
        //If sync out fails, sync should stop immediately to avoid losing user data .
        _pendingActions = SMA_IDLE;
    }
    [self moveToNextStep];
    
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinshForAction:withError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidFinshForAction:SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT withError:error];
        });
    }

    
}

#pragma mark sycout step updating JobPhotos syncout step 2
-(void)updateJobOrderPhotos{
    
    if(![self APIInvocationPrecheck])
        return;
    NSLog(@"SMA_JOBPHOTOS_SYNC_OUT Started");
    _currentStatus = SMA_JOBPHOTOS_SYNC_OUT;
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStartForAction:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStartForAction:_currentStatus];
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Need to frame parameters based on the Unique JobOrders So we can combine call to Update Photo into one for Unique JobOrders
        NSManagedObjectContext *ctx = [CoreDataBase activeContext];
        NSArray *jobOrdersToSync = [AttachPhotoDAO getUniqueRecordsForJob:ctx];
        if (jobOrdersToSync.count) {
            for (NSDictionary *currentDict in jobOrdersToSync){
                NSString *jobId =[currentDict valueForKey:@"jobId"];
                NSArray *recordsForEachJobOrder=[AttachPhotoDAO getIndividualRecordsForJobOrder:jobId inContext:ctx];
                NSMutableArray *inputArray=[NSMutableArray new];
                NSString *ownerId=@"";
                for (AttachPhoto *currentPhoto in recordsForEachJobOrder) {
                    
                    NSLog(@"Details to insert:%@",currentPhoto);
                    
                    NSData *retrievedData = currentPhoto.imageData;
                    NSString *photoStr = [retrievedData  base64EncodedString];

                    NSDictionary *inputParams =@{
                                                @"id":@0,
                                                @"UserID":currentPhoto.userID.length?currentPhoto.userID:@"",
                                                @"PictureName":@"Sam4Test.jpg",//currentPhoto.pictureName.length?currentPhoto.pictureName:@"",
                                                @"Thumbnail":@"",
                                                @"Image":(photoStr)?photoStr:@""
                                                };
                    [inputArray addObject:inputParams];
                    ownerId=currentPhoto.ownerId;

                }
                if (inputArray.count >0){
                APIRequest *request=[APIRequest apiRequestWithKey:EGORDIAN_API_PHOTOS_ATTACH_TO_JOB];
                request.delegate = (id<APIRequestDelegate>)self;
                NSDictionary *headerDictionary = @{@"Authorization":[AppConfig getAuthorizationToken],
                                                   @"X-Version":APP_XVERSION,
                                                   @"X-Database":[AppConfig getDataBase],
                                                   @"Referer":APP_TGG_REFERER
                                                   };
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputArray
                                                                   options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                     error:&error];
                id jsonDict  = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
                //v1/Owners/{ownerId}/JobOrders/{jobId}/AttachPictures
                NSString *photoToJobURLString = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/UploadPictures",APP_HEADER_URL,ownerId,jobId];
                [request invokeWithURL:photoToJobURLString withData:jsonDict andAditionalHeaders:headerDictionary];

                jobStatusCount++;
                }
            }
            
            
        }
        else {
            [self updateJobOrderPhotosWithError:nil];
        }
        
    });

}

-(void)updateJobOrderPhotosWithError:(NSError *)error{
    if((_pendingActions & SMA_JOBPHOTOS_SYNC_OUT) == SMA_JOBPHOTOS_SYNC_OUT)
        _pendingActions=_pendingActions - SMA_JOBPHOTOS_SYNC_OUT;
    NSLog(@"SMA_JOBPHOTOS_SYNC_OUT Ended");
    _currentStatus = SMA_IDLE;
    
    if (error) {
        //If sync out fails, sync should stop immediately to avoid losing user data .
        _pendingActions = SMA_IDLE;
    }else{
        
        
    }
    [self moveToNextStep];
    
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidFinshForAction:withError:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidFinshForAction:SMA_JOBPHOTOS_SYNC_OUT withError:error];
        });
    }

    
}
#pragma mark - Sync calls

- (BOOL)fullSync
{//return false;
    if(_currentStatus!=SMA_IDLE)
        return false;
    
    _pendingActions = (SMA_OPENJOBS |SMA_JOBSCOPE
                       );
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:CTSyncStartedDidStartNotification object:nil];
        if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStart)])
        {
            [statusDelegate syncDidStart];
        }
    });
    
    
    [self moveToNextStep];
    
    return YES;
}

- (BOOL)syncOnNetworkAwake{
    
    BOOL shouldSync = FALSE;
    
    
    NSManagedObjectContext *ctx =[CoreDataBase activeContext];
    NSArray *jobOrdersToSync = [JobOrderDAO fetchItemsForSyncJobOrdersInContext:ctx];
    NSArray *jobPhotostoSYnc= [AttachPhotoDAO fetchItemsForSyncJobOrdersInContext:ctx];

    
    if( jobOrdersToSync.count > 0 || jobPhotostoSYnc.count > 0){
        
        shouldSync = TRUE;
    }
    
    return shouldSync;
}

- (void)runScheduledTask {
    NSLog(@"runScheduledTask");
    [self fullSync];
}


#pragma mark syncOut

- (BOOL)syncOut
{//return false;
    if(_currentStatus!=SMA_IDLE)
        return false;
    
    _pendingActions = (SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT| SMA_JOBPHOTOS_SYNC_OUT
                       );//| SMA_JOBPHOTOS_SYNC_OUT
    if(statusDelegate && [statusDelegate respondsToSelector:@selector(syncDidStart)])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [statusDelegate syncDidStart];
        });
    }
    [self moveToNextStep];
    return YES;
}

#pragma mark - APIRequest Delegate
- (void)requestFinished:(APIRequest *)request
{
   
    if(200 != request.operation.response.statusCode)
    {
        [self requestFailed:request];
        return;
    }
    NSInteger statustCode = request.operation.response.statusCode;
    NSURL *URL = request.operation.request.URL;
    NSDictionary *header = [request.operation.request allHTTPHeaderFields];
    NSString *body = [[NSString alloc] initWithData:[request.operation.request HTTPBody] encoding:NSUTF8StringEncoding];
   // NSLog(@"\n\n\n\nREQUEST\n  URL: %@\n  HEADERS: %@\n  BODY: %@",URL,header,body);
    
    URL = request.operation.response.URL;
    header = [request.operation.response allHeaderFields];
    NSLog(@"\n\n\n\nRESPONSE\n  URL: %@\n  HEADERS: %@\n  STATUS CODE: %i\n  BODY: %@",URL,header,statustCode,request.responseObject);
//
    dispatch_queue_t backgroundQueue = [SyncManager getSyncQueue];
    dispatch_async(backgroundQueue, ^{
        switch (request.apiKey) {
            case EGORDIAN_API_JOBSOFFLINE:{
                NSDictionary *jobOrderDict = (NSDictionary*)request.responseObject;
                if ([jobOrderDict hasValidJSONKey:@"Data"]) {
                    NSManagedObjectContext *ctx = [CoreDataBase getTemporaryContext];
                    NSArray *jobOrderArray = [jobOrderDict objectForKey:@"Data"];
                    for (NSDictionary * insertDict in jobOrderArray) {
                        [JobOrderDAO insertIfDoesNotExists:insertDict inContext:ctx];
                    }
                    [CoreDataBase saveTemporaryContextAndRelease:ctx];
                }
                
                
                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    if ([jobOrderDict hasValidJSONKey:@"Data"]) {
//                        NSArray *jobOrderArray = [jobOrderDict objectForKey:@"Data"];
//                        for (NSDictionary * insertDict in jobOrderArray) {
//                            [JobOrderDAO insertIfDoesNotExists:insertDict inContext:[CoreDataBase activeContext]];
//                        }
//                    }
//                    
//                    
//                    [CoreDataBase saveActiveContext];
//                });
                [self downloadOpenJobsWithError:nil];
                
                break;
            }
            case EGORDIAN_API_JOBORDER_DETAIL:
            {
                NSDictionary *responseDict = (NSDictionary*)request.responseObject;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([responseDict hasValidJSONKey:@"Data"]) {
                        NSDictionary *jobOrderDict = [responseDict objectForKey:@"Data"];
                        NSString *jobOrderId =[jobOrderDict getStringForKey:@"Id"];
                            JobOrderDetail  *jd = [JobOrderDetailsDAO insertIfDoesNotExists:jobOrderDict inContext:[CoreDataBase activeContext]];
                        
                            JobOrder *jobOrder =[JobOrderDAO objectWithID:jobOrderId inContext:[CoreDataBase activeContext]];
                            if  (jobOrder)
                                jobOrder.jobOrderDetail =jd;
                                //Frame Status URL
                                if([jobOrder.jobOrderLink rangeOfString:@"Owners"].location != NSNotFound){
                                    NSString *originalString = jobOrder.jobOrderLink;
                                    NSRange end = [originalString rangeOfString:@"JobOrders/"];
                                    NSString *statusURL = [NSString stringWithFormat:@"%@Status",[originalString substringToIndex:end.location]];
                                    jobOrder.jobOrderDetail.statusUrl =statusURL;
                                }
                        
                    }
                    [CoreDataBase saveActiveContext];
                });
                jobOrdersCount --;
                if(jobOrdersCount ==0)
                {
                    [self downloadJobOrderDetailsCompletedWithError:nil];
                }
                break;
            }
            case EGORDIAN_API_JOBORDER_DETAILEDSCOPE:{
                NSURL *URL = request.operation.request.URL;
                NSDictionary *responseDict = (NSDictionary*)request.responseObject;
                
                NSLog(@"Job Order Detailed Scope Success:\n%@",request.responseObject);
                
                if([[URL  absoluteString] rangeOfString:@"Owners"].location != NSNotFound){
                    NSString *originalString = [URL absoluteString];
                    NSRange end = [originalString rangeOfString:@"JobOrders/"];
                    NSString *statusURL = [originalString substringFromIndex:end.location+end.length];
                    if ([statusURL rangeOfString:@"/DetailedScope"].location != NSNotFound) {
                        statusURL =[statusURL stringByReplacingOccurrencesOfString:@"/DetailedScope" withString:@""];
                        self.currentJobId =statusURL;
                    }
                }
                NSMutableDictionary *dict =[responseDict mutableCopy];
                [dict setObject:self.currentJobId forKey:@"jobOrderId"];
                NSManagedObjectContext *ctx = [CoreDataBase getTemporaryContext];
                JobOrderScope *jobScope = [JobOrderScopeDAO insertIfDoesNotExists:dict
                                                                        inContext:ctx];
                
                JobOrder *jd =[JobOrderDAO objectWithID:self.currentJobId inContext:ctx];
                if (jd) {
                    jd.jobScope =jobScope;
                }
                [CoreDataBase saveTemporaryContextAndRelease:ctx];
                
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                    if([[URL  absoluteString] rangeOfString:@"Owners"].location != NSNotFound){
//                        NSString *originalString = [URL absoluteString];
//                        NSRange end = [originalString rangeOfString:@"JobOrders/"];
//                        NSString *statusURL = [originalString substringFromIndex:end.location+end.length];
//                        if ([statusURL rangeOfString:@"/DetailedScope"].location != NSNotFound) {
//                            statusURL =[statusURL stringByReplacingOccurrencesOfString:@"/DetailedScope" withString:@""];
//                            self.currentJobId =statusURL;
//                        }
//                    }
//                    NSMutableDictionary *dict =[responseDict mutableCopy];
//                    [dict setObject:self.currentJobId forKey:@"jobOrderId"];
//                    NSManagedObjectContext *ctx = [CoreDataBase getTemporaryContext];
//                    JobOrderScope *jobScope = [JobOrderScopeDAO insertIfDoesNotExists:dict
//                                                      inContext:ctx];
//                    
//                    JobOrder *jd =[JobOrderDAO objectWithID:self.currentJobId inContext:ctx];
//                    if (jd) {
//                        jd.jobScope =jobScope;
//                    }
//                    [CoreDataBase saveTemporaryContextAndRelease:ctx];
//                 });

                jobStatusCount--;
                if(jobStatusCount ==0)
                {
                    [self downloadJobScopeCompletedWithError:nil];
                }
                break;
            }
                
            case EGORDIAN_API_SCOPEOFWORK_UPDATE:{
                NSURL *URL = request.operation.request.URL;
                //NSDictionary *responseDict = (NSDictionary*)request.responseObject;
                NSLog(@"Job Order Detailed Scope Update Success:\n%@",request.responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //Extract JobOrder
                    if([[URL  absoluteString] rangeOfString:@"Owners"].location != NSNotFound){
                        NSString *originalString = [URL absoluteString];
                        NSRange end = [originalString rangeOfString:@"JobOrders/"];
                        NSString *statusURL = [originalString substringFromIndex:end.location+end.length];
                        if ([statusURL rangeOfString:@"/DetailedScope"].location != NSNotFound) {
                            statusURL =[statusURL stringByReplacingOccurrencesOfString:@"/DetailedScope" withString:@""];
                            self.currentJobId =statusURL;
                        }
                    }
                        JobOrder *jobOrder =[JobOrderDAO objectWithID:self.currentJobId inContext:[CoreDataBase activeContext]];
                        if  (jobOrder)
                            jobOrder.syncStatus =JT_ALREADYSYNCED;
                    
                    
                    [CoreDataBase saveActiveContext];
                });
                jobStatusCount --;
                if(jobStatusCount ==0)
                {
                    [self updateJobDetailScopeCompletedWithError:nil];
                    
                }

                break;
            }
            case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:{
                
//                 NSString *photoToJobURLString = [NSString stringWithFormat:@"%@Owners/%@/JobOrders/%@/AttachPictures",APP_HEADER_URL,ownerId,jobId];
                NSURL *URL = request.operation.request.URL;
                NSDictionary *responseDict = (NSDictionary*)request.responseObject;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //Extract JobOrder
                    NSString *currentJobOrderId=@"";
                    if([[URL  absoluteString] rangeOfString:@"Owners"].location != NSNotFound){
                        NSString *originalString = [URL absoluteString];
                        NSRange end = [originalString rangeOfString:@"JobOrders/"];
                        NSString *statusURL = [originalString substringFromIndex:end.location+end.length];
                        if ([statusURL rangeOfString:@"/UploadPictures"].location != NSNotFound) {
                            statusURL =[statusURL stringByReplacingOccurrencesOfString:@"/UploadPictures" withString:@""];
                            currentJobOrderId =statusURL;
                        }
                        
                    }
                    
                    
                    NSManagedObjectContext *ctx =[CoreDataBase getTemporaryContext];
                    NSArray *jobOrdersToSync = [AttachPhotoDAO getUniqueRecordsForJob:ctx];
                    if(jobOrdersToSync.count >0){
                        for (NSDictionary *currDict in jobOrdersToSync) {
                            AttachPhoto *attPhoto =[AttachPhotoDAO objectWithJobID:[currDict valueForKey:@"jobId"] inContext:ctx];
                            attPhoto.syncStatus =JT_ALREADYSYNCED;
                        }
                    NSArray *jobPhtosToUpdate =[AttachPhotoDAO getIndividualRecordsForJobOrder:currentJobOrderId inContext:ctx];
                    for (AttachPhoto *currPhoto in jobPhtosToUpdate) {
                        currPhoto.syncStatus =JT_ALREADYSYNCED;
                    }
                    [CoreDataBase saveTemporaryContextAndRelease:ctx];
                   }
                    
                });
                NSLog(@"EGORDIAN_API_PHOTOS_ATTACH_TO_JOB Update Success");
                jobStatusCount --;
                if(jobStatusCount ==0)
                {
                    [self updateJobOrderPhotosWithError:nil];
                    
                }
               
                break;
            }
                
            default:{
                break;
            }
                
        }
        
       });

}
- (void)requestFailed:(APIRequest *)request
{
    //Handle only NSError object initialization
    //Never do any error handling here. Error handling should be done on "requestFailed:withError:"
    
    NSString *errorMessage = @"";
    
    switch (request.apiKey) {
        case EGORDIAN_API_JOBSOFFLINE:{
            
            break;
        }
        case EGORDIAN_API_JOBORDER_DETAIL:{
            
            break;
        }
        case EGORDIAN_API_JOBORDER_STATUS:{
            
            break;
        }
        default:
            break;
    }
    
    
    NSInteger statustCode = request.operation.response.statusCode;
    NSError *error = [NSError errorWithDomain:[AppConfig appName]
                                         code:statustCode
                                     userInfo:@{@"NSLocalizedDescription": errorMessage}];
    [self requestFailed:request withError:error];
}

- (void)requestFailed:(APIRequest *)request withError:(NSError *)error
{
    NSInteger statustCode = request.operation.response.statusCode;
    NSURL *URL = request.operation.request.URL;
    NSDictionary *header = [request.operation.request allHTTPHeaderFields];
    NSString *body = [[NSString alloc] initWithData:[request.operation.request HTTPBody] encoding:NSUTF8StringEncoding];
    NSLog(@"\nREQUEST\n  URL: %@\n  HEADERS: %@\n  BODY: %@",URL,header,body);
    
    URL = request.operation.response.URL;
    header = [request.operation.response allHeaderFields];
    NSLog(@"\nRESPONSE\n  URL: %@\n  HEADERS: %@\n  STATUS CODE: %i\n  BODY: %@",URL,header,statustCode,request.responseObject);
    switch (request.apiKey) {
        case EGORDIAN_API_JOBSOFFLINE:{
            [self downloadOpenJobsWithError:error];
            break;
        }
        case EGORDIAN_API_JOBORDER_DETAIL:{
            jobOrdersCount --;
            if(jobOrdersCount ==0)
            {
                currentPage++;
                [self downloadJobOrderDetailsCompletedWithError:error];
            }
            [self downloadJobOrderDetailsCompletedWithError:error];
       }
        case EGORDIAN_API_JOBORDER_DETAILEDSCOPE:{
            self.currentJobId =@"";
            jobStatusCount--;
            if(jobStatusCount ==0)
            {
                [self downloadJobScopeCompletedWithError:error];
            }
        }
        case EGORDIAN_API_SCOPEOFWORK_UPDATE:{
            jobStatusCount--;
            if(jobStatusCount ==0)
            {
                [self updateJobDetailScopeCompletedWithError:error];
            }
        }
            
            default:
            break;
    }
    
}
@end
