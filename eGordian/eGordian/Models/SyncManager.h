//
//  SyncManager.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/22/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    SMA_IDLE                            = 0,
    SMA_OPENJOBS                        = 1 << 0,
    SMA_JOBSCOPE                        = 1 << 1,
    SMA_JOBSTATUS                       = 1 << 2,
    SMA_JOBDETAILS                      = 1 << 3,
    SMA_JOBDETAILSCOPE_UPDATE_SYNC_OUT  = 1 << 4,
    SMA_JOBPHOTOS_SYNC_OUT              = 1 << 5
} SyncManagerAction;

#define SycOutStatusNotification @"SycOutStatusNotification"


@protocol SyncManagerDelegate <NSObject>
@optional
-(void)syncDidStart;
-(void)syncDidStartForAction:(SyncManagerAction) status;
-(void)syncDidFinshForAction:(SyncManagerAction) status withError:(NSError *)error;
-(void)syncDidFinsh;
@end

@interface SyncManager : NSObject

@property (nonatomic, strong) NSTimer *scheduler;
@property (nonatomic, readonly) SyncManagerAction pendingActions;
@property (nonatomic, readonly) SyncManagerAction currentStatus;
@property (nonatomic, weak) id<SyncManagerDelegate> statusDelegate;

+ (instancetype)sharedManager;
+ (void)releaseInstance;

- (BOOL)fullSync;
- (BOOL)syncOut;
- (BOOL)isSyncing;
- (BOOL)syncOnNetworkAwake;
@end

