//
//  JobOrderScope+CoreDataProperties.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/4/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderScope.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobOrderScope (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSAttributedString *jobScopeDescription;
@property (nonatomic) int16_t scopeStatus;
@property (nullable, nonatomic, retain) NSString *jobId;
@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) JobOrder *jobOrder;

@end

NS_ASSUME_NONNULL_END
