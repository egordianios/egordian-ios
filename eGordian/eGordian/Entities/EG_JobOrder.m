//
//  EG_JobOrder.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/27/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EG_JobOrder.h"
#import "NSDictionary+JSON.h"

@implementation EG_JobOrder
@synthesize jobOrderId =_jobOrderId;
@synthesize jobOrderNumber = _jobOrderNumber;
@synthesize jobTitle = _jobTitle;;
@synthesize ClientName = _ClientName;
@synthesize locationName= _locationName;
@synthesize address1 = _address1;
@synthesize address2 = _address2;
@synthesize City = _City;
@synthesize State = _State;
@synthesize ZipCode = _ZipCode;
@synthesize LastUpdatedDate = _LastUpdatedDate;
@synthesize jobOrderLink = _jobOrderLink;
@synthesize ownerID = _ownerID;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_JobOrder *)getJobOrdersJsonDictionary:(NSDictionary *)jobOrderDict{
    
    if(!jobOrderDict || [jobOrderDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_JobOrder *currentJobOrderDict = [[EG_JobOrder alloc]init];
    
    currentJobOrderDict.jobOrderId = [jobOrderDict getStringForKey:@"Id"];
    
    currentJobOrderDict.jobOrderLink = [jobOrderDict getStringForKey:@"JobOrderLink"];
    
    NSArray *fieldsArray = [jobOrderDict objectForKey:@"Fields"];
    
    NSMutableDictionary *fieldsDict = [self getFieldsDict:fieldsArray];
    
    currentJobOrderDict.jobTitle        = [fieldsDict getStringForKey:@"JobTitle"];
    currentJobOrderDict.jobOrderNumber  = [fieldsDict getStringForKey:@"JobOrderNumber"];
    currentJobOrderDict.ClientName      = [fieldsDict getStringForKey:@"ClientName"];
    currentJobOrderDict.locationName    = [fieldsDict getStringForKey:@"LocationName"];
    currentJobOrderDict.address1        = [fieldsDict getStringForKey:@"Address1"];
    currentJobOrderDict.address2        = [fieldsDict getStringForKey:@"Address2"];
    currentJobOrderDict.City            = [fieldsDict getStringForKey:@"City"];
    currentJobOrderDict.State           = [fieldsDict getStringForKey:@"State"];
    currentJobOrderDict.ZipCode         = [fieldsDict getStringForKey:@"ZipCode"];
    currentJobOrderDict.LastUpdatedDate = [fieldsDict getStringForKey:@"LastUpdatedDate"];
    
    return currentJobOrderDict;
}

+(NSMutableDictionary *)getFieldsDict:(NSArray *)fields{
    
    NSMutableDictionary *fieldsDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in fields) {
        [fieldsDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return fieldsDict;
}


+(NSMutableArray*) sortJobs:(NSArray*)jobs WithSortKey:(NSString*)Key 
{
    
    
    NSMutableArray* sortedArray = nil;
    
    if(!jobs)
        return nil;
    
    if(![jobs count])
        return nil;
    
    if([Key isEqualToString: @"LastUpdatedDate"] || !Key.length)
        return [jobs mutableCopy];
    
//    if(!Key)
//        return [jobs mutableCopy];
    
    NSSortDescriptor* firstSortDescriptor = [[NSSortDescriptor alloc] initWithKey:Key  ascending:YES];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastUpdatedDate"  ascending:NO];
    NSArray *sortDescriptors = @[firstSortDescriptor, dateDescriptor];
    
    NSArray* tempArray = [jobs sortedArrayUsingDescriptors:sortDescriptors];
    
    if(tempArray)
        sortedArray = [[NSMutableArray alloc] initWithArray: tempArray];
    
    return sortedArray;
}


@end
