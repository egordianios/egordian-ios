//
//  JobOrderScope.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/4/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobOrder;

NS_ASSUME_NONNULL_BEGIN

@interface JobOrderScope : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "JobOrderScope+CoreDataProperties.h"
