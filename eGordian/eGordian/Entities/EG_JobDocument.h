//
//  EGJobDocument.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/30/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_JobDocument : NSObject

+(EG_JobDocument *)getJobDocumentsJsonDictionary:(NSDictionary *)jobDocumentDict;
@property(nonatomic,retain) NSString* contextClassifier;
@property(nonatomic,retain) NSString* documentTitle;
@property(nonatomic,retain) NSString* comments;
@property(nonatomic,retain) NSString* fileName;
@property(nonatomic,retain) NSString* fileUrl;
@end
