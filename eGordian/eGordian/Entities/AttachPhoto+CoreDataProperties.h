//
//  AttachPhoto+CoreDataProperties.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/8/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AttachPhoto.h"

NS_ASSUME_NONNULL_BEGIN

@interface AttachPhoto (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, retain) NSString *jobId;
@property (nullable, nonatomic, retain) NSString *photoFilePath;
@property (nullable, nonatomic, retain) NSString *pictureName;
@property (nullable, nonatomic, retain) NSData *thumbnailData;
@property (nullable, nonatomic, retain) NSString *userID;
@property (nonatomic) int16_t syncStatus;
@property (nullable, nonatomic, retain) NSString *ownerId;

@end

NS_ASSUME_NONNULL_END
