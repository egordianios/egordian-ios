//
//  Owner.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/11/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "Owner.h"
#import "NSDictionary+JSON.h"
@implementation Owner
@synthesize CompanyID = _CompanyID;
@synthesize CompanyName = _CompanyName;
@synthesize OwnerID = _OwnerID;
@synthesize OwnerName = _OwnerName;
@synthesize Side = _Side;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(Owner *)getOwnerAndContractorJsonDictionary:(NSDictionary *)ownerDict;{
    if(!ownerDict || [ownerDict isEqual:[NSNull null]]){
        return nil;
    }
    
    Owner *ownerAndContractorDict = [[Owner alloc]init];
    ownerAndContractorDict.CompanyID = [ownerDict getStringForKey:@"CompanyID"];
    ownerAndContractorDict.CompanyName = [ownerDict getStringForKey:@"CompanyName"];
    ownerAndContractorDict.OwnerID = [ownerDict getStringForKey:@"OwnerID"];
    ownerAndContractorDict.OwnerName = [ownerDict getStringForKey:@"OwnerName"];
    ownerAndContractorDict.Side = [ownerDict getStringForKey:@"Side"];
    return ownerAndContractorDict;
}
@end
