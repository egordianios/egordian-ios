//
//  EG_JobOrderDetail.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/9/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EG_JobOrderDetail.h"
#import "NSDictionary+JSON.h"
#import "EG_JobLocation.h"

@implementation EG_JobOrderDetail
@synthesize jobOrderId =_jobOrderId;
@synthesize jobOrderNumber = _jobOrderNumber;
@synthesize jobTitle = _jobTitle;
@synthesize clientName = _clientName;
@synthesize jobStatus = _jobStatus;
@synthesize trackingDateLink = _trackingDateLink;
@synthesize detailedScopeLink = _detailedScopeLink;
@synthesize contactsLink = _contactsLink;
@synthesize jobNotesLink = _jobNotesLink;
@synthesize picturesLink = _picturesLink;
@synthesize documentsLink = _documentsLink;
@synthesize totalPhotosCount = _totalPhotosCount;
@synthesize totalDocumentsCount = _totalDocumentsCount;
@synthesize totalContactsCount = _totalContactsCount;
@synthesize locationArray = _locationArray;
@synthesize ownerID = _ownerID;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_JobOrderDetail *)getJobOrdersJsonDictionary:(NSDictionary *)jobOrderDetailDict{
    
    if(!jobOrderDetailDict || [jobOrderDetailDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_JobOrderDetail *jobDetailDict = [[EG_JobOrderDetail alloc]init];
    
    jobDetailDict.jobOrderId            = [jobOrderDetailDict getStringForKey:@"Id"];
    
    jobDetailDict.clientName            = [jobOrderDetailDict getStringForKey:@"ClientName"];
    
    jobDetailDict.trackingDateLink  = [jobOrderDetailDict getStringForKey:@"TrackingDatesLink"];
    
    jobDetailDict.detailedScopeLink     = [jobOrderDetailDict getStringForKey:@"DetailedScopeLink"];
    
    jobDetailDict.contactsLink          = [jobOrderDetailDict getStringForKey:@"ContactsLink"];
    
    jobDetailDict.jobNotesLink          = [jobOrderDetailDict getStringForKey:@"JobNotesLink"];
    
    jobDetailDict.picturesLink          = [jobOrderDetailDict getStringForKey:@"PicturesLink"];
    
    jobDetailDict.documentsLink         = [jobOrderDetailDict getStringForKey:@"DocumentsLink"];
    
    jobDetailDict.totalContactsCount    = [jobOrderDetailDict getIntegerForKey:@"TotalContacts"];
    
    jobDetailDict.totalDocumentsCount   = [jobOrderDetailDict getIntegerForKey:@"TotalDocuments"];
    
    jobDetailDict.totalPhotosCount      = [jobOrderDetailDict getIntegerForKey:@"TotalPhotos"];
    
    NSArray *fieldsArray = [jobOrderDetailDict objectForKey:@"Fields"];
    
    NSMutableDictionary *fieldsDict = [self getFieldsDict:fieldsArray];
    
    jobDetailDict.jobTitle          = [fieldsDict getStringForKey:@"JobTitle"];
    jobDetailDict.clientName        = [fieldsDict getStringForKey:@"ClientName"];
    jobDetailDict.jobStatus         = [fieldsDict getStringForKey:@"StatusId"];
    
    NSArray *locations = [jobOrderDetailDict objectForKey:@"Locations"];
    
    if(![[jobOrderDetailDict objectForKey:@"Locations"] isEqual:[NSNull null]]){
        
        NSMutableArray *locationDictArray = [[NSMutableArray alloc]init];
        
        for (NSDictionary *currentLocation in locations) {
            
            EG_JobLocation *jobLocation = [EG_JobLocation getLocationsJsonDictionary:currentLocation];
            
            [locationDictArray addObject:jobLocation];
        }
        
        jobDetailDict.locationArray     = locationDictArray;
    }
    
    return jobDetailDict;
    
    
    
}
+(NSMutableDictionary *)getFieldsDict:(NSArray *)fields{
    
    NSMutableDictionary *fieldsDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in fields) {
        [fieldsDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return fieldsDict;
}

@end
