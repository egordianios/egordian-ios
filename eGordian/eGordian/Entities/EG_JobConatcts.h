//
//  EG_JobConatcts.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/22/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_JobConatcts : NSObject
+(EG_JobConatcts *)getJobContactJsonDictionary:(NSDictionary *)jobContactDict;
@property(nonatomic,retain) NSString* ownerId;
@property(nonatomic,retain) NSString* contactSuffix;
@property(nonatomic,retain) NSString* contactFirstName;
@property(nonatomic,retain) NSString* contactMiddleName;
@property(nonatomic,retain) NSString* contactLastName;
@property(nonatomic,retain) NSString* contactDisplayName;
@property(nonatomic,retain) NSString* contactRole;
@property(nonatomic,retain) NSString* contactBusinessPhone1;
@property(nonatomic,retain) NSString* contactBusinessPhone2;
@property(nonatomic,retain) NSString* contactBusinessMobile;
@property(nonatomic,retain) NSString* contactEmail;
@property(nonatomic,retain) NSString* companyId;
@property(nonatomic,retain) NSString* companyName;
@property(nonatomic,retain) NSString* contactLink;

@end
