//
//  EG_JobLocation.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/9/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EG_JobLocation.h"
#import "NSDictionary+JSON.h"

@implementation EG_JobLocation

@synthesize locationName= _locationName;
@synthesize locationId = _locationId;
@synthesize address1 = _address1;
@synthesize address2 = _address2;
@synthesize city = _city;
@synthesize state = _state;
@synthesize zipCode = _zipCode;
@synthesize lastUpdatedDate = _lastUpdatedDate;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_JobLocation *)getLocationsJsonDictionary:(NSDictionary *)jobLocationDict{
    
    if(!jobLocationDict || [jobLocationDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_JobLocation *currentJobLocationDict = [[EG_JobLocation alloc]init];
    
    currentJobLocationDict.locationId      = [jobLocationDict getStringForKey:@"Id"];
    
    NSArray *locationArray = [jobLocationDict objectForKey:@"Fields"];
    
    NSMutableDictionary *locationDict = [self getLocationsDict:locationArray];
    
    currentJobLocationDict.locationName    = [locationDict getStringForKey:@"LocationName"];
    currentJobLocationDict.address1        = [locationDict getStringForKey:@"Address1"];
    currentJobLocationDict.address2        = [locationDict getStringForKey:@"Address2"];
    currentJobLocationDict.city            = [locationDict getStringForKey:@"City"];
    currentJobLocationDict.state           = [locationDict getStringForKey:@"State"];
    currentJobLocationDict.zipCode         = [locationDict getStringForKey:@"ZipCode"];
    currentJobLocationDict.lastUpdatedDate = [locationDict getStringForKey:@"LastUpdatedDate"];
    
    return currentJobLocationDict;
}

+(NSMutableDictionary *)getLocationsDict:(NSArray *)locations{
    NSMutableDictionary *locationDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in locations) {
        [locationDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return locationDict;
}

@end
