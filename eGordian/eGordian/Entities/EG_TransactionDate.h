//
//  EG_TransactionDate.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_TransactionDate : NSObject
+(EG_TransactionDate *)getTranscationsJsonDictionary:(NSDictionary *)jobTransactionDict;
@property(nonatomic,retain) NSString* transcationId;
@property(nonatomic,retain) NSString* jobDescription;
@property(nonatomic,retain) NSString* jobPlannedDate;
@property(nonatomic,retain) NSString* jobAdjustedDate;
@property(nonatomic,retain) NSString* jobActualDate;
@property(nonatomic,retain) NSString* trackingDateLink;
@end
