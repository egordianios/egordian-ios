//
//  EG_ContactDetail.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/25/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EG_ContactDetail.h"
#import "NSDictionary+JSON.h"

@implementation EG_ContactDetail

@synthesize contactOwnerID      = _contactOwnerID;
@synthesize contactSuffix       = _contactSuffix;
@synthesize contactCompanyId    = _contactCompanyId;
@synthesize contactCompanyName  = _contactCompanyName;
@synthesize contactFirstName    = _contactFirstName;
@synthesize contactMiddleName   = _contactMiddleName;
@synthesize contactLastName     = _contactLastName;
@synthesize contactDisplayName  = _contactDisplayName;
@synthesize contactBusinessPhone1= contactBusinessPhone1;
@synthesize contactBusinessPhone2= _contactBusinessPhone2;
@synthesize contactMobilePhone  = _contactMobilePhone;
@synthesize contactEMail        = _contactEMail;
@synthesize contactAddress1     = _contactAddress1;
@synthesize contactAddress2     = _contactAddress2;
@synthesize contactCity         = _contactCity;
@synthesize contactState        = _contactState;
@synthesize contactZipCode      = _contactZipCode;
@synthesize contactId           = _contactId;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_ContactDetail *)getContactDetailJsonDictionary:(NSDictionary *)contacDetailDict{
    
    if(!contacDetailDict || [contacDetailDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_ContactDetail *detailedDict              = [[EG_ContactDetail alloc]init];
    
    detailedDict.contactId                      = [contacDetailDict getStringForKey:@"Id"];
    
    NSDictionary *companyDict                   = [contacDetailDict objectForKey:@"Company"];
    
    detailedDict.contactCompanyId               = [companyDict getStringForKey:@"CompanyId"];
    
    detailedDict.contactCompanyName             = [companyDict getStringForKey:@"CompanyName"];
    
    NSArray *fieldsArray = [contacDetailDict objectForKey:@"Fields"];
    
    NSMutableDictionary *fieldsDict = [self getOwnerContactDict:fieldsArray];
    
    detailedDict.contactOwnerID                 = [fieldsDict getStringForKey:@"OwnerId"];
    
    detailedDict.contactSuffix                  = [fieldsDict getStringForKey:@"Suffix"];
    
    detailedDict.contactFirstName               = [fieldsDict getStringForKey:@"FirstName"];
    
    detailedDict.contactMiddleName              = [fieldsDict getStringForKey:@"MiddleName"];
    
    detailedDict.contactLastName                = [fieldsDict getStringForKey:@"LastName"];
    
    detailedDict.contactDisplayName             = [fieldsDict getStringForKey:@"DisplayName"];
    
    detailedDict.contactBusinessPhone1          = [fieldsDict getStringForKey:@"BusinessPhone1"];
    
    detailedDict.contactBusinessPhone2          = [fieldsDict getStringForKey:@"BusinessPhone2"];
    
    detailedDict.contactMobilePhone             = [fieldsDict getStringForKey:@"MobilePhone"];
    
    detailedDict.contactEMail                   = [fieldsDict getStringForKey:@"Email"];
    
    detailedDict.contactAddress1                = [fieldsDict getStringForKey:@"Address1"];
    
    detailedDict.contactAddress2                = [fieldsDict getStringForKey:@"Address2"];
    
    detailedDict.contactCity                    = [fieldsDict getStringForKey:@"City"];
    
    detailedDict.contactState                   = [fieldsDict getStringForKey:@"State"];
    
    detailedDict.contactZipCode                 = [fieldsDict getStringForKey:@"ZipCode"];

    return detailedDict;
    
}
+(NSMutableDictionary *)getOwnerContactDict:(NSArray *)contacts{
    NSMutableDictionary *ownerContactDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in contacts) {
        [ownerContactDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return ownerContactDict;
}

@end
