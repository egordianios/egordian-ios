//
//  JobOrder+CoreDataProperties.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrder+CoreDataProperties.h"

@implementation JobOrder (CoreDataProperties)

@dynamic address1;
@dynamic address2;
@dynamic city;
@dynamic clientName;
@dynamic jobId;
@dynamic jobOrderLink;
@dynamic jobTitle;
@dynamic key;
@dynamic lastUpdatedDate;
@dynamic locationName;
@dynamic state;
@dynamic syncStatus;
@dynamic zipCode;
@dynamic ownerId;
@dynamic jobOrderDetail;
@dynamic jobScope;

@end
