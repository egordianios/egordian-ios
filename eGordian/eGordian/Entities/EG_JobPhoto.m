//
//  EG_JobPhoto.m
//  eGordian-iOS
//
//  Created by Laila on 05/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EG_JobPhoto.h"

@implementation EG_JobPhoto

@synthesize PictureName = _PictureName;
@synthesize Thumbnail = _Thumbnail;
@synthesize DownloadLink = _DownloadLink;
@synthesize fullSizeLink = _fullSizeLink;

#define ID @"Id"
#define pictureName @"Name"
#define thumbnail @"Thumbnail"
#define FullSizeLink @"FullSizeLink"
#define downloadLink @"DownloadLink"

-(EG_JobPhoto*) initWithPhotoDict:(NSDictionary*)photoDict
{
    self = [super init];
    
    if(self)
    {
        self.PictureName = [photoDict objectForKey:pictureName]?[photoDict objectForKey:pictureName]:@"";
        NSString * thumbnailString = [photoDict objectForKey:thumbnail]?[photoDict objectForKey:thumbnail]:@"";
        
        self.Thumbnail = nil;
        if(thumbnailString.length)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:thumbnailString options:0];
            self.Thumbnail = decodedData;
        }
//        self.Thumbnail = nil;
        self.fullSizeLink = [photoDict objectForKey:FullSizeLink]?[photoDict objectForKey:FullSizeLink]:@"";
        self.DownloadLink = [photoDict objectForKey:downloadLink]?[photoDict objectForKey:downloadLink]:@"" ;
        
    }
    
    return self;
}
@end
