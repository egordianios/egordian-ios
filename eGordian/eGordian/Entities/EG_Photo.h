//
//  EG_Photo.h
//  eGordian-iOS
//
//  Created by Laila on 15/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_Photo : NSObject

@property(nonatomic,retain) NSString* Id;
@property(nonatomic,retain) NSString* PictureName;
@property(nonatomic,retain) NSData* Thumbnail;
@property(nonatomic,retain) NSString* UserId;
@property(nonatomic,retain) NSString* UserPictureLink;

-(EG_Photo*) initWithPhotoDict:(NSDictionary*)photoDict;
@end
