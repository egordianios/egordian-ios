//
//  JobOrder+CoreDataProperties.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrder.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobOrder (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address1;
@property (nullable, nonatomic, retain) NSString *address2;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *clientName;
@property (nullable, nonatomic, retain) NSString *jobId;
@property (nullable, nonatomic, retain) NSString *jobOrderLink;
@property (nullable, nonatomic, retain) NSString *jobTitle;
@property (nullable, nonatomic, retain) NSString *key;
@property (nonatomic) NSTimeInterval lastUpdatedDate;
@property (nullable, nonatomic, retain) NSString *locationName;
@property (nullable, nonatomic, retain) NSString *state;
@property (nonatomic) int16_t syncStatus;
@property (nullable, nonatomic, retain) NSString *zipCode;
@property (nullable, nonatomic, retain) NSString *ownerId;
@property (nullable, nonatomic, retain) JobOrderDetail *jobOrderDetail;
@property (nullable, nonatomic, retain) JobOrderScope *jobScope;

@end

NS_ASSUME_NONNULL_END
