//
//  StringtoAttributedString.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/4/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "StringtoAttributedString.h"

@implementation StringtoAttributedString
+(Class)transformedValueClass
{
    return [NSAttributedString class];
}

+(void)initialize {
    [NSValueTransformer setValueTransformer:[[self alloc]init] forName:@"NSAttributedStringValueTransformer"];
}

+(BOOL) allowsReverseTransformation
{
    return NO;
}

-(NSAttributedString *)transformedValue:(id) value
{
    if(value == nil)
        return nil;
    NSError *error ;
    NSData *data = [value dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *attribString = [[NSAttributedString alloc] initWithData:data
                               
                                                               options:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType,
                                                                         NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}
                                                             documentAttributes:nil error:&error];
    return attribString;
}
/*
-(NSAttributedString *)reverseTransformedValue:(id) value
{
    NSError *error ;
    NSAttributedString *str = [[NSAttributedString alloc] initWithData:value
                               
                                                               options:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType,
                                                                         NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}
                                                    documentAttributes:nil error:&error];

    return str;
}

-(NSAttributedString*)reverseTransformedValue:(NSData*)value {
    NSAttributedString* string = [NSKeyedUnarchiver unarchiveObjectWithData:value];
    
    return string;
}
 */
@end
