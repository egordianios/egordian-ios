//
//  EG_PhotoDetail.h
//  eGordian-iOS
//
//  Created by Laila on 14/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_PhotoDetail : NSObject

@property(nonatomic,retain) NSString* PictureID;
@property(nonatomic,retain) NSData* Image;
@property(nonatomic,strong) NSString* PictureName;
@property(nonatomic,strong) NSString* Thumbnail;
@property(nonatomic,strong) NSString* userID;

-(EG_PhotoDetail*) initWithPhotoDict:(NSDictionary*)photoDetailDict;

@end
