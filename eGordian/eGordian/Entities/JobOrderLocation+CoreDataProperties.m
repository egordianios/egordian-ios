//
//  JobOrderLocation+CoreDataProperties.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderLocation+CoreDataProperties.h"

@implementation JobOrderLocation (CoreDataProperties)

@dynamic locationName;
@dynamic address1;
@dynamic address2;
@dynamic city;
@dynamic state;
@dynamic zipCode;
@dynamic lastUpdatedDate;
@dynamic locationId;
@dynamic key;
@dynamic jobOrderDetail;

@end
