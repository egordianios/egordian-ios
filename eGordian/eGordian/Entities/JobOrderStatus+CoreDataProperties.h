//
//  JobOrderStatus+CoreDataProperties.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderStatus.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobOrderStatus (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *jobStatusId;
@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) NSString *jobStatusName;
@property (nullable, nonatomic, retain) JobOrderDetail *jobOrderDetail;

@end

NS_ASSUME_NONNULL_END
