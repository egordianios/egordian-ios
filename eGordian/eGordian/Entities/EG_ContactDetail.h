//
//  EG_ContactDetail.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/25/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_ContactDetail : NSObject

@property (nonatomic, copy) NSString *contactOwnerID;
@property (nonatomic, copy) NSString *contactSuffix;
@property (nonatomic, copy) NSString *contactCompanyId;
@property (nonatomic, copy) NSString *contactCompanyName;
@property (nonatomic, copy) NSString *contactFirstName;
@property (nonatomic, copy) NSString *contactMiddleName;
@property (nonatomic, copy) NSString *contactLastName;
@property (nonatomic, copy) NSString *contactDisplayName;
@property (nonatomic, copy) NSString *contactBusinessPhone1;
@property (nonatomic, copy) NSString *contactBusinessPhone2;
@property (nonatomic, copy) NSString *contactMobilePhone;
@property (nonatomic, copy) NSString *contactEMail;
@property (nonatomic, copy) NSString *contactAddress1;
@property (nonatomic, copy) NSString *contactAddress2;
@property (nonatomic, copy) NSString *contactCity;
@property (nonatomic, copy) NSString *contactState;
@property (nonatomic, copy) NSString *contactZipCode;
@property (nonatomic, copy) NSString *contactId;


+(EG_ContactDetail *)getContactDetailJsonDictionary:(NSDictionary *)contacDetailDict;

@end
