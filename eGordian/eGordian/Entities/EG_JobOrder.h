//
//  EG_JobOrder.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/27/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_JobOrder : NSObject

+(EG_JobOrder *)getJobOrdersJsonDictionary:(NSDictionary *)jobOrderDict;
+(NSMutableArray*) sortJobs:(NSArray*)jobs WithSortKey:(NSString*)Key;

@property(nonatomic,retain) NSString* jobOrderId;
@property(nonatomic,retain) NSString* jobOrderNumber;
@property(nonatomic,retain) NSString* jobTitle;
@property(nonatomic,retain) NSString* ClientName;
@property(nonatomic,retain) NSString* locationName;
@property(nonatomic,retain) NSString* address1;
@property(nonatomic,retain) NSString* address2;
@property(nonatomic,retain) NSString* City;
@property(nonatomic,retain) NSString* State;
@property(nonatomic,retain) NSString* ZipCode;
@property(nonatomic,retain) NSString* LastUpdatedDate;
@property(nonatomic,retain) NSString* jobOrderLink;
@property(nonatomic, strong) NSString* ownerID;

@end
