//
//  EG_JobConatcts.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/22/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EG_JobConatcts.h"
#import "NSDictionary+JSON.h"
@implementation EG_JobConatcts

@synthesize ownerId             = _ownerId;
@synthesize contactSuffix       = _contactSuffix;
@synthesize contactFirstName    = _contactFirstName;
@synthesize contactMiddleName   = _contactMiddleName;
@synthesize contactLastName     = _contactLastName;
@synthesize contactDisplayName  = _contactDisplayName;
@synthesize contactRole         = _contactRole;
@synthesize contactBusinessPhone1= _contactBusinessPhone1;
@synthesize contactBusinessPhone2= _contactBusinessPhone2;
@synthesize contactBusinessMobile= _contactBusinessMobile;
@synthesize contactEmail        = _contactEmail;
@synthesize companyId           = _companyId;
@synthesize companyName         = _companyName;
@synthesize contactLink         = _contactLink;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_JobConatcts *)getJobContactJsonDictionary:(NSDictionary *)jobContactDict{
    
    if(!jobContactDict || [jobContactDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_JobConatcts *jobContact = [[EG_JobConatcts alloc]init];
    
    jobContact.contactLink              = [jobContactDict getStringForKey:@"ContactLink"];
    
    NSArray *contacts                   = [jobContactDict objectForKey:@"Fields"];
    
    NSMutableDictionary *currentContactDict = [self getContactDict:contacts];
    
    jobContact.ownerId                  = [currentContactDict getStringForKey:@"ownerId"];
    
    jobContact.contactSuffix            = [currentContactDict getStringForKey:@"Suffix"];
    
    jobContact.contactFirstName         = [currentContactDict getStringForKey:@"FirstName"];
    
    jobContact.contactMiddleName        = [currentContactDict getStringForKey:@"MiddleName"];
    
    jobContact.contactLastName          = [currentContactDict getStringForKey:@"LastName"];
    
    jobContact.contactDisplayName       = [currentContactDict getStringForKey:@"DisplayName"];
    
    jobContact.contactRole              = [currentContactDict getStringForKey:@"Role"];
    
    jobContact.contactBusinessPhone1    = [currentContactDict getStringForKey:@"BusinessPhone1"];
    
    jobContact.contactBusinessPhone2    = [currentContactDict getStringForKey:@"BusinessPhone2"];
    
    jobContact.contactBusinessMobile    = [currentContactDict getStringForKey:@"MobilePhone"];
    
    jobContact.contactEmail             = [currentContactDict getStringForKey:@"Email"];
    
    NSMutableDictionary *currentCompanyDict = [jobContactDict objectForKey:@"Company"];
    
    jobContact.companyId                = [currentCompanyDict getStringForKey:@"CompanyId"];

    jobContact.companyName              = [currentCompanyDict getStringForKey:@"CompanyName"];
    
    return jobContact;
    
}
+(NSMutableDictionary *)getContactDict:(NSArray *)contacts{
    NSMutableDictionary *transactionDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in contacts) {
        [transactionDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return transactionDict;
}
@end
