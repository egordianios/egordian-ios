//
//  JobOrderStatus.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobOrderDetail;

NS_ASSUME_NONNULL_BEGIN

@interface JobOrderStatus : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "JobOrderStatus+CoreDataProperties.h"
