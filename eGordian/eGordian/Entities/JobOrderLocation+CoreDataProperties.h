//
//  JobOrderLocation+CoreDataProperties.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderLocation.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobOrderLocation (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *locationName;
@property (nullable, nonatomic, retain) NSString *address1;
@property (nullable, nonatomic, retain) NSString *address2;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *state;
@property (nullable, nonatomic, retain) NSString *zipCode;
@property (nonatomic) NSTimeInterval lastUpdatedDate;
@property (nullable, nonatomic, retain) NSString *locationId;
@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) JobOrderDetail *jobOrderDetail;

@end

NS_ASSUME_NONNULL_END
