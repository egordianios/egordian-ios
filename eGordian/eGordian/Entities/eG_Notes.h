//
//  eG_Notes.h
//  eGordian-iOS
//
//  Created by Laila on 24/11/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface eG_Notes : NSObject

extern NSString *const createdBy;
extern NSString *const createdDate;

@property(nonatomic,retain) NSString* CreatedBy;
@property(nonatomic,retain) NSString* CreatedDate;
@property(nonatomic,retain) NSString* Id ;
@property(nonatomic,assign) BOOL IsHidden;
@property(nonatomic,retain) NSString* JobOrderId;
@property(nonatomic,retain) NSString* Subject;
@property(nonatomic,retain) NSString* UpdatedBy;
@property(nonatomic,retain) NSString* UpdatedDate;
@property(nonatomic,retain) NSString* UserId;
@property(nonatomic,retain) NSString* Value;
@property(nonatomic, retain) NSString* jobID;

+(eG_Notes *)getJobNotesJsonDictionary:(NSDictionary *)noteDict;
@end
