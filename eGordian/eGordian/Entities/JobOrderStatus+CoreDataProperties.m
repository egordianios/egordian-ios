//
//  JobOrderStatus+CoreDataProperties.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderStatus+CoreDataProperties.h"

@implementation JobOrderStatus (CoreDataProperties)

@dynamic jobStatusId;
@dynamic key;
@dynamic jobStatusName;
@dynamic jobOrderDetail;

@end
