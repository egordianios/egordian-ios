//
//  EG_PhotoDetail.m
//  eGordian-iOS
//
//  Created by Laila on 14/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EG_PhotoDetail.h"

@implementation EG_PhotoDetail

@synthesize PictureID = _PictureID;
@synthesize Image = _Image;
@synthesize PictureName = _PictureName;
@synthesize Thumbnail = _Thumbnail;
@synthesize userID = _userID;

#define ID @"Id"
#define pictureName @"PictureName"
#define thumbnail @"Thumbnail"
#define userId @"UserId"
#define image @"Image"

-(EG_PhotoDetail*) initWithPhotoDict:(NSDictionary*)photoDict
{
    self = [super init];
    
    if(self)
    {
        self.PictureName = [photoDict objectForKey:pictureName]?[photoDict objectForKey:pictureName]:@"";
        NSString * imageStr = [photoDict objectForKey:image]?[photoDict objectForKey:image]:@"";
        
        self.Image = nil;
        if(imageStr.length)
        {
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:imageStr options:0];
            self.Image = decodedData;
        }
        self.Thumbnail = nil;
        self.userID = [photoDict objectForKey:userId]?[photoDict objectForKey:userId]:@"";
        self.PictureID = [photoDict objectForKey:ID]?[photoDict objectForKey:ID]:@"";
    }
    
    return self;
}

@end
