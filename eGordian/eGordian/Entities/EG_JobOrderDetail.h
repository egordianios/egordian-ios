//
//  EG_JobOrderDetail.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/9/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_JobOrderDetail : NSObject

+(EG_JobOrderDetail *)getJobOrdersJsonDictionary:(NSDictionary *)jobOrderDict;
@property(nonatomic,retain) NSString* jobOrderId;
@property(nonatomic,retain) NSString* jobOrderNumber;
@property(nonatomic,retain) NSString* jobTitle;
@property(nonatomic,retain) NSString* clientName;
@property(nonatomic,retain) NSString* jobStatus;
@property(nonatomic,retain) NSMutableArray *locationArray;
@property(nonatomic,retain) NSString* trackingDateLink;
@property(nonatomic,retain) NSString* detailedScopeLink;
@property(nonatomic,retain) NSString* contactsLink;
@property(nonatomic,retain) NSString* jobNotesLink;
@property(nonatomic,retain) NSString* picturesLink;
@property(nonatomic,retain) NSString* documentsLink;
@property(nonatomic,assign) NSUInteger totalPhotosCount;
@property(nonatomic,assign) NSUInteger totalDocumentsCount;
@property(nonatomic,assign) NSUInteger totalContactsCount;
@property (nonatomic, retain) NSString* ownerID;
@end
