//
//  Owner.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 8/11/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Owner : NSObject

@property (nonatomic,retain) NSString  *CompanyID;
@property (nonatomic,retain) NSString  *CompanyName;
@property (nonatomic,retain) NSString  *OwnerID;
@property (nonatomic,retain) NSString  *OwnerName;
@property (nonatomic,retain) NSString  *Side;

+(Owner *)getOwnerAndContractorJsonDictionary:(NSDictionary *)ownerDict;

@end
