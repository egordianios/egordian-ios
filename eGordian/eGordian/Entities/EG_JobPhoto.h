//
//  EG_JobPhoto.h
//  eGordian-iOS
//
//  Created by Laila on 05/01/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_JobPhoto : NSObject

@property(nonatomic,retain) NSString* PictureName;
@property(nonatomic,retain) NSData* Thumbnail;
@property(nonatomic,strong) NSString* DownloadLink;
@property(nonatomic,strong) NSString* fullSizeLink;

-(EG_JobPhoto*) initWithPhotoDict:(NSDictionary*)photoDict;

@end
