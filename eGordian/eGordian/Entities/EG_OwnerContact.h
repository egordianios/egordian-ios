//
//  EG_OwnerContact.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/25/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_OwnerContact : NSObject

@property (nonatomic, retain) NSString *contactId;
@property (nonatomic, retain) NSString *FirstName;
@property (nonatomic, retain) NSString *LastName;
@property (nonatomic, retain) NSString *contactMiddleName;
@property (nonatomic, retain) NSString *contactDisplayName;
@property (nonatomic, retain) NSString *contactOwnerID;
@property (nonatomic, retain) NSString *contactCompanyName;
@property (nonatomic, retain) NSString *contactSuffix;
@property (nonatomic, retain) NSString *contactDetailLink;

+(EG_OwnerContact *)getOwnerContactJsonDictionary:(NSDictionary *)ownerContactDict;

@end
