//
//  JobOrderDetail+CoreDataProperties.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/5/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderDetail+CoreDataProperties.h"

@implementation JobOrderDetail (CoreDataProperties)

@dynamic contactsLink;
@dynamic detailedScopeLink;
@dynamic documentsLink;
@dynamic jobTitle;
@dynamic clientName;
@dynamic jobStatusId;
@dynamic jobId;
@dynamic jobNotesLink;
@dynamic picturesLink;
@dynamic totalContactsCount;
@dynamic totalDocumentsCount;
@dynamic totalPhotosCount;
@dynamic trackingDatesLink;
@dynamic key;
@dynamic statusUrl;
@dynamic jobOrder;
@dynamic jobLocations;
@dynamic jobStatus;

@end
