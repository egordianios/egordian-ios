//
//  JobOrderScope+CoreDataProperties.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/4/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderScope+CoreDataProperties.h"

@implementation JobOrderScope (CoreDataProperties)

@dynamic title;
@dynamic jobScopeDescription;
@dynamic scopeStatus;
@dynamic jobId;
@dynamic key;
@dynamic jobOrder;

@end
