//
//  EGJobDocument.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 12/30/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EG_JobDocument.h"
#import "NSDictionary+JSON.h"

@implementation EG_JobDocument

@synthesize contextClassifier   = _contextClassifier;
@synthesize documentTitle       = _documentTitle;
@synthesize comments            = _comments;
@synthesize fileName            = _fileName;
@synthesize fileUrl             = _fileUrl;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_JobDocument *)getJobDocumentsJsonDictionary:(NSDictionary *)jobDocumentDict;{
    
    
    if(!jobDocumentDict || [jobDocumentDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_JobDocument *currentJobDocumentDict = [[EG_JobDocument alloc]init];
    
    currentJobDocumentDict.contextClassifier    = [jobDocumentDict getStringForKey:@"ContextClassifier"];
    
    currentJobDocumentDict.documentTitle        = [jobDocumentDict getStringForKey:@"DocumentTitle"];
    
    currentJobDocumentDict.comments             = [jobDocumentDict getStringForKey:@"Comments"];
    
    currentJobDocumentDict.fileName             = [jobDocumentDict getStringForKey:@"FileName"];
    
    currentJobDocumentDict.fileUrl              = [jobDocumentDict getStringForKey:@"FileUrl"];
    
    return currentJobDocumentDict;
}

@end
