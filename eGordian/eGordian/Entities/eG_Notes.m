//
//  eG_Notes.m
//  eGordian-iOS
//
//  Created by Laila on 24/11/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "eG_Notes.h"
#import "NSDictionary+JSON.h"

@implementation eG_Notes

@synthesize CreatedBy = _CreatedBy;
@synthesize CreatedDate = _CreatedDate;
@synthesize Id = _Id;
@synthesize IsHidden = _IsHidden;
@synthesize JobOrderId = _JobOrderId;
@synthesize Subject = _Subject;
@synthesize UpdatedBy = _UpdatedBy;
@synthesize UpdatedDate = _UpdatedDate;
@synthesize UserId = _UserId;
@synthesize Value = _Value;
@synthesize jobID;

NSString *const createdBy = @"CreatedBy";
NSString *const createdDate = @"CreatedDate";
#define ID @"Id"
#define isHidden @"IsHidden"
#define jobOrderId @"JobOrderId"
#define subject @"Subject"
#define updatedBy @"UpdatedBy"
#define updatedDate @"UpdatedDate"
#define userId @"UserId"
#define value @"Value"

+(eG_Notes *)getJobNotesJsonDictionary:(NSDictionary *)noteDict{
    
    if(!noteDict || [noteDict isEqual:[NSNull null]]){
        return nil;
    }
    
    eG_Notes *currentNotesDict = [[eG_Notes alloc]init];
    
    currentNotesDict.CreatedBy = [noteDict getStringForKey:createdBy];
    if(![noteDict getStringForKey:@"UpdatedDate"]){
        currentNotesDict.CreatedDate = [self formatDate:[noteDict getStringForKey:createdDate]];
    }else{
        currentNotesDict.CreatedDate = [self formatDate:[noteDict getStringForKey:@"UpdatedDate"]];
    }
    
    currentNotesDict.Id = [noteDict getStringForKey:ID] ;
    currentNotesDict.IsHidden = [noteDict getBoolForKey:isHidden];
    currentNotesDict.JobOrderId = [noteDict getStringForKey:jobOrderId] ;
    currentNotesDict.Subject = [noteDict getStringForKey:subject] ;
    currentNotesDict.UpdatedBy = [noteDict getStringForKey:updatedBy];
    currentNotesDict.UpdatedDate = [noteDict getStringForKey:updatedDate];
    currentNotesDict.UserId = [noteDict getStringForKey:userId] ;
    currentNotesDict.Value = [noteDict getStringForKey:value] ;
    
    
    return currentNotesDict;
}

/*
 * Method Name	: formatDate
 * Description	: format the date to the format which should be displayed
 * Parameters	: date - the unformated date
 * Return value	: formated date which is to be displayed in the UI
 */
+(NSString*) formatDate:(NSString*)dateStr
{
    NSString* formattedDateStr = @"";
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    dateFormat.dateFormat = @"MM/dd/yy hh:mm a";
    formattedDateStr = [dateFormat stringFromDate:date];
    formattedDateStr = [NSString stringWithFormat:@"%@", formattedDateStr];
    
    /*
    // Your dates:
    NSDate * today = [NSDate date];
    NSDate * yesterday = [NSDate dateWithTimeIntervalSinceNow:-86400]; //86400 is the seconds in a day
    NSDate * refDate = date; // your reference date
    
    // 10 first characters of description is the calendar date:
    NSString * todayString = [[today description] substringToIndex:10];
    NSString * yesterdayString = [[yesterday description] substringToIndex:10];
    NSString * refDateString = [[refDate description] substringToIndex:10];
    
    if ([refDateString isEqualToString:todayString])
    {
        dateFormat.dateFormat = @"M/d/yy";
        formattedDateStr = [dateFormat stringFromDate:date];
    }
    else if ([refDateString isEqualToString:yesterdayString])
    {
        dateFormat.dateFormat = @"hh:mm a";
        formattedDateStr = [dateFormat stringFromDate:date];
        formattedDateStr = [NSString stringWithFormat:@"Yesterday, %@", formattedDateStr];
    } else
    {
        dateFormat.dateFormat = @"hh mm a";
        formattedDateStr = [dateFormat stringFromDate:date];
    }
     
     */
    
    return formattedDateStr;
}
@end
