//
//  JobOrder.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobOrderDetail, JobOrderScope;

NS_ASSUME_NONNULL_BEGIN

@interface JobOrder : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "JobOrder+CoreDataProperties.h"
