//
//  EG_OwnerContact.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 1/25/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "EG_OwnerContact.h"
#import "NSDictionary+JSON.h"

@implementation EG_OwnerContact

@synthesize contactId           = _contactId;
@synthesize FirstName           = _FirstName;
@synthesize LastName            = _LastName;
@synthesize contactMiddleName   = _contactMiddleName;
@synthesize contactDisplayName  = _contactDisplayName;
@synthesize contactOwnerID      = _contactOwnerID;
@synthesize contactCompanyName  = _contactCompanyName;
@synthesize contactSuffix       = _contactSuffix;
@synthesize contactDetailLink   = _contactDetailLink;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_OwnerContact *)getOwnerContactJsonDictionary:(NSDictionary *)ownerContactDict{
    
    if(!ownerContactDict || [ownerContactDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_OwnerContact *ownerContact           =     [[EG_OwnerContact alloc]init];
    
    NSArray *contacts                       = [ownerContactDict objectForKey:@"Fields"];
    
    NSMutableDictionary *currentOwnerContactDict = [self getOwnerContactDict:contacts];
    
    ownerContact.contactOwnerID             = [currentOwnerContactDict getStringForKey:@"ownerId"];
    
    ownerContact.contactSuffix              = [currentOwnerContactDict getStringForKey:@"Suffix"];
    
    ownerContact.FirstName                  = [currentOwnerContactDict getStringForKey:@"FirstName"];
    
    ownerContact.contactMiddleName          = [currentOwnerContactDict getStringForKey:@"MiddleName"];
    
    ownerContact.LastName                   = [currentOwnerContactDict getStringForKey:@"LastName"];
    
    ownerContact.contactDisplayName         = [currentOwnerContactDict getStringForKey:@"DisplayName"];
    
    ownerContact.contactId                  = [ownerContactDict getStringForKey:@"Id"];
    
    ownerContact.contactDetailLink          = [ownerContactDict getStringForKey:@"ContactLink"];
        
    return ownerContact;
}


+(NSMutableDictionary *)getOwnerContactDict:(NSArray *)contacts{
    NSMutableDictionary *ownerContactDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in contacts) {
        [ownerContactDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return ownerContactDict;
}

@end
