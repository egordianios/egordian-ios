//
//  EG_Photo.m
//  eGordian-iOS
//
//  Created by Laila on 15/12/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "EG_Photo.h"
#import "NSData+Base64.h"

@implementation EG_Photo

@synthesize Id = _Id;
@synthesize PictureName = _PictureName;
@synthesize Thumbnail = _Thumbnail;
@synthesize UserId = _UserId;
@synthesize UserPictureLink = _UserPictureLink;

#define ID @"Id"
#define pictureName @"PictureName"
#define thumbnail @"Thumbnail"
#define userId @"UserId"
#define userPicLink @"UserPictureLink"

-(EG_Photo*) initWithPhotoDict:(NSDictionary*)photoDict
{
    self = [super init];
    
//    NSMutableArray* photosMutArr = [photoArr mutableCopy];
    if(self)
    {
        self.Id = [photoDict objectForKey:ID];
        self.PictureName = [photoDict objectForKey:pictureName];
        
        
        NSString * thumbnailString = [photoDict objectForKey:thumbnail];
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:thumbnailString options:0];
        self.Thumbnail = decodedData;
        self.UserId = [photoDict objectForKey:userId];
        self.UserPictureLink = [photoDict objectForKey:userPicLink] ;
     
    }
    
    return self;
}
@end
