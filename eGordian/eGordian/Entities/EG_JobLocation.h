//
//  EG_JobLocation.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/9/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EG_JobLocation : NSObject

+(EG_JobLocation *)getLocationsJsonDictionary:(NSDictionary *)jobLocationDict;
@property(nonatomic,retain) NSString* locationId;
@property(nonatomic,retain) NSString* locationName;
@property(nonatomic,retain) NSString* address1;
@property(nonatomic,retain) NSString* address2;
@property(nonatomic,retain) NSString* city;
@property(nonatomic,retain) NSString* state;
@property(nonatomic,retain) NSString* zipCode;
@property(nonatomic,retain) NSString* lastUpdatedDate;
@end
