//
//  EG_TransactionDate.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/10/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "EG_TransactionDate.h"
#import "NSDictionary+JSON.h"

@implementation EG_TransactionDate
@synthesize transcationId       = _transcationId;
@synthesize jobDescription      = _jobDescription;
@synthesize jobPlannedDate      = _jobPlannedDate;
@synthesize jobAdjustedDate     = _jobAdjustedDate;
@synthesize jobActualDate       = _jobActualDate;
@synthesize trackingDateLink    = _trackingDateLink;

-(id)init
{
    self=[super init];
    if (self)    {
    }
    return self;
}

+(EG_TransactionDate *)getTranscationsJsonDictionary:(NSDictionary *)jobTransactionDict{
    if(!jobTransactionDict || [jobTransactionDict isEqual:[NSNull null]]){
        return nil;
    }
    
    EG_TransactionDate *transactionDate = [[EG_TransactionDate alloc]init];
    
    transactionDate.transcationId       = [jobTransactionDict getStringForKey:@"Id"];
    
    transactionDate.trackingDateLink    = [jobTransactionDict getStringForKey:@"TrackingDateLink"];
    
    NSArray *transactions               = [jobTransactionDict objectForKey:@"Fields"];
    
    NSMutableDictionary *currentTransactionDict = [self getTransactionDict:transactions];
    
    transactionDate.jobDescription      = [currentTransactionDict getStringForKey:@"Description"];
    transactionDate.jobPlannedDate      = [currentTransactionDict getStringForKey:@"Planned Date"];
    transactionDate.jobActualDate       = [currentTransactionDict getStringForKey:@"Actual Date"];
    transactionDate.jobAdjustedDate     = [currentTransactionDict getStringForKey:@"Adjusted Date"];
    
    
    return transactionDate;
}

+(NSMutableDictionary *)getTransactionDict:(NSArray *)transactions{
    NSMutableDictionary *transactionDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in transactions) {
        [transactionDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return transactionDict;
}

@end
