//
//  AttachPhoto+CoreDataProperties.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/8/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "AttachPhoto+CoreDataProperties.h"

@implementation AttachPhoto (CoreDataProperties)

@dynamic imageData;
@dynamic jobId;
@dynamic photoFilePath;
@dynamic pictureName;
@dynamic thumbnailData;
@dynamic userID;
@dynamic syncStatus;
@dynamic ownerId;

@end
