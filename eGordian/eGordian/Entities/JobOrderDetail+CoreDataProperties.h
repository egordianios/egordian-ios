//
//  JobOrderDetail+CoreDataProperties.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/5/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "JobOrderDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface JobOrderDetail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *contactsLink;
@property (nullable, nonatomic, retain) NSString *detailedScopeLink;
@property (nullable, nonatomic, retain) NSString *documentsLink;
@property (nullable, nonatomic, retain) NSString *jobTitle;
@property (nullable, nonatomic, retain) NSString *clientName;
@property (nullable, nonatomic, retain) NSString *jobStatusId;
@property (nullable, nonatomic, retain) NSString *jobId;
@property (nullable, nonatomic, retain) NSString *jobNotesLink;
@property (nullable, nonatomic, retain) NSString *picturesLink;
@property (nonatomic) int64_t totalContactsCount;
@property (nonatomic) int64_t totalDocumentsCount;
@property (nonatomic) int64_t totalPhotosCount;
@property (nullable, nonatomic, retain) NSString *trackingDatesLink;
@property (nullable, nonatomic, retain) NSString *key;
@property (nullable, nonatomic, retain) NSString *statusUrl;
@property (nullable, nonatomic, retain) JobOrder *jobOrder;
@property (nullable, nonatomic, retain) NSSet<JobOrderLocation *> *jobLocations;
@property (nullable, nonatomic, retain) NSSet<JobOrderStatus *> *jobStatus;

@end

@interface JobOrderDetail (CoreDataGeneratedAccessors)

- (void)addJobLocationsObject:(JobOrderLocation *)value;
- (void)removeJobLocationsObject:(JobOrderLocation *)value;
- (void)addJobLocations:(NSSet<JobOrderLocation *> *)values;
- (void)removeJobLocations:(NSSet<JobOrderLocation *> *)values;

- (void)addJobStatusObject:(JobOrderStatus *)value;
- (void)removeJobStatusObject:(JobOrderStatus *)value;
- (void)addJobStatus:(NSSet<JobOrderStatus *> *)values;
- (void)removeJobStatus:(NSSet<JobOrderStatus *> *)values;

@end

NS_ASSUME_NONNULL_END
