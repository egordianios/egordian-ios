//
//  AppDelegate.m
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "EGContainerViewController.h"
#import "TAGContainer.h"
#import "TAGContainerOpener.h"
#import "TAGLogger.h"
#import "TAGManager.h"
#import "JobOrderDAO.h"
#import "AttachPhotoDAO.h"

@import GoogleMaps;
@import Crashlytics;
@import Fabric;
Reachability *reachability;

@interface AppDelegate ()<TAGContainerOpenerNotifier>{
    
    BOOL inboundSyncShouldStart;
}

@end

@implementation AppDelegate

@synthesize tagManager = _tagManager, isFromJobDetails;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.isFromJobDetails = FALSE;
    
    [Fabric with:@[CrashlyticsKit]];
    self.tagManager = [TAGManager instance];
    inboundSyncShouldStart =NO;
    
    // Optional: Change the LogLevel to Verbose to enable logging at VERBOSE and higher levels.
    [self.tagManager.logger setLogLevel:kTAGLoggerLogLevelVerbose];
//    self.jobOrder=nil;
    /*
     * Opens a container.
     *
     * @param containerId The ID of the container to load.
     * @param tagManager The TAGManager instance for getting the container.
     * @param openType The choice of how to open the container.
     * @param timeout The timeout period (default is 2.0 seconds).
     * @param notifier The notifier to inform on container load events.
     */
    [TAGContainerOpener openContainerWithId:APP_GTM_CONTAINER_ID   // Update with your Container ID.
                                 tagManager:self.tagManager
                                   openType:kTAGOpenTypePreferFresh
                                    timeout:nil
                                   notifier:self];
    
    //Google Maps
    
    [GMSServices provideAPIKey:APP_GOOGLE_MAPS_API_KEY];
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [self resetRootViewController];
    //[CoreDataBase clearUserData];
    
    return YES;
}

- (void)resetRootViewController
{
    self.window.rootViewController = nil;
    
    if(self.objUINavigationController)
        self.objUINavigationController = nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:APP_AUTHORIZATION_PREFERENCE]){
        EGContainerViewController *containerVC = [storyboard instantiateViewControllerWithIdentifier:@"EGContainerViewController"];
        self.objUINavigationController = [[UINavigationController alloc] initWithRootViewController:containerVC];
        [self.window setRootViewController:self.objUINavigationController];
        [self.objUINavigationController setNavigationBarHidden:YES];
        [self.window makeKeyAndVisible];
    }
    else{
        ViewController *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        self.objUINavigationController = [[UINavigationController alloc] initWithRootViewController:loginVC];
        [self.window setRootViewController:self.objUINavigationController];
        [self.objUINavigationController setNavigationBarHidden:YES];
        [self.window makeKeyAndVisible];
    }
}

+ (BOOL)isConnectedToInternet
{
    if (!reachability) {
        reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
    }
    return ([reachability currentReachabilityStatus] != NotReachable);
}
// TAGContainerOpenerNotifier callback.
- (void)containerAvailable:(TAGContainer *)container {
    // Note that containerAvailable may be called on any thread, so you may need to dispatch back to
    // your main thread.
    dispatch_async(dispatch_get_main_queue(), ^{
        self.container = container;
    });
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:kReachabilityChangedNotification
//                                                  object:nil];
//    [reachability stopNotifier];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (reachability) {
        reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
    }
}

- (void)reachabilityChanged:(NSNotification *)notice {
    // called after network status changes
    NetworkStatus internetStatus = [reachability  currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            break;
        }
        case ReachableViaWiFi:
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN!");
            if([[NSUserDefaults standardUserDefaults] objectForKey:APP_AUTHORIZATION_PREFERENCE]){
                if([[SyncManager sharedManager] syncOnNetworkAwake]){
                    
                    
                    if(![[SyncManager sharedManager]isSyncing]){
                        [[SyncManager sharedManager]setStatusDelegate:self];
                        [[SyncManager sharedManager] syncOut];
                        inboundSyncShouldStart =YES;
                    }
                }
                else{
                    if(![[SyncManager sharedManager]isSyncing]){
                        [[SyncManager sharedManager]setStatusDelegate:self];
                        [[SyncManager sharedManager] fullSync];
                        inboundSyncShouldStart =NO;
                    }
                }
            }
            break;
        }
            
            default:
            break;
    }
    
//    NetworkStatus hostStatus = [reachability currentReachabilityStatus];
//    switch (hostStatus)
//    {
//        case NotReachable:
//        {
//            NSLog(@"A gateway to the host server is down.");
//            //            self.hostActive = NO;
//            
//            break;
//        }
//        case ReachableViaWiFi:
//        {
//            NSLog(@"A gateway to the host server is working via WIFI.");
//            //            self.hostActive = YES;
//            
//            break;
//        }
//        case ReachableViaWWAN:
//        {
//            NSLog(@"A gateway to the host server is working via WWAN.");
//            //            self.hostActive = YES;
//            
//            break;
//        }
//    }
}

-(void)callInboundSync{
    
    NSManagedObjectContext *ctx =[CoreDataBase activeContext];
    NSArray *jobOrdersToSync = [JobOrderDAO fetchItemsForSyncJobOrdersInContext:ctx];
    NSArray *jobPhotostoSYnc= [AttachPhotoDAO fetchItemsForSyncJobOrdersInContext:ctx];
    
    if (jobOrdersToSync.count == 0) {
        [CoreDataBase removeAllObjectFromTable:TBL_JOBORDERS inContext:ctx];
        
        [CoreDataBase removeAllObjectFromTable:TBL_JOBDETAILEDSCOPE inContext:ctx];
    }
    else if(jobPhotostoSYnc.count == 0){
        [CoreDataBase removeAllObjectFromTable:TBL_ATTACHPHOTO inContext:ctx];
    }
    
    if  (inboundSyncShouldStart){
        
        if (![[SyncManager sharedManager] isSyncing]) {
            [[SyncManager sharedManager] setStatusDelegate:nil];
            [[SyncManager sharedManager] fullSync];
            
        }
    }
}



#pragma StatusDelegate Methods
-(void)syncDidStart{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONSTART object:nil];
    
}
-(void)syncDidStartForAction:(SyncManagerAction) status{
    
}
-(void)syncDidFinshForAction:(SyncManagerAction) status withError:(NSError *)error{
    if(!error && status == SMA_JOBPHOTOS_SYNC_OUT)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONEND object:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CTDASHBOARDTABLERELOAD object:nil];
    }
}
-(void)syncDidFinsh{
    
    NSDictionary *dictToPass = nil;
    
    if([[SyncManager sharedManager] syncOnNetworkAwake]){
        dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:YES]};
    }
    else{
        dictToPass = @{EGOfflineRecordUpdateKey:[NSNumber numberWithBool:NO]};
    }
    

    [[NSNotificationCenter defaultCenter] postNotificationName:EGOfflineRecordUpdatedNotification object:nil userInfo:dictToPass];
    
    if  (!inboundSyncShouldStart){
        [[NSNotificationCenter defaultCenter] postNotificationName:CTSYNCICONEND object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:CTDASHBOARDTABLERELOAD object:nil];
    }
    else{
        [self callInboundSync];
    }
}



- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReachabilityChangedNotification
                                                  object:nil];
    [reachability stopNotifier];
}

@end
