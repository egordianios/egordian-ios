//
//  CustomSyncButtonView.h
//  ClearTouch
//
//  Created by Shineeth Hamza on 2/17/15.
//  Copyright (c) 2015 ClearPoint. All rights reserved.
//

#import "CustomButtonView.h"

@interface CustomSyncButtonView : CustomButtonView
@property (nonatomic, readonly) BOOL isAnimating;
- (void)startAnimating;
- (void)stopAnimating;
@end
