//
//  CustomButtonView.m
//  ClearTouch
//
//  Created by Shineeth Hamza on 25/09/14.
//  Copyright (c) 2014 ClearPoint. All rights reserved.
//

#import "CustomButtonView.h"
#import "UIImage+Utilities.h"

@implementation CustomButtonView

@synthesize delegate = _delegate;
@synthesize imageView = _imageView;
@synthesize textLabel = _textLabel;


- (void)awakeFromNib
{
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent:)];
    
    [self addGestureRecognizer:tap];
}
- (void)tapEvent:(UITapGestureRecognizer *)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(buttonDidPressed:)]) {
        [_delegate buttonDidPressed:self];
    }
}

- (void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [super setUserInteractionEnabled:userInteractionEnabled];
}

- (void)setColor:(UIColor*)color
{
    [_textLabel setTextColor:color];
    _imageView.image = [_imageView.image imageWithColor:color];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
