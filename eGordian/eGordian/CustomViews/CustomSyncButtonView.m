//
//  CustomSyncButtonView.m
//  ClearTouch
//
//  Created by Shineeth Hamza on 2/17/15.
//  Copyright (c) 2015 ClearPoint. All rights reserved.
//

#import "CustomSyncButtonView.h"

#import "SyncManager.h"
#import "AppDelegate.h"

@implementation CustomSyncButtonView

@synthesize isAnimating = _isAnimating;

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)initialization
{
    //self.hidden = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    if ([[SyncManager sharedManager] isSyncing]) {
//        [self startAnimating];
//    }
    
    self.userInteractionEnabled = [AppDelegate isConnectedToInternet];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startAnimating)
                                                 name:CTSyncStartedDidStartNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopAnimating)
                                                 name:CTSyncStartedDidEndNotification
                                               object:nil];
    [self initialization];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if ([[SyncManager sharedManager] isSyncing]) {
        [self startAnimating];
    }
    else
    {
        [self stopAnimating];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CTSyncStartedDidStartNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CTSyncStartedDidEndNotification
                                                  object:nil];
}

- (void)startAnimating
{
    if (_isAnimating) {
        return;
    }
    _isAnimating = YES;
    self.userInteractionEnabled = NO;
    self.imageView.animationImages = @[[UIImage imageNamed:@"SyncAnimationFrame21"],
                                       [UIImage imageNamed:@"SyncAnimationFrame20"],
                                       [UIImage imageNamed:@"SyncAnimationFrame19"],
                                       [UIImage imageNamed:@"SyncAnimationFrame18"],
                                       [UIImage imageNamed:@"SyncAnimationFrame17"],
                                       [UIImage imageNamed:@"SyncAnimationFrame16"],
                                       [UIImage imageNamed:@"SyncAnimationFrame15"],
                                       [UIImage imageNamed:@"SyncAnimationFrame14"],
                                       [UIImage imageNamed:@"SyncAnimationFrame13"],
                                       [UIImage imageNamed:@"SyncAnimationFrame12"],
                                       [UIImage imageNamed:@"SyncAnimationFrame11"],
                                       [UIImage imageNamed:@"SyncAnimationFrame10"],
                                       [UIImage imageNamed:@"SyncAnimationFrame9"],
                                       [UIImage imageNamed:@"SyncAnimationFrame8"],
                                       [UIImage imageNamed:@"SyncAnimationFrame7"],
                                       [UIImage imageNamed:@"SyncAnimationFrame6"],
                                       [UIImage imageNamed:@"SyncAnimationFrame5"],
                                       [UIImage imageNamed:@"SyncAnimationFrame4"],
                                       [UIImage imageNamed:@"SyncAnimationFrame3"],
                                       [UIImage imageNamed:@"SyncAnimationFrame2"],
                                       [UIImage imageNamed:@"SyncAnimationFrame1"]];
    self.imageView.animationDuration = 1.0f;
    self.imageView.animationRepeatCount = 0;
    [self.imageView startAnimating];
}

- (void)stopAnimating
{
    if (!_isAnimating && ([AppDelegate isConnectedToInternet])) {
        return;
    }
    _isAnimating = NO;
    self.userInteractionEnabled = YES;
    [self.imageView stopAnimating];
    //self.imageView.animationImages = @[[UIImage imageNamed:@"SyncAnimationFrame"]];
    //[self setImage:[UIImage imageNamed:@"SyncAnimationFrame"] forState:UIControlStateNormal];
}

@end
