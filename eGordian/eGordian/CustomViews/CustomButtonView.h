//
//  CustomButtonView.h
//  ClearTouch
//
//  Created by Shineeth Hamza on 25/09/14.
//  Copyright (c) 2014 ClearPoint. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomButtonView;

@protocol ButtonDelegate <NSObject>
- (void)buttonDidPressed:(CustomButtonView*)sender;
@end

@interface CustomButtonView : UIView

@property (nonatomic,weak) IBOutlet UILabel *textLabel;
@property (nonatomic,weak) IBOutlet UIImageView *imageView;
@property (nonatomic,weak) IBOutlet id<ButtonDelegate> delegate;

- (void)setColor:(UIColor*)color;

@end
