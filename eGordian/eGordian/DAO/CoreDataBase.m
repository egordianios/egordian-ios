//
//  CoreDataBase.m
//  ComplianceWire
//
//  Created by Shineeth Hamza on 27/02/14.
//  Copyright (c) 2014 EduNeering Holdings, Inc. All rights reserved.
//

#import "CoreDataBase.h"

@implementation CoreDataBase

NSManagedObjectContext *activeContext;// context for UI. Childe of sync context
NSManagedObjectContext *syncContext; // Context for sync, Parent context
NSMutableArray *ctxQueue;
NSManagedObjectModel *managedObjectModel;
NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (void)initialize
{
    ctxQueue = [[NSMutableArray alloc] init];
    /* // Old fashion
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncContextDidSave:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:syncContext];
     */
}
/*
#if !(TARGET_OS_EMBEDDED)  // This will work for Mac or Simulator but excludes physical iOS devices
+ (void) createCoreDataDebugProjectWithType: (NSNumber*) storeFormat storeUrl:(NSString*) storeURL modelFilePath:(NSString*) modelFilePath {
    NSDictionary* project = @{
                              @"storeFilePath": storeURL,
                              @"storeFormat" : storeFormat,
                              @"modelFilePath": modelFilePath,
                              @"v" : @(1)
                              };
    
    NSString* projectFile = [NSString stringWithFormat:@"/tmp/%@.cdp", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey]];
    
    [project writeToFile:projectFile atomically:YES];
    
}
#endif
*/
#pragma mark - Core Data stack
// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
+ (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"eGordianModel" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
+ (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    
    NSURL *appStoresDirectory = [self applicationStoresDirectory];
    
    NSURL *storeURL = [appStoresDirectory URLByAppendingPathComponent:@"eGordianModel.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                              NSInferMappingModelAutomaticallyOption:@YES};
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:storeURL
                                                        options:options
                                                          error:&error])
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        // Move Incompatible Store
        if ([fm fileExistsAtPath:[storeURL path]]) {
            NSURL *corruptURL = [[self applicationIncompatibleStoresDirectory] URLByAppendingPathComponent:[self nameForIncompatibleStore]];
            
            // Move Corrupt Store
            NSError *errorMoveStore = nil;
            [fm moveItemAtURL:storeURL toURL:corruptURL error:&errorMoveStore];
            
            if (errorMoveStore) {
                NSLog(@"Unable to move corrupt store.");
            }
            else
                return [self persistentStoreCoordinator];
        }

    }
    return persistentStoreCoordinator;
}

+ (NSURL *)applicationIncompatibleStoresDirectory
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *URL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Incompatible"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for corrupt data stores.");
            
            return nil;
        }
    }
    
    return URL;
}

+ (NSURL *)applicationStoresDirectory
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *applicationApplicationSupportDirectory = [[fm URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *URL = [applicationApplicationSupportDirectory URLByAppendingPathComponent:@"Stores"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for data stores.");
            
            return nil;
        }
    }
    
    return URL;
}

+ (NSString *)nameForIncompatibleStore
{
    // Initialize Date Formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Configure Date Formatter
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    
    return [NSString stringWithFormat:@"%@.sqlite", [dateFormatter stringFromDate:[NSDate date]]];
}

#pragma mark - Managed Object Contexts Initializers

// ********** CHILD CONTEXT FOR UI ************
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
+ (NSManagedObjectContext *)activeContext
{
    @synchronized(self)
    {
        if (activeContext != nil)
        {
            return activeContext;
        }
        
        activeContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [activeContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        activeContext.parentContext = [self syncContext];
        [activeContext setUndoManager:nil];
        
        return activeContext;
    }
}


// ********** PARENT CONTEXT ************
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
+ (NSManagedObjectContext *)syncContext
{
    
    NSThread *thread = [NSThread currentThread];
    
    if ([thread isMainThread]) {
        return [self initSyncContext];
    }
    else{
        __block NSManagedObjectContext *retMOC;
        dispatch_sync(dispatch_get_main_queue(),^{
            retMOC = [self initSyncContext];//Access it once to make sure it's there
        });
        return retMOC;
    }
}

+ (NSManagedObjectContext *)initSyncContext
{
    @synchronized(self)
    {
        if (syncContext != nil)
        {
            
        }
        else{
            NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
            if (coordinator != nil) {
                syncContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
                [syncContext setPersistentStoreCoordinator:coordinator];
                [syncContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
                [syncContext setUndoManager:nil];
            }
        }
        return syncContext;
    }
}

// ********** CHILD CONTEXT FOR SYNC OPERATION ************
// temporary context for sync operation, will discard the context on save.
+ (NSManagedObjectContext*)getTemporaryContext
{
#ifdef DEBUG
    if ([NSThread isMainThread]) {
        
        NSString *sourceString = [[NSThread callStackSymbols] objectAtIndex:1];
        // Example: 1   UIKit  0x00540c89 -[UIApplication _callInitializationDelegatesForURL:payload:suspended:] + 1163
        NSCharacterSet *separatorSet = [NSCharacterSet characterSetWithCharactersInString:@" -[]+?.,"];
        NSMutableArray *array = [NSMutableArray arrayWithArray:[sourceString  componentsSeparatedByCharactersInSet:separatorSet]];
        [array removeObject:@""];
        if (array.count>=5) {
            NSLog(@"\n\nCore Data: Warrning Temp ctx working on main thread.\nStack = %@\nFramework = %@\nMemory address = %@\nClass caller = %@\nFunction caller = %@\nLine caller = %@",[array objectAtIndex:0],[array objectAtIndex:1], [array objectAtIndex:2],[array objectAtIndex:3],[array objectAtIndex:4],[array objectAtIndex:5]);
        }
    }
#endif
    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc]
                                                initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [temporaryContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
    temporaryContext.parentContext = [self activeContext];
    [temporaryContext setUndoManager:nil];
    [ctxQueue addObject:temporaryContext];
    return temporaryContext;
}

#pragma mark - Save Managed Object Context
+ (void)saveSyncContext
{
    @synchronized(self)
    {
        [syncContext performBlockAndWait:^{
            NSError *error = nil;
            if (syncContext != nil)
            {
                if ( [syncContext hasChanges] && ![syncContext save:&error])
                {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
#ifdef DEBUG
                    //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
#endif
                }
            }
        }];
    }
}

+ (void)saveActiveContext
{
    [activeContext performBlockAndWait:^{
        NSError *error = nil;
        if (activeContext != nil)
        {
            if ( [activeContext hasChanges] && ![activeContext save:&error])
            {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
#ifdef DEBUG
                //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
#endif
            }
            else
                [self saveSyncContext];
        }
    }];
}

+ (BOOL)saveTemporaryContextAndRelease:(NSManagedObjectContext*)ctx
{
    //remove from queue
    __block BOOL flag = NO;
    [self saveTemporaryContext:ctx completion:^{
        [self releaseTemporaryContext:ctx];
        flag = YES;
    }];
    
    return flag;
}

+ (BOOL)saveTemporaryContext:(NSManagedObjectContext*)ctx completion:(void (^)(void))callbackBlock
{
    __block NSManagedObjectContext *_activeContext = [self activeContext];
    __block NSManagedObjectContext *_syncContext = [self syncContext];
    __block BOOL returnFlag = YES;
    if ([ctx hasChanges]) {
        [ctx performBlockAndWait:^{
            NSError *error = nil;
            [ctx save:&error]; // Save the context.
            if (error) {
#ifdef DEBUG
                NSLog(@"ERROR: while saving temp ctx: %@",[error localizedDescription]);
#endif
                returnFlag = NO;
            }
            else{
                [_activeContext performBlockAndWait:^{
                    NSError *error = nil;
                    [_activeContext save:&error];// Save the context.
                    if (error) {
#ifdef DEBUG
                        NSLog(@"ERROR: while saving actv ctx: %@",[error localizedDescription]);
#endif
                        returnFlag = NO;
                    }
                    else{
                        [_syncContext performBlockAndWait:^{
                            NSError *error = nil;
                            [_syncContext save:&error]; // Save the context.
                            if (error) {
#ifdef DEBUG
                                NSLog(@"ERROR: while saving sync ctx: %@",[error localizedDescription]);
#endif
                                returnFlag = NO;
                            }
                        }]; // writer
                    }
                }]; // main
                if (callbackBlock) {
                    callbackBlock();
                }
            }
        }];
    }
    
    return returnFlag;
}

+ (void)releaseTemporaryContext:(NSManagedObjectContext*)ctx
{
    if (![ctx hasChanges])
    {
        [ctx performBlockAndWait:^{
            [ctx reset];
        }];
    }
    
    [ctxQueue removeObject:ctx];
    ctx = nil;
}

/* 
 //old fashion
+ (void)syncContextDidSave:(NSNotification*)notification
{
    [[self activeContext] performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:)
                                           withObject:notification
                                        waitUntilDone:NO];
}
 */

#pragma mark - clear

+ (void)deleteStore
{
    NSURL *appStoresDirectory = [self applicationStoresDirectory];
    
    NSURL *storeURL = [appStoresDirectory URLByAppendingPathComponent:@"eGordianModel.sqlite"];
    NSFileManager *fm = [NSFileManager defaultManager];
    // Delete Incompatible Store
    if ([fm fileExistsAtPath:[storeURL path]]) {;
        NSError *errorMoveStore = nil;
        [fm removeItemAtURL:storeURL error:&errorMoveStore];
        
        if (errorMoveStore) {
            NSLog(@"Unable to Delete corrupt store.");
        }
    }
    
}

+ (void)clearUserData
{
    //dispatch_queue_t backgroundQueue = dispatch_queue_create("com.cleartouch.dbClearQueue", 0);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSManagedObjectContext *ctx = [CoreDataBase activeContext];
        
        [CoreDataBase removeAllObjectFromTable:TBL_JOBORDERS inContext:ctx];
        [CoreDataBase removeAllObjectFromTable:TBL_JOBDETAILEDSCOPE inContext:ctx];
        [CoreDataBase removeAllObjectFromTable:TBL_JOBSTATUS inContext:ctx];
        [CoreDataBase removeAllObjectFromTable:TBL_JOBORDER_DETAIL inContext:ctx];
        [CoreDataBase removeAllObjectFromTable:TBL_ATTACHPHOTO inContext:ctx];

        [CoreDataBase saveActiveContext];
        /*
        dispatch_async(dispatch_get_main_queue(), ^{
            [CoreDataBase saveActiveContext];
        });
         */
    });
    
    
}

+ (void)removeAllObjectFromTable:(NSString*)table inContext:(NSManagedObjectContext *)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:table];
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        for(id row in fetchedItems)
        {
            [ctx deleteObject:row];
        }
    }
}

@end
