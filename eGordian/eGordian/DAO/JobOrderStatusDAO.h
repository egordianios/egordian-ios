//
//  JobOrderStatusDAO.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobOrderStatus.h"
#import "CoreDataBase.h"
@interface JobOrderStatusDAO : NSObject
+ (JobOrderStatus*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx;
+ (JobOrderStatus*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx;

@end
