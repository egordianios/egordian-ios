//
//  JobOrdersDAO.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/21/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataBase.h"
#import "JobOrder.h"
@interface JobOrderDAO : NSObject
+ (JobOrder*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx;
+ (JobOrder*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)fetchItemsForSyncJobOrdersInContext:(NSManagedObjectContext*)ctx;
@end
