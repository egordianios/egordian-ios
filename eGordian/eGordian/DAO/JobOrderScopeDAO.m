//
//  JobOrderScopeDAO.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/30/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "JobOrderScopeDAO.h"
#import "NSDictionary+JSON.h"
@implementation JobOrderScopeDAO

+ (JobOrderScope*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx
{
    if (!jsonDic || [jsonDic isEqual:[NSNull null]])
    {
        return nil;
    }
    NSString *key = [jsonDic getStringForKey:@"jobOrderId"];
    JobOrderScope *row = [self objectWithID:key inContext:ctx WithJobId:key];
    if (!row) {
        row = [NSEntityDescription insertNewObjectForEntityForName:TBL_JOBDETAILEDSCOPE
                                            inManagedObjectContext:ctx];
        row.key = key;
        row.jobId = key;
        NSLog(@"Inserting New record for JobId :%@",row.jobId);
    }
    row.title = [jsonDic getStringForKey:@"Description"];
    NSError *error ;
    NSString *detailedScopeString = [jsonDic getStringForKey:@"Data"];
    NSData *data = [detailedScopeString dataUsingEncoding:NSUTF8StringEncoding];
    
    if (detailedScopeString.length >0 && data) {
        NSAttributedString *str = [[NSAttributedString alloc] initWithData:data
                                   
                                                                   options:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType,
                                                                             NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}
                                                            documentAttributes:nil error:&error];
        row.jobScopeDescription =(str.length?str:[[NSAttributedString alloc]initWithString:detailedScopeString]);

    }else{
        
        row.jobScopeDescription =[[NSAttributedString alloc] initWithString:@""];
    }
    
    
    
    return row;
    
}


+ (JobOrderScope*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx WithJobId:(NSString *) assetId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@", key];
    return [[self objectsWithPredicate:predicate inContext:ctx] firstObject];
}

+ (JobOrderScope*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    return [[self objectsWithID:key inContext:ctx] firstObject];
}

+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@", key];
    return [self objectsWithPredicate:predicate inContext:ctx];
}

+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_JOBDETAILEDSCOPE];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx
{
    return [self objectsWithPredicate:nil inContext:ctx];
}

@end
