//
//  JobOrderStatusDAO.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/29/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "JobOrderStatusDAO.h"
#import "NSDictionary+JSON.h"

@implementation JobOrderStatusDAO
+ (JobOrderStatus *)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx
{
    
    if (!jsonDic || [jsonDic isEqual:[NSNull null]])
    {
        return nil;
    }
    NSString *key = [jsonDic getStringForKey:@"Id"];
    JobOrderStatus *row = [self objectWithID:key inContext:ctx];
    if (!row) {
        row = [NSEntityDescription insertNewObjectForEntityForName:TBL_JOBSTATUS
                                            inManagedObjectContext:ctx];
        row.key = key;
        
    }
    row.jobStatusId =[jsonDic getStringForKey:@"Id"];
    row .jobStatusName =[jsonDic getStringForKey:@"Name"];
    
    return row;
}

+(NSMutableDictionary *)getFieldsDict:(NSArray *)fields{
    
    NSMutableDictionary *fieldsDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in fields) {
        [fieldsDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return fieldsDict;
}

+ (JobOrderStatus*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    return [[self objectsWithID:key inContext:ctx] firstObject];
}

+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@",key ];
    return [self objectsWithPredicate:predicate inContext:ctx];
}

+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_JOBSTATUS];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx
{
    return [self objectsWithPredicate:nil inContext:ctx];
}
@end
