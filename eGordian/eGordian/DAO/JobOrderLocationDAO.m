//
//  JobOrderLocationsDAO.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "JobOrderLocationDAO.h"
#import "JobOrderLocation.h"
#import "NSDictionary+JSON.h"

@implementation JobOrderLocationDAO
+ (JobOrderLocation *)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx
{
    
    if (!jsonDic || [jsonDic isEqual:[NSNull null]])
    {
        return nil;
    }
    NSString *key = [jsonDic getStringForKey:@"Id"];
    JobOrderLocation *row = [self objectWithID:key inContext:ctx];
    if (!row) {
        row = [NSEntityDescription insertNewObjectForEntityForName:TBL_JOB_LOCATION
                                            inManagedObjectContext:ctx];
        row.key = key;
        
    }
    row.locationName =[jsonDic getStringForKey:@"LocationName"];
    row.address1 =[jsonDic getStringForKey:@"Address1"];
    row.address2 =[jsonDic getStringForKey:@"Address2"];row.city =[jsonDic getStringForKey:@"City"];
    row.state =[jsonDic getStringForKey:@"State"];
    row.zipCode =[jsonDic getStringForKey:@"ZipCode"];
    row.lastUpdatedDate =[jsonDic getNSTimeIntervalForKey:@"LastUpdatedDate"];
    return row;
}

+(NSMutableDictionary *)getFieldsDict:(NSArray *)fields{
    
    NSMutableDictionary *fieldsDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in fields) {
        [fieldsDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return fieldsDict;
}

+ (JobOrderLocation*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    return [[self objectsWithID:key inContext:ctx] firstObject];
}

+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@",key ];
    return [self objectsWithPredicate:predicate inContext:ctx];
}

+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_JOB_LOCATION];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx
{
    return [self objectsWithPredicate:nil inContext:ctx];
}


@end
