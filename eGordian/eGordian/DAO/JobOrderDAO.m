//
//  JobOrdersDAO.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/21/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "JobOrderDAO.h"
#import "NSDictionary+JSON.h"

@implementation JobOrderDAO
+ (JobOrder*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx
{
    
    if (!jsonDic || [jsonDic isEqual:[NSNull null]])
    {
        return nil;
    }
    NSArray *fieldsArray = [jsonDic objectForKey:@"Fields"];
    NSMutableDictionary *fieldsDict = [self getFieldsDict:fieldsArray];
    NSString *key = [jsonDic getStringForKey:@"Id"];
    JobOrder *row = [self objectWithID:key inContext:ctx];
    if (!row) {
        row = [NSEntityDescription insertNewObjectForEntityForName:TBL_JOBORDERS
                                            inManagedObjectContext:ctx];
        row.key = key;
        //NSLog(@"Inserting New Record for DAO as its a new Key:%@",row.key);
        
    }
    row.jobTitle =[fieldsDict getStringForKey:@"JobTitle"];
    row.clientName =[fieldsDict getStringForKey:@"ClientName"];
    row.locationName =[fieldsDict getStringForKey:@"LocationName"];
    row.jobOrderLink =[jsonDic getStringForKey:@"JobOrderLink"];
    row.address1 =[fieldsDict getStringForKey:@"Address1"];
    row.address2 =[fieldsDict getStringForKey:@"Address2"];
    row.state =[fieldsDict getStringForKey:@"State"];
    row.zipCode =[fieldsDict getStringForKey:@"ZipCode"];
    row.lastUpdatedDate =[fieldsDict getNSTimeIntervalForKey:@"LastUpdatedDate"];
    row.city =[fieldsDict getStringForKey:@"City"];
    row.jobId =[jsonDic getStringForKey:@"Id"];
    
    NSString* jobOrderLink = row.jobOrderLink;
    if([jobOrderLink rangeOfString:@"Owners"].location != NSNotFound){
        NSRange end = [jobOrderLink rangeOfString:@"Owners/"];
        NSString *ownerID = [jobOrderLink substringFromIndex:end.location+end.length];
        end = [ownerID rangeOfString:@"/"];
        ownerID =[ownerID substringToIndex:end.location];
        row.ownerId =ownerID;
    }
    
//    if(row.syncStatus != JT_ALREADYPRESENT)
    
    row.syncStatus =JT_ALREADYSYNCED;
    return row;
}

+ (NSArray*)fetchItemsForSyncJobOrdersInContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncStatus != %d",JT_ALREADYSYNCED];
    return [self objectPredicate:predicate inContext:ctx];
}

+(NSMutableDictionary *)getFieldsDict:(NSArray *)fields{
    
    NSMutableDictionary *fieldsDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in fields) {
        [fieldsDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return fieldsDict;
}
+ (NSArray*)objectPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_JOBORDERS];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}
+ (JobOrder*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    return [[self objectsWithID:key inContext:ctx] firstObject];
}

+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@",key ];
    return [self objectsWithPredicate:predicate inContext:ctx];
}

+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_JOBORDERS];
    [fetch setPredicate:predicate];
    //[fetch setFetchLimit:10];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx
{
    return [self objectsWithPredicate:nil inContext:ctx];
}

@end
