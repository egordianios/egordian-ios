//
//  JobOrderLocationsDAO.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataBase.h"
#import "JobOrderLocation.h"
@interface JobOrderLocationDAO : NSObject
+ (JobOrderLocation*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx;
+ (JobOrderLocation*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx;
@end
