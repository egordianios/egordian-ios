//
//  AttachPhotoDAO.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import "AttachPhotoDAO.h"
#import "NSDictionary+JSON.h"

@implementation AttachPhotoDAO

+ (id)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx
{
    if (!jsonDic || [jsonDic isEqual:[NSNull null]])
    {
        return nil;
    }
    
    NSString *jobId = [jsonDic getStringForKey:@"jobId"];
    NSString *picturename = [jsonDic getStringForKey:@"pictureName"];
    AttachPhoto *row = [self objectWithJobID:jobId pictureName:picturename inContext:ctx];
    if (!row) {
        row = [NSEntityDescription insertNewObjectForEntityForName:TBL_ATTACHPHOTO
                                            inManagedObjectContext:ctx];
        row.jobId = jobId;
        row.pictureName = [jsonDic getStringForKey:@"pictureName"];
        
    }
    row.imageData =[jsonDic valueForKey:@"imageData"];
    row.ownerId =[jsonDic getStringForKey:@"ownerId"];
    row.userID = [jsonDic getStringForKey:@"useriD"];
    row.photoFilePath =[jsonDic getStringForKey:@"filePath"];
    row.syncStatus =JS_WAITINGTOSYNC;
    return row;
}


+ (id)objectWithJobID:(NSString *)jobID pictureName:(NSString *)picName inContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %@ AND pictureName == %@", jobID,picName];
    return [[self objectPredicate:predicate inContext:ctx] firstObject];
}

+ (id)objectWithJobID:(NSString *)jobID  inContext:(NSManagedObjectContext*)ctx{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobId == %@", jobID];
    return [[self objectPredicate:predicate inContext:ctx] firstObject];
    
}
+ (NSArray*)objectPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ATTACHPHOTO];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}
+ (NSArray*)fetchItemsForSyncJobOrdersInContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncStatus != %d",JT_ALREADYSYNCED];
    return [self objectPredicate:predicate inContext:ctx];
}

+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_ATTACHPHOTO];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+(NSArray*)getIndividualRecordsForJobOrder:(NSString *)jobOrderId inContext:(NSManagedObjectContext *)ctx{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"jobId == %@ AND syncStatus != %d",jobOrderId,JT_ALREADYSYNCED];
    return [self objectPredicate:predicate inContext:ctx];
}

+(NSArray *)getUniqueRecordsForJob:(NSManagedObjectContext *)ctx{
    
    NSEntityDescription *entity = [NSEntityDescription  entityForName:TBL_ATTACHPHOTO inManagedObjectContext:ctx];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:TBL_ATTACHPHOTO];
    [request setEntity:entity];
    [request setReturnsDistinctResults:YES];
    [request setPropertiesToFetch:@[@"jobId"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncStatus != %d",JT_ALREADYSYNCED];
    [request setPredicate:predicate];
    request.resultType = NSDictionaryResultType;

    
    // Execute the fetch.
    NSError *error;
    NSArray *objects = [ctx executeFetchRequest:request error:&error];
    if (!error && [objects count])
    {
        return objects;
    }
    return nil;
    

}

+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx
{
    return [self objectsWithPredicate:nil inContext:ctx];
}
@end
