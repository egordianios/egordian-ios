//
//  AttachPhotoDAO.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 1/7/16.
//  Copyright © 2016 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AttachPhoto.h"
@interface AttachPhotoDAO : AttachPhoto
+ (id)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx;
+ (id)objectWithJobID:(NSString *)jobID pictureName:(NSString *)picName inContext:(NSManagedObjectContext*)ctx;
+(NSArray *)getUniqueRecordsForJob:(NSManagedObjectContext *)ctx;
+(NSArray*)getIndividualRecordsForJobOrder:(NSString *)jobOrderId inContext:(NSManagedObjectContext *)ctx;
+ (NSArray*)fetchItemsForSyncJobOrdersInContext:(NSManagedObjectContext*)ctx;
+ (id)objectWithJobID:(NSString *)jobID  inContext:(NSManagedObjectContext*)ctx;
@end
