//
//  JobOrderDetailsDAO.m
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/28/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import "JobOrderDetailsDAO.h"
#import "NSDictionary+JSON.h"
#import "JobOrderLocation.h"
#import "JobOrderLocationDAO.h"
@implementation JobOrderDetailsDAO
+ (JobOrderDetail*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx
{
    
    if (!jsonDic || [jsonDic isEqual:[NSNull null]])
    {
        return nil;
    }
    NSArray *fieldsArray = [jsonDic objectForKey:@"Fields"];
    NSMutableDictionary *fieldsDict = [self getFieldsDict:fieldsArray];
    NSString *key = [jsonDic getStringForKey:@"Id"];
    JobOrderDetail *row = [self objectWithID:key inContext:ctx];
    if (!row) {
        row = [NSEntityDescription insertNewObjectForEntityForName:TBL_JOBORDER_DETAIL
                                            inManagedObjectContext:ctx];
        row.key = key;
        NSLog(@"Inserting New Record for Detail DAO as its a new Key:%@",row.key);
        
    }
    
    row.jobTitle =[fieldsDict getStringForKey:@"JobTitle"];
    row.clientName =[fieldsDict getStringForKey:@"ClientName"];
    row.jobStatusId =[fieldsDict getStringForKey:@"StatusId"];
    row.jobId =[jsonDic getStringForKey:@"Id"];
    row.jobNotesLink =[jsonDic  getStringForKey:@"JobNotesLink"];
    row.picturesLink =[jsonDic getStringForKey:@"PicturesLink"];
    row.totalContactsCount =[jsonDic getIntegerForKey:@"TotalContacts"];
    row.totalDocumentsCount =[jsonDic getIntegerForKey:@"TotalDocuments"];
    row.totalPhotosCount =[jsonDic getIntegerForKey:@"TotalPhotos"];
    row.trackingDatesLink =[jsonDic getStringForKey:@"TrackingDatesLink"];
    row.totalDocumentsCount =[jsonDic getIntegerForKey:@"TotalDocuments"];
    
    if ([jsonDic hasValidJSONKey:@"Locations"]) {
        NSArray *locationsArray =[jsonDic objectForKey:@"Locations"];
        NSArray *fields =[locationsArray valueForKey:@"Fields"];
        for (NSArray *valArray in fields) {
            NSDictionary *crctValues =[self getFieldsDict:valArray];
            [self travelTroughLocations:crctValues withParent:row inContext:ctx];
            NSLog(@"Location Fileds Values:%@",crctValues);
            
        }
    }
    return row;
}



+ (void)travelTroughLocations:(NSDictionary *)locations withParent:(JobOrderDetail *)parent inContext:(NSManagedObjectContext*)ctx
{
    
        JobOrderLocation *location = [JobOrderLocationDAO insertIfDoesNotExists:locations
                                                                            inContext:ctx];
        if (location) {
            [parent addJobLocationsObject:location];
        }
}
+(NSMutableDictionary *)getFieldsDict:(NSArray *)fields{
    
    NSMutableDictionary *fieldsDict = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *currentField in fields) {
        [fieldsDict setObject:[currentField objectForKey:@"Value"] forKey:[currentField objectForKey:@"Name"]];
    }
    
    return fieldsDict;
}

+ (JobOrderDetail*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    return [[self objectsWithID:key inContext:ctx] firstObject];
}

+ (NSArray*)objectsWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@",key ];
    return [self objectsWithPredicate:predicate inContext:ctx];
}

+ (NSArray*)objectsWithPredicate:(NSPredicate *) predicate inContext:(NSManagedObjectContext*)ctx
{
    NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:TBL_JOBORDER_DETAIL];
    [fetch setPredicate:predicate];
    
    NSError *fetchError;
    NSArray *fetchedItems = [ctx executeFetchRequest:fetch error:&fetchError];
    if (!fetchError && [fetchedItems count])
    {
        return fetchedItems;
    }
    return nil;
}

+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx
{
    return [self objectsWithPredicate:nil inContext:ctx];
}

@end
