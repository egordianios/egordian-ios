//
//  CoreDataBase.h
//  ComplianceWire
//
//  Created by Shineeth Hamza on 27/02/14.
//  Copyright (c) 2014 EduNeering Holdings, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Enums.h"

#define TBL_JOBORDERS @"JobOrder"
#define TBL_JOBORDER_DETAIL @"JobOrderDetail"
#define TBL_JOB_LOCATION @"JobOrderLocation"
#define TBL_JOBSTATUS @"JobOrderStatus"
#define TBL_JOBDETAILEDSCOPE @"JobOrderScope"
#define TBL_ATTACHPHOTO @"AttachPhoto"

/*
#define TBL_SLIDER_ANNOTATION @"SliderAnnotation"
#define TBL_ASSESSMENT @"Assessment"
#define TBL_ASSESSMENT_COMMENTS @"AssessmentComments"
#define TBL_ASSETS @"Asset"
#define TBL_ASSIGNMENT @"Assignment"
#define TBL_BADGE @"Badge"
#define TBL_COMMENT_APPROVAL_REQUESTS @"CommentApprovalRequests"
#define TBL_CONFIGURATIONS @"Configurations"
#define TBL_CURRICULUM @"Curriculum"
#define TBL_CURRICULUM_COMMENTS @"Comments"
#define TBL_ENTITY_VERSION @"EntityVersions"
#define TBL_EVENT @"Event"
#define TBL_EVENTLEADERBOARD @"EventLeaderboard"
#define TBL_LEADERBOARD @"Leaderboard"
#define TBL_LEADERBOARD_USERS @"LeaderboardUsers"
#define TBL_LEARNER_USERS @"LearnerUsers"
#define TBL_LEARNER_USERS_ANSWERS @"LearnerUsersAnswers"
#define TBL_NOTIFICATIONS @"Notifications"
#define TBL_ORG_HELP @"OrganizationHelpInfo"
#define TBL_PDF_ANNOTATION @"PDFAnnotation"
#define TBL_PDFTRON_ANNOTATION @"PDFTronAnnotation"
#define TBL_PREREQUISITES @"Prerequisites"
#define TBL_PRESENTER @"Presenter"
#define TBL_QUESTIONS @"Questions"
#define TBL_RATED_ASESSMENT_COMMENTS @"RAComment"
#define TBL_RATED_SURVEY @"RatedSurvey"
#define TBL_RATING_REQUESTS @"RatingRequests"
#define TBL_REFERENCES @"References"
#define TBL_SESSION @"Session"
#define TBL_SESSION_POLL @"SessionPoll"
#define TBL_STATES @"States"
#define TBL_SYSTEMDATA @"SystemData"
#define TBL_USER  @"User"
#define TBL_USER_ANSWERS @"UserAnswers"
#define TBL_USER_ANSWER_OPTIONS @"UserAnswerOptions"
#define TBL_USER_ASSESSMENT_RATINGS @"UserAssessmentRatings"
#define TBL_TRANSLATION @"Translation"
#define TBL_LANGUAGE @"Language"
#define TBL_ASSESSMENT_USER_ANSWERS @"AssessmentUserAnswers"
#define TBL_USER_ASSESSMENTS @"UserAssessments"
 */

@interface CoreDataBase : NSManagedObjectContext
+ (NSManagedObjectContext*)activeContext;
+ (NSManagedObjectContext*)syncContext;
+ (void)saveSyncContext;
+ (void)saveActiveContext;
+ (NSManagedObjectContext*)getTemporaryContext;
+ (BOOL)saveTemporaryContextAndRelease:(NSManagedObjectContext*)ctx;
+ (BOOL)saveTemporaryContext:(NSManagedObjectContext*)ctx completion:(void (^)(void))callbackBlock;
+ (void)releaseTemporaryContext:(NSManagedObjectContext*)ctx;
+ (void)clearUserData;
+ (void)removeAllObjectFromTable:(NSString*)table inContext:(NSManagedObjectContext *)ctx;

+ (void)deleteStore;
@end
