//
//  JobOrderScopeDAO.h
//  eGordian-iOS
//
//  Created by Ajeya Sriharsha on 12/30/15.
//  Copyright © 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobOrderScope.h"
#import "CoreDataBase.h"
@interface JobOrderScopeDAO : NSObject
+ (JobOrderScope*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx WithJobId:(NSString *) assetId;
+ (JobOrderScope*)objectWithID:(NSString *)key inContext:(NSManagedObjectContext*)ctx;
+ (JobOrderScope*)insertIfDoesNotExists:(NSDictionary*)jsonDic inContext:(NSManagedObjectContext*)ctx;
+ (NSArray*)fetchAllDataInContext:(NSManagedObjectContext*)ctx;

@end
