//
//  APIManager.m
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/1/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "APIManager.h"
#import "NSDictionary+JSON.h"
#import "AFNetworking.h"
#import "EG_JobOrder.h"
#import "EG_JobOrderDetail.h"
#import "EG_TransactionDate.h"
#import "EG_JobConatcts.h"
#import "eGordianContact.h"

@implementation APIManager
@synthesize apiManagerDelegate = _apiManagerDelegate;

#pragma mark "Implemention of Single Ton"

static APIManager *_sharedInstance;

static dispatch_once_t oncePredicate;

+ (APIManager *)sharedAPIManager
{
    if(!_sharedInstance)
    {
        dispatch_once(&oncePredicate, ^
                      {
                          _sharedInstance = [[super allocWithZone:nil] init];
                      });
    }
    return _sharedInstance;
}

@end
