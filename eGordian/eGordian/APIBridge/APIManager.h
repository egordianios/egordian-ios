//
//  APIManager.h
//  eGordian-iOS
//
//  Created by Naresh Kumar on 9/1/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EG_JobOrderDetail.h"

typedef enum : NSUInteger{
    API_OPENJOBCOUNT,
    API_JOBORDERS,
    API_JOBORDERLISTBYSEARCH,
}ServiceType;

@protocol APIManagerDelegate <NSObject>

@optional

- (void)serviceRequestFinished:(NSMutableArray *)responseArray serviceType:(ServiceType)type;
- (void)serviceRequestFailed:(NSError *)error serviceType:(ServiceType)type;

@end

@interface APIManager : NSObject

+ (id)sharedAPIManager;

@property (nonatomic, weak) id<APIManagerDelegate> apiManagerDelegate;

@end
