//
//  APIRequest.h
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Enums.h"

typedef enum
{
    EGORDIAN_API_LOGIN,
    EGORDIAN_API_AUTHORIZATION,
    EGORDIAN_API_OPENJOBS,
    EGORDIAN_API_JOBORDERS_LIST,
    EGORDIAN_API_JOBORDER_DETAIL,
    EGORDIAN_API_JOBORDER_STATUS,
    EGORDIAN_API_CONTACTS,
    EGORDIAN_API_TRACKING_DATES,
    EGORDIAN_API_NOTES,
    EGORDIAN_API_JOBORDER_CONTACTS,
    EGORDIAN_API_JOBORDER_CONTACT_UPDATE,
    EGORDIAN_API_GLOBAL_CONTATCS,
    EGORDIAN_API_OWNER_CONTACTS,
    EGORDIAN_API_CONTACTS_DETAIL,
    EGORDIAN_API_CONTACTS_SEARCH,
    EGORDIAN_API_GET_COORDINATES_FROMADDRESS,
    EGORDIAN_API_JOBSTATUS_UPDATE,
    EGORDIAN_API_JOBNOTES_UPDATE,
    EGORDIAN_API_JOBNOTES_ADD,
    EGORDIAN_API_TRACKING_DATES_UPDATE,
    EGORDIAN_API_DASHBOARD_COUNT,
    EGORDIAN_API_PHOTO_THUMBNAIL,
    EGORDIAN_API_PHOTO_DETAIL,
    EGORDIAN_API_JOB_PHOTO_THUMBNAIL,
    EGORDIAN_API_PHOTO_DELETE,
    EGORDIAN_API_JOB_PHOTO_DELETE,
    EGORDIAN_API_PHOTOS_ATTACH_TO_JOB,
    EGORDIAN_API_UPLOAD_USER_PHOTO,
    EGORDIAN_API_JOBSOFFLINE,
    EGORDIAN_API_JOBORDER_DOCUMENTS,
    EGORDIAN_API_JOBORDER_DETAILEDSCOPE,
    EGORDIAN_API_SCOPEOFWORK_UPDATE,
    EGORDIAN_API_OWNER_ROLES,
    EGORDIAN_API_ASSIGNROLE,
    API_COUNT
}
APIKey;

@class APIRequest;

@protocol APIRequestDelegate <NSObject>
@optional
- (void)requestFinished:(APIRequest*)request;
- (void)requestFailed:(APIRequest*)request withError:(NSError*)error;
- (void)networkErrorWithMessage:(NSError*)error;
@end

@interface APIRequest : AFHTTPRequestOperationManager
{
    NSURLRequest *request;
    NSString *acceptHeader;
}
@property(readonly, nonatomic) HttpMethod httpMethod;
@property(readonly, nonatomic) APIKey apiKey;
@property(strong, nonatomic) NSString* apiUri;
@property(readonly, nonatomic) bool isSessionFree;
@property(strong,nonatomic) NSData* requestData;
@property(strong,nonatomic) NSDictionary* aditionalHeaders;
@property(strong, nonatomic) id<APIRequestDelegate> delegate;
@property(strong, nonatomic) id responseObject;
@property(strong, readonly) AFHTTPRequestOperation *operation;

+ (APIRequest*)apiRequestWithKey:(APIKey) key;
- (void)invokeWithParameter:(id)parameters;
//- (void) invokeWithParameter:(NSDictionary *)parameterDictionary;
- (void) invokeWithParameter:(NSDictionary *)parameter andAditionalHeaders:(NSDictionary*)headersDictionary;
- (void) invokeWithURL:(NSString*)url;
- (void)invokeWithURL:(NSString*)requestUrl andAditionalHeaders:(NSDictionary*)headersDictionary;
- (id) initWithApiKey:(APIKey) aKey URI:(NSString*) aUri HttpMethod:(HttpMethod) aMethod fromEGordianAPI:(bool)fromEGordian;
-(void)invokeWithURL:(NSString *)requestUrl withData:(id)uploadData andAditionalHeaders:(NSDictionary*)headersDictionary;
@end
