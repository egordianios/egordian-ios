//
//  APIRequest.m
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "APIRequest.h"
#import "APIClient.h"

@interface APIRequest ()
{
    NSDictionary *_headersDictionary;
}

@end


@implementation APIRequest

@synthesize apiKey, apiUri, isSessionFree, requestData, aditionalHeaders, httpMethod;
@synthesize delegate = _delegate;
@synthesize responseObject = _responseObject;
@synthesize operation = _operation;


static NSString* apiUris[API_COUNT];
static NSNumber* apiVersions[API_COUNT];
static HttpMethod apiMethods[API_COUNT];
static bool apiFromEGordian[API_COUNT];

+(void)initialize
{
    apiUris[EGORDIAN_API_LOGIN]                         = @"mobilelogin?";
    apiMethods[EGORDIAN_API_LOGIN]                      = HM_GET;
    
//    apiUris[EGORDIAN_API_AUTHORIZATION]                 = @"accessToken?"; //authorization New
    apiUris[EGORDIAN_API_AUTHORIZATION]                 = @"authorizationToken?"; //old
    apiMethods[EGORDIAN_API_AUTHORIZATION]              = HM_GET;
    
    apiUris[EGORDIAN_API_CONTACTS]                      = @"contacts";
    apiMethods[EGORDIAN_API_CONTACTS]                   = HM_GET;
    
    apiUris[EGORDIAN_API_OPENJOBS]                      = @"";
    apiMethods[EGORDIAN_API_OPENJOBS]                   = HM_GET;
    
    apiUris[EGORDIAN_API_JOBORDERS_LIST]                = @"";
    apiMethods[EGORDIAN_API_JOBORDERS_LIST]             = HM_GET;
    
    apiUris[EGORDIAN_API_JOBORDER_DETAIL]               = @"";
    apiMethods[EGORDIAN_API_JOBORDER_DETAIL]            = HM_GET;
    
    apiUris[EGORDIAN_API_JOBORDER_STATUS]               = @"";
    apiMethods[EGORDIAN_API_JOBORDER_STATUS]            = HM_GET;
    
    apiUris[EGORDIAN_API_TRACKING_DATES]                = @"";
    apiMethods[EGORDIAN_API_TRACKING_DATES]             = HM_GET;
    
    apiUris[EGORDIAN_API_JOBORDER_CONTACTS]             = @"";
    apiMethods[EGORDIAN_API_JOBORDER_CONTACTS]          = HM_GET;
    
    apiUris[EGORDIAN_API_GLOBAL_CONTATCS]               = @"";
    apiMethods[EGORDIAN_API_GLOBAL_CONTATCS]            = HM_GET;
    
    apiUris[EGORDIAN_API_OWNER_CONTACTS]               = @"";
    apiMethods[EGORDIAN_API_OWNER_CONTACTS]            = HM_GET;
    
    
    
    apiUris[EGORDIAN_API_CONTACTS_DETAIL]               = @"";
    apiMethods[EGORDIAN_API_CONTACTS_DETAIL]            = HM_GET;
    
    apiUris[EGORDIAN_API_CONTACTS_SEARCH]               = @"";
    apiMethods[EGORDIAN_API_CONTACTS_SEARCH]            = HM_GET;
    
    apiUris[EGORDIAN_API_GET_COORDINATES_FROMADDRESS]   = @"";
    apiMethods[EGORDIAN_API_GET_COORDINATES_FROMADDRESS]= HM_GET;
    
    apiUris[EGORDIAN_API_NOTES]                         = @"";
    apiMethods[EGORDIAN_API_NOTES]                      = HM_GET;
    
    apiUris[EGORDIAN_API_DASHBOARD_COUNT]               = @"";
    apiMethods[EGORDIAN_API_DASHBOARD_COUNT]            = HM_GET;
    
    apiUris[EGORDIAN_API_PHOTO_THUMBNAIL]               = @"";
    apiMethods[EGORDIAN_API_PHOTO_THUMBNAIL]            = HM_GET;
    
    apiUris[EGORDIAN_API_PHOTO_DETAIL]                  = @"";
    apiMethods[EGORDIAN_API_PHOTO_DETAIL]               = HM_GET;
    
    apiUris[EGORDIAN_API_JOB_PHOTO_THUMBNAIL]           = @"";
    apiMethods[EGORDIAN_API_JOB_PHOTO_THUMBNAIL]        = HM_GET;
    
    apiUris[EGORDIAN_API_JOBSTATUS_UPDATE]              = @"";
    apiMethods[EGORDIAN_API_JOBSTATUS_UPDATE]           = HM_PUT;
    
    apiUris[EGORDIAN_API_JOBNOTES_UPDATE]               = @"";
    apiMethods[EGORDIAN_API_JOBNOTES_UPDATE]            = HM_PUT;
    
    apiUris[EGORDIAN_API_TRACKING_DATES_UPDATE]         = @"";
    apiMethods[EGORDIAN_API_TRACKING_DATES_UPDATE]      = HM_PUT;
    
    apiUris[EGORDIAN_API_PHOTO_DELETE]                  = @"";
    apiMethods[EGORDIAN_API_PHOTO_DELETE]               = HM_DELETE;
    
    apiUris[EGORDIAN_API_JOB_PHOTO_DELETE]              = @"";
    apiMethods[EGORDIAN_API_JOB_PHOTO_DELETE]           = HM_DELETE;
    
    apiUris[EGORDIAN_API_PHOTOS_ATTACH_TO_JOB]          = @"";
    apiMethods[EGORDIAN_API_PHOTOS_ATTACH_TO_JOB]       = HM_POST;
    
    apiUris[EGORDIAN_API_UPLOAD_USER_PHOTO]             = @"";
    apiMethods[EGORDIAN_API_UPLOAD_USER_PHOTO]       = HM_POST;
    
    apiUris[EGORDIAN_API_JOBSOFFLINE]                   =@"";
    apiMethods[EGORDIAN_API_JOBSOFFLINE]                =HM_GET;
    
    apiUris[EGORDIAN_API_JOBORDER_DOCUMENTS]            = @"";
    apiMethods[EGORDIAN_API_JOBORDER_DOCUMENTS]         =HM_GET;
    
    apiUris[EGORDIAN_API_JOBORDER_DETAILEDSCOPE]        = @"";
    apiMethods[EGORDIAN_API_JOBORDER_DETAILEDSCOPE]     =HM_GET;
    
    apiUris[EGORDIAN_API_SCOPEOFWORK_UPDATE]            = @"";
    apiMethods[EGORDIAN_API_SCOPEOFWORK_UPDATE]         = HM_PUT;
    
    apiUris[EGORDIAN_API_OWNER_ROLES]                   = @"";
    apiMethods[EGORDIAN_API_OWNER_ROLES]                = HM_GET;
    
    apiUris[EGORDIAN_API_ASSIGNROLE]                    = @"";
    apiMethods[EGORDIAN_API_ASSIGNROLE]                 = HM_POST;
    
    apiUris[EGORDIAN_API_JOBNOTES_ADD]                  = @"";
    apiMethods[EGORDIAN_API_JOBNOTES_ADD]               = HM_POST;
    
    apiUris[EGORDIAN_API_JOBORDER_CONTACT_UPDATE]       = @"";
    apiMethods[EGORDIAN_API_JOBORDER_CONTACT_UPDATE]    = HM_PUT;
}

+(APIRequest*) apiRequestWithKey:(APIKey) key
{
    return [[APIRequest alloc] initWithApiKey:key URI:apiUris[key] HttpMethod:apiMethods[key] fromEGordianAPI:apiFromEGordian[key]];

}

-(id)initWithApiKey:(APIKey)aKey URI:(NSString *)aUri HttpMethod:(HttpMethod)aMethod fromEGordianAPI:(bool)fromEGordian{
    
    if((self = [super init]))
    {
        apiKey=aKey;
        
        switch (apiKey) {
            case EGORDIAN_API_AUTHORIZATION:
            {
                apiUri = [APP_AUTHENTICATION_URL stringByAppendingString:aUri];
            }
                break;
            case EGORDIAN_API_LOGIN:{
                apiUri = [APP_BASE_URL stringByAppendingString:aUri];
            }
                break;
            default:
                apiUri = [APP_HEADER_URL stringByAppendingString:aUri];
                break;
        }
       
        
        httpMethod=aMethod;
        isSessionFree=fromEGordian;
    }
    return self;
}



- (void)invokeWithParameter:(NSDictionary *)parameter andAditionalHeaders:(NSDictionary*)headersDictionary
{
    _headersDictionary = headersDictionary;
    [self invokeWithParameter:parameter];
}

//pass the url string...
-(void)invokeWithURL:(NSString*)url
{
    request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [self invokeWithParameter:nil];
}

- (void)invokeWithURL:(NSString*)requestUrl andAditionalHeaders:(NSDictionary*)headersDictionary{
    apiUri = requestUrl;
    _headersDictionary = headersDictionary;
    [self invokeWithParameter:nil];
}

-(void)invokeWithURL:(NSString *)requestUrl withData:(id)uploadData andAditionalHeaders:(NSDictionary*)headersDictionary{
    apiUri = requestUrl;
    _headersDictionary = headersDictionary;
    request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestUrl]];
    [self invokeWithParameter:uploadData];
}

//- (void)invokeWithParameter:(NSDictionary *)parameterDictionary
//{
////*** offline ****//////////////
//    if ([AppConfig isOfflineAccessAvailable])
//    {
//        if (!self.isSessionFree || self.isSessionFree)
//        {
//            
//        }
//        NSString *myapiUri=@"";
//        switch (self.apiKey) {
//            case EGORDIAN_API_JOBORDERS_LIST:
//            case EGORDIAN_API_OPENJOBS:{
//                myapiUri =@"JobsList";
//                break;
//            }
//            case EGORDIAN_API_GLOBAL_CONTATCS:
//                myapiUri =@"ContactsResponse";
//                break;
//                
//            case EGORDIAN_API_JOBORDER_DETAIL:
//                myapiUri =@"jobOrderDetail";
//                break;
//                
//            case EGORDIAN_API_TRACKING_DATES:
//                myapiUri =@"TrackingDates";
//                break;
//            case EGORDIAN_API_JOBORDER_CONTACTS:
//                myapiUri =@"JobOrderContactsResponse";
//                break;
//            case EGORDIAN_API_CONTACTS_DETAIL:
//                myapiUri =@"ContactsDetailResponse";
//                break;
//            case EGORDIAN_API_CONTACTS_SEARCH:
//                myapiUri =@"contactSearchResponse";
//                break;
//            case EGORDIAN_API_NOTES:
//                myapiUri =@"notesResponse";
//                break;
//            case EGORDIAN_API_LOGIN:
//            {
//                
//                    [[NSUserDefaults standardUserDefaults]setObject:@"sdflj23243jk43234k3jk34l342k-3jm34" forKey:APP_AUTHORIZATION_PREFERENCE];
//                    [[NSUserDefaults standardUserDefaults] setObject:@"3f0ebb4a-9677-4374-a2cb-835ee1982224" forKey:APP_USERID];
//                    
//                    [[NSUserDefaults standardUserDefaults] setObject:@"dfgldjkgldjkf" forKey:APP_OWNERID];
//                    
//                    [[NSUserDefaults standardUserDefaults]synchronize];
//                    
//                
//            }
//                break;
//                
//            case EGORDIAN_API_PHOTO_THUMBNAIL:
//                break;
//                
//            case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
//                break;
//                
//            default:
//                break;
//                
//        }
//        
//        if(self.apiKey == EGORDIAN_API_LOGIN)
//        {
//            if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//                [_delegate requestFinished:self];
//            }
//            return;
//        }
//        
//        NSString *filePath = [[NSBundle mainBundle] pathForResource:myapiUri ofType:@"json" inDirectory:@"OfflineJSON"];
//        
//        NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
//        
//        NSError *error = nil;
//        
//        if (jsonData) {
//            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                                         options:kNilOptions
//                                                                           error:&error];
//            _responseObject = responseDict;
//        }
//        
//        if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//            [_delegate requestFinished:self];
//        }
//        return;
//    }
//    
//    //*** offline ****//////////////
//    
//    
//    if (!self.isSessionFree) {
//        AFHTTPRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
//        [[APIClient sharedClient] setRequestSerializer:serializer];
//    }
//    else{
//        AFHTTPRequestSerializer *serializer=[AFHTTPRequestSerializer serializer];
//        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        [[APIClient sharedClient] setRequestSerializer:serializer];
//    }
//    
//    if ([_headersDictionary count]) {
//        AFHTTPRequestSerializer *serializer = [[APIClient sharedClient] requestSerializer];
//        [_headersDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//            [serializer setValue:obj forHTTPHeaderField:key];
//        }];
//        [[APIClient sharedClient] setRequestSerializer:serializer];
//    }
//    
//    AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//    [[APIClient sharedClient] setResponseSerializer:responseSerializer];
//    
//    
//    switch (httpMethod) {
//        case HM_GET:{
//            
//            _operation = [[APIClient sharedClient] GET:apiUri
//                                            parameters:parameterDictionary
//                                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                                   _responseObject = responseObject;
//                                                   if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//                                                       [_delegate requestFinished:self];
//                                                   }
//                                               }
//                                               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                   if ([operation responseData]) {
//                                                       NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
//                                                       if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
//                                                           NSError *_error = nil;
//                                                           
//                                                           _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
//                                                       }
//                                                   }
//                                                   if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
//                                                       [_delegate requestFailed:self withError:error];
//                                                   }
//                                               }];
//            
//            break;
//        }
//        case HM_HEAD:{
//            _operation = [[APIClient sharedClient] HEAD:apiUri
//                                             parameters:parameterDictionary
//                                                success:^(AFHTTPRequestOperation *operation) {
//                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//                                                        [_delegate requestFinished:self];
//                                                    }
//                                                }
//                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                    if ([operation responseData]) {
//                                                        NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
//                                                        if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
//                                                            NSError *_error = nil;
//                                                            
//                                                            _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
//                                                        }
//                                                    }
//                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
//                                                        [_delegate requestFailed:self withError:error];
//                                                    }
//                                                }];
//            break;
//        }
//        case HM_POST:{
//            _operation = [[APIClient sharedClient] POST:apiUri
//                                             parameters:parameterDictionary
//                                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                                    _responseObject = responseObject;
//                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//                                                        [_delegate requestFinished:self];
//                                                    }
//                                                }
//                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                    if ([operation responseData]) {
//                                                        NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
//                                                        if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
//                                                            NSError *_error = nil;
//                                                            
//                                                            _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
//                                                        }
//                                                    }
//                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
//                                                        [_delegate requestFailed:self withError:error];
//                                                    }
//                                                    
//                                                }];
//            break;
//        }
//        case HM_PUT:{
//            
//            _operation = [[APIClient sharedClient] PUT:apiUri
//                                             parameters:parameterDictionary
//                                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                                    _responseObject = responseObject;
//                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//                                                        [_delegate requestFinished:self];
//                                                    }
//                                                }
//                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                    if ([operation responseData]) {
//                                                        NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
//                                                        if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
//                                                            NSError *_error = nil;
//                                                            
//                                                            _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
//                                                        }
//                                                    }
//                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
//                                                        [_delegate requestFailed:self withError:error];
//                                                    }
//                                                    
//                                                }];
//            
//            break;
//        }
//        case HM_DELETE:{
//            _operation = [[APIClient sharedClient] DELETE:apiUri
//                                               parameters:parameterDictionary
//                                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                                      _responseObject = responseObject;
//                                                      if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
//                                                          [_delegate requestFinished:self];
//                                                      }
//                                                  }
//                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                                      if ([operation responseData]) {
//                                                          NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
//                                                          if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
//                                                              NSError *_error = nil;
//                                                              
//                                                              _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
//                                                          }
//                                                      }
//                                                      if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
//                                                          [_delegate requestFailed:self withError:error];
//                                                      }
//                                                  }];
//            break;
//        }
//        default:
//            break;
//    }
//}

//need to be removed in optimization... introduced at the time of Photo Attach...
//changed according to Shineeth
- (void)invokeWithParameter:(id)parameters
{
    //*** offline ****//////////////
    if ([AppConfig isOfflineAccessAvailable])
    {
        if (!self.isSessionFree || self.isSessionFree)
        {
            
        }
        NSString *myapiUri=@"";
        switch (self.apiKey) {
            case EGORDIAN_API_JOBORDERS_LIST:
            case EGORDIAN_API_OPENJOBS:{
                myapiUri =@"JobsList";
                break;
            }
            case EGORDIAN_API_GLOBAL_CONTATCS:
                myapiUri =@"ContactsResponse";
                break;
                
            case EGORDIAN_API_JOBORDER_DETAIL:
                myapiUri =@"jobOrderDetail";
                break;
                
            case EGORDIAN_API_TRACKING_DATES:
                myapiUri =@"TrackingDates";
                break;
            case EGORDIAN_API_JOBORDER_CONTACTS:
                myapiUri =@"JobOrderContactsResponse";
                break;
            case EGORDIAN_API_CONTACTS_DETAIL:
                myapiUri =@"ContactsDetailResponse";
                break;
            case EGORDIAN_API_CONTACTS_SEARCH:
                myapiUri =@"contactSearchResponse";
                break;
            case EGORDIAN_API_NOTES:
                myapiUri =@"notesResponse";
                break;
            case EGORDIAN_API_LOGIN:
            {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"sdflj23243jk43234k3jk34l342k-3jm34" forKey:APP_AUTHORIZATION_PREFERENCE];
                [[NSUserDefaults standardUserDefaults] setObject:@"3f0ebb4a-9677-4374-a2cb-835ee1982224" forKey:APP_USERID];
                
                [[NSUserDefaults standardUserDefaults] setObject:@"dfgldjkgldjkf" forKey:APP_OWNERID];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                
            }
                break;
                
            case EGORDIAN_API_PHOTO_THUMBNAIL:
                break;
                
            case EGORDIAN_API_PHOTOS_ATTACH_TO_JOB:
                break;
                
            default:
                break;
                
        }
        
        if(self.apiKey == EGORDIAN_API_LOGIN)
        {
            if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
                [_delegate requestFinished:self];
            }
            return;
        }
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:myapiUri ofType:@"json" inDirectory:@"OfflineJSON"];
        
        NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
        
        NSError *error = nil;
        
        if (jsonData) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                         options:kNilOptions
                                                                           error:&error];
            _responseObject = responseDict;
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
            [_delegate requestFinished:self];
        }
        return;
    }
    
    //*** offline ****//////////////
    
    
    if (!self.isSessionFree) {
        AFHTTPRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
        [[APIClient sharedClient] setRequestSerializer:serializer];
    }
    else{
        AFHTTPRequestSerializer *serializer=[AFHTTPRequestSerializer serializer];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [[APIClient sharedClient] setRequestSerializer:serializer];
    }
    
    if ([_headersDictionary count]) {
        AFHTTPRequestSerializer *serializer = [[APIClient sharedClient] requestSerializer];
        [_headersDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [serializer setValue:obj forHTTPHeaderField:key];
        }];
        [[APIClient sharedClient] setRequestSerializer:serializer];
    }
    
    AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [[APIClient sharedClient] setResponseSerializer:responseSerializer];
    
    
    switch (httpMethod) {
        case HM_GET:{
            
            _operation = [[APIClient sharedClient] GET:apiUri
                                            parameters:parameters
                                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                   _responseObject = responseObject;
                                                   if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
                                                       [_delegate requestFinished:self];
                                                   }
                                               }
                                               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                   if ([operation responseData]) {
                                                       NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
                                                       if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
                                                           NSError *_error = nil;
                                                           
                                                           _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
                                                       }
                                                   }
                                                   if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
                                                       [_delegate requestFailed:self withError:error];
                                                   }
                                               }];
            break;
        }
        case HM_HEAD:{
            _operation = [[APIClient sharedClient] HEAD:apiUri
                                             parameters:parameters
                                                success:^(AFHTTPRequestOperation *operation) {
                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
                                                        [_delegate requestFinished:self];
                                                    }
                                                }
                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                    if ([operation responseData]) {
                                                        NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
                                                        if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
                                                            NSError *_error = nil;
                                                            
                                                            _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
                                                        }
                                                    }
                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
                                                        [_delegate requestFailed:self withError:error];
                                                    }
                                                }];
            break;
        }
        case HM_POST:{
            _operation = [[APIClient sharedClient] POST:apiUri
                                             parameters:parameters
                                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                    _responseObject = responseObject;
                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
                                                        [_delegate requestFinished:self];
                                                    }
                                                }
                                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                    if ([operation responseData]) {
                                                        NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
                                                        if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
                                                            NSError *_error = nil;
                                                            
                                                            _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
                                                        }
                                                    }
                                                    if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
                                                        [_delegate requestFailed:self withError:error];
                                                    }
                                                    
                                                }];
            break;
        }
        case HM_PUT:{
            
            _operation = [[APIClient sharedClient] PUT:apiUri
                                            parameters:parameters
                                               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                   _responseObject = responseObject;
                                                   if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
                                                       [_delegate requestFinished:self];
                                                   }
                                               }
                                               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                   if ([operation responseData]) {
                                                       NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
                                                       if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
                                                           NSError *_error = nil;
                                                           
                                                           _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
                                                       }
                                                   }
                                                   if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
                                                       [_delegate requestFailed:self withError:error];
                                                   }
                                                   
                                               }];
            
            break;
        }
        case HM_DELETE:{
            _operation = [[APIClient sharedClient] DELETE:apiUri
                                               parameters:parameters
                                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                      _responseObject = responseObject;
                                                      if (_delegate && [_delegate respondsToSelector:@selector(requestFinished:)]) {
                                                          [_delegate requestFinished:self];
                                                      }
                                                  }
                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                      if ([operation responseData]) {
                                                          NSString * contentType = [[[operation response]allHeaderFields] valueForKey:@"content-type"];
                                                          if ([[contentType lowercaseString] rangeOfString:@"application/json"].location != NSNotFound) {
                                                              NSError *_error = nil;
                                                              
                                                              _responseObject = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:0 error:&_error];
                                                          }
                                                      }
                                                      if (_delegate && [_delegate respondsToSelector:@selector(requestFailed:withError:)]) {
                                                          [_delegate requestFailed:self withError:error];
                                                      }
                                                  }];
            break;
        }
        default:
            break;
    }
}
@end
