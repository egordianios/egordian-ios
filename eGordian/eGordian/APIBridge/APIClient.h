//
//  APIClient.h
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface APIClient : AFHTTPRequestOperationManager
+ (instancetype)sharedClient;
+ (void)cancelAllRequest;
@end
