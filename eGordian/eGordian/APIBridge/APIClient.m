//
//  APIClient.m
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import "APIClient.h"


@implementation APIClient
+ (instancetype)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APP_BASE_URL]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

+ (void)cancelAllRequest
{
    [[[self sharedClient] operationQueue] cancelAllOperations];
}

@end
