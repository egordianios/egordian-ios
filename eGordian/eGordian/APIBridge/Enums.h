//
//  Enums.h
//  eGordian
//
//  Created by Naresh Kumar on 7/30/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    HM_GET,
    HM_POST,
    HM_PUT,
    HM_PUT_CUSTOM_REQUEST,
    HM_DELETE,
    HM_HEAD,
    HM_OPTIONS
}HttpMethod;
typedef enum : NSUInteger {
    JT_NOTCREATED = 0,
    JT_ALREADYPRESENT = 1
} JobDetailScopeType;

typedef enum : NSUInteger {
    JS_WAITINGTOSYNC = 0,
    JT_ALREADYSYNCED = 1
} JobSyncStatus;


@interface Enums : NSObject

+(NSString*) StringForHTTPMethod:(HttpMethod) httpMethod;

@end
