//
//  ViewController.h
//  eGordian
//
//  Created by Naresh Kumar on 7/28/15.
//  Copyright (c) 2015 Marlabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIGestureRecognizerDelegate,DataDownloaderDelegate>

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIView *forgotPasswordView;
@property (assign, nonatomic) CGRect mySensitiveRect;
@property (weak, nonatomic) IBOutlet UIView *actualForgotPassowrdView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actvityLoader;
@property (weak, nonatomic) IBOutlet UIView *loaderView;

- (IBAction)loginButtonTapped:(id)sender;
- (IBAction)aboutButtonTapped:(id)sender;
- (IBAction)forgotPasswordButtonTapped:(id)sender;
- (IBAction)helpButtonTapped:(id)sender;
- (IBAction)callSupportRepresentativeButtonTapped:(id)sender;
- (IBAction)emailRepresentativebuttonTapped:(id)sender;

@end

